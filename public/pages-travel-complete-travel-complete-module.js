(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-travel-complete-travel-complete-module"],{

/***/ "2fQ+":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel-complete/travel-complete.page.ts ***!
  \*********************************************************************************/
/*! exports provided: TravelCompletePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelCompletePage", function() { return TravelCompletePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_travel_complete_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./travel-complete.page.html */ "W2mA");
/* harmony import */ var _travel_complete_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./travel-complete.page.scss */ "AWAi");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/admin/travels.service */ "KlPZ");









let TravelCompletePage = class TravelCompletePage {
    constructor(menuController, alertsService, activatedRoute, travelsService, router) {
        this.menuController = menuController;
        this.alertsService = alertsService;
        this.activatedRoute = activatedRoute;
        this.travelsService = travelsService;
        this.router = router;
        this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
    }
    ngOnInit() {
        this.loadTravelData();
        this.travelPrice = this.travelsService.getTravelAndUserById(this.travelId, "driver");
    }
    loadTravelData() {
        this.travelsService.getTravelId(this.travelId).subscribe((data) => {
            this.travelData = data;
        });
    }
    redirectTo(pageName) {
        this.alertsService.closeModal();
        this.router.navigate(['dashboard/' + pageName]);
    }
    openMenu() {
        this.menuController.open('principal');
    }
};
TravelCompletePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_7__["TravelsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
TravelCompletePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-travel-complete',
        template: _raw_loader_travel_complete_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_travel_complete_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TravelCompletePage);



/***/ }),

/***/ "AWAi":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel-complete/travel-complete.page.scss ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("body {\n  background: #F7F8F9;\n}\n\n/* Estilo del boton verde del modal */\n\n.green {\n  --background: #008D36;\n  --background-activated:#008D36;\n  color: white;\n  font-weight: bold;\n}\n\n/* Estilo del boton blanco del modal */\n\n.white {\n  --background: white;\n  --background-activated:white;\n  color: #4B545A;\n  font-weight: bold;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n}\n\n/* Se coloca paddings para ubicar los elementos de mejor manera */\n\nion-content {\n  --padding-start:5%;\n  --padding-end:5%;\n}\n\n/* Estilos de los parrafos del componenete */\n\np {\n  margin-top: 0px;\n  font-size: 15px;\n  margin-left: 0px;\n}\n\nh1 {\n  margin-top: 0px;\n  font-size: 15px;\n}\n\n/* Estilos de la linea que cierra el modal */\n\n.close-modal-line {\n  width: 30px;\n  height: 5px;\n  background-color: #D5DDE0;\n  border-radius: 50px;\n}\n\n.border {\n  border: 1px solid red;\n  margin: 0px;\n}\n\n/* Estilos del contenedor de la linea que cierra el modal */\n\n.container-close-modal {\n  padding: 5%;\n  justify-content: center;\n}\n\n.center {\n  margin-top: 15px;\n  text-align: center;\n  font-size: 26px;\n}\n\n.marg {\n  margin: 0em auto 0em;\n}\n\n.card-home {\n  position: absolute;\n  height: 68%;\n  left: 2%;\n  right: 2%;\n  top: 12%;\n}\n\n.card-home .container-figure {\n  margin: -5%;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n}\n\n.card-home .container-figure .circle {\n  width: 9px;\n  height: 9px;\n  border-radius: 100%;\n  background-color: #008D36;\n  margin-bottom: 4px;\n}\n\n.card-home .container-figure .line {\n  background-color: #ff0000;\n  height: 35%;\n  width: 1px;\n}\n\n.card {\n  position: absolute;\n  height: 160px;\n  top: calc(50% - 182px/2 + 25px);\n  background: #FFFFFF;\n  border: 1px solid rgba(151, 173, 182, 0.2);\n  border-radius: 15px;\n  width: 91%;\n  display: flex;\n  padding: 10px;\n}\n\n.card .container-figure {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n}\n\n.card .container-figure .circle {\n  width: 9px;\n  height: 9px;\n  border-radius: 100%;\n  background-color: #008D36;\n  margin-bottom: 4px;\n}\n\n.card .container-figure .line {\n  background-color: #3E4958;\n  height: 50%;\n  width: 1px;\n}\n\n.card .container-info .line {\n  background-color: white;\n  height: 30px;\n}\n\n.card .container-info .line-hors {\n  background-color: black;\n  height: 50px;\n}\n\n.container-info-number .line {\n  background-color: white;\n  height: 55px;\n}\n\nion-icon {\n  font-size: 20px;\n  color: #3E4958;\n  align-items: center;\n  width: 100%;\n  margin: 0 auto;\n}\n\n.content-tile {\n  margin: 160px 0em 0 auto;\n  position: absolute;\n  height: 60px;\n  left: 17px;\n  right: 19px;\n  top: calc(50% - 60px/2);\n  /* Grey 3 */\n  background: #F7F8F9;\n  border-radius: 15px;\n}\n\nion-card {\n  border-radius: 15px;\n  padding: 10px;\n  margin: 24px 5px;\n}\n\nion-card .container-price {\n  display: flex;\n  justify-content: flex-end;\n  flex-direction: column;\n  text-align: right;\n  align-items: center;\n}\n\nion-card .container-price div {\n  background-color: #4F4F4F;\n  border-radius: 50px;\n  width: 72px;\n  height: 24px;\n  color: #ffff;\n  display: flex;\n  justify-content: center;\n}\n\nion-card .container-price div p {\n  margin: 3px 0;\n}\n\nion-card .col-container-img {\n  display: flex;\n  justify-content: flex-start;\n}\n\nion-card .col-container-img .container-img {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n}\n\nion-card .col-container-img .container-img p {\n  margin: 2px 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3RyYXZlbC1jb21wbGV0ZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBQTtBQUNGOztBQUNFLHFDQUFBOztBQUNBO0VBQ0UscUJBQUE7RUFDQSw4QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtBQUVKOztBQUNFLHNDQUFBOztBQUNBO0VBQ0UsbUJBQUE7RUFDQSw0QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGlEQUFBO0FBRUo7O0FBQ0UsaUVBQUE7O0FBQ0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FBRUo7O0FBQ0UsNENBQUE7O0FBQ0E7RUFDRSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBRUo7O0FBQUU7RUFDRSxlQUFBO0VBQ0EsZUFBQTtBQUdKOztBQUFFLDRDQUFBOztBQUNBO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FBR0o7O0FBQUU7RUFDRCxxQkFBQTtFQUNBLFdBQUE7QUFHRDs7QUFBRSwyREFBQTs7QUFDQTtFQUNFLFdBQUE7RUFDQSx1QkFBQTtBQUdKOztBQUFFO0VBQ0UsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUFHSjs7QUFDRTtFQUNFLG9CQUFBO0FBRUo7O0FBQ0U7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7QUFFSjs7QUFESTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBR047O0FBRk07RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQUlWOztBQUZNO0VBQ0kseUJBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBQUlWOztBQUdBO0VBQ00sa0JBQUE7RUFDQSxhQUFBO0VBQ0EsK0JBQUE7RUFDQSxtQkFBQTtFQUNBLDBDQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGFBQUE7QUFBTjs7QUFFSTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFBTjs7QUFDTTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0FBQ1Y7O0FBQ007RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FBQ1Y7O0FBR007RUFDSSx1QkFBQTtFQUNBLFlBQUE7QUFEVjs7QUFHTTtFQUNJLHVCQUFBO0VBQ0EsWUFBQTtBQURWOztBQVFFO0VBQ0ksdUJBQUE7RUFDQSxZQUFBO0FBTE47O0FBU0E7RUFDRSxlQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7QUFORjs7QUFTQTtFQUNFLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FBTkY7O0FBVUE7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQVBKOztBQVFJO0VBQ0ksYUFBQTtFQUNBLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBTlI7O0FBT1E7RUFDSSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0FBTFo7O0FBTVk7RUFDSSxhQUFBO0FBSmhCOztBQVNJO0VBQ0ksYUFBQTtFQUNBLDJCQUFBO0FBUFI7O0FBUVE7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0FBTlo7O0FBT1k7RUFDSSxlQUFBO0FBTGhCIiwiZmlsZSI6InRyYXZlbC1jb21wbGV0ZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJib2R5IHtcbiAgYmFja2dyb3VuZDogI0Y3RjhGOTtcbn1cbiAgLyogRXN0aWxvIGRlbCBib3RvbiB2ZXJkZSBkZWwgbW9kYWwgKi9cbiAgLmdyZWVuIHtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDhEMzY7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDojMDA4RDM2O1xuICAgIGNvbG9yOndoaXRlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB9XG4gIFxuICAvKiBFc3RpbG8gZGVsIGJvdG9uIGJsYW5jbyBkZWwgbW9kYWwgKi9cbiAgLndoaXRlIHtcbiAgICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6d2hpdGU7XG4gICAgY29sb3I6IzRCNTQ1QTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAtLWJveC1zaGFkb3c6IDBweCAycHggOHB4IHJnYmEoMTYsIDEwNSwgMjI3LCAwLjMpO1xuICB9XG4gIFxuICAvKiBTZSBjb2xvY2EgcGFkZGluZ3MgcGFyYSB1YmljYXIgbG9zIGVsZW1lbnRvcyBkZSBtZWpvciBtYW5lcmEgKi9cbiAgaW9uLWNvbnRlbnQge1xuICAgIC0tcGFkZGluZy1zdGFydDo1JTtcbiAgICAtLXBhZGRpbmctZW5kOjUlO1xuICB9XG4gIFxuICAvKiBFc3RpbG9zIGRlIGxvcyBwYXJyYWZvcyBkZWwgY29tcG9uZW5ldGUgKi9cbiAgcCB7XG4gICAgbWFyZ2luLXRvcDogMHB4O1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xuICB9XG4gIGgxIHtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIH1cblxuICAvKiBFc3RpbG9zIGRlIGxhIGxpbmVhIHF1ZSBjaWVycmEgZWwgbW9kYWwgKi9cbiAgLmNsb3NlLW1vZGFsLWxpbmUge1xuICAgIHdpZHRoOiAzMHB4O1xuICAgIGhlaWdodDogNXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNENURERTAgO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIH1cbiAgXG4gIC5ib3JkZXIge1xuXHRib3JkZXI6IDFweCBzb2xpZCByZWQ7XG5cdG1hcmdpbjogMHB4O1xuICB9XG5cbiAgLyogRXN0aWxvcyBkZWwgY29udGVuZWRvciBkZSBsYSBsaW5lYSBxdWUgY2llcnJhIGVsIG1vZGFsICovXG4gIC5jb250YWluZXItY2xvc2UtbW9kYWwge1xuICAgIHBhZGRpbmc6IDUlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB9XG4gIFxuICAuY2VudGVyIHtcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDI2cHg7XG4gIFxuICB9XG5cbiAgLm1hcmd7XG4gICAgbWFyZ2luOiAwZW0gYXV0byAwZW07XG4gIH1cbiBcbiAgLmNhcmQtaG9tZXtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiA2OCU7XG4gICAgbGVmdDogMiU7XG4gICAgcmlnaHQ6IDIlO1xuICAgIHRvcDogMTIlO1xuICAgIC5jb250YWluZXItZmlndXJle1xuICAgICAgbWFyZ2luOiAtNSU7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIC5jaXJjbGV7XG4gICAgICAgICAgd2lkdGg6IDlweDtcbiAgICAgICAgICBoZWlnaHQ6IDlweDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IzAwOEQzNjtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiA0cHg7XG4gICAgICB9XG4gICAgICAubGluZXtcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiNmZjAwMDA7XG4gICAgICAgICAgaGVpZ2h0OiAzNSU7XG4gICAgICAgICAgd2lkdGg6IDFweDtcblxuICAgICAgfVxuICAgIH1cbiBcbiAgICB9XG5cbi5jYXJke1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgaGVpZ2h0OiAxNjBweDtcbiAgICAgIHRvcDogY2FsYyg1MCUgLSAxODJweC8yICsgMjVweCk7XG4gICAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgxNTEsIDE3MywgMTgyLCAwLjIpO1xuICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgIHdpZHRoOiA5MSU7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgcGFkZGluZzogMTBweDtcbiAgXG4gICAgLmNvbnRhaW5lci1maWd1cmV7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIC5jaXJjbGV7XG4gICAgICAgICAgd2lkdGg6IDlweDtcbiAgICAgICAgICBoZWlnaHQ6IDlweDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IzAwOEQzNiA7XG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogNHB4O1xuICAgICAgfVxuICAgICAgLmxpbmV7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjojM0U0OTU4O1xuICAgICAgICAgIGhlaWdodDogNTAlO1xuICAgICAgICAgIHdpZHRoOiAxcHg7XG4gICAgICB9XG4gIH1cbiAgLmNvbnRhaW5lci1pbmZve1xuICAgICAgLmxpbmV7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjp3aGl0ZTtcbiAgICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICB9XG4gICAgICAubGluZS1ob3Jze1xuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6YmxhY2s7XG4gICAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgfVxuXG4gIH1cbn1cblxuLmNvbnRhaW5lci1pbmZvLW51bWJlcntcbiAgLmxpbmV7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlO1xuICAgICAgaGVpZ2h0OiA1NXB4O1xuICB9XG59XG4gICAgICBcbmlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBjb2xvcjogIzNFNDk1ODtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogMCBhdXRvOyBcbn1cblxuLmNvbnRlbnQtdGlsZXtcbiAgbWFyZ2luOiAxNjBweCAwZW0gMCBhdXRvO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogNjBweDtcbiAgbGVmdDogMTdweDtcbiAgcmlnaHQ6IDE5cHg7XG4gIHRvcDogY2FsYyg1MCUgLSA2MHB4LzIpO1xuICAvKiBHcmV5IDMgKi9cbiAgYmFja2dyb3VuZDogI0Y3RjhGOTtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbn1cblxuXG5pb24tY2FyZHtcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgbWFyZ2luOiAyNHB4IDVweDtcbiAgICAuY29udGFpbmVyLXByaWNle1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgZGl2e1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjojNEY0RjRGO1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgICAgICAgICAgIHdpZHRoOiA3MnB4O1xuICAgICAgICAgICAgaGVpZ2h0OiAyNHB4O1xuICAgICAgICAgICAgY29sb3I6I2ZmZmY7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICBwe1xuICAgICAgICAgICAgICAgIG1hcmdpbjogM3B4IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICBcbiAgICB9XG4gICAgLmNvbC1jb250YWluZXItaW1ne1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgICAgIC5jb250YWluZXItaW1ne1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICBwe1xuICAgICAgICAgICAgICAgIG1hcmdpbjoycHggMHB4IDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cblxuIl19 */");

/***/ }),

/***/ "CuF/":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel-complete/travel-complete.module.ts ***!
  \***********************************************************************************/
/*! exports provided: TravelCompletePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelCompletePageModule", function() { return TravelCompletePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _travel_complete_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./travel-complete-routing.module */ "FEgP");
/* harmony import */ var _travel_complete_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./travel-complete.page */ "2fQ+");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let TravelCompletePageModule = class TravelCompletePageModule {
};
TravelCompletePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _travel_complete_routing_module__WEBPACK_IMPORTED_MODULE_5__["TravelCompletePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_travel_complete_page__WEBPACK_IMPORTED_MODULE_6__["TravelCompletePage"]]
    })
], TravelCompletePageModule);



/***/ }),

/***/ "FEgP":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel-complete/travel-complete-routing.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: TravelCompletePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelCompletePageRoutingModule", function() { return TravelCompletePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _travel_complete_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./travel-complete.page */ "2fQ+");




const routes = [
    {
        path: '',
        component: _travel_complete_page__WEBPACK_IMPORTED_MODULE_3__["TravelCompletePage"]
    }
];
let TravelCompletePageRoutingModule = class TravelCompletePageRoutingModule {
};
TravelCompletePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TravelCompletePageRoutingModule);



/***/ }),

/***/ "W2mA":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/travel-complete/travel-complete.page.html ***!
  \*************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-principal-header (openMenu)=\"openMenu()\"></app-principal-header>\n  <body></body>\n  <ion-row>\n    <ion-card class=\"card-home\">\n      <ion-card-header>\n        <div class=\"center\" >\n        <img src=\"../../../../../../assets/img/shapes.png\" class=\"marg\">\n        <div> \n          <h1>Viaje Completado</h1>\n        </div>\n      </div>\n    </ion-card-header>\n\n    <ion-row>\n      <ion-card class=\"card\">\n        <ion-col  class=\"container-info-number\">\n          <p>11:20</p>\n          <div class=\"line\"></div>\n          <p>11:02</p>\n        </ion-col>\n        <ion-col size=\"2\" class=\"container-figure\">\n          <div class=\"circle\"></div>\n          <div class=\"line\"></div>\n          <ion-icon name=\"caret-down-outline\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"10\" class=\"container-info\">\n          <p>{{travelData?.destinyLocation.address}}</p>\n          <div class=\"line\"></div>\n          <p>{{travelData?.startLocation.address}}</p>\n        </ion-col>\n      </ion-card>\n  </ion-row>\n\n      <div class=\"content-tile\">\n        <ion-row class=\"container-info\">\n          <ion-col size=\"8\" class=\"container-card\">\n            <div *ngIf=\"travelData?.payMethod == 'card'\">\n              <img src=\"../../../../../../assets/img/card.svg\" alt=\"\">\n             <div >\n               Tarjeta de Crédito<p>****-****</p>\n              </div>\n            </div>\n            <div *ngIf=\"travelData?.payMethod == 'cash'\">\n              <div>\n                <img src=\"../../../../../../assets/img/cash.png\" alt=\"\">    \n                <p>Pago en Efectivo</p>\n               </div>\n                    \n            </div>\n         </ion-col>\n         <ion-col size=\"4\" class=\"container-price\">\n           <h2> $ {{(travelPrice | async)?.price.dollars}}.{{(travelPrice | async)?.price.cents  }}  <sup></sup></h2>\n          </ion-col>\n        </ion-row>\n      </div> \n    </ion-card>\n  </ion-row>\n  <ion-row >\n    <ion-col size=\"12\">\n    <app-button (click)=\"redirectTo('recommendations/'+travelId)\" title=\"Ok\" ></app-button>\n  </ion-col>\n</ion-row> \n");

/***/ })

}]);
//# sourceMappingURL=pages-travel-complete-travel-complete-module.js.map