(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-confirm-transfers-confirm-transfers-module"],{

/***/ "7Okr":
/*!***********************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/confirm-transfers/confirm-transfers-routing.module.ts ***!
  \***********************************************************************************************/
/*! exports provided: ConfirmTransfersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmTransfersPageRoutingModule", function() { return ConfirmTransfersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _confirm_transfers_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./confirm-transfers.page */ "ee54");




const routes = [
    {
        path: '',
        component: _confirm_transfers_page__WEBPACK_IMPORTED_MODULE_3__["ConfirmTransfersPage"]
    }
];
let ConfirmTransfersPageRoutingModule = class ConfirmTransfersPageRoutingModule {
};
ConfirmTransfersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ConfirmTransfersPageRoutingModule);



/***/ }),

/***/ "Fglc":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/confirm-transfers/confirm-transfers.page.scss ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n/* Estilos del header */\nion-toolbar ion-row {\n  align-items: center;\n  display: flex;\n  justify-content: center;\n  width: 85%;\n}\nion-toolbar ion-row ion-title {\n  font-size: 20px;\n  font-weight: bold;\n  --color: #3E4958;\n  text-align: center;\n}\n/* Estilo para colocar un margen izquierdo a cualquier elemento */\n.margin-left {\n  margin-left: 5%;\n}\n/* Estilos de las tarjetas */\nion-card {\n  border-radius: 15px;\n  box-shadow: none;\n  margin-top: 10%;\n  width: 90%;\n  --background: rgba(0, 0, 0, 0);\n  border: none;\n}\nion-card ion-card-header ion-card-title {\n  color: #333333;\n  font-weight: bold;\n  font-size: 18px;\n}\nion-card ion-card-header ion-card-subtitle {\n  color: #7E7E7E;\n  font-size: 17px;\n  font-weight: 500;\n  margin: 8px 0px;\n}\nion-card ion-card-header .container-starts ion-icon {\n  font-size: 20px;\n  margin-right: 9px;\n}\n/* Contenedor de todas las tarjetas de navegación */\n.container {\n  padding: 0px 25px;\n}\n.container ion-row .title {\n  font-weight: bold;\n  font-size: 19px;\n  color: #242424;\n  font-size: 20px;\n  line-height: 30px;\n}\n.container ion-row .cost {\n  font-weight: bold;\n  font-size: 19px;\n  color: #242424;\n  font-size: 20px;\n  line-height: 30px;\n  margin-left: auto;\n}\n.container ion-row ion-col .subtitle {\n  color: #3E4958;\n  font-weight: bold;\n  font-size: 14px;\n}\n.container ion-row ion-col .img-license {\n  width: 200%;\n  border-radius: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n}\n.btn {\n  width: 130%;\n  height: 66px;\n  left: 36px;\n  top: 722px;\n  --border-radius: 50px;\n  font-size: 17px;\n}\n.white-btn {\n  --background: transparent;\n  border: solid;\n  border-radius: 50px;\n  --background-activated: #214E9D;\n  --color: blue;\n  font-weight: bold;\n  margin-top: 10%;\n}\n.green-btn {\n  --background: #008D36;\n  --background-activated: #008D36;\n  --color: white;\n  font-weight: bold;\n  margin-top: 10%;\n}\n.blue-btn {\n  --background: #214E9D;\n  --background-activated: #214E9D;\n  --color: white;\n  font-weight: bold;\n  margin-top: 10%;\n}\n/* Estilo del boton blanco */\n.white {\n  --background: white;\n  --background-activated:white;\n  --color:#4B545A;\n  font-weight: bold;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n}\n/* Estilo del boton blanco */\n.red {\n  --background: rgb(255, 0, 0);\n  --background-activated:rgb(255, 0, 0);\n  --color:#ffffff;\n  font-weight: bold;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n}\n/* Contenedor de imagen del usuario */\n.container-img {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.container-img .img {\n  width: 82px;\n  height: 80px;\n  border-radius: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n/* Color verde para algunos elementos */\n.green {\n  color: #008D36;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2NvbmZpcm0tdHJhbnNmZXJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUFBaEIsdUJBQUE7QUFFSTtFQUNFLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtBQUNOO0FBQU07RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBRVI7QUFLRSxpRUFBQTtBQUNBO0VBQ0UsZUFBQTtBQUZKO0FBS0UsNEJBQUE7QUFDQTtFQUNFLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtBQUZKO0FBS007RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBSFI7QUFNTTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBSlI7QUFRUTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQU5WO0FBV0UsbURBQUE7QUFDQTtFQUNFLGlCQUFBO0FBUko7QUFVTTtFQUNFLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFSUjtBQVVNO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FBUlI7QUFXUTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUFUVjtBQVdRO0VBQ0UsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0FBVFY7QUFlRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7QUFaSjtBQWVFO0VBQ0UseUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSwrQkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUFaSjtBQWNFO0VBQ0UscUJBQUE7RUFDQSwrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUFYSjtBQWFFO0VBQ0UscUJBQUE7RUFDQSwrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUFWSjtBQVlFLDRCQUFBO0FBQ0E7RUFDSSxtQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsaURBQUE7QUFUTjtBQVdJLDRCQUFBO0FBQ0Y7RUFDRSw0QkFBQTtFQUNBLHFDQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsaURBQUE7QUFSSjtBQVVFLHFDQUFBO0FBQ0E7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQVBKO0FBUUk7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQU5OO0FBU0UsdUNBQUE7QUFDQTtFQUNFLGNBQUE7QUFOSiIsImZpbGUiOiJjb25maXJtLXRyYW5zZmVycy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBFc3RpbG9zIGRlbCBoZWFkZXIgKi9cbmlvbi10b29sYmFyIHtcbiAgICBpb24tcm93IHtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICB3aWR0aDogODUlO1xuICAgICAgaW9uLXRpdGxlIHtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgLS1jb2xvcjogIzNFNDk1ODtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBcbiAgICAgIH0gXG4gIFxuICAgIH1cbiAgfVxuICBcbiAgLyogRXN0aWxvIHBhcmEgY29sb2NhciB1biBtYXJnZW4gaXpxdWllcmRvIGEgY3VhbHF1aWVyIGVsZW1lbnRvICovXG4gIC5tYXJnaW4tbGVmdCB7XG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xuICB9XG4gIFxuICAvKiBFc3RpbG9zIGRlIGxhcyB0YXJqZXRhcyAqL1xuICBpb24tY2FyZCB7XG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIG1hcmdpbi10b3A6IDEwJTtcbiAgICB3aWR0aDogOTAlO1xuICAgIC0tYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwKTtcbiAgICBib3JkZXI6IG5vbmU7XG4gIFxuICAgIGlvbi1jYXJkLWhlYWRlciB7XG4gICAgICBpb24tY2FyZC10aXRsZSB7XG4gICAgICAgIGNvbG9yOiAjMzMzMzMzO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgfVxuICBcbiAgICAgIGlvbi1jYXJkLXN1YnRpdGxlIHtcbiAgICAgICAgY29sb3I6ICM3RTdFN0U7XG4gICAgICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgbWFyZ2luOiA4cHggMHB4O1xuICAgICAgfVxuICBcbiAgICAgIC5jb250YWluZXItc3RhcnRzIHtcbiAgICAgICAgaW9uLWljb24ge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDlweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuICAvKiBDb250ZW5lZG9yIGRlIHRvZGFzIGxhcyB0YXJqZXRhcyBkZSBuYXZlZ2FjacOzbiAqL1xuICAuY29udGFpbmVyIHtcbiAgICBwYWRkaW5nOiAwcHggMjVweDtcbiAgICBpb24tcm93IHtcbiAgICAgIC50aXRsZSB7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICBmb250LXNpemU6IDE5cHg7XG4gICAgICAgIGNvbG9yOiAjMjQyNDI0O1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xuICAgICAgfVxuICAgICAgLmNvc3Qge1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgZm9udC1zaXplOiAxOXB4O1xuICAgICAgICBjb2xvcjogIzI0MjQyNDtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICB9XG4gICAgICBpb24tY29sIHtcbiAgICAgICAgLnN1YnRpdGxlIHtcbiAgICAgICAgICBjb2xvcjogIzNFNDk1ODtcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIH1cbiAgICAgICAgLmltZy1saWNlbnNlIHtcbiAgICAgICAgICB3aWR0aDogMjAwJTtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgLy9Fc3RpbG9zIGRlIGxvcyBib3RvbmVzIGJvdG9uIFxuICAuYnRuIHtcbiAgICB3aWR0aDogMTMwJTtcbiAgICBoZWlnaHQ6IDY2cHg7XG4gICAgbGVmdDogMzZweDtcbiAgICB0b3A6IDcyMnB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogNTBweDtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgXG4gIH1cbiAgLndoaXRlLWJ0biB7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXI6IHNvbGlkO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzIxNEU5RDtcbiAgICAtLWNvbG9yOiBibHVlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIG1hcmdpbi10b3A6IDEwJTtcbiAgfVxuICAuZ3JlZW4tYnRuIHtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDhEMzY7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzAwOEQzNjtcbiAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBtYXJnaW4tdG9wOiAxMCU7XG4gIH1cbiAgLmJsdWUtYnRuIHtcbiAgICAtLWJhY2tncm91bmQ6ICMyMTRFOUQ7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzIxNEU5RDtcbiAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBtYXJnaW4tdG9wOiAxMCU7XG4gIH1cbiAgLyogRXN0aWxvIGRlbCBib3RvbiBibGFuY28gKi9cbiAgLndoaXRle1xuICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6d2hpdGU7XG4gICAgICAtLWNvbG9yOiM0QjU0NUE7XG4gICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgIC0tYm94LXNoYWRvdzogMHB4IDJweCA4cHggcmdiYSgxNiwgMTA1LCAyMjcsIDAuMyk7XG4gICAgfVxuICAgIC8qIEVzdGlsbyBkZWwgYm90b24gYmxhbmNvICovXG4gIC5yZWR7XG4gICAgLS1iYWNrZ3JvdW5kOiByZ2IoMjU1LCAwLCAwKTtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOnJnYigyNTUsIDAsIDApO1xuICAgIC0tY29sb3I6I2ZmZmZmZjtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAtLWJveC1zaGFkb3c6IDBweCAycHggOHB4IHJnYmEoMTYsIDEwNSwgMjI3LCAwLjMpO1xuICB9XG4gIC8qIENvbnRlbmVkb3IgZGUgaW1hZ2VuIGRlbCB1c3VhcmlvICovXG4gIC5jb250YWluZXItaW1nIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgLmltZyB7XG4gICAgICB3aWR0aDogODJweDtcbiAgICAgIGhlaWdodDogODBweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICB9XG4gIH1cbiAgLyogQ29sb3IgdmVyZGUgcGFyYSBhbGd1bm9zIGVsZW1lbnRvcyAqL1xuICAuZ3JlZW4ge1xuICAgIGNvbG9yOiAjMDA4RDM2O1xuICB9XG4gIFxuICBcbiJdfQ== */");

/***/ }),

/***/ "S4Co":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/confirm-transfers/confirm-transfers.module.ts ***!
  \***************************************************************************************/
/*! exports provided: ConfirmTransfersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmTransfersPageModule", function() { return ConfirmTransfersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _confirm_transfers_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./confirm-transfers-routing.module */ "7Okr");
/* harmony import */ var _confirm_transfers_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./confirm-transfers.page */ "ee54");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let ConfirmTransfersPageModule = class ConfirmTransfersPageModule {
};
ConfirmTransfersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _confirm_transfers_routing_module__WEBPACK_IMPORTED_MODULE_5__["ConfirmTransfersPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_confirm_transfers_page__WEBPACK_IMPORTED_MODULE_6__["ConfirmTransfersPage"]]
    })
], ConfirmTransfersPageModule);



/***/ }),

/***/ "ee54":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/confirm-transfers/confirm-transfers.page.ts ***!
  \*************************************************************************************/
/*! exports provided: ConfirmTransfersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmTransfersPage", function() { return ConfirmTransfersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_confirm_transfers_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./confirm-transfers.page.html */ "q1qz");
/* harmony import */ var _confirm_transfers_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./confirm-transfers.page.scss */ "Fglc");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_modules_dashboard_services_admin_travels_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/modules/dashboard/services/admin/travels.service */ "KlPZ");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../shared/services/alerts.service */ "X0HJ");







let ConfirmTransfersPage = class ConfirmTransfersPage {
    constructor(activatedRoute, travelsService, alertsService, router, renderer) {
        this.activatedRoute = activatedRoute;
        this.travelsService = travelsService;
        this.alertsService = alertsService;
        this.router = router;
        this.renderer = renderer;
        this.travelId = activatedRoute.snapshot.paramMap.get("travelId");
    }
    ngOnInit() {
        this.travel = this.travelsService.getTravelAndUserById(this.travelId, "driver");
    }
    confirmPaymentMethod() {
        this.alertsService.presentLoading('Confirmando...');
        this.travelsService.updatePayMethodTransfer(this.travelId, 'paid')
            .then(() => {
            this.alertsService.loading.dismiss();
            this.router.navigate(['dashboard/review-transfer'])
                .then(nav => console.log(nav))
                .catch(error => console.log(error));
        })
            .catch((error) => {
            this.alertsService.loading.dismiss();
            console.log(error);
        });
    }
    confirmPaymentMethodReject() {
        this.alertsService.presentLoading('Rechazando...');
        this.travelsService.updatePayMethodTransfer(this.travelId, 'reject')
            .then(() => {
            this.alertsService.loading.dismiss();
            this.router.navigate(['dashboard/review-transfer']);
        })
            .catch((error) => {
            this.alertsService.loading.dismiss();
            console.log(error);
        });
    }
};
ConfirmTransfersPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: src_app_modules_dashboard_services_admin_travels_service__WEBPACK_IMPORTED_MODULE_5__["TravelsService"] },
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Renderer2"] }
];
ConfirmTransfersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-confirm-transfers',
        template: _raw_loader_confirm_transfers_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_confirm_transfers_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ConfirmTransfersPage);



/***/ }),

/***/ "q1qz":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/confirm-transfers/confirm-transfers.page.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons class=\"margin-left\" slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div class=\"container\">\n    <ion-card>\n      <ion-row>\n        <ion-col size=\"4\" class=\"container-img\">\n          <div *ngIf=\"(travel | async)?.user.imagen == ''\" style=\"background-image: url('../../../../../assets/img/user.png')\" class=\"img\">\n          </div>\n          <div *ngIf=\"(travel | async)?.user.imagen != ''\" class=\"img\">\n            <img src=\"{{ ((travel | async)?.user.imagen)}}\">\n          </div>\n        </ion-col>\n        <ion-col size=\"8\">\n           <ion-card-header>\n            <ion-card-title>{{ (travel | async)?.user.name }}</ion-card-title>\n            <ion-card-subtitle>{{ (travel | async)?.createdAt.toMillis() | date:'short'  }}</ion-card-subtitle>    \n            <ion-card-title>{{ (travel | async)?.destinyLocation.address }}</ion-card-title>\n        </ion-card-header> \n        </ion-col>\n      </ion-row>\n    </ion-card>\n    <ion-row>\n      <p class=\"title\">Transporte {{(travel | async)?.vehicleType}} </p>\n      <p class=\"cost\"> $ {{(travel | async)?.price.dollars}}.{{(travel | async)?.price.cents}} </p>\n    </ion-row>  \n    <ion-row>\n      <ion-col size=\"6\">\n         <div class=\"img-license\" >\n          <img src=\"{{ ((travel | async)?.invoiceImg)}}\">\n        </div> \n      </ion-col>\n    </ion-row>\n    <ion-row >\n      <ion-col size=\"9\">\n      </ion-col>\n      <ion-button *ngIf=\"(travel | async)?.status === 'reject' || (travel | async)?.status === 'waiting_payment' \" class=\"btn blue-btn\" (click)=\"confirmPaymentMethod()\" >\n        Aceptar Solicitud\n      </ion-button>\n      <ion-button *ngIf=\"(travel | async)?.status === 'paid' || (travel | async)?.status === 'waiting_payment' \" class=\"btn green-btn\" (click)=\"confirmPaymentMethodReject()\">\n        Rechazar Solicitud\n      </ion-button>\n    </ion-row>\n  </div>\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=pages-confirm-transfers-confirm-transfers-module.js.map