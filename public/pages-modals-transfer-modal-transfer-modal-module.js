(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-transfer-modal-transfer-modal-module"],{

/***/ "CGbD":
/*!****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/transfer-modal/transfer-modal.module.ts ***!
  \****************************************************************************************/
/*! exports provided: TransferModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferModalPageModule", function() { return TransferModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _transfer_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./transfer-modal-routing.module */ "YToT");
/* harmony import */ var _transfer_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./transfer-modal.page */ "Y70h");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");








let TransferModalPageModule = class TransferModalPageModule {
};
TransferModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _transfer_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["TransferModalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_transfer_modal_page__WEBPACK_IMPORTED_MODULE_6__["TransferModalPage"]]
    })
], TransferModalPageModule);



/***/ }),

/***/ "YToT":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/transfer-modal/transfer-modal-routing.module.ts ***!
  \************************************************************************************************/
/*! exports provided: TransferModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferModalPageRoutingModule", function() { return TransferModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _transfer_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./transfer-modal.page */ "Y70h");




const routes = [
    {
        path: '',
        component: _transfer_modal_page__WEBPACK_IMPORTED_MODULE_3__["TransferModalPage"]
    }
];
let TransferModalPageRoutingModule = class TransferModalPageRoutingModule {
};
TransferModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TransferModalPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-transfer-modal-transfer-modal-module.js.map