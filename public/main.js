(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "+sSY":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/payment-type/payment-type.component.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-card #card >\n  <ion-row>\n    <ion-col size=\"2\" class=\"container-icon\">\n      <div *ngIf=\"icon === 'no-icon'\" class=\"oval\">\n\n      </div>\n      <ion-icon *ngIf=\"icon !== 'no-icon'\" name=\"{{icon}}\"></ion-icon>\n    </ion-col>\n    <ion-col size=\"8\">\n      <p>{{text}}</p>\n    </ion-col>\n    <ion-col size=\"2\"  class=\"container-icon\">\n      <ion-icon class=\"arrow\" name=\"chevron-forward-outline\"></ion-icon>\n    </ion-col>\n  </ion-row>\n</ion-card>\n");

/***/ }),

/***/ "/+Y6":
/*!*******************************************************************!*\
  !*** ./src/app/modules/dashboard/services/admin/stops.service.ts ***!
  \*******************************************************************/
/*! exports provided: StopsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StopsService", function() { return StopsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");




let StopsService = class StopsService {
    constructor(firestore) {
        this.firestore = firestore;
        this.stopsCollection = firestore.collection('stops');
        this.stops = this.stopsCollection.valueChanges({ idField: 'stopId' });
    }
    addStop(stop) {
        stop.updateDate = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        stop.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firestore.collection('stops').add(stop);
    }
    getStop() {
        return this.firestore.collection('stops').valueChanges({ idField: 'uid' });
    }
    updateStop(stop) {
        const uid = stop.uid;
        delete stop.uid;
        this.firestore.collection('stops').doc(uid).update(stop);
    }
    deleteStop(uid) {
        this.firestore.collection('stops').doc(uid).delete();
    }
};
StopsService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
StopsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], StopsService);



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/victorsanchez/Documents/Nous Projects/igo/src/main.ts */"zUnb");


/***/ }),

/***/ "1Ijv":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/accept-button/accept-button.component.scss ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".accept-button {\n  width: 100%;\n  height: 60px;\n  --background: #008D36;\n  box-shadow: 0px 2px 6px rgba(16, 105, 227, 0.3);\n  border-radius: 15px;\n  border: 1px solid red;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2FjY2VwdC1idXR0b24uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxXQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsK0NBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0FBQ0QiLCJmaWxlIjoiYWNjZXB0LWJ1dHRvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hY2NlcHQtYnV0dG9uIHtcblx0d2lkdGg6IDEwMCU7XG5cdGhlaWdodDogNjBweDtcblx0LS1iYWNrZ3JvdW5kOiAjMDA4RDM2O1xuXHRib3gtc2hhZG93OiAwcHggMnB4IDZweCByZ2JhKDE2LCAxMDUsIDIyNywgMC4zKTtcblx0Ym9yZGVyLXJhZGl1czogMTVweDtcblx0Ym9yZGVyOiAxcHggc29saWQgcmVkO1xufVxuIl19 */");

/***/ }),

/***/ "2+Va":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/initial-modal-button/initial-modal-button.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: InitialModalButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InitialModalButtonComponent", function() { return InitialModalButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_initial_modal_button_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./initial-modal-button.component.html */ "oscc");
/* harmony import */ var _initial_modal_button_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./initial-modal-button.component.scss */ "JjpH");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let InitialModalButtonComponent = class InitialModalButtonComponent {
    constructor() {
        this.openModal = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
    }
    ngOnInit() { }
};
InitialModalButtonComponent.ctorParameters = () => [];
InitialModalButtonComponent.propDecorators = {
    openModal: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"] }]
};
InitialModalButtonComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-initial-modal-button',
        template: _raw_loader_initial_modal_button_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_initial_modal_button_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], InitialModalButtonComponent);



/***/ }),

/***/ "3IAN":
/*!******************************************!*\
  !*** ./src/app/core/guard/auth.guard.ts ***!
  \******************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/auth */ "6nsN");





let AuthGuard = class AuthGuard {
    constructor(router) {
        this.router = router;
    }
    canActivate(next, state) {
        return new Promise((resolve, reject) => {
            firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].auth().onAuthStateChanged((user) => {
                if (user) {
                    if (user.emailVerified) {
                        resolve(true);
                    }
                    else {
                        this.router.navigate(['/login']);
                    }
                }
                else {
                    this.router.navigate(['/login']);
                    resolve(false);
                }
            });
        });
    }
};
AuthGuard.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AuthGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], AuthGuard);



/***/ }),

/***/ "3Mby":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/header/header.component.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-row>\n      <ion-buttons>\n        <ion-back-button defaultHref=\"../\"></ion-back-button>\n      </ion-buttons>\n      <div class=\"title-container\">\n        <ion-title>{{ title }}</ion-title>\n      </div>\n    </ion-row>    \n  </ion-toolbar>  \n</ion-header>\n");

/***/ }),

/***/ "5TGK":
/*!*************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/close-modal-button/close-modal-button.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: CloseModalButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CloseModalButtonComponent", function() { return CloseModalButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_close_modal_button_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./close-modal-button.component.html */ "nH6m");
/* harmony import */ var _close_modal_button_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./close-modal-button.component.scss */ "Kkp9");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let CloseModalButtonComponent = class CloseModalButtonComponent {
    constructor() {
        this.closeModal = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
    }
    ngOnInit() { }
};
CloseModalButtonComponent.ctorParameters = () => [];
CloseModalButtonComponent.propDecorators = {
    closeModal: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"] }]
};
CloseModalButtonComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-close-modal-button',
        template: _raw_loader_close_modal_button_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_close_modal_button_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], CloseModalButtonComponent);



/***/ }),

/***/ "5Xz3":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/travel-card/travel-card.component.scss ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".border {\n  border: 1px solid red;\n  margin: 0px;\n}\n\n.icons-container {\n  border: 1px solid yellow;\n  display: flex;\n  justify-content: center;\n}\n\n.items {\n  width: 50%;\n  border: 1px solid blue;\n}\n\n.point {\n  width: 20px;\n  height: 20px;\n  border-radius: 100%;\n  background: blue;\n}\n\n.line {\n  width: 20px;\n  height: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3RyYXZlbC1jYXJkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MscUJBQUE7RUFDQSxXQUFBO0FBQ0Q7O0FBRUE7RUFDQyx3QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtBQUNEOztBQUVBO0VBQ0MsVUFBQTtFQUNBLHNCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUFDRDs7QUFFQTtFQUNDLFdBQUE7RUFDQSxZQUFBO0FBQ0QiLCJmaWxlIjoidHJhdmVsLWNhcmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYm9yZGVyIHtcblx0Ym9yZGVyOiAxcHggc29saWQgcmVkO1xuXHRtYXJnaW46IDBweDtcbn1cblxuLmljb25zLWNvbnRhaW5lciB7XG5cdGJvcmRlcjogMXB4IHNvbGlkIHllbGxvdztcblx0ZGlzcGxheTogZmxleDtcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5pdGVtcyB7XG5cdHdpZHRoOiA1MCU7XG5cdGJvcmRlcjogMXB4IHNvbGlkIGJsdWU7XG59XG5cbi5wb2ludCB7XG5cdHdpZHRoOiAyMHB4O1xuXHRoZWlnaHQ6IDIwcHg7XG5cdGJvcmRlci1yYWRpdXM6IDEwMCU7XG5cdGJhY2tncm91bmQ6IGJsdWU7XG59XG5cbi5saW5lIHtcblx0d2lkdGg6IDIwcHg7XG5cdGhlaWdodDogMjBweDtcbn1cbiJdfQ== */");

/***/ }),

/***/ "6CRC":
/*!***************************************************************!*\
  !*** ./src/app/core/authentication/authentication.service.ts ***!
  \***************************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "Jgta");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "UbJi");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/modules/home/services/users.service */ "6JOU");










let AuthenticationService = class AuthenticationService {
    constructor(afs, angularFireAuth, router, ngZone, alertsService, loadingController, usersService) {
        this.afs = afs;
        this.angularFireAuth = angularFireAuth;
        this.router = router;
        this.ngZone = ngZone;
        this.alertsService = alertsService;
        this.loadingController = loadingController;
        this.usersService = usersService;
        this.angularFireAuth.authState.subscribe(user => {
            if (user) {
                this.userState = user;
                localStorage.setItem('user', JSON.stringify(this.userState));
                JSON.parse(localStorage.getItem('user'));
            }
            else {
                localStorage.setItem('user', null);
                JSON.parse(localStorage.getItem('user'));
            }
        });
    }
    signIn(email, password) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].auth().signInWithEmailAndPassword(email, password);
        });
    }
    // signUp(user: User) {
    //   return this.angularFireAuth.createUserWithEmailAndPassword(user.email, user.password)
    //     .then(result => {
    //       this.sendVerificationEmail();
    //       this.setUserDataClient(user);
    //     }).catch(error => {
    //       console.log(error.message);
    //     })
    // }
    signUp(user) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const newUser = yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].auth().createUserWithEmailAndPassword(user.email, user.password);
            const uid = newUser.user.uid;
            user.isActive = true;
            user.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].firestore.Timestamp.now().toDate();
            user.updateAt = firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].firestore.Timestamp.now().toDate();
            yield this.afs.doc(`users/${uid}`).set(user);
            yield newUser.user.sendEmailVerification();
            yield this.logoutUser();
        });
    }
    logoutUser() {
        return firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].auth().signOut();
    }
    sendVerificationEmail() {
        return this.angularFireAuth.currentUser.then(user => user.sendEmailVerification())
            .then(() => {
            // this.router.navigate(['email-verification']);
        });
    }
    forgotPassword(passwordResetEmail) {
        return this.angularFireAuth.sendPasswordResetEmail(passwordResetEmail)
            .then(() => {
            console.log('Password reset email sent, check your inbox.');
        }).catch(error => {
            console.log(error);
        });
    }
    get isLoggedIn() {
        const user = JSON.parse(localStorage.getItem('user'));
        return (user !== null && user.emailVerified !== false) ? true : false;
    }
    googleAuth() {
        return this.authLogin(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].auth.GoogleAuthProvider());
    }
    facebookAuth() {
        return this.authLogin(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].auth.FacebookAuthProvider());
    }
    authLogin(provider) {
        return this.angularFireAuth.signInWithPopup(provider)
            .then(result => {
            this.ngZone.run(() => {
                this.router.navigate(['dashboard']);
            });
            this.setUserData(result.user);
        }).catch(error => {
            console.log(error);
        });
    }
    signOut() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.alertsService.presentLoading('Saliendo...');
            return this.angularFireAuth.signOut().then(() => {
                localStorage.removeItem('user');
                this.alertsService.loading.dismiss();
                this.router.navigate(['../home/login']);
            });
        });
    }
    setUserData(user) {
        const userRef = this.afs.doc(`users/${user.uid}`);
        const userState = {
            name: user.displayName,
            email: user.email,
            imagen: user.photoURL,
            role: 'client',
            gender: '',
            isActive: true,
            createAt: firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].firestore.Timestamp.now().toDate(),
            updateAt: firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].firestore.Timestamp.now().toDate(),
            telephoneNumber: user.phoneNumber
        };
        return userRef.set(userState, {
            merge: true
        });
    }
    setUserDataClient(user) {
        const userRef = this.afs.doc(`users/${user.uid}`);
        const userState = user;
        return userRef.set(userState, {
            merge: true
        });
    }
    getCurrentUserId() {
        return firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].auth().currentUser.uid;
    }
};
AuthenticationService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"] },
    { type: src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_8__["UsersService"] }
];
AuthenticationService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthenticationService);



/***/ }),

/***/ "6JOU":
/*!********************************************************!*\
  !*** ./src/app/modules/home/services/users.service.ts ***!
  \********************************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");



let UsersService = class UsersService {
    constructor(afs) {
        this.afs = afs;
        this.usersCollection = afs.collection('users');
    }
    add(user) {
        return new Promise((resolve, reject) => {
            this.usersCollection.add(user);
            resolve();
        });
    }
    getUserById(userId) {
        this.userDoc = this.afs.doc('users/' + userId);
        this.user = this.userDoc.valueChanges({ idField: 'uid' });
        return this.user;
    }
    getUserByIdSimple(uid) {
        return this.afs.collection('users').doc(uid).valueChanges({ idField: 'uid' });
    }
    read() {
        this.usersCollection = this.afs.collection('users');
        this.users = this.usersCollection.valueChanges({ idField: 'userId' });
        return this.users;
    }
    readDrivers() {
        this.usersCollection = this.afs.collection('users', ref => ref.where('role', '==', 'driver'));
        this.users = this.usersCollection.valueChanges({ idField: 'userId' });
        return this.users;
    }
    //service powered Mario
    getUsersByRole(role) {
        return this.afs.collection('users', (ref) => ref.where('role', '==', role))
            .valueChanges({ idField: 'uid' });
    }
    update(user) {
        this.userDoc = this.afs.doc('users/' + user.uid);
        return this.userDoc.update(user);
    }
};
UsersService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
UsersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UsersService);



/***/ }),

/***/ "6kNZ":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/address-input/address-input.component.ts ***!
  \***************************************************************************************/
/*! exports provided: AddressInputComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressInputComponent", function() { return AddressInputComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_address_input_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./address-input.component.html */ "Gfle");
/* harmony import */ var _address_input_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./address-input.component.scss */ "UtKx");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
var AddressInputComponent_1;





let AddressInputComponent = AddressInputComponent_1 = class AddressInputComponent {
    constructor() {
        this.val = '';
        this.onChange = () => { };
        this.onTouch = () => { };
    }
    ngOnInit() {
    }
    get value() {
        return this.val;
    }
    set value(val) {
        if (val !== undefined && this.val !== val) {
            this.val = val;
            this.onChange(val);
        }
    }
    writeValue(value) {
        this.value = value;
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouch = fn;
    }
};
AddressInputComponent.ctorParameters = () => [];
AddressInputComponent.propDecorators = {
    text: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    disabled: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    placeholder: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
AddressInputComponent = AddressInputComponent_1 = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-address-input',
        template: _raw_loader_address_input_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        providers: [
            {
                provide: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NG_VALUE_ACCESSOR"],
                useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["forwardRef"])(() => AddressInputComponent_1),
                multi: true,
            }
        ],
        styles: [_address_input_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AddressInputComponent);



/***/ }),

/***/ "8Thi":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/stop-update/stop-update.component.html ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar translucent>\n    <ion-buttons class=\"margin-left\" slot=\"start\">\n      <ion-back-button defaultHref=\"dashboard/stops\"  (click)=\"closeModal()\"></ion-back-button>\n    </ion-buttons>\n    <ion-row>\n      <ion-title>Editar parada</ion-title>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n<ion-content >\n\n  <!-- fab placed to the top end -->\n <!--  <ion-fab vertical=\"top\" horizontal=\"start\" slot=\"fixed\" defaultHref=\"/home\">\n    <ion-fab-button color=\"#fffff\" menu=\"main menu\">\n      <ion-icon name=\"filter-outline\"></ion-icon>\n    </ion-fab-button>\n    <ion-fab-list side=\"bottom\">\n      <ion-fab-button>\n        <ion-back-button slot=\"start\" defaultHref=\"/liststops\"></ion-back-button>\n      </ion-fab-button>\n      <ion-fab-button>\n        <ion-icon name=\"share\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab-list>\n  </ion-fab> -->\n\n  <div class=\"map\" #divMap></div>\n\n\n  <div class=\"container\">\n    <ion-row class=\"container-title\">\n      <h5>Editar nueva parada</h5>\n    </ion-row>\n    <ion-row class=\"container-all-inputs\">\n\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">DIRECCIÓN*</ion-label>\n        <ion-input class=\"input-ionItem\" [(ngModel)]=\"stop.address\" id=\"stop_address\" autocomplete=\"off\" (keyup)=\"keyUpHandler()\"></ion-input>\n\n      </div>\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">NOMBRE*</ion-label>\n        <ion-item class=\"input-ionItem\">\n          <ion-input type=\"text\" [(ngModel)]=\"stop.name\"></ion-input>\n        </ion-item>\n      </div>\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">DESCRIPCIÓN*</ion-label>\n        <ion-item class=\"input-ionItem\">\n          <ion-textarea [(ngModel)]=\"stop.description\"></ion-textarea>\n        </ion-item>\n      </div>\n    </ion-row>\n    <ion-row class=\"container-btn\">\n      <ion-button class=\"btn\" (click)=\"update()\"> Actualizar Parada </ion-button>\n    </ion-row>\n  </div>\n");

/***/ }),

/***/ "8WUZ":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/stop-update/stop-update.component.scss ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n/* Estilos del header */\nion-toolbar ion-row {\n  align-items: center;\n  display: flex;\n  justify-content: center;\n  width: 100%;\n}\nion-toolbar ion-row ion-title {\n  font-size: 20px;\n  font-weight: bold;\n  --color: #3E4958;\n  text-align: center;\n}\n.map {\n  width: 100%;\n  height: 60%;\n}\n.map.show-map {\n  opacity: 1;\n}\n/* Contenedor del ingreso de la información de la parada */\n.container {\n  height: 50%;\n  width: 100%;\n  z-index: 2;\n  position: relative;\n  bottom: 30px;\n  border-radius: 9px 9px 0px 0px;\n  margin-top: 60px;\n}\n/* Contenedor del titulo */\n.container-title {\n  margin-left: 10%;\n  margin-bottom: 2%;\n}\n.container-title h5 {\n  font-weight: bold;\n  font-size: 18px;\n  color: #3E4958;\n}\n.container-all-inputs {\n  display: flex;\n  justify-content: center;\n}\n.container-all-inputs .container-inputs {\n  margin-bottom: 3%;\n  padding: 10px;\n  width: 90%;\n  text-align: left;\n}\n.container-all-inputs .container-inputs ion-select {\n  width: 100%;\n  justify-content: center;\n}\n.container-all-inputs .input-ionItem {\n  --background: #F7F8F9;\n  --highlight-color-focused: #008D36;\n  --highlight-color-valid: #008D36;\n  border: 0.5px solid #D5DDE0;\n  border-radius: 15px;\n  margin-top: 3%;\n}\n/* Estilos del boton de agregar una nueva parada */\n.container-btn {\n  justify-content: center;\n}\n.container-btn .btn {\n  width: 75%;\n  height: 60px;\n  --background: #008D36;\n  --border-radius: 15px;\n  --box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  color: white;\n  font-size: 17px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3N0b3AtdXBkYXRlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQUFoQix1QkFBQTtBQUVJO0VBQ0UsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0FBQ047QUFDTTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFDUjtBQUtFO0VBQ0ksV0FBQTtFQUNBLFdBQUE7QUFGTjtBQUdNO0VBQ0UsVUFBQTtBQURSO0FBTUksMERBQUE7QUFDQTtFQUNFLFdBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0VBQ0EsZ0JBQUE7QUFITjtBQU1JLDBCQUFBO0FBQ0E7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0FBSE47QUFLTTtFQUNFLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFIUjtBQU9JO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0FBSk47QUFNTTtFQUNFLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtBQUpSO0FBTVE7RUFDRSxXQUFBO0VBQ0EsdUJBQUE7QUFKVjtBQVFNO0VBQ0UscUJBQUE7RUFDQSxrQ0FBQTtFQUNBLGdDQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFOUjtBQWVJLGtEQUFBO0FBQ0E7RUFDRSx1QkFBQTtBQVpOO0FBY007RUFDRSxVQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxvREFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBWlIiLCJmaWxlIjoic3RvcC11cGRhdGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBFc3RpbG9zIGRlbCBoZWFkZXIgKi9cbmlvbi10b29sYmFyIHtcbiAgICBpb24tcm93IHtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgXG4gICAgICBpb24tdGl0bGUge1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAtLWNvbG9yOiAjM0U0OTU4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIFxuICAgICAgfVxuICAgIH1cbiAgfVxuICBcbiAgLm1hcCB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogNjAlO1xuICAgICAgJi5zaG93LW1hcCB7XG4gICAgICAgIG9wYWNpdHk6IDE7XG4gICAgICB9XG4gICAgfVxuICAgIFxuICAgIFxuICAgIC8qIENvbnRlbmVkb3IgZGVsIGluZ3Jlc28gZGUgbGEgaW5mb3JtYWNpw7NuIGRlIGxhIHBhcmFkYSAqL1xuICAgIC5jb250YWluZXIge1xuICAgICAgaGVpZ2h0OiA1MCU7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIHotaW5kZXg6IDI7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBib3R0b206IDMwcHg7XG4gICAgICBib3JkZXItcmFkaXVzOiA5cHggOXB4IDBweCAwcHg7XG4gICAgICBtYXJnaW4tdG9wOiA2MHB4O1xuICAgIH1cbiAgICBcbiAgICAvKiBDb250ZW5lZG9yIGRlbCB0aXR1bG8gKi9cbiAgICAuY29udGFpbmVyLXRpdGxlIHtcbiAgICAgIG1hcmdpbi1sZWZ0OiAxMCU7XG4gICAgICBtYXJnaW4tYm90dG9tOiAyJTtcbiAgICBcbiAgICAgIGg1IHtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgY29sb3I6ICMzRTQ5NTg7XG4gICAgICB9XG4gICAgfVxuICAgIFxuICAgIC5jb250YWluZXItYWxsLWlucHV0cyB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgXG4gICAgICAuY29udGFpbmVyLWlucHV0cyB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMlO1xuICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICB3aWR0aDogOTAlO1xuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgXG4gICAgICAgIGlvbi1zZWxlY3Qge1xuICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgXG4gICAgICAuaW5wdXQtaW9uSXRlbSB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogI0Y3RjhGOTtcbiAgICAgICAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDogIzAwOEQzNjtcbiAgICAgICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6ICMwMDhEMzY7XG4gICAgICAgIGJvcmRlcjogMC41cHggc29saWQgI0Q1RERFMDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMyU7XG4gICAgICB9XG4gICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIC8qIEVzdGlsb3MgZGVsIGJvdG9uIGRlIGFncmVnYXIgdW5hIG51ZXZhIHBhcmFkYSAqL1xuICAgIC5jb250YWluZXItYnRuIHtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIFxuICAgICAgLmJ0biB7XG4gICAgICAgIHdpZHRoOiA3NSU7XG4gICAgICAgIGhlaWdodDogNjBweDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjMDA4RDM2O1xuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgICAgIC0tYm94LXNoYWRvdzogMHB4IDRweCAyMHB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yNSk7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgICAgfVxuICAgIFxuICAgIFxuICAgIH1cbiAgICAiXX0= */");

/***/ }),

/***/ "8qac":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/accept-button/accept-button.component.html ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-button color=\"accept-btn\">{{ title }}</ion-button>\n");

/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyBmAsDEwRcMFRcIGdJPYBP2mPsGjnHAh7I",
        authDomain: "igo-app-c3ed0.firebaseapp.com",
        projectId: "igo-app-c3ed0",
        storageBucket: "igo-app-c3ed0.appspot.com",
        messagingSenderId: "796178968721",
        appId: "1:796178968721:web:98db16ef1da21696394f30",
        measurementId: "G-S8FCVN9237"
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "BK/L":
/*!*************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/travel-detail-card/travel-detail-card.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: TravelDetailCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelDetailCardComponent", function() { return TravelDetailCardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_travel_detail_card_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./travel-detail-card.component.html */ "OYn4");
/* harmony import */ var _travel_detail_card_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./travel-detail-card.component.scss */ "nt4N");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let TravelDetailCardComponent = class TravelDetailCardComponent {
    constructor() { }
    ngOnInit() { }
};
TravelDetailCardComponent.ctorParameters = () => [];
TravelDetailCardComponent.propDecorators = {
    vehicleType: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    wholePartPrice: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    decimalPartPrice: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    duration: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
TravelDetailCardComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-travel-detail-card',
        template: _raw_loader_travel_detail_card_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_travel_detail_card_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TravelDetailCardComponent);



/***/ }),

/***/ "Dx6G":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/item-map/item-map.component.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".stop {\n  height: 18px;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 15px;\n  line-height: 136%;\n  color: #3E4958;\n}\n\nion-thumbnail {\n  display: flex;\n  justify-content: flex-start;\n  align-items: center;\n  margin-right: -14px;\n}\n\n.icon-container {\n  border-radius: 100%;\n  display: inline;\n  justify-content: center;\n  align-items: center;\n  padding: 3px 5px;\n}\n\nion-item {\n  --padding-start: 0px;\n}\n\nion-item:hover {\n  --padding-start: 0px;\n  --background: #F2F2F2;\n}\n\n.green-item {\n  background-color: #008D36;\n}\n\n.white-item {\n  background-color: grey;\n}\n\nion-icon {\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2l0ZW0tbWFwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBQ0Q7O0FBRUE7RUFDQyxhQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FBQ0Q7O0FBRUE7RUFDQyxtQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUFDRDs7QUFFQTtFQUNDLG9CQUFBO0FBQ0Q7O0FBRUE7RUFDQyxvQkFBQTtFQUNBLHFCQUFBO0FBQ0Q7O0FBRUE7RUFDQyx5QkFBQTtBQUNEOztBQUVBO0VBQ0Msc0JBQUE7QUFDRDs7QUFFQTtFQUNDLFlBQUE7QUFDRCIsImZpbGUiOiJpdGVtLW1hcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zdG9wIHtcblx0aGVpZ2h0OiAxOHB4O1xuXHRmb250LXN0eWxlOiBub3JtYWw7XG5cdGZvbnQtd2VpZ2h0OiBub3JtYWw7XG5cdGZvbnQtc2l6ZTogMTVweDtcblx0bGluZS1oZWlnaHQ6IDEzNiU7XG5cdGNvbG9yOiAjM0U0OTU4O1xufVxuXG5pb24tdGh1bWJuYWlsIHtcblx0ZGlzcGxheTogZmxleDtcblx0anVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuXHRhbGlnbi1pdGVtczogY2VudGVyO1xuXHRtYXJnaW4tcmlnaHQ6IC0xNHB4O1xufVxuXG4uaWNvbi1jb250YWluZXIge1xuXHRib3JkZXItcmFkaXVzOiAxMDAlO1xuXHRkaXNwbGF5OiBpbmxpbmU7XG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXHRhbGlnbi1pdGVtczogY2VudGVyO1xuXHRwYWRkaW5nOiAzcHggNXB4O1xufVxuXG5pb24taXRlbSB7XG5cdC0tcGFkZGluZy1zdGFydDogMHB4O1xufVxuXG5pb24taXRlbTpob3ZlciB7XG5cdC0tcGFkZGluZy1zdGFydDogMHB4O1xuXHQtLWJhY2tncm91bmQ6ICNGMkYyRjI7XG59XG5cbi5ncmVlbi1pdGVtIHtcblx0YmFja2dyb3VuZC1jb2xvcjogIzAwOEQzNjsgXHRcbn1cblxuLndoaXRlLWl0ZW0ge1xuXHRiYWNrZ3JvdW5kLWNvbG9yOiBncmV5O1xufVxuXG5pb24taWNvbiB7XG5cdGNvbG9yOiB3aGl0ZTtcbn1cbiJdfQ== */");

/***/ }),

/***/ "Gfle":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/address-input/address-input.component.html ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-item>\n  <ion-input id=\"autocomplete-input\" [placeholder]=\"placeholder\" [disabled]=\"disabled\" [(ngModel)]=\"value\">{{ text }}</ion-input>\n  <ion-icon name=\"location-sharp\"></ion-icon>\n</ion-item>\n");

/***/ }),

/***/ "IQa3":
/*!*******************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/cards/bank-account-card/bank-account-card.component.scss ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card {\n  border-radius: 10px;\n}\nion-card .cruds-cart ion-card-content p {\n  color: black;\n  margin-bottom: 8px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL2JhbmstYWNjb3VudC1jYXJkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7QUFDSjtBQUdRO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0FBRFYiLCJmaWxlIjoiYmFuay1hY2NvdW50LWNhcmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCB7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBcbiAgICAuY3J1ZHMtY2FydCB7XG4gICAgICBpb24tY2FyZC1jb250ZW50IHtcbiAgICAgICAgcCB7XG4gICAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuICAiXX0= */");

/***/ }),

/***/ "JjpH":
/*!*******************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/initial-modal-button/initial-modal-button.component.scss ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".initial-modal-container {\n  height: 5%;\n  position: absolute;\n  bottom: 0px;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  border-radius: 12px 12px 0px 0px;\n  background: #FFFFFF;\n  box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);\n  padding-top: 1%;\n}\n\n/* Estilos de la linea que cierra el modal */\n\n.close-modal-line {\n  width: 30px;\n  height: 5px;\n  background-color: #D5DDE0;\n  border-radius: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2luaXRpYWwtbW9kYWwtYnV0dG9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsMkNBQUE7RUFDQSxlQUFBO0FBQ0Y7O0FBRUEsNENBQUE7O0FBQ0E7RUFDRSxXQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUFDRiIsImZpbGUiOiJpbml0aWFsLW1vZGFsLWJ1dHRvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbml0aWFsLW1vZGFsLWNvbnRhaW5lciB7XG4gIGhlaWdodDogNSU7ICAgIFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDEycHggMTJweCAwcHggMHB4O1xuICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICBib3gtc2hhZG93OiAwcHggMHB4IDE1cHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICBwYWRkaW5nLXRvcDogMSU7XG59XG5cbi8qIEVzdGlsb3MgZGUgbGEgbGluZWEgcXVlIGNpZXJyYSBlbCBtb2RhbCAqL1xuLmNsb3NlLW1vZGFsLWxpbmUge1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNENURERTAgO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xufVxuIl19 */");

/***/ }),

/***/ "KQ6T":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/principal-header/principal-header.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: PrincipalHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrincipalHeaderComponent", function() { return PrincipalHeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_principal_header_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./principal-header.component.html */ "cXOV");
/* harmony import */ var _principal_header_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./principal-header.component.scss */ "mGml");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let PrincipalHeaderComponent = class PrincipalHeaderComponent {
    constructor() {
        this.openMenu = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
    }
    ngOnInit() { }
};
PrincipalHeaderComponent.ctorParameters = () => [];
PrincipalHeaderComponent.propDecorators = {
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    openMenu: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"] }]
};
PrincipalHeaderComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-principal-header',
        template: _raw_loader_principal_header_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_principal_header_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], PrincipalHeaderComponent);



/***/ }),

/***/ "KQTD":
/*!*******************************************************************!*\
  !*** ./src/app/modules/dashboard/components/components.module.ts ***!
  \*******************************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./header/header.component */ "j82e");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _dashboard_components_request_button_request_button_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../dashboard/components/request-button/request-button.component */ "wJ+g");
/* harmony import */ var _request_card_request_card_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./request-card/request-card.component */ "jZdo");
/* harmony import */ var _button_button_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./button/button.component */ "rQM5");
/* harmony import */ var _principal_header_principal_header_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./principal-header/principal-header.component */ "KQ6T");
/* harmony import */ var _close_modal_button_close_modal_button_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./close-modal-button/close-modal-button.component */ "5TGK");
/* harmony import */ var _vehicle_type_button_vehicle_type_button_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./vehicle-type-button/vehicle-type-button.component */ "lnVj");
/* harmony import */ var _initial_modal_button_initial_modal_button_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./initial-modal-button/initial-modal-button.component */ "2+Va");
/* harmony import */ var _address_input_address_input_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./address-input/address-input.component */ "6kNZ");
/* harmony import */ var _item_map_item_map_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./item-map/item-map.component */ "pQWY");
/* harmony import */ var _accept_button_accept_button_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./accept-button/accept-button.component */ "Pf0y");
/* harmony import */ var _travel_card_travel_card_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./travel-card/travel-card.component */ "YLGK");
/* harmony import */ var _payment_type_payment_type_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./payment-type/payment-type.component */ "f3p9");
/* harmony import */ var _travel_detail_card_travel_detail_card_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./travel-detail-card/travel-detail-card.component */ "BK/L");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _cards_bank_account_card_bank_account_card_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./cards/bank-account-card/bank-account-card.component */ "l3rL");




















let ComponentsModule = class ComponentsModule {
};
ComponentsModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_18__["FormsModule"]
        ],
        declarations: [
            _header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"],
            _dashboard_components_request_button_request_button_component__WEBPACK_IMPORTED_MODULE_5__["RequestButtonComponent"],
            _request_card_request_card_component__WEBPACK_IMPORTED_MODULE_6__["RequestCardComponent"],
            _button_button_component__WEBPACK_IMPORTED_MODULE_7__["ButtonComponent"],
            _principal_header_principal_header_component__WEBPACK_IMPORTED_MODULE_8__["PrincipalHeaderComponent"],
            _close_modal_button_close_modal_button_component__WEBPACK_IMPORTED_MODULE_9__["CloseModalButtonComponent"],
            _vehicle_type_button_vehicle_type_button_component__WEBPACK_IMPORTED_MODULE_10__["VehicleTypeButtonComponent"],
            _initial_modal_button_initial_modal_button_component__WEBPACK_IMPORTED_MODULE_11__["InitialModalButtonComponent"],
            _address_input_address_input_component__WEBPACK_IMPORTED_MODULE_12__["AddressInputComponent"],
            _item_map_item_map_component__WEBPACK_IMPORTED_MODULE_13__["ItemMapComponent"],
            _accept_button_accept_button_component__WEBPACK_IMPORTED_MODULE_14__["AcceptButtonComponent"],
            _travel_card_travel_card_component__WEBPACK_IMPORTED_MODULE_15__["TravelCardComponent"],
            _payment_type_payment_type_component__WEBPACK_IMPORTED_MODULE_16__["PaymentTypeComponent"],
            _travel_detail_card_travel_detail_card_component__WEBPACK_IMPORTED_MODULE_17__["TravelDetailCardComponent"],
            _cards_bank_account_card_bank_account_card_component__WEBPACK_IMPORTED_MODULE_19__["BankAccountCardComponent"]
        ],
        exports: [
            _header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"],
            _dashboard_components_request_button_request_button_component__WEBPACK_IMPORTED_MODULE_5__["RequestButtonComponent"],
            _request_card_request_card_component__WEBPACK_IMPORTED_MODULE_6__["RequestCardComponent"],
            _button_button_component__WEBPACK_IMPORTED_MODULE_7__["ButtonComponent"],
            _principal_header_principal_header_component__WEBPACK_IMPORTED_MODULE_8__["PrincipalHeaderComponent"],
            _close_modal_button_close_modal_button_component__WEBPACK_IMPORTED_MODULE_9__["CloseModalButtonComponent"],
            _vehicle_type_button_vehicle_type_button_component__WEBPACK_IMPORTED_MODULE_10__["VehicleTypeButtonComponent"],
            _initial_modal_button_initial_modal_button_component__WEBPACK_IMPORTED_MODULE_11__["InitialModalButtonComponent"],
            _address_input_address_input_component__WEBPACK_IMPORTED_MODULE_12__["AddressInputComponent"],
            _item_map_item_map_component__WEBPACK_IMPORTED_MODULE_13__["ItemMapComponent"],
            _accept_button_accept_button_component__WEBPACK_IMPORTED_MODULE_14__["AcceptButtonComponent"],
            _travel_card_travel_card_component__WEBPACK_IMPORTED_MODULE_15__["TravelCardComponent"],
            _payment_type_payment_type_component__WEBPACK_IMPORTED_MODULE_16__["PaymentTypeComponent"],
            _travel_detail_card_travel_detail_card_component__WEBPACK_IMPORTED_MODULE_17__["TravelDetailCardComponent"],
            _cards_bank_account_card_bank_account_card_component__WEBPACK_IMPORTED_MODULE_19__["BankAccountCardComponent"]
        ]
    })
], ComponentsModule);



/***/ }),

/***/ "Kkp9":
/*!***************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/close-modal-button/close-modal-button.component.scss ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilos de la linea que cierra el modal */\n.close-modal-line {\n  width: 30px;\n  height: 5px;\n  background-color: #D5DDE0;\n  border-radius: 50px;\n  margin-bottom: 0px;\n}\n/* Estilos del contenedor de la linea que cierra el modal */\n.container-close-modal {\n  padding-top: 5%;\n  padding-bottom: 0%;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2Nsb3NlLW1vZGFsLWJ1dHRvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSw0Q0FBQTtBQUNBO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFDRjtBQUVBLDJEQUFBO0FBQ0E7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtBQUNGIiwiZmlsZSI6ImNsb3NlLW1vZGFsLWJ1dHRvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIEVzdGlsb3MgZGUgbGEgbGluZWEgcXVlIGNpZXJyYSBlbCBtb2RhbCAqL1xuLmNsb3NlLW1vZGFsLWxpbmUge1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNENURERTAgO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG5cbi8qIEVzdGlsb3MgZGVsIGNvbnRlbmVkb3IgZGUgbGEgbGluZWEgcXVlIGNpZXJyYSBlbCBtb2RhbCAqL1xuLmNvbnRhaW5lci1jbG9zZS1tb2RhbCB7XG4gIHBhZGRpbmctdG9wOiA1JTsgXG4gIHBhZGRpbmctYm90dG9tOiAwJTtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4iXX0= */");

/***/ }),

/***/ "LW0V":
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/cards/bank-account-card/bank-account-card.component.html ***!
  \*********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-card class=\"cruds-cart\">\n  <ion-card-content>\n    <p><strong>Nombre del banco: </strong>{{ nameBank }}</p>\n    <p><strong>Numero de cuenta: </strong>{{ accountNumber }}</p>\n    <p><strong>Tipo de cuenta: </strong>{{ typeAccount }}</p>\n    <p><strong>Nombre: </strong>{{ nameOwner }}</p>\n    <p><strong>Correo: </strong>{{ email }}</p>\n  </ion-card-content>\n</ion-card>\n");

/***/ }),

/***/ "OYn4":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/travel-detail-card/travel-detail-card.component.html ***!
  \*****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-card>\n  <ion-row>\n    <ion-col size=\"8\" class=\"col-container-img\">\n      <div class=\"container-img\">\n        <img src=\"../../../../../assets/img/cartypes.png\" alt=\"\">\n        <p>{{vehicleType}}</p>\n      </div>\n    \n    </ion-col>\n    <ion-col size=\"4\" class=\"container-price\">\n      <h2>{{ wholePartPrice }}<sup>{{ decimalPartPrice }}</sup></h2>\n      <div>\n        <p>{{ duration }}</p>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-card>\n");

/***/ }),

/***/ "OvAv":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/stop-update/stop-update.component.ts ***!
  \***********************************************************************************/
/*! exports provided: StopUpdateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StopUpdateComponent", function() { return StopUpdateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_stop_update_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./stop-update.component.html */ "8Thi");
/* harmony import */ var _stop_update_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./stop-update.component.scss */ "8WUZ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/admin/stops.service */ "/+Y6");



/// <reference path="../../../../../../node_modules/@types/googlemaps/index.d.ts" />




let StopUpdateComponent = class StopUpdateComponent {
    constructor(stopsService, alertsService, modalController) {
        this.stopsService = stopsService;
        this.alertsService = alertsService;
        this.modalController = modalController;
        this.markers = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.GoogleAutocomplete = new google.maps.places.Autocomplete(document.getElementById('stop_address').getElementsByTagName('input')[0], {
            types: ['address'],
            componentRestrictions: { country: 'ec' },
            fields: [
                'address_components',
                'geometry',
                'icon',
                'name',
                'formatted_address',
            ],
        });
        this.stop = {
            uid: this.stopId,
            address: this.address,
            name: this.name,
            description: this.description,
            city: this.city,
            provinces: this.provinces,
            lat: this.lat,
            lng: this.lng,
            state: this.state,
            isActive: this.isActive,
        };
        console.log(this.lat);
    }
    ngAfterViewInit() {
        this.loadMap();
    }
    loadMap() {
        let latLng = new google.maps.LatLng(this.lat, this.lng);
        this.map = new google.maps.Map(this.divMap.nativeElement, {
            center: latLng,
            zoom: 16,
            //estilo del mapa (color gris)
            styles: [
                {
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#f5f5f5',
                        },
                    ],
                },
                {
                    elementType: 'labels.icon',
                    stylers: [
                        {
                            visibility: 'off',
                        },
                    ],
                },
                {
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#616161',
                        },
                    ],
                },
                {
                    elementType: 'labels.text.stroke',
                    stylers: [
                        {
                            color: '#f5f5f5',
                        },
                    ],
                },
                {
                    featureType: 'administrative.land_parcel',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#bdbdbd',
                        },
                    ],
                },
                {
                    featureType: 'poi',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#eeeeee',
                        },
                    ],
                },
                {
                    featureType: 'poi',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#757575',
                        },
                    ],
                },
                {
                    featureType: 'poi.park',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#e5e5e5',
                        },
                    ],
                },
                {
                    featureType: 'poi.park',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#9e9e9e',
                        },
                    ],
                },
                {
                    featureType: 'road',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#ffffff',
                        },
                    ],
                },
                {
                    featureType: 'road.arterial',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#757575',
                        },
                    ],
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#dadada',
                        },
                    ],
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#616161',
                        },
                    ],
                },
                {
                    featureType: 'road.local',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#9e9e9e',
                        },
                    ],
                },
                {
                    featureType: 'transit.line',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#e5e5e5',
                        },
                    ],
                },
                {
                    featureType: 'transit.station',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#eeeeee',
                        },
                    ],
                },
                {
                    featureType: 'water',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#c9c9c9',
                        },
                    ],
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#9e9e9e',
                        },
                    ],
                },
            ],
        });
        const icon = {
            url: '../../assets/img/stops.png',
            scaledSize: new google.maps.Size(100, 100),
        };
        const marketPosition = new google.maps.Marker({
            position: this.map.getCenter(),
            animation: google.maps.Animation.DROP,
            icon: icon,
        });
        let market = {
            position: {
                lat: this.lat,
                lng: this.lng,
            },
            title: 'Mi Ubicación',
        };
        this.addMarker(market);
        /*     marketPosition.setMap(this.map); */
    }
    //busqueda de datos por me dio de un input
    keyUpHandler() {
        if (this.stop.address.length > 0) {
            google.maps.event.addListener(this.GoogleAutocomplete, `place_changed`, () => {
                const place = this.GoogleAutocomplete.getPlace();
                let market = {
                    position: {
                        lat: place.geometry.location.lat(),
                        lng: place.geometry.location.lng(),
                    },
                    title: 'Mi Ubicación',
                };
                var centro = {
                    lat: place.geometry.location.lat(),
                    lng: place.geometry.location.lng(),
                };
                this.map.setCenter(centro);
                this.addMarker(market);
            });
        }
    }
    //añadir marcador
    addMarker(marker) {
        this.deleteMarkers();
        let marke;
        const meta = '../../assets/img/stops.png';
        const icon2 = {
            url: meta,
            /*     size: new google.maps.Size(100, 100),
              origin: new google.maps.Point(0, 0),
               anchor: new google.maps.Point(40, 60), */
            scaledSize: new google.maps.Size(100, 100),
        };
        marke = new google.maps.Marker({
            position: marker.position,
            draggable: true,
            animation: google.maps.Animation.DROP,
            map: this.map,
            icon: icon2,
            title: marker.title,
        });
        const geocoder = new google.maps.Geocoder();
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    //Info Windows de google
                    var content = results[0].formatted_address;
                    var infoWindow = new google.maps.InfoWindow({
                        content: content,
                    });
                    //   infoWindow.close(this.map, marke);
                    this.stop.lat = marke.getPosition().lat();
                    this.stop.lng = marke.getPosition().lng();
                    this.stop.address = results[0].formatted_address;
                    this.stop.city = results[4].formatted_address;
                    this.stop.provinces = results[7].formatted_address;
                }
                else {
                    window.alert('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
        google.maps.event.addListener(marke, 'dragend', () => {
            var currentInfoWindow = null;
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        //Info Windows de google
                        var content = results[0].formatted_address;
                        var infoWindow = new google.maps.InfoWindow({
                            content: content,
                        });
                        //   infoWindow.close(this.map, marke);
                        this.stop.lat = marke.getPosition().lat();
                        this.stop.lng = marke.getPosition().lng();
                        this.stop.address = results[0].formatted_address;
                        this.stop.city = results[4].formatted_address;
                        this.stop.provinces = results[7].formatted_address;
                    }
                    else {
                        window.alert('No results found');
                    }
                }
                else {
                    console.log('Geocoder failed due to: ' + status);
                }
            });
        });
        this.markers.push(marke);
    }
    update() {
        if (!this.thereAreEmptyFields(this.stop)) {
            this.stopsService.updateStop(this.stop);
            this.modalController.dismiss();
        }
        else {
            this.alertsService.presentAlert('Por favor complete todos los campos requeridos.');
        }
    }
    thereAreEmptyFields(stop) {
        if (stop.address == '' || stop.name == '' || stop.description == '')
            return true;
        else
            return false;
    }
    closeModal() {
        this.modalController.dismiss();
    }
    //funciones para eliminar las marcas de mapa de google
    //desde
    deleteMarkers() {
        this.clearMarkers();
        this.markers = [];
    }
    clearMarkers() {
        this.setMapOnAll(null);
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
};
StopUpdateComponent.ctorParameters = () => [
    { type: _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_6__["StopsService"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__["AlertsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
StopUpdateComponent.propDecorators = {
    stopId: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    address: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    name: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    description: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    city: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    provinces: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    lat: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    lng: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    state: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    isActive: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    divMap: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['divMap',] }],
    inputPlaces: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['inputPlaces',] }]
};
StopUpdateComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-stop-update',
        template: _raw_loader_stop_update_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_stop_update_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], StopUpdateComponent);



/***/ }),

/***/ "P/hC":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/travel-card/travel-card.component.html ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-card class=\"border\">\n  <div class=\"icons-container\">\n    <div class=\"point\"></div>\n    <div class=\"line\"></div>\n    <div class=\"icon\"></div>\n  </div>\n  <div class=\"items\">\n    <ion-item>\n      <ion-label>24, Oviedo y Sucre, Ibarra</ion-label>\n    </ion-item>\n    <ion-item>\n      <ion-label>24, Oviedo y Sucre, Ibarra</ion-label>\n    </ion-item>\n  </div>\n</ion-card>\n");

/***/ }),

/***/ "Pf0y":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/accept-button/accept-button.component.ts ***!
  \***************************************************************************************/
/*! exports provided: AcceptButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcceptButtonComponent", function() { return AcceptButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_accept_button_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./accept-button.component.html */ "8qac");
/* harmony import */ var _accept_button_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accept-button.component.scss */ "1Ijv");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let AcceptButtonComponent = class AcceptButtonComponent {
    constructor() { }
    ngOnInit() { }
};
AcceptButtonComponent.ctorParameters = () => [];
AcceptButtonComponent.propDecorators = {
    text: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
AcceptButtonComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-accept-button',
        template: _raw_loader_accept_button_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_accept_button_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AcceptButtonComponent);



/***/ }),

/***/ "QXcN":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/button/button.component.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-button color=\"accept-btn\" class=\"ion-text-capitalize\" (click)=\"pressButton.emit()\">{{ title }}</ion-button>\n");

/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu");
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component.scss */ "ynWL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./core/authentication/authentication.service */ "6CRC");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! firebase/app */ "Jgta");
/* harmony import */ var _modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modules/home/services/users.service */ "6JOU");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _shared_services_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./shared/services/shipping-parameters.service */ "jNID");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "Wwn5");











let AppComponent = class AppComponent {
    constructor(callNumber, shippingParametersService, menuController, authenticationService, usersService, router) {
        this.callNumber = callNumber;
        this.shippingParametersService = shippingParametersService;
        this.menuController = menuController;
        this.authenticationService = authenticationService;
        this.usersService = usersService;
        this.router = router;
        this.numbersEmergency = [];
    }
    ngOnInit() {
        this.getAuthenticatedUser();
        this.getParameters();
    }
    getParameters() {
        this.shippingParametersService.getCollection().subscribe((res) => {
            this.numbersEmergency = res;
        });
    }
    showHiddenMenu() {
        this.menuController.toggle();
    }
    getAuthenticatedUser() {
        firebase_app__WEBPACK_IMPORTED_MODULE_6__["default"].auth().onAuthStateChanged((u) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (u) {
                this.user = this.usersService.getUserById(u.uid);
            }
        }));
    }
    openLink(link) {
        window.open(link);
    }
    goToHome() {
        this.menuController.toggle();
        this.router.navigate(['dashboard']);
    }
    goToHistory() {
        this.menuController.toggle();
        this.router.navigate(['dashboard/history']);
    }
    onNavigate() {
        window.open("https://api.whatsapp.com/send/?phone=593" + this.numbersEmergency[0].numCall + "&text=" + this.numbersEmergency[0].messageCall + "&app_absent=0", "_blank");
    }
    clickCallNumber(number) {
        console.log(typeof number);
        this.callNumber.callNumber(number, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
        console.log(number);
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_10__["CallNumber"] },
    { type: _shared_services_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_9__["ShippingParametersService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: _core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"] },
    { type: _modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_7__["UsersService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] }
];
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AppComponent);



/***/ }),

/***/ "UkGf":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/request-button/request-button.component.scss ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card {\n  display: flex;\n  justify-content: space-between;\n  height: 100px;\n  box-shadow: 0px 2px 15px rgba(0, 0, 0, 0.15);\n  border-radius: 15px;\n  color: white;\n}\nion-card .title-container {\n  width: 50%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  padding-left: 10%;\n}\nion-card .title-container h1 {\n  font-weight: bold;\n  font-size: 19px;\n}\nion-card .icon-container {\n  width: 50%;\n  display: flex;\n  align-items: center;\n  padding-left: 10px;\n  display: relative;\n}\nion-card .icon-container ion-icon {\n  font-size: 50px;\n  position: absolute;\n  right: 10%;\n}\n.green {\n  --background: #008D36;\n}\n.blue {\n  --background: #214E9D;\n}\n.data-container {\n  background: #FFFFFF;\n  box-shadow: 0px 0px 30px rgba(255, 255, 255, 0.2);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3JlcXVlc3QtYnV0dG9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsYUFBQTtFQUNBLDRDQUFBO0VBQ0EsbUJBQUE7RUF3QkEsWUFBQTtBQXRCRDtBQURDO0VBQ0MsVUFBQTtFQUtBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUFERjtBQU5FO0VBQ0MsaUJBQUE7RUFDQSxlQUFBO0FBUUg7QUFEQztFQUNDLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FBR0Y7QUFGRTtFQUNDLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUFJSDtBQUVBO0VBQ0MscUJBQUE7QUFDRDtBQUVBO0VBQ0MscUJBQUE7QUFDRDtBQUVBO0VBQ0MsbUJBQUE7RUFDQSxpREFBQTtBQUNEIiwiZmlsZSI6InJlcXVlc3QtYnV0dG9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNhcmQge1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG5cdGhlaWdodDogMTAwcHg7XG5cdGJveC1zaGFkb3c6IDBweCAycHggMTVweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xuXHRib3JkZXItcmFkaXVzOiAxNXB4O1xuXHQudGl0bGUtY29udGFpbmVyIHtcblx0XHR3aWR0aDogNTAlO1xuXHRcdGgxIHtcblx0XHRcdGZvbnQtd2VpZ2h0OiBib2xkO1xuXHRcdFx0Zm9udC1zaXplOiAxOXB4O1xuXHRcdH1cblx0XHRkaXNwbGF5OiBmbGV4O1xuXHRcdGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cdFx0cGFkZGluZy1sZWZ0OiAxMCU7XG5cdH1cblx0Lmljb24tY29udGFpbmVyIHtcblx0XHR3aWR0aDogNTAlO1xuXHRcdGRpc3BsYXk6IGZsZXg7XG5cdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcblx0XHRwYWRkaW5nLWxlZnQ6IDEwcHg7XG5cdFx0ZGlzcGxheTogcmVsYXRpdmU7XG5cdFx0aW9uLWljb24ge1xuXHRcdFx0Zm9udC1zaXplOiA1MHB4O1xuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xuXHRcdFx0cmlnaHQ6IDEwJTtcblx0XHR9XG5cdH1cblx0Y29sb3I6IHdoaXRlO1xufVxuXG4uZ3JlZW4ge1xuXHQtLWJhY2tncm91bmQ6ICMwMDhEMzY7XG59XG5cbi5ibHVlIHtcblx0LS1iYWNrZ3JvdW5kOiAjMjE0RTlEO1xufVxuXG4uZGF0YS1jb250YWluZXIge1xuXHRiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuXHRib3gtc2hhZG93OiAwcHggMHB4IDMwcHggcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpO1xufVxuIl19 */");

/***/ }),

/***/ "UtKx":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/address-input/address-input.component.scss ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-item {\n  --background: #F2F2F2;\n  box-shadow: inset 0px 4px 15px rgba(0, 0, 0, 0.14);\n  border-radius: 8px;\n}\n\nion-icon {\n  color: #008D36;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2FkZHJlc3MtaW5wdXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxxQkFBQTtFQUNBLGtEQUFBO0VBQ0Esa0JBQUE7QUFDRDs7QUFFQTtFQUNDLGNBQUE7QUFDRCIsImZpbGUiOiJhZGRyZXNzLWlucHV0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWl0ZW0ge1xuXHQtLWJhY2tncm91bmQ6ICNGMkYyRjI7XG5cdGJveC1zaGFkb3c6IGluc2V0IDBweCA0cHggMTVweCByZ2JhKDAsIDAsIDAsIDAuMTQpO1xuXHRib3JkZXItcmFkaXVzOiA4cHg7XG59XG5cbmlvbi1pY29uIHtcblx0Y29sb3I6ICMwMDhEMzY7XG59XG4iXX0= */");

/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\n  <ion-menu type=\"overlay\" side=\"start\" autoHide=\"true\"  menuId=\"principal\" content-id=\"main\">\n    <ion-header>\n      <ion-toolbar>\n        <ion-row class=\"container-img margin-left\">\n          <div *ngIf=\"(user | async)?.imagen == ''\" class=\"img\" style=\"background-image: url('../assets/img/user.png')\">\n          </div>\n          <div *ngIf=\"(user | async)?.imagen != ''\" class=\"img\" style=\"background-image: url({{ (user | async)?.imagen }})\">\n          </div>\n          <ion-icon routerLink=\"dashboard/profile\" (click)=\"showHiddenMenu()\" name=\"pencil-outline\"></ion-icon>\n        </ion-row>\n        <ion-row class=\"margin-left\">\n          <p>{{ (user | async)?.name }}</p>\n        </ion-row>\n        <ion-row class=\"margin-left\">\n          <p>{{ (user | async)?.email }}</p>\n        </ion-row>\n      </ion-toolbar>\n    </ion-header>\n\n    <ion-content>\n     <div class=\"container\">\n      <ion-list class=\"margin-left\">\n  \n        <ion-item lines=\"none black\" (click)=\"goToHome()\">Home</ion-item>\n        <ion-item lines=\"none black\" >Mi Agenda</ion-item>\n        <ion-item lines=\"none black\" (click)=\"goToHistory()\" >Historial</ion-item>\n        <ion-item lines=\"none black\" (click)=\"openLink('https://nousclic.com/')\">\n          <img src=\"../assets/img/logo_nous.png\" alt=\"\">\n        </ion-item>\n\n        <ion-item class=\"contac\" lines=\"none black\" (click)=\"onNavigate()\">\n          <img src=\"../assets/img/ico-whatsapp.png\" alt=\"\">\n        </ion-item>\n        <ion-item class=\"contac\" lines=\"none black\" (click)='clickCallNumber(\"+593\"+numbersEmergency[0].numCall)'>\n          <img src=\"../assets/img/call.png\" alt=\"\">\n        </ion-item>\n        \n        <ion-item class=\"exit green\" routerLink=\"/logout\" lines=\"none\" (click)=\"showHiddenMenu()\">Salir</ion-item>\n      </ion-list>\n     \n     </div>\n    </ion-content>\n\n  </ion-menu>\n  <ion-router-outlet id=\"main\"></ion-router-outlet>\n</ion-app>\n");

/***/ }),

/***/ "X0HJ":
/*!***************************************************!*\
  !*** ./src/app/shared/services/alerts.service.ts ***!
  \***************************************************/
/*! exports provided: AlertsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertsService", function() { return AlertsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let AlertsService = class AlertsService {
    constructor(loadingController, alertController, modalController) {
        this.loadingController = loadingController;
        this.alertController = alertController;
        this.modalController = modalController;
    }
    presentLoading(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loadingController.create({
                message
            });
            yield this.loading.present();
        });
    }
    dismissLoading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading.dismiss();
        });
    }
    presentAlert(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message,
                buttons: [{
                        text: 'Aceptar',
                        role: 'cancel'
                    }]
            });
            yield alert.present();
        });
    }
    presentAlertConfirm(header, message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return new Promise((resolve) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const alert = yield this.alertController.create({
                    cssClass: 'my-custom-class',
                    header: header,
                    message: message,
                    buttons: [
                        {
                            text: 'Cancelar',
                            role: 'cancel',
                            cssClass: 'secondary',
                            handler: (blah) => {
                                return resolve(false);
                            },
                        },
                        {
                            text: 'Aceptar',
                            handler: () => {
                                return resolve(true);
                            },
                        },
                    ],
                });
                yield alert.present();
            }));
        });
    }
    presentAlertConfirmAccept(header, message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return new Promise((resolve) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const alert = yield this.alertController.create({
                    cssClass: 'my-custom-class',
                    header: header,
                    message: message,
                    buttons: [
                        {
                            text: 'Aceptar',
                            handler: () => {
                                return resolve(true);
                            },
                        },
                    ],
                });
                yield alert.present();
            }));
        });
    }
    presentAlertOk(header, message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return new Promise((resolve) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const alert = yield this.alertController.create({
                    cssClass: 'my-custom-class',
                    header: header,
                    message: message,
                    buttons: [
                        {
                            text: 'Aceptar',
                            handler: () => {
                                return resolve(true);
                            },
                        },
                    ],
                });
                yield alert.present();
            }));
        });
    }
    presentModal(modalPageName, data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: modalPageName,
                componentProps: data,
            });
            yield modal.present();
        });
    }
    presentModalWithData(modalPageName, data, modalClass) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: modalPageName,
                componentProps: data,
                cssClass: modalClass
            });
            yield modal.present();
        });
    }
    presentModalWithoutTravelData(modalPageName, modalClass) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: modalPageName,
                cssClass: modalClass,
                showBackdrop: true
            });
            yield modal.present();
        });
    }
    closeModal() {
        this.modalController.dismiss();
    }
    presentAlertWithHeader(header, message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header,
                message,
                buttons: [
                    {
                        text: 'Aceptar',
                        role: 'cancel',
                    },
                ],
            });
            yield alert.present();
        });
    }
};
AlertsService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
AlertsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AlertsService);



/***/ }),

/***/ "Y4dy":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/request-card/request-card.component.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-card>\n  <div class=\"container\">\n    <img [src]=\"imagen\">\n    <div class=\"data-card\">\n      <h2 class=\"title\">{{ title }}</h2> \n      <p class=\"subtitle\">DESDE {{ date }}</p>\n      <div class=\"stars\">\n        <ion-icon name=\"star\"></ion-icon>\n        <ion-icon name=\"star\"></ion-icon>\n        <ion-icon name=\"star\"></ion-icon>\n        <ion-icon name=\"star\"></ion-icon>\n        <ion-icon name=\"star\"></ion-icon>\n      </div>\n    </div> \n    <div class=\"price\">\n      <p class=\"dollars\">{{ dollars }}<p>\n      <p class=\"cents\">{{ cents }}<p>\n    </div>\n  </div>\n  <div class=\"details\">\n    <div class=\"description\">\n      <p class=\"text address ion-text-uppercase\">{{ address }}</p>\n      <p class=\"text date\">{{ schedulingDate }}</p>\n      <p class=\"text stop ion-text-uppercase\">{{ stop }}</p>\n      <p class=\"space-card\"></p>\n    </div>\n    <div class=\"button-container\">\n      <ion-button *ngIf=\"status === 'pending'\" class=\"button ion-text-capitalize\" (click)=\"accept.emit()\">Aceptar</ion-button>\n      <ion-button *ngIf=\"status === 'waiting_payment' || status === 'reject'\" class=\"accepted-button ion-text-capitalize\" (click)=\"toTravel.emit()\">Esperando pago</ion-button>\n      <ion-button *ngIf=\"status === 'paid'\" class=\"waiting-driver-button ion-text-capitalize\" (click)=\"toTravel.emit()\">Ir al viaje</ion-button>\n    </div>\n  </div>\n</ion-card>\n");

/***/ }),

/***/ "YLGK":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/travel-card/travel-card.component.ts ***!
  \***********************************************************************************/
/*! exports provided: TravelCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelCardComponent", function() { return TravelCardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_travel_card_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./travel-card.component.html */ "P/hC");
/* harmony import */ var _travel_card_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./travel-card.component.scss */ "5Xz3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let TravelCardComponent = class TravelCardComponent {
    constructor() { }
    ngOnInit() { }
};
TravelCardComponent.ctorParameters = () => [];
TravelCardComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-travel-card',
        template: _raw_loader_travel_card_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_travel_card_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TravelCardComponent);



/***/ }),

/***/ "YrfR":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/bank-data/bank-data.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons class=\"margin-left\" slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-row>\n      <ion-title>Parámetros iGo</ion-title>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n<ion-content fullscreen>\n  <div class=\"margin\"> \n    <form [formGroup]=\"form\" (ngSubmit)=\"update()\">\n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">NOMBRE DEL BANCO</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"text\" formControlName=\"nameBank\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div\n        class=\"error\"\n        *ngIf=\"form.controls.nameBank.errors && form.controls.nameBank.touched\">\n        El campo nombre del banco es requerido.\n      </div>     \n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">NOMBRE DEL PROPIETARIO</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"text\" formControlName=\"nameOwner\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div class=\"error\" *ngIf=\"form.controls.nameOwner.errors && form.controls.nameOwner.touched\">\n        El campo nombre del propietario es requerido.\n      </div>\n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">NÚMERO DE CUENTA</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"number\" formControlName=\"accountNumber\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div\n        class=\"error\"\n        *ngIf=\"form.controls.accountNumber.errors && form.controls.accountNumber.touched\">\n        El campo número de cuenta es requerido.\n      </div>\n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">CORREO ELECTRÓNICO</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"text\" formControlName=\"email\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div class=\"error\" *ngIf=\"form.controls.email.errors && form.controls.email.touched\">\n        El campo correo electrónico es requerido.\n      </div>\n      <div class=\"container-select\">\n        <ion-label class=\"label\">TIPO DE CUENTA</ion-label>\n        <ion-item>\n          <ion-select\n            name=\"gender\"\n            formControlName=\"typeAccount\"\n            cancelText=\"Cancelar\"\n            name=\"role\"\n            value=\"client\"\n            okText=\"Aceptar\">\n            <ion-select-option value=\"Cuenta corriente\">Cuenta corriente</ion-select-option>\n            <ion-select-option value=\"Cuenta de ahorros\">Cuenta de ahorros</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </div>\n      <div\n        class=\"error\"\n        *ngIf=\"form.controls.typeAccount.errors && form.controls.typeAccount.touched\">\n        El campo tipo de cuenta es requerido.\n      </div> \n      <ion-row class=\"container-btn\">\n        <ion-button class=\"btn green\" (click)=\"update()\" [disabled]=\"!form.valid\"> Actualizar </ion-button>\n      </ion-row>\n    </form>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire */ "spgP");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/auth */ "UbJi");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/environments/environment */ "AytR");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! firebase */ "JZFu");
/* harmony import */ var _modules_dashboard_components_stop_update_stop_update_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./modules/dashboard/components/stop-update/stop-update.component */ "OvAv");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @agm/core */ "pxUr");
/* harmony import */ var src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! src/app/shared/services/storage.service */ "fbMX");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "Wwn5");
/* harmony import */ var _angular_service_worker__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/service-worker */ "Jho9");
/* harmony import */ var _modules_dashboard_components_components_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./modules/dashboard/components/components.module */ "KQTD");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "wljF");



















// Libreria para enviar datos por  el metodo POST

//one signal para  recibir notificaciones

firebase__WEBPACK_IMPORTED_MODULE_11__["default"].initializeApp(src_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].firebaseConfig);
let AppModule = class AppModule {
};
AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"], _modules_dashboard_components_stop_update_stop_update_component__WEBPACK_IMPORTED_MODULE_12__["StopUpdateComponent"]],
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _modules_dashboard_components_components_module__WEBPACK_IMPORTED_MODULE_18__["ComponentsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClientModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
            _angular_fire__WEBPACK_IMPORTED_MODULE_7__["AngularFireModule"].initializeApp(src_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].firebaseConfig),
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__["AngularFireAuthModule"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_9__["AngularFirestoreModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormsModule"],
            _agm_core__WEBPACK_IMPORTED_MODULE_14__["AgmCoreModule"].forRoot({ apiKey: 'AIzaSyDgC1m_Lj7v02-Q0ZPf9W_sy3EisEysXl0' }),
            _angular_service_worker__WEBPACK_IMPORTED_MODULE_17__["ServiceWorkerModule"].register('ngsw-worker.js', {
                enabled: src_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].production,
                // Register the ServiceWorker as soon as the app is stable
                // or after 30 seconds (whichever comes first).
                registrationStrategy: 'registerWhenStable:30000'
            })
        ],
        providers: [_ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_16__["CallNumber"], { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] }, src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_15__["StorageService"], _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_20__["OneSignal"]],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
    })
], AppModule);



/***/ }),

/***/ "cXOV":
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/principal-header/principal-header.component.html ***!
  \*************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-padding-start ion-no-border\" translucent=\"true\">\n  <ion-toolbar>\n    <ion-row>\n      <div class=\"menu-icon-container\" (click)=\"openMenu.emit()\">\n        <img src=\"../../../../../assets/img/menu.png\">\n      </div>\n      <div class=\"title-container\">\n        <ion-title>{{ title }}</ion-title>\n      </div>\n    </ion-row>    \n  </ion-toolbar>  \n</ion-header>\n");

/***/ }),

/***/ "ekox":
/*!*******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/vehicle-type-button/vehicle-type-button.component.html ***!
  \*******************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-card (click)=\"redirectTo.emit()\">\n  <div class=\"container\">\n    <img src=\"/assets/img/cartypes.png\">\n    <p>{{ title }}</p>\n  </div>\n</ion-card>\n");

/***/ }),

/***/ "f3p9":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/payment-type/payment-type.component.ts ***!
  \*************************************************************************************/
/*! exports provided: PaymentTypeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentTypeComponent", function() { return PaymentTypeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_payment_type_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./payment-type.component.html */ "+sSY");
/* harmony import */ var _payment_type_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./payment-type.component.scss */ "vrQx");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let PaymentTypeComponent = class PaymentTypeComponent {
    constructor() { }
    ngOnInit() { }
};
PaymentTypeComponent.ctorParameters = () => [];
PaymentTypeComponent.propDecorators = {
    text: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    icon: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
PaymentTypeComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-payment-type',
        template: _raw_loader_payment_type_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_payment_type_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], PaymentTypeComponent);



/***/ }),

/***/ "fbMX":
/*!****************************************************!*\
  !*** ./src/app/shared/services/storage.service.ts ***!
  \****************************************************/
/*! exports provided: StorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageService", function() { return StorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ "gcOT");



const { Storage } = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"];
let StorageService = class StorageService {
    constructor(storage) {
        this.storage = storage;
        this._storage = null;
        this.init();
    }
    init() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const storage = yield this.storage.create();
            this._storage = storage;
        });
    }
    set(key, value) {
        var _a;
        (_a = this._storage) === null || _a === void 0 ? void 0 : _a.set(key, value);
    }
    get(key) {
        var _a;
        return (_a = this._storage) === null || _a === void 0 ? void 0 : _a.get(key);
    }
};
StorageService.ctorParameters = () => [
    { type: Storage }
];
StorageService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], StorageService);



/***/ }),

/***/ "ghWw":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/vehicle-type-button/vehicle-type-button.component.scss ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card {\n  height: 163.63px;\n  width: 100%;\n  display: flex;\n  align-items: center;\n  background: #FFFFFF;\n  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.15);\n  margin-left: 0px;\n  margin-top: 0px;\n}\n\np {\n  text-align: center;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  font-style: normal;\n  font-size: 15px;\n  font-weight: normal;\n  line-height: 136%;\n  color: #3E4958;\n  position: relative;\n  top: -35px;\n}\n\n.container {\n  position: relative;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3ZlaGljbGUtdHlwZS1idXR0b24uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxnQkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLDRDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQ0Q7O0FBRUE7RUFDQyxrQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBQ0Q7O0FBRUE7RUFDQyxrQkFBQTtBQUNEIiwiZmlsZSI6InZlaGljbGUtdHlwZS1idXR0b24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCB7XG5cdGhlaWdodDogMTYzLjYzcHg7XG5cdHdpZHRoOiAxMDAlO1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRhbGlnbi1pdGVtczogY2VudGVyO1xuXHRiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuXHRib3gtc2hhZG93OiAwcHggNHB4IDE1cHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcblx0bWFyZ2luLWxlZnQ6IDBweDtcblx0bWFyZ2luLXRvcDogMHB4O1xufVxuXG5wIHtcblx0dGV4dC1hbGlnbjogY2VudGVyO1xuXHRtYXJnaW4tdG9wOiAwcHg7XG5cdG1hcmdpbi1ib3R0b206IDBweDtcblx0Zm9udC1zdHlsZTogbm9ybWFsO1xuXHRmb250LXNpemU6IDE1cHg7XG5cdGZvbnQtd2VpZ2h0OiBub3JtYWw7XG5cdGxpbmUtaGVpZ2h0OiAxMzYlO1xuXHRjb2xvcjogIzNFNDk1ODtcblx0cG9zaXRpb246IHJlbGF0aXZlO1xuXHR0b3A6IC0zNXB4O1xufVxuXG4uY29udGFpbmVyIHtcblx0cG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4iXX0= */");

/***/ }),

/***/ "j82e":
/*!*************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/header/header.component.ts ***!
  \*************************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./header.component.html */ "3Mby");
/* harmony import */ var _header_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./header.component.scss */ "ltzD");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let HeaderComponent = class HeaderComponent {
    constructor() {
        this.openMenu = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
    }
    ngOnInit() { }
};
HeaderComponent.ctorParameters = () => [];
HeaderComponent.propDecorators = {
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    openMenu: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"] }]
};
HeaderComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-header',
        template: _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_header_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], HeaderComponent);



/***/ }),

/***/ "jNID":
/*!****************************************************************!*\
  !*** ./src/app/shared/services/shipping-parameters.service.ts ***!
  \****************************************************************/
/*! exports provided: ShippingParametersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingParametersService", function() { return ShippingParametersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");



let ShippingParametersService = class ShippingParametersService {
    constructor(afs) {
        this.afs = afs;
        this.parametersCollection = afs.collection('parameters');
    }
    getShippingParameters() {
        return new Promise((resolve, reject) => {
            this.shippingParameters = this.parametersCollection.doc('shippingParameters').valueChanges();
            resolve(this.shippingParameters);
        });
    }
    getparametes() {
        return this.afs.collection('parameters').doc('shippingParameters').valueChanges();
    }
    getCollection() {
        const collection = this.afs.collection('parameters');
        return collection.valueChanges();
    }
    update(shippingParameters) {
        return new Promise((resolve, reject) => {
            this.parametersCollection.doc('shippingParameters').update(shippingParameters);
            resolve();
        });
    }
};
ShippingParametersService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
ShippingParametersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], ShippingParametersService);



/***/ }),

/***/ "jZdo":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/request-card/request-card.component.ts ***!
  \*************************************************************************************/
/*! exports provided: RequestCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestCardComponent", function() { return RequestCardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_request_card_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./request-card.component.html */ "Y4dy");
/* harmony import */ var _request_card_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./request-card.component.scss */ "z+ob");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let RequestCardComponent = class RequestCardComponent {
    constructor() {
        this.accept = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
        this.toTravel = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
    }
    ngOnInit() { }
};
RequestCardComponent.ctorParameters = () => [];
RequestCardComponent.propDecorators = {
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    date: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    dollars: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    cents: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    address: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    schedulingDate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    stop: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    imagen: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    status: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    accept: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"] }],
    toTravel: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"] }]
};
RequestCardComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-request-card',
        template: _raw_loader_request_card_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_request_card_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RequestCardComponent);



/***/ }),

/***/ "kLfG":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet.entry.js": [
		"dUtr",
		"common",
		0
	],
	"./ion-alert.entry.js": [
		"Q8AI",
		"common",
		1
	],
	"./ion-app_8.entry.js": [
		"hgI1",
		"common",
		2
	],
	"./ion-avatar_3.entry.js": [
		"CfoV",
		"common",
		3
	],
	"./ion-back-button.entry.js": [
		"Nt02",
		"common",
		4
	],
	"./ion-backdrop.entry.js": [
		"Q2Bp",
		5
	],
	"./ion-button_2.entry.js": [
		"0Pbj",
		"common",
		6
	],
	"./ion-card_5.entry.js": [
		"ydQj",
		"common",
		7
	],
	"./ion-checkbox.entry.js": [
		"4fMi",
		"common",
		8
	],
	"./ion-chip.entry.js": [
		"czK9",
		"common",
		9
	],
	"./ion-col_3.entry.js": [
		"/CAe",
		10
	],
	"./ion-datetime_3.entry.js": [
		"WgF3",
		"common",
		11
	],
	"./ion-fab_3.entry.js": [
		"uQcF",
		"common",
		12
	],
	"./ion-img.entry.js": [
		"wHD8",
		13
	],
	"./ion-infinite-scroll_2.entry.js": [
		"2lz6",
		14
	],
	"./ion-input.entry.js": [
		"ercB",
		"common",
		15
	],
	"./ion-item-option_3.entry.js": [
		"MGMP",
		"common",
		16
	],
	"./ion-item_8.entry.js": [
		"9bur",
		"common",
		17
	],
	"./ion-loading.entry.js": [
		"cABk",
		"common",
		18
	],
	"./ion-menu_3.entry.js": [
		"kyFE",
		"common",
		19
	],
	"./ion-modal.entry.js": [
		"TvZU",
		"common",
		20
	],
	"./ion-nav_2.entry.js": [
		"vnES",
		"common",
		21
	],
	"./ion-popover.entry.js": [
		"qCuA",
		"common",
		22
	],
	"./ion-progress-bar.entry.js": [
		"0tOe",
		"common",
		23
	],
	"./ion-radio_2.entry.js": [
		"h11V",
		"common",
		24
	],
	"./ion-range.entry.js": [
		"XGij",
		"common",
		25
	],
	"./ion-refresher_2.entry.js": [
		"nYbb",
		"common",
		26
	],
	"./ion-reorder_2.entry.js": [
		"smMY",
		"common",
		27
	],
	"./ion-ripple-effect.entry.js": [
		"STjf",
		28
	],
	"./ion-route_4.entry.js": [
		"k5eQ",
		"common",
		29
	],
	"./ion-searchbar.entry.js": [
		"OR5t",
		"common",
		30
	],
	"./ion-segment_2.entry.js": [
		"fSgp",
		"common",
		31
	],
	"./ion-select_3.entry.js": [
		"lfGF",
		"common",
		32
	],
	"./ion-slide_2.entry.js": [
		"5xYT",
		33
	],
	"./ion-spinner.entry.js": [
		"nI0H",
		"common",
		34
	],
	"./ion-split-pane.entry.js": [
		"NAQR",
		35
	],
	"./ion-tab-bar_2.entry.js": [
		"knkW",
		"common",
		36
	],
	"./ion-tab_2.entry.js": [
		"TpdJ",
		"common",
		37
	],
	"./ion-text.entry.js": [
		"ISmu",
		"common",
		38
	],
	"./ion-textarea.entry.js": [
		"U7LX",
		"common",
		39
	],
	"./ion-toast.entry.js": [
		"L3sA",
		"common",
		40
	],
	"./ion-toggle.entry.js": [
		"IUOf",
		"common",
		41
	],
	"./ion-virtual-scroll.entry.js": [
		"8Mb5",
		42
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "kLfG";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "l2UL":
/*!***************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/button/button.component.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-button {\n  width: 100%;\n  height: 60px;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n  --border-radius: 15px;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 28px;\n  color: #FFFFFF;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2J1dHRvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaURBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUNEIiwiZmlsZSI6ImJ1dHRvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1idXR0b24ge1xuXHR3aWR0aDogMTAwJTtcblx0aGVpZ2h0OiA2MHB4O1xuXHQtLWJveC1zaGFkb3c6IDBweCAycHggOHB4IHJnYmEoMTYsIDEwNSwgMjI3LCAwLjMpO1xuXHQtLWJvcmRlci1yYWRpdXM6IDE1cHg7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xuXHRmb250LXNpemU6IDE4cHg7XG5cdGxpbmUtaGVpZ2h0OiAyOHB4O1xuXHRjb2xvcjogI0ZGRkZGRjtcbn1cbiJdfQ== */");

/***/ }),

/***/ "l3rL":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/cards/bank-account-card/bank-account-card.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: BankAccountCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankAccountCardComponent", function() { return BankAccountCardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_bank_account_card_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./bank-account-card.component.html */ "LW0V");
/* harmony import */ var _bank_account_card_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bank-account-card.component.scss */ "IQa3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _pages_bank_data_bank_data_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../pages/bank-data/bank-data.page */ "oaVf");







let BankAccountCardComponent = class BankAccountCardComponent {
    constructor(alertController, alertService) {
        this.alertController = alertController;
        this.alertService = alertService;
    }
    ngOnInit() { }
    presentAlertConfirm(categoryId) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: '¡Confirme!',
                message: '¿Esta seguro de que desea <strong>eliminar</strong> esta cuenta bancaría?',
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        cssClass: 'secondary',
                    },
                    {
                        text: 'Ok',
                        handler: () => {
                        },
                    },
                ],
            });
            yield alert.present();
        });
    }
    presentModal(accountNumber, email, nameBank, nameOwner, typeAccount) {
        this.alertService.presentModal(_pages_bank_data_bank_data_page__WEBPACK_IMPORTED_MODULE_6__["BankDataPage"], {
            accountNumber: accountNumber,
            email: email,
            nameBank: nameBank,
            nameOwner: nameOwner,
            typeAccount: typeAccount,
        });
    }
};
BankAccountCardComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__["AlertsService"] }
];
BankAccountCardComponent.propDecorators = {
    accountNumber: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    email: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    nameBank: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    nameOwner: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    typeAccount: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
BankAccountCardComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-bank-account-card',
        template: _raw_loader_bank_account_card_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_bank_account_card_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], BankAccountCardComponent);



/***/ }),

/***/ "lnVj":
/*!***************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/vehicle-type-button/vehicle-type-button.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: VehicleTypeButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicleTypeButtonComponent", function() { return VehicleTypeButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_vehicle_type_button_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./vehicle-type-button.component.html */ "ekox");
/* harmony import */ var _vehicle_type_button_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./vehicle-type-button.component.scss */ "ghWw");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let VehicleTypeButtonComponent = class VehicleTypeButtonComponent {
    constructor() {
        this.redirectTo = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
    }
    ngOnInit() { }
};
VehicleTypeButtonComponent.ctorParameters = () => [];
VehicleTypeButtonComponent.propDecorators = {
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    redirectTo: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"] }]
};
VehicleTypeButtonComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-vehicle-type-button',
        template: _raw_loader_vehicle_type_button_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_vehicle_type_button_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], VehicleTypeButtonComponent);



/***/ }),

/***/ "ltzD":
/*!***************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/header/header.component.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header ion-row {\n  align-items: center;\n}\nion-header ion-row .title-container {\n  display: flex;\n  justify-content: center;\n  width: 75%;\n}\nion-header ion-row .title-container ion-title {\n  font-size: 20px;\n  font-weight: bold;\n  --color: #3E4958;\n  text-align: center;\n}\nion-back-button {\n  margin-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2hlYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQztFQUNDLG1CQUFBO0FBQUY7QUFXRTtFQUNDLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUFUSDtBQVVHO0VBQ0MsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQVJKO0FBY0E7RUFDQyxnQkFBQTtBQVhEIiwiZmlsZSI6ImhlYWRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXIge1xuXHRpb24tcm93IHtcblx0XHRhbGlnbi1pdGVtczogY2VudGVyO1xuXHRcdC8vLm1lbnUtaWNvbi1jb250YWluZXIge1xuXHRcdFx0Ly9iYWNrZ3JvdW5kLWNvbG9yOiAjRkZGRkZGO1xuXHRcdFx0Ly9ib3gtc2hhZG93OiAwcHggNHB4IDE1cHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcblx0XHRcdC8vZGlzcGxheTogZmxleDtcblx0XHRcdC8vanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cdFx0XHQvL2FsaWduLWl0ZW1zOiBqdXN0aWZ5O1xuXHRcdFx0Ly9ib3JkZXItcmFkaXVzOiAxMDAlO1xuXHRcdFx0Ly93aWR0aDogNDBweDtcblx0XHRcdC8vaGVpZ2h0OiA0MHB4O1xuXHRcdC8vfVxuXHRcdC50aXRsZS1jb250YWluZXIge1xuXHRcdFx0ZGlzcGxheTogZmxleDtcblx0XHRcdGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXHRcdFx0d2lkdGg6IDc1JTtcblx0XHRcdGlvbi10aXRsZSB7XG5cdFx0XHRcdGZvbnQtc2l6ZTogMjBweDtcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG5cdFx0XHRcdC0tY29sb3I6ICMzRTQ5NTg7XG5cdFx0XHRcdHRleHQtYWxpZ246IGNlbnRlcjtcblx0XHRcdH1cblx0XHR9XG5cdH1cbn1cblxuaW9uLWJhY2stYnV0dG9uIHtcblx0bWFyZ2luLWxlZnQ6IDVweDtcbn1cbiJdfQ== */");

/***/ }),

/***/ "mGml":
/*!***********************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/principal-header/principal-header.component.scss ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header ion-row {\n  align-items: center;\n}\nion-header ion-row .menu-icon-container {\n  background-color: #FFFFFF;\n  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.15);\n  display: flex;\n  justify-content: center;\n  align-items: justify;\n  border-radius: 100%;\n  width: 40px;\n  height: 40px;\n}\nion-header ion-row .title-container {\n  display: flex;\n  justify-content: center;\n  width: 75%;\n}\nion-header ion-row .title-container ion-title {\n  font-size: 20px;\n  font-weight: bold;\n  --color: #3E4958;\n  text-align: center;\n}\nion-back-button {\n  margin-left: 5px;\n}\nion-toolbar {\n  --background: transparent;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3ByaW5jaXBhbC1oZWFkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0M7RUFDQyxtQkFBQTtBQUFGO0FBQ0U7RUFDQyx5QkFBQTtFQUNBLDRDQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBQ0g7QUFDRTtFQUNDLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUFDSDtBQUFHO0VBQ0MsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQUVKO0FBSUE7RUFDQyxnQkFBQTtBQUREO0FBSUE7RUFDQyx5QkFBQTtBQUREIiwiZmlsZSI6InByaW5jaXBhbC1oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVyIHtcblx0aW9uLXJvdyB7XG5cdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcblx0XHQubWVudS1pY29uLWNvbnRhaW5lciB7XG5cdFx0XHRiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGRkZGO1xuXHRcdFx0Ym94LXNoYWRvdzogMHB4IDRweCAxNXB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XG5cdFx0XHRkaXNwbGF5OiBmbGV4O1xuXHRcdFx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cdFx0XHRhbGlnbi1pdGVtczoganVzdGlmeTtcblx0XHRcdGJvcmRlci1yYWRpdXM6IDEwMCU7XG5cdFx0XHR3aWR0aDogNDBweDtcblx0XHRcdGhlaWdodDogNDBweDtcblx0XHR9XG5cdFx0LnRpdGxlLWNvbnRhaW5lciB7XG5cdFx0XHRkaXNwbGF5OiBmbGV4O1xuXHRcdFx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cdFx0XHR3aWR0aDogNzUlO1xuXHRcdFx0aW9uLXRpdGxlIHtcblx0XHRcdFx0Zm9udC1zaXplOiAyMHB4O1xuXHRcdFx0XHRmb250LXdlaWdodDogYm9sZDtcblx0XHRcdFx0LS1jb2xvcjogIzNFNDk1ODtcblx0XHRcdFx0dGV4dC1hbGlnbjogY2VudGVyO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxufVxuXG5pb24tYmFjay1idXR0b24ge1xuXHRtYXJnaW4tbGVmdDogNXB4O1xufVxuXG5pb24tdG9vbGJhciB7XG5cdC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG4iXX0= */");

/***/ }),

/***/ "nH6m":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/close-modal-button/close-modal-button.component.html ***!
  \*****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-row class=\"container-close-modal\" (click)=\"closeModal.emit()\">\n  <div class=\"close-modal-line\"> \n  </div>\n</ion-row>\n");

/***/ }),

/***/ "nrTL":
/*!******************************************************!*\
  !*** ./src/app/shared/services/bank-data.service.ts ***!
  \******************************************************/
/*! exports provided: BankParametersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankParametersService", function() { return BankParametersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");



let BankParametersService = class BankParametersService {
    constructor(afs) {
        this.afs = afs;
        this.parametersCollection = afs.collection('bank');
    }
    getShippingParameters() {
        return new Promise((resolve, reject) => {
            this.bankParameters = this.parametersCollection.doc('bankParameters').valueChanges();
            resolve(this.bankParameters);
        });
    }
    getparametes() {
        return this.afs.collection('bank').doc('bankParameters').valueChanges();
    }
    getCollection() {
        const collection = this.afs.collection('parameters');
        return collection.valueChanges();
    }
    update(bankParameters) {
        return new Promise((resolve, reject) => {
            this.parametersCollection.doc('bankParameters').update(bankParameters);
            resolve();
        });
    }
};
BankParametersService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
BankParametersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], BankParametersService);



/***/ }),

/***/ "nt4N":
/*!***************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/travel-detail-card/travel-detail-card.component.scss ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card {\n  border-radius: 15px;\n  padding: 10px;\n  margin: 24px 5px;\n}\nion-card .container-price {\n  display: flex;\n  justify-content: flex-end;\n  flex-direction: column;\n  text-align: right;\n  align-items: center;\n}\nion-card .container-price div {\n  background-color: #4F4F4F;\n  border-radius: 50px;\n  width: 72px;\n  height: 24px;\n  color: #ffff;\n  display: flex;\n  justify-content: center;\n}\nion-card .container-price div p {\n  margin: 3px 0;\n}\nion-card .col-container-img {\n  display: flex;\n  justify-content: flex-start;\n}\nion-card .col-container-img .container-img {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n  max-width: 70%;\n  border: 0;\n  margin: -12%;\n}\nion-card .col-container-img .container-img p {\n  margin-top: -30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3RyYXZlbC1kZXRhaWwtY2FyZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0FBQ0o7QUFBSTtFQUNJLGFBQUE7RUFDQSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQUVSO0FBRFE7RUFDSSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0FBR1o7QUFGWTtFQUNJLGFBQUE7QUFJaEI7QUFDSTtFQUNJLGFBQUE7RUFDQSwyQkFBQTtBQUNSO0FBQ1E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0FBQ1o7QUFBWTtFQUNJLGlCQUFBO0FBRWhCIiwiZmlsZSI6InRyYXZlbC1kZXRhaWwtY2FyZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJke1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBtYXJnaW46IDI0cHggNXB4O1xuICAgIC5jb250YWluZXItcHJpY2V7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBkaXZ7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiM0RjRGNEY7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICAgICAgICAgICAgd2lkdGg6IDcycHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDI0cHg7XG4gICAgICAgICAgICBjb2xvcjojZmZmZjtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgIHB7XG4gICAgICAgICAgICAgICAgbWFyZ2luOiAzcHggMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgIFxuICAgIH1cbiAgICAuY29sLWNvbnRhaW5lci1pbWd7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICAgIFxuICAgICAgICAuY29udGFpbmVyLWltZ3tcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgbWF4LXdpZHRoOiA3MCU7XG4gICAgICAgICAgICBib3JkZXI6IDA7XG4gICAgICAgICAgICBtYXJnaW46IC0xMiU7XG4gICAgICAgICAgICBwe1xuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IC0zMHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgXG4gICAgICAgIH1cbiAgICB9XG59XG5cbiJdfQ== */");

/***/ }),

/***/ "oaVf":
/*!*********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/bank-data/bank-data.page.ts ***!
  \*********************************************************************/
/*! exports provided: BankDataPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankDataPage", function() { return BankDataPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_bank_data_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./bank-data.page.html */ "YrfR");
/* harmony import */ var _bank_data_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bank-data.page.scss */ "u1yu");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_bank_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../shared/services/bank-data.service */ "nrTL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "TEn/");








let BankDataPage = class BankDataPage {
    constructor(bankParametersService, alertService, menuController) {
        this.bankParametersService = bankParametersService;
        this.alertService = alertService;
        this.menuController = menuController;
        this.observableList = [];
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"]({
            accountNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]),
            nameBank: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]),
            nameOwner: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]),
            typeAccount: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]),
        });
    }
    ngOnInit() {
        this.getParametrers();
    }
    getParametrers() {
        const observable = this.bankParametersService.getparametes().subscribe((res) => {
            this.getbankParameters = res;
            this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"]({
                accountNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.getbankParameters['accountNumber'], [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,]),
                email: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.getbankParameters['email'], [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]),
                nameBank: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.getbankParameters['nameBank'], [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,]),
                nameOwner: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.getbankParameters['nameOwner'], [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]),
                typeAccount: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.getbankParameters['typeAccount'], [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]),
            });
        });
    }
    update() {
        this.bankParameters = {
            accountNumber: this.form.controls.accountNumber.value,
            email: this.form.controls.email.value,
            nameBank: this.form.controls.nameBank.value,
            nameOwner: this.form.controls.nameOwner.value,
            typeAccount: this.form.controls.typeAccount.value
        };
        this.bankParametersService
            .update(this.bankParameters)
            .then(() => this.alertService.presentAlert('Parametros Actualizados ✔'))
            .catch((error) => console.log(error));
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
    openMenu() {
        this.menuController.open('principal');
    }
};
BankDataPage.ctorParameters = () => [
    { type: _shared_services_bank_data_service__WEBPACK_IMPORTED_MODULE_4__["BankParametersService"] },
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"] }
];
BankDataPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-bank-data',
        template: _raw_loader_bank_data_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_bank_data_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], BankDataPage);



/***/ }),

/***/ "oscc":
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/initial-modal-button/initial-modal-button.component.html ***!
  \*********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"initial-modal-container\" (click)=\"openModal.emit()\">\n  <div class=\"close-modal-line\"> \n  </div>\n</div>\n");

/***/ }),

/***/ "pQWY":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/item-map/item-map.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ItemMapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemMapComponent", function() { return ItemMapComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_item_map_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./item-map.component.html */ "vF5s");
/* harmony import */ var _item_map_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./item-map.component.scss */ "Dx6G");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let ItemMapComponent = class ItemMapComponent {
    constructor() {
        this.chooseStop = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
    }
    ngOnInit() { }
};
ItemMapComponent.ctorParameters = () => [];
ItemMapComponent.propDecorators = {
    classes: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    name: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    description: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    chooseStop: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"] }],
    city: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
ItemMapComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-item-map',
        template: _raw_loader_item_map_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_item_map_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ItemMapComponent);



/***/ }),

/***/ "r3mn":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/request-button/request-button.component.html ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-card [class]=\"backgroundColorCard\">\n  <div class=\"title-container\">\n    <h1>{{ title }}</h1>\n  </div>\n  <div class=\"icon-container\">\n    <ion-icon [name]=\"iconName\"></ion-icon>\n  </div>\n</ion-card>\n");

/***/ }),

/***/ "rQM5":
/*!*************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/button/button.component.ts ***!
  \*************************************************************************/
/*! exports provided: ButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonComponent", function() { return ButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_button_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./button.component.html */ "QXcN");
/* harmony import */ var _button_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./button.component.scss */ "l2UL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let ButtonComponent = class ButtonComponent {
    constructor() {
        this.pressButton = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
    }
    ngOnInit() { }
};
ButtonComponent.ctorParameters = () => [];
ButtonComponent.propDecorators = {
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    pressButton: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"] }]
};
ButtonComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-button',
        template: _raw_loader_button_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_button_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ButtonComponent);



/***/ }),

/***/ "u1yu":
/*!***********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/bank-data/bank-data.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  display: flex;\n  justify-content: center;\n}\n.container .container-inputs {\n  margin-bottom: 3%;\n  padding: 10px;\n  width: 90%;\n  text-align: left;\n}\n.container .container-inputs ion-select {\n  width: 100%;\n  justify-content: center;\n}\n.container .container-inputs .label {\n  color: #3E4958;\n  font-weight: bold;\n  padding-left: 10px;\n  font-size: 18px;\n  line-height: 20px;\n}\n.container .container-inputs .input-ionItem {\n  --background: #F7F8F9;\n  --highlight-color-focused: #008D36;\n  --highlight-color-valid: #008D36;\n  border: 0.5px solid #D5DDE0;\n  border-radius: 15px;\n  margin-top: 3%;\n}\n/* Estilo del boton */\n.container-btn {\n  display: flex;\n  justify-content: center;\n}\n.error {\n  color: tomato;\n  font-size: 15px;\n  margin-left: 16px;\n}\n.margin {\n  margin-top: 5%;\n}\n.btn {\n  margin-top: 10%;\n  width: 78%;\n  height: 60px;\n}\n.green {\n  --background: #008D36;\n  --background-activated:#008D36;\n  color: white;\n  font-weight: bold;\n}\n/* Estilos de los select */\n.container-select {\n  margin-bottom: 3%;\n  padding: 10px;\n  width: 90%;\n  text-align: left;\n  margin-left: 5%;\n}\n.container-select ion-label {\n  color: #3E4958;\n  font-weight: bold;\n  padding-left: 10px;\n  font-size: 18px;\n  line-height: 20px;\n}\n.container-select ion-item {\n  --background: #F7F8F9;\n  --highlight-color-focused: #008D36;\n  --highlight-color-valid: #008D36;\n  border: 0.5px solid #D5DDE0;\n  border-radius: 15px;\n  margin-top: 3%;\n}\n.container-select ion-item ion-select {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2JhbmstZGF0YS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUFBSjtBQUVJO0VBQ0UsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0FBQU47QUFFTTtFQUNFLFdBQUE7RUFDQSx1QkFBQTtBQUFSO0FBSUk7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUZOO0FBS007RUFDRSxxQkFBQTtFQUNBLGtDQUFBO0VBQ0EsZ0NBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQUhSO0FBU0kscUJBQUE7QUFDRjtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBQU5OO0FBUUk7RUFDRSxhQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBTE47QUFPRTtFQUNJLGNBQUE7QUFKTjtBQU9FO0VBQ0ksZUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0FBSk47QUFNRTtFQUNJLHFCQUFBO0VBQ0EsOEJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUFITjtBQUtFLDBCQUFBO0FBQ0Y7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBRko7QUFHRTtFQUNJLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBRE47QUFJRTtFQUNJLHFCQUFBO0VBQ0Esa0NBQUE7RUFDQSxnQ0FBQTtFQUNBLDJCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FBRk47QUFJSTtFQUNFLFdBQUE7QUFGTiIsImZpbGUiOiJiYW5rLWRhdGEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4uY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBcbiAgICAuY29udGFpbmVyLWlucHV0cyB7XG4gICAgICBtYXJnaW4tYm90dG9tOiAzJTtcbiAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgICB3aWR0aDogOTAlO1xuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBcbiAgICAgIGlvbi1zZWxlY3Qge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICB9XG4gICAgXG4gICAgICBcbiAgICAubGFiZWwge1xuICAgICAgY29sb3I6ICMzRTQ5NTg7XG4gICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgIH1cbiAgXG4gICAgICAuaW5wdXQtaW9uSXRlbSB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogI0Y3RjhGOTtcbiAgICAgICAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDogIzAwOEQzNjtcbiAgICAgICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6ICMwMDhEMzY7XG4gICAgICAgIGJvcmRlcjogMC41cHggc29saWQgI0Q1RERFMDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMyU7XG4gICAgICB9XG4gICAgICBcbiAgICB9XG4gIH1cbiAgICBcbiAgICAvKiBFc3RpbG8gZGVsIGJvdG9uICovXG4gIC5jb250YWluZXItYnRuIHtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICB9XG4gICAgLmVycm9yIHtcbiAgICAgIGNvbG9yOiB0b21hdG87XG4gICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgICBtYXJnaW4tbGVmdDogMTZweDtcbiAgfVxuICAubWFyZ2lue1xuICAgICAgbWFyZ2luLXRvcDogNSU7XG4gIH1cbiAgXG4gIC5idG4ge1xuICAgICAgbWFyZ2luLXRvcDogMTAlO1xuICAgICAgd2lkdGg6IDc4JTtcbiAgICAgIGhlaWdodDogNjBweDtcbiAgICB9XG4gIC5ncmVlbiB7XG4gICAgICAtLWJhY2tncm91bmQ6ICMwMDhEMzY7XG4gICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiMwMDhEMzY7XG4gICAgICBjb2xvcjp3aGl0ZTtcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB9XG4gIC8qIEVzdGlsb3MgZGUgbG9zIHNlbGVjdCAqL1xuLmNvbnRhaW5lci1zZWxlY3Qge1xuICAgIG1hcmdpbi1ib3R0b206IDMlO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgd2lkdGg6IDkwJTtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgaW9uLWxhYmVsIHtcbiAgICAgIGNvbG9yOiAjM0U0OTU4O1xuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgfVxuXG4gIGlvbi1pdGVtIHtcbiAgICAgIC0tYmFja2dyb3VuZDogI0Y3RjhGOTtcbiAgICAgIC0taGlnaGxpZ2h0LWNvbG9yLWZvY3VzZWQ6ICMwMDhEMzY7XG4gICAgICAtLWhpZ2hsaWdodC1jb2xvci12YWxpZDogIzAwOEQzNjtcbiAgICAgIGJvcmRlcjogMC41cHggc29saWQgI0Q1RERFMDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgICBtYXJnaW4tdG9wOiAzJTtcblxuICAgIGlvbi1zZWxlY3Qge1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICB9XG59Il19 */");

/***/ }),

/***/ "vF5s":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/components/item-map/item-map.component.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-item (click)=\"chooseStop.emit()\">\n  <ion-thumbnail slot=\"start\">\n    <div [class]=\"classes\">\n      <ion-icon name=\"location-sharp\"></ion-icon>\n    </div>\n  </ion-thumbnail>\n  <ion-label>\n    <h2 class=\"stop\">{{ name }}</h2>\n    <h2 class=\"stop\">{{ city }}</h2>\n    <p class=\"city ion-text-justify ion-text-wrap \">{{ description }}</p>\n  </ion-label>\n</ion-item>\n");

/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _app_core_guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app/core/guard/auth.guard */ "3IAN");
/* harmony import */ var _angular_fire_auth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/auth-guard */ "HTFn");





// Enviar usuarios no autorizados para iniciar sesión
const redirectUnauthorizedToLogin = () => Object(_angular_fire_auth_guard__WEBPACK_IMPORTED_MODULE_4__["redirectUnauthorizedTo"])(['/home/login']);
// Iniciar sesión automáticamente en usuarios
const redirectLoggedInToHome = () => Object(_angular_fire_auth_guard__WEBPACK_IMPORTED_MODULE_4__["redirectLoggedInTo"])(['/dashboard']);
const routes = [
    Object.assign({ path: 'home', loadChildren: () => __webpack_require__.e(/*! import() | modules-home-home-module */ "modules-home-home-module").then(__webpack_require__.bind(null, /*! ./modules/home/home.module */ "iydT")).then(m => m.HomeModule) }, Object(_angular_fire_auth_guard__WEBPACK_IMPORTED_MODULE_4__["canActivate"])(redirectLoggedInToHome)),
    {
        path: 'dashboard',
        loadChildren: () => __webpack_require__.e(/*! import() | modules-dashboard-dashboard-module */ "modules-dashboard-dashboard-module").then(__webpack_require__.bind(null, /*! ./modules/dashboard/dashboard.module */ "XoyV")).then(m => m.DashboardModule),
        canActivate: [_app_core_guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]],
    },
    {
        path: 'logout',
        loadChildren: () => __webpack_require__.e(/*! import() | modules-home-pages-logout-logout-module */ "modules-home-pages-logout-logout-module").then(__webpack_require__.bind(null, /*! ./modules/home/pages/logout/logout.module */ "UY9W")).then(m => m.LogoutPageModule)
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'test',
        loadChildren: () => __webpack_require__.e(/*! import() | test-test-module */ "test-test-module").then(__webpack_require__.bind(null, /*! ./test/test.module */ "FScs")).then(m => m.TestPageModule)
    },
    {
        path: 'new-stop',
        loadChildren: () => __webpack_require__.e(/*! import() | modules-dashboard-pages-new-stop-new-stop-module */ "modules-dashboard-pages-new-stop-new-stop-module").then(__webpack_require__.bind(null, /*! ./modules/dashboard/pages/new-stop/new-stop.module */ "wepa")).then(m => m.NewStopPageModule)
    },
    {
        path: 'test',
        loadChildren: () => __webpack_require__.e(/*! import() | test-test-module */ "test-test-module").then(__webpack_require__.bind(null, /*! ./test/test.module */ "FScs")).then(m => m.TestPageModule)
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "vrQx":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/payment-type/payment-type.component.scss ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card {\n  border-radius: 15px;\n  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.08);\n  margin: 0px 0px 5% 0px;\n  color: #3E4958;\n}\nion-card ion-row {\n  justify-content: center;\n  align-items: center;\n}\nion-card ion-row .container-icon {\n  display: flex;\n  justify-content: center;\n}\nion-card ion-row .container-icon ion-icon {\n  font-size: 25px;\n}\nion-card ion-row .container-icon .arrow {\n  color: #97ADB6;\n}\nion-card .oval {\n  width: 70%;\n  height: 20px;\n  background-color: #C4C4C4;\n  border-radius: 10px;\n}\n.select {\n  background: #214E9D;\n  color: white;\n}\n.select ion-icon {\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3BheW1lbnQtdHlwZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0VBQ0EsNENBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7QUFDSjtBQUFJO0VBQ0ksdUJBQUE7RUFDQSxtQkFBQTtBQUVSO0FBRFE7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUFHWjtBQUZZO0VBQ0ksZUFBQTtBQUloQjtBQUZZO0VBQ0ksY0FBQTtBQUloQjtBQUFJO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FBRVI7QUFFQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtBQUNKO0FBQUk7RUFDSSxZQUFBO0FBRVIiLCJmaWxlIjoicGF5bWVudC10eXBlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNhcmR7XG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDE1cHggcmdiYSgwLCAwLCAwLCAwLjA4KTtcbiAgICBtYXJnaW46IDBweCAwcHggNSUgMHB4O1xuICAgIGNvbG9yOiMzRTQ5NTg7XG4gICAgaW9uLXJvd3tcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIC5jb250YWluZXItaWNvbntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgIGlvbi1pY29ue1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5hcnJvd3tcbiAgICAgICAgICAgICAgICBjb2xvcjogIzk3QURCNjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbiAgICAub3ZhbHtcbiAgICAgICAgd2lkdGg6IDcwJTtcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiNDNEM0QzQ7IFxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIH1cbn1cblxuLnNlbGVjdHtcbiAgICBiYWNrZ3JvdW5kOiAjMjE0RTlEO1xuICAgIGNvbG9yOndoaXRlO1xuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxufSJdfQ== */");

/***/ }),

/***/ "wJ+g":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/request-button/request-button.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: RequestButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestButtonComponent", function() { return RequestButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_request_button_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./request-button.component.html */ "r3mn");
/* harmony import */ var _request_button_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./request-button.component.scss */ "UkGf");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let RequestButtonComponent = class RequestButtonComponent {
    constructor() { }
    ngOnInit() { }
};
RequestButtonComponent.ctorParameters = () => [];
RequestButtonComponent.propDecorators = {
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    iconName: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    backgroundColorCard: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
RequestButtonComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-request-button',
        template: _raw_loader_request_button_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_request_button_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RequestButtonComponent);



/***/ }),

/***/ "ynWL":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  height: 200px;\n  --background: #008D36;\n  --color: white;\n}\nion-toolbar ion-row p {\n  margin: 5px 0px;\n}\n/* Contenedor de imagen del usuario */\n.container-img {\n  margin-top: 14%;\n}\n.container-img ion-icon {\n  font-size: 25px;\n}\n.container-img .img {\n  width: 80px;\n  height: 80px;\n  border-radius: 100%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n.margin-left {\n  margin-left: 10%;\n}\n.container {\n  height: 100%;\n  flex-direction: column;\n  justify-content: center;\n}\n.container ion-item {\n  margin-bottom: 7%;\n  font-weight: bold;\n}\n.exit {\n  margin-top: 50%;\n  text-decoration: underline;\n}\n.black {\n  --color:#3E4958;\n}\n.green {\n  --color:#008D36;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7QUFDSjtBQUVNO0VBQ0UsZUFBQTtBQUFSO0FBS0UscUNBQUE7QUFFQTtFQUNJLGVBQUE7QUFITjtBQUlNO0VBQ0ksZUFBQTtBQUZWO0FBSU07RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FBRlY7QUFPRTtFQUNJLGdCQUFBO0FBSk47QUFPRTtFQUNJLFlBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0FBSk47QUFLTTtFQUNJLGlCQUFBO0VBQ0EsaUJBQUE7QUFIVjtBQU9FO0VBQ0ksZUFBQTtFQUNBLDBCQUFBO0FBSk47QUFRRTtFQUNJLGVBQUE7QUFMTjtBQVFFO0VBQ0ksZUFBQTtBQUxOIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFyIHtcbiAgICBoZWlnaHQ6IDIwMHB4O1xuICAgIC0tYmFja2dyb3VuZDogIzAwOEQzNjtcbiAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgXG4gICAgaW9uLXJvdyB7XG4gICAgICBwIHtcbiAgICAgICAgbWFyZ2luOiA1cHggMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuICBcbiAgLyogQ29udGVuZWRvciBkZSBpbWFnZW4gZGVsIHVzdWFyaW8gKi9cbiAgXG4gIC5jb250YWluZXItaW1ne1xuICAgICAgbWFyZ2luLXRvcDogMTQlO1xuICAgICAgaW9uLWljb257XG4gICAgICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgfVxuICAgICAgLmltZyB7XG4gICAgICAgICAgd2lkdGg6IDgwcHg7XG4gICAgICAgICAgaGVpZ2h0OiA4MHB4O1xuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgfVxuICAgICAgICBcbiAgfVxuICBcbiAgLm1hcmdpbi1sZWZ0e1xuICAgICAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgfVxuICBcbiAgLmNvbnRhaW5lciB7XG4gICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBpb24taXRlbXtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiA3JTtcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgIH1cbiAgfVxuICBcbiAgLmV4aXR7XG4gICAgICBtYXJnaW4tdG9wOiA1MCU7XG4gICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgICAgIFxuICB9XG4gIFxuICAuYmxhY2t7XG4gICAgICAtLWNvbG9yOiMzRTQ5NTg7XG4gIH1cbiAgXG4gIC5ncmVlbntcbiAgICAgIC0tY29sb3I6IzAwOEQzNjtcbiAgfVxuIl19 */");

/***/ }),

/***/ "z+ob":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/components/request-card/request-card.component.scss ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  display: flex;\n  position: relative;\n}\n\nimg {\n  width: 82px;\n  height: 80px;\n}\n\n.data-card {\n  margin-left: 23px;\n}\n\n.title {\n  font-weight: bold;\n  line-height: 28px;\n  font-size: 17px;\n  color: #333333;\n  margin-top: 5px;\n  margin-bottom: 0px;\n}\n\n.subtitle {\n  font-weight: 500;\n  font-size: 11px;\n  color: #3E4958;\n  line-height: 20px;\n  margin-top: 0px;\n  margin-bottom: 5px;\n}\n\n.stars ion-icon {\n  color: #008D36;\n}\n\n.price {\n  display: flex;\n  position: absolute;\n  right: 22px;\n}\n\n.dollars {\n  margin-top: 5px;\n  font-size: 26px;\n  line-height: 31px;\n}\n\n.cents {\n  font-size: 13px;\n  line-height: 16px;\n  margin-top: 5px;\n}\n\n.text {\n  font-weight: 500;\n  font-size: 11px;\n  line-height: 20px;\n  margin-left: 20px;\n}\n\n.date, .stop {\n  margin-top: 0px;\n}\n\n.date, .address {\n  margin-bottom: 0px;\n}\n\n.details, .button {\n  display: flex;\n}\n\n.container-button {\n  position: relative;\n}\n\n.button {\n  position: absolute;\n  bottom: 16px;\n  right: 13px;\n  --border-radius: 100px;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n  --background: #008D36;\n  left: 5%;\n}\n\n.accepted-button {\n  position: absolute;\n  bottom: 16px;\n  right: 13px;\n  --border-radius: 100px;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n  --background: #214E9D ;\n}\n\n.waiting-driver-button {\n  position: absolute;\n  bottom: 16px;\n  right: 13px;\n  --border-radius: 100px;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n  --background: #33D4FF ;\n}\n\nion-card {\n  margin-bottom: 50px;\n}\n\n.space-card {\n  margin-bottom: 30%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3JlcXVlc3QtY2FyZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGFBQUE7RUFDQSxrQkFBQTtBQUNEOztBQUVBO0VBQ0MsV0FBQTtFQUNBLFlBQUE7QUFDRDs7QUFFQTtFQUNDLGlCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUFDRDs7QUFFQTtFQUNDLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQUNEOztBQUdDO0VBQ0MsY0FBQTtBQUFGOztBQUlBO0VBQ0MsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQUREOztBQUlBO0VBQ0MsZUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUREOztBQUlBO0VBQ0MsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQUREOztBQUlBO0VBQ0MsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQUREOztBQUlBO0VBQ0MsZUFBQTtBQUREOztBQUlBO0VBQ0Msa0JBQUE7QUFERDs7QUFJQTtFQUNDLGFBQUE7QUFERDs7QUFJQTtFQUNDLGtCQUFBO0FBREQ7O0FBSUE7RUFDQyxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxpREFBQTtFQUNBLHFCQUFBO0VBQ0csUUFBQTtBQURKOztBQUlBO0VBQ0Msa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsaURBQUE7RUFDQSxzQkFBQTtBQUREOztBQUlBO0VBQ0Msa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsaURBQUE7RUFDQSxzQkFBQTtBQUREOztBQUlBO0VBQ0MsbUJBQUE7QUFERDs7QUFLQTtFQUNDLGtCQUFBO0FBRkQiLCJmaWxlIjoicmVxdWVzdC1jYXJkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XG5cdGRpc3BsYXk6IGZsZXg7XG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuaW1nIHtcblx0d2lkdGg6IDgycHg7XG5cdGhlaWdodDogODBweDtcbn1cblxuLmRhdGEtY2FyZCB7XG5cdG1hcmdpbi1sZWZ0OiAyM3B4O1xufVxuXG4udGl0bGUge1xuXHRmb250LXdlaWdodDogYm9sZDtcblx0bGluZS1oZWlnaHQ6IDI4cHg7XG5cdGZvbnQtc2l6ZTogMTdweDtcblx0Y29sb3I6ICMzMzMzMzM7XG5cdG1hcmdpbi10b3A6IDVweDtcblx0bWFyZ2luLWJvdHRvbTogMHB4O1xufVxuXG4uc3VidGl0bGUge1xuXHRmb250LXdlaWdodDogNTAwO1xuXHRmb250LXNpemU6IDExcHg7XG5cdGNvbG9yOiAjM0U0OTU4O1xuXHRsaW5lLWhlaWdodDogMjBweDtcblx0bWFyZ2luLXRvcDogMHB4O1xuXHRtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbi5zdGFycyB7XG5cdGlvbi1pY29uIHtcblx0XHRjb2xvcjogIzAwOEQzNjtcblx0fVxufVxuXG4ucHJpY2Uge1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRwb3NpdGlvbjogYWJzb2x1dGU7XG5cdHJpZ2h0OiAyMnB4O1xufVxuXG4uZG9sbGFycyB7XG5cdG1hcmdpbi10b3A6IDVweDtcblx0Zm9udC1zaXplOiAyNnB4O1xuXHRsaW5lLWhlaWdodDogMzFweDtcbn1cblxuLmNlbnRzIHtcblx0Zm9udC1zaXplOiAxM3B4O1xuXHRsaW5lLWhlaWdodDogMTZweDtcblx0bWFyZ2luLXRvcDogNXB4O1xufVxuXG4udGV4dCB7XG5cdGZvbnQtd2VpZ2h0OiA1MDA7XG5cdGZvbnQtc2l6ZTogMTFweDtcblx0bGluZS1oZWlnaHQ6IDIwcHg7XG5cdG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuXG4uZGF0ZSwgLnN0b3Age1xuXHRtYXJnaW4tdG9wOiAwcHg7XG59XG5cbi5kYXRlLCAuYWRkcmVzcyB7XG5cdG1hcmdpbi1ib3R0b206IDBweDtcbn1cblxuLmRldGFpbHMsIC5idXR0b24ge1xuXHRkaXNwbGF5OiBmbGV4O1xufVxuXG4uY29udGFpbmVyLWJ1dHRvbiB7XG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmJ1dHRvbiB7XG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0Ym90dG9tOiAxNnB4O1xuXHRyaWdodDogMTNweDtcblx0LS1ib3JkZXItcmFkaXVzOiAxMDBweDtcblx0LS1ib3gtc2hhZG93OiAwcHggMnB4IDhweCByZ2JhKDE2LCAxMDUsIDIyNywgMC4zKTtcblx0LS1iYWNrZ3JvdW5kOiAjMDA4RDM2O1xuICAgIGxlZnQ6IDUlO1xufVxuXG4uYWNjZXB0ZWQtYnV0dG9uIHtcblx0cG9zaXRpb246IGFic29sdXRlO1xuXHRib3R0b206IDE2cHg7XG5cdHJpZ2h0OiAxM3B4O1xuXHQtLWJvcmRlci1yYWRpdXM6IDEwMHB4O1xuXHQtLWJveC1zaGFkb3c6IDBweCAycHggOHB4IHJnYmEoMTYsIDEwNSwgMjI3LCAwLjMpO1xuXHQtLWJhY2tncm91bmQ6ICMyMTRFOUQgO1xufVxuXG4ud2FpdGluZy1kcml2ZXItYnV0dG9uIHtcblx0cG9zaXRpb246IGFic29sdXRlO1xuXHRib3R0b206IDE2cHg7XG5cdHJpZ2h0OiAxM3B4O1xuXHQtLWJvcmRlci1yYWRpdXM6IDEwMHB4O1xuXHQtLWJveC1zaGFkb3c6IDBweCAycHggOHB4IHJnYmEoMTYsIDEwNSwgMjI3LCAwLjMpO1xuXHQtLWJhY2tncm91bmQ6ICMzM0Q0RkYgO1xufVxuXG5pb24tY2FyZCB7XG5cdG1hcmdpbi1ib3R0b206IDUwcHg7XG5cdFxufVxuXG4uc3BhY2UtY2FyZHtcblx0bWFyZ2luLWJvdHRvbTogMzAlO1xufVxuIl19 */");

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "a3Wg");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map