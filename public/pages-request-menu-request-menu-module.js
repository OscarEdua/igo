(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-request-menu-request-menu-module"],{

/***/ "8kji":
/*!*************************************************!*\
  !*** ./src/app/shared/services/push.service.ts ***!
  \*************************************************/
/*! exports provided: PushService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PushService", function() { return PushService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "wljF");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





let PushService = class PushService {
    constructor(oneSignal, http, router) {
        this.oneSignal = oneSignal;
        this.http = http;
        this.router = router;
    }
    configuracionInicial(uid) {
        /*     this.oneSignal. */
        this.oneSignal.setExternalUserId(uid);
        this.oneSignal.startInit('4f3b79de-e102-47ff-a1ac-f7753796d478', '796178968721');
        this.oneSignal.enableSound(true);
        this.oneSignal.enableVibrate(true);
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
        this.oneSignal.handleNotificationReceived().subscribe((noti) => {
            // Cuando recibe
            this.ext = noti.payload.additionalData['ruta'] + '';
        });
        this.oneSignal.handleNotificationOpened().subscribe((noti) => {
            // Cuando abre una notifiacion
            // this.router.navigate(['/notice/'+this.ext]);
            this.router.navigate([this.ext]);
        });
        //obtener uid de onsignal
        this.oneSignal.getIds().then((info) => {
            this.useId = info.userId;
        });
        this.oneSignal.endInit();
    }
    //Enviar  notificaciones individuales
    sendByUid(title, subtitle, detalle, ruta, uiduser) {
        //dato a mandar
        const datos = {
            app_id: '4f3b79de-e102-47ff-a1ac-f7753796d478',
            included_segments: ['Active User', 'Inactive User'],
            headings: { en: title },
            subtitle: { en: subtitle },
            data: { ruta: ruta, fecha: '100-210-250' },
            contents: { en: detalle },
            channel_for_external_user_ids: 'push',
            include_external_user_ids: [uiduser],
        };
        this.post(datos);
    }
    deleteuser() {
        this.oneSignal.removeExternalUserId();
    }
    //Enviar  notificaciones Global
    sendGlobal(title, subtitle, detalle, ruta) {
        //dato a mandar
        const datos = {
            app_id: '4f3b79de-e102-47ff-a1ac-f7753796d478',
            included_segments: ['Active Users', 'Inactive Users'],
            headings: { en: title },
            subtitle: { en: subtitle },
            data: { ruta: ruta, fecha: '100-210-250' },
            contents: { en: detalle },
        };
        this.post(datos);
    }
    post(datos) {
        const headers = {
            Authorization: 'Basic ODM0ODMzNzQtZjIzNi00NWU1LTk3ZDMtZmI1NmI3ZDhjZTFk',
        };
        var url = 'https://onesignal.com/api/v1/notifications';
        return new Promise((resolve) => {
            this.http.post(url, datos, { headers }).subscribe((data) => {
                resolve(data);
            });
        });
    }
};
PushService.ctorParameters = () => [
    { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_2__["OneSignal"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
PushService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], PushService);



/***/ }),

/***/ "WCqY":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/request-menu/request-menu.module.ts ***!
  \*****************************************************************************/
/*! exports provided: RequestMenuPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestMenuPageModule", function() { return RequestMenuPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _request_menu_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./request-menu-routing.module */ "dTPR");
/* harmony import */ var _request_menu_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./request-menu.page */ "BLIM");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let RequestMenuPageModule = class RequestMenuPageModule {
};
RequestMenuPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _request_menu_routing_module__WEBPACK_IMPORTED_MODULE_5__["RequestMenuPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_request_menu_page__WEBPACK_IMPORTED_MODULE_6__["RequestMenuPage"]]
    })
], RequestMenuPageModule);



/***/ }),

/***/ "dTPR":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/request-menu/request-menu-routing.module.ts ***!
  \*************************************************************************************/
/*! exports provided: RequestMenuPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestMenuPageRoutingModule", function() { return RequestMenuPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _request_menu_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./request-menu.page */ "BLIM");
/* harmony import */ var _requests_requests_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../requests/requests.page */ "LYy4");





const routes = [
    {
        path: '',
        component: _request_menu_page__WEBPACK_IMPORTED_MODULE_3__["RequestMenuPage"]
    },
    {
        path: 'requests',
        component: _requests_requests_page__WEBPACK_IMPORTED_MODULE_4__["RequestsPage"]
    }
];
let RequestMenuPageRoutingModule = class RequestMenuPageRoutingModule {
};
RequestMenuPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RequestMenuPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-request-menu-request-menu-module.js.map