(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "/gzB":
/*!****************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-stop-schedule-modal/select-stop-schedule-modal.page.scss ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".button-container-col {\n  margin-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3NlbGVjdC1zdG9wLXNjaGVkdWxlLW1vZGFsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0FBQ0YiLCJmaWxlIjoic2VsZWN0LXN0b3Atc2NoZWR1bGUtbW9kYWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ1dHRvbi1jb250YWluZXItY29sIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbiJdfQ== */");

/***/ }),

/***/ "2fpJ":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-stop-modal/select-stop-modal.page.scss ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".subtitle-modal {\n  margin-bottom: 0px;\n}\n\n.searchbar {\n  width: 100%;\n  padding-left: 0px;\n  padding-right: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3NlbGVjdC1zdG9wLW1vZGFsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGtCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUNEIiwiZmlsZSI6InNlbGVjdC1zdG9wLW1vZGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zdWJ0aXRsZS1tb2RhbCB7XG5cdG1hcmdpbi1ib3R0b206IDBweDtcbn1cblxuLnNlYXJjaGJhciB7XG5cdHdpZHRoOiAxMDAlO1xuXHRwYWRkaW5nLWxlZnQ6IDBweDtcblx0cGFkZGluZy1yaWdodDogMHB4O1xufVxuIl19 */");

/***/ }),

/***/ "3GtE":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/users/users.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons class=\"margin-left\" slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons>\n    <ion-row>\n      <ion-title>{{ titlePage }}</ion-title>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"container\">\n    <ion-searchbar\n      placeholder=\"Buscar por nombre\" \n      inputmode=\"text\"\n      (ionChange)=\"onSearchChange($event)\"\n    >\n    </ion-searchbar>\n\n    <ion-card *ngFor=\"let user of users | async | filter:searchText\">\n\n      <ion-row *ngIf=\"user.role == 'client'\" [routerLink]=\"['../../user-detail/' + user.userId]\">\n        <ion-col size=\"4\" class=\"container-img\">\n          <div *ngIf=\"user.imagen != ''\" style=\"background-image: url({{ user.imagen }});\" class=\"img\">\n          </div>\n          <div *ngIf=\"user.imagen == ''\" style=\"background-image: url('../../../../../assets/img/user.png');\" class=\"img\">\n          </div>\n        </ion-col>\n        <ion-col size=\"8\">\n          <ion-card-header>\n            <ion-card-title>{{ user.name }}</ion-card-title>\n            <ion-card-subtitle>{{ user.email }}</ion-card-subtitle>\n          </ion-card-header>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"user.role == 'driver'\" [routerLink]=\"['../../driver-detail/' + user.userId]\">\n        <ion-col size=\"4\" class=\"container-img\">\n          <div *ngIf=\"user.imagen != ''\" style=\"background-image: url({{ user.imagen }});\" class=\"img\">\n          </div>\n          <div *ngIf=\"user.imagen == ''\" style=\"background-image: url('../../../../../assets/img/user.png');\" class=\"img\">\n          </div>\n        </ion-col>\n        <ion-col size=\"8\">\n          <ion-card-header>\n            <ion-card-title>{{ user.name }}</ion-card-title>\n            <ion-card-subtitle>{{ user.email }}</ion-card-subtitle>\n            <div class=\"container-starts\">\n              <ion-icon class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon name=\"star-outline\"></ion-icon>\n            </div>\n          </ion-card-header>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </div>\n</ion-content>\n\n");

/***/ }),

/***/ "3fK3":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-route-modal/select-route-modal.page.scss ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".card {\n  width: 110%;\n  display: flex;\n  border-radius: 15px;\n  margin: 10% 0px 10% 0px;\n  padding: 0px;\n  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.15);\n}\n.card .container-figure {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n}\n.card .container-figure .circle {\n  width: 9px;\n  height: 9px;\n  border-radius: 100%;\n  background-color: #214E9D;\n  margin-bottom: 4px;\n}\n.card .container-figure .line {\n  background-color: #3E4958;\n  height: 35%;\n  width: 1px;\n}\n.card .container-info .line {\n  background-color: #d5dde0b4;\n  height: 2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3NlbGVjdC1yb3V0ZS1tb2RhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsNENBQUE7QUFDSjtBQUFJO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQUVSO0FBRFE7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQUdaO0FBRFE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FBR1o7QUFFUTtFQUNJLDJCQUFBO0VBQ0EsV0FBQTtBQUFaIiwiZmlsZSI6InNlbGVjdC1yb3V0ZS1tb2RhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZHtcbiAgICB3aWR0aDogMTEwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgbWFyZ2luOiAxMCUgMHB4IDEwJSAwcHg7XG4gICAgcGFkZGluZzogMHB4O1xuICAgIGJveC1zaGFkb3c6IDBweCA0cHggMTVweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xuICAgIC5jb250YWluZXItZmlndXJle1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgLmNpcmNsZXtcbiAgICAgICAgICAgIHdpZHRoOiA5cHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDlweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiMyMTRFOUQgO1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogNHB4O1xuICAgICAgICB9XG4gICAgICAgIC5saW5le1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjojM0U0OTU4O1xuICAgICAgICAgICAgaGVpZ2h0OiAzNSU7XG4gICAgICAgICAgICB3aWR0aDogMXB4O1xuXG4gICAgICAgIH1cbiAgICB9XG4gICAgLmNvbnRhaW5lci1pbmZve1xuICAgICAgICAubGluZXtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6I2Q1ZGRlMGI0O1xuICAgICAgICAgICAgaGVpZ2h0OiAycHg7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0= */");

/***/ }),

/***/ "4Q2i":
/*!******************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/select-stop-schedule-modal/select-stop-schedule-modal.page.html ***!
  \******************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n  <app-close-modal-button (click)=\"alertsService.closeModal()\"></app-close-modal-button>\n\n<ion-content class=\"ion-padding\">\n  <ion-grid>\n      <ion-row>\n        <ion-col size=\"12\">\n          <p class=\"title-modal\">Elige tu parada</p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"12\">\n          <app-address-input text=\"24, Oviedo y Sucre, Ibarra\"></app-address-input>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"12\">\n         <ion-input class=\"input\" placeholder=\"Referencia\"></ion-input>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"12\" class=\"button-container-col\">\n          <app-button title=\"Seleccionar ubicación\" (click)=\"redirectTo('ubication-destination-schedule')\"></app-button>\n        </ion-col>\n      </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "4laz":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal.page.html ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-close-modal-button (closeModal)=\"alertsService.closeModal()\"></app-close-modal-button>\n<ion-content class=\"ion-padding\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"12\">\n        <p class=\"title-modal\">Ubicación del Destino</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"12\">\n        <app-address-input text=\"24, Oviedo y Sucre, Ibarra\"></app-address-input>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\" class=\"button-container-col\">\n        <app-button title=\"Selecciona ubicación\" (pressButton)=\"redirectTo('destination-address')\"></app-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "74mu":
/*!*************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/theme-ff3fc52f.js ***!
  \*************************************************************/
/*! exports provided: c, g, h, o */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return createColorClasses; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getClassMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return hostContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "o", function() { return openURL; });
const hostContext = (selector, el) => {
  return el.closest(selector) !== null;
};
/**
 * Create the mode and color classes for the component based on the classes passed in
 */
const createColorClasses = (color, cssClassMap) => {
  return (typeof color === 'string' && color.length > 0) ? Object.assign({ 'ion-color': true, [`ion-color-${color}`]: true }, cssClassMap) : cssClassMap;
};
const getClassList = (classes) => {
  if (classes !== undefined) {
    const array = Array.isArray(classes) ? classes : classes.split(' ');
    return array
      .filter(c => c != null)
      .map(c => c.trim())
      .filter(c => c !== '');
  }
  return [];
};
const getClassMap = (classes) => {
  const map = {};
  getClassList(classes).forEach(c => map[c] = true);
  return map;
};
const SCHEME = /^[a-z][a-z0-9+\-.]*:/;
const openURL = async (url, ev, direction, animation) => {
  if (url != null && url[0] !== '#' && !SCHEME.test(url)) {
    const router = document.querySelector('ion-router');
    if (router) {
      if (ev != null) {
        ev.preventDefault();
      }
      return router.push(url, direction, animation);
    }
  }
  return false;
};




/***/ }),

/***/ "854H":
/*!****************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-payment-modal/select-payment-modal.page.scss ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZWxlY3QtcGF5bWVudC1tb2RhbC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "8hOI":
/*!*********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/requests/requests.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-searchbar {\n  margin-top: 85px;\n  margin-bottom: 49px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3JlcXVlc3RzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGdCQUFBO0VBQ0EsbUJBQUE7QUFDRCIsImZpbGUiOiJyZXF1ZXN0cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tc2VhcmNoYmFyIHtcblx0bWFyZ2luLXRvcDogODVweDtcblx0bWFyZ2luLWJvdHRvbTogNDlweDtcbn1cbiJdfQ== */");

/***/ }),

/***/ "9Xeq":
/*!**********************************************!*\
  !*** ./src/app/shared/pipes/pipes.module.ts ***!
  \**********************************************/
/*! exports provided: PipesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PipesModule", function() { return PipesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _filter_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./filter.pipe */ "UhSo");



let PipesModule = class PipesModule {
};
PipesModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_filter_pipe__WEBPACK_IMPORTED_MODULE_2__["FilterPipe"]],
        exports: [
            _filter_pipe__WEBPACK_IMPORTED_MODULE_2__["FilterPipe"]
        ]
    })
], PipesModule);



/***/ }),

/***/ "BEzL":
/*!**************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/destination-address-modal/destination-address-modal.page.scss ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".border {\n  border: 1px solid red;\n  margin: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL2Rlc3RpbmF0aW9uLWFkZHJlc3MtbW9kYWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MscUJBQUE7RUFDQSxXQUFBO0FBQ0QiLCJmaWxlIjoiZGVzdGluYXRpb24tYWRkcmVzcy1tb2RhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYm9yZGVyIHtcblx0Ym9yZGVyOiAxcHggc29saWQgcmVkO1xuXHRtYXJnaW46IDBweDtcbn1cbiJdfQ== */");

/***/ }),

/***/ "BLIM":
/*!***************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/request-menu/request-menu.page.ts ***!
  \***************************************************************************/
/*! exports provided: RequestMenuPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestMenuPage", function() { return RequestMenuPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_request_menu_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./request-menu.page.html */ "UWbE");
/* harmony import */ var _request_menu_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./request-menu.page.scss */ "dkpw");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");





let RequestMenuPage = class RequestMenuPage {
    constructor(menuController) {
        this.menuController = menuController;
    }
    ngOnInit() {
    }
    openMenu() {
        this.menuController.open('principal');
    }
};
RequestMenuPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] }
];
RequestMenuPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-request-menu',
        template: _raw_loader_request_menu_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_request_menu_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RequestMenuPage);



/***/ }),

/***/ "Czdn":
/*!************************************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal.page.scss ***!
  \************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".button-container-col {\n  margin-top: 3%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3ViaWNhdGlvbi1kZXN0aW5hdGlvbi1zY2hlZHVsZS1tb2RhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxjQUFBO0FBQ0QiLCJmaWxlIjoidWJpY2F0aW9uLWRlc3RpbmF0aW9uLXNjaGVkdWxlLW1vZGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idXR0b24tY29udGFpbmVyLWNvbCB7XG5cdG1hcmdpbi10b3A6IDMlO1xufVxuIl19 */");

/***/ }),

/***/ "DXXd":
/*!**************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/select-route-modal/select-route-modal.page.html ***!
  \**************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<app-close-modal-button (click)=\"alertsService.closeModal()\"></app-close-modal-button>\n\n<ion-content class=\"ion-padding-horizontal\">\n  <ion-row >\n    <ion-col size=\"12\">\n      <p class=\"title-modal\">Ruta de viaje</p>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n      <ion-card class=\"card\">\n        <ion-col size=\"2\" class=\"container-figure\">\n          <div class=\"circle\"></div>\n          <div class=\"line\"></div>\n          <ion-icon name=\"caret-down-outline\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"10\" class=\"container-info\">\n          <p>{{ travel?.startLocation.address }}</p>\n          <div class=\"line\"></div>\n          <p>{{ travel?.destinyLocation.address }}</p>\n        </ion-col>\n      </ion-card>\n  </ion-row>\n  \n  <ion-row>\n    <ion-col size=\"12\">\n      <app-button title=\"Confirmar Ruta\" (pressButton)=\"confirmRoute('travel-details')\"></app-button>\n    </ion-col>\n  </ion-row>\n</ion-content>\n");

/***/ }),

/***/ "IhZq":
/*!**********************************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal.page.ts ***!
  \**********************************************************************************************************************************/
/*! exports provided: UbicationDestinationScheduleModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UbicationDestinationScheduleModalPage", function() { return UbicationDestinationScheduleModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_ubication_destination_schedule_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./ubication-destination-schedule-modal.page.html */ "4laz");
/* harmony import */ var _ubication_destination_schedule_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ubication-destination-schedule-modal.page.scss */ "Czdn");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");






let UbicationDestinationScheduleModalPage = class UbicationDestinationScheduleModalPage {
    constructor(alertsService, router) {
        this.alertsService = alertsService;
        this.router = router;
    }
    ngOnInit() {
    }
    redirectTo(pageName) {
        this.alertsService.closeModal();
        this.router.navigate(['dashboard/' + pageName]);
    }
};
UbicationDestinationScheduleModalPage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
UbicationDestinationScheduleModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-ubication-destination-schedule-modal',
        template: _raw_loader_ubication_destination_schedule_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_ubication_destination_schedule_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UbicationDestinationScheduleModalPage);



/***/ }),

/***/ "JbSX":
/*!*********************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/button-active-4927a4c1.js ***!
  \*********************************************************************/
/*! exports provided: c */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return createButtonActiveGesture; });
/* harmony import */ var _index_7a8b7a1c_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index-7a8b7a1c.js */ "wEJo");
/* harmony import */ var _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./haptic-27b3f981.js */ "qULd");
/* harmony import */ var _index_f49d994d_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index-f49d994d.js */ "iWo5");




const createButtonActiveGesture = (el, isButton) => {
  let currentTouchedButton;
  let initialTouchedButton;
  const activateButtonAtPoint = (x, y, hapticFeedbackFn) => {
    if (typeof document === 'undefined') {
      return;
    }
    const target = document.elementFromPoint(x, y);
    if (!target || !isButton(target)) {
      clearActiveButton();
      return;
    }
    if (target !== currentTouchedButton) {
      clearActiveButton();
      setActiveButton(target, hapticFeedbackFn);
    }
  };
  const setActiveButton = (button, hapticFeedbackFn) => {
    currentTouchedButton = button;
    if (!initialTouchedButton) {
      initialTouchedButton = currentTouchedButton;
    }
    const buttonToModify = currentTouchedButton;
    Object(_index_7a8b7a1c_js__WEBPACK_IMPORTED_MODULE_0__["c"])(() => buttonToModify.classList.add('ion-activated'));
    hapticFeedbackFn();
  };
  const clearActiveButton = (dispatchClick = false) => {
    if (!currentTouchedButton) {
      return;
    }
    const buttonToModify = currentTouchedButton;
    Object(_index_7a8b7a1c_js__WEBPACK_IMPORTED_MODULE_0__["c"])(() => buttonToModify.classList.remove('ion-activated'));
    /**
     * Clicking on one button, but releasing on another button
     * does not dispatch a click event in browsers, so we
     * need to do it manually here. Some browsers will
     * dispatch a click if clicking on one button, dragging over
     * another button, and releasing on the original button. In that
     * case, we need to make sure we do not cause a double click there.
     */
    if (dispatchClick && initialTouchedButton !== currentTouchedButton) {
      currentTouchedButton.click();
    }
    currentTouchedButton = undefined;
  };
  return Object(_index_f49d994d_js__WEBPACK_IMPORTED_MODULE_2__["createGesture"])({
    el,
    gestureName: 'buttonActiveDrag',
    threshold: 0,
    onStart: ev => activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__["a"]),
    onMove: ev => activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__["b"]),
    onEnd: () => {
      clearActiveButton(true);
      Object(_haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__["h"])();
      initialTouchedButton = undefined;
    }
  });
};




/***/ }),

/***/ "LYy4":
/*!*******************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/requests/requests.page.ts ***!
  \*******************************************************************/
/*! exports provided: RequestsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestsPage", function() { return RequestsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_requests_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./requests.page.html */ "SHym");
/* harmony import */ var _requests_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./requests.page.scss */ "8hOI");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/admin/travels.service */ "KlPZ");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_shared_services_push_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/services/push.service */ "8kji");








let RequestsPage = class RequestsPage {
    constructor(travelsService, alertsService, pushService, router) {
        this.travelsService = travelsService;
        this.alertsService = alertsService;
        this.pushService = pushService;
        this.router = router;
    }
    ngOnInit() {
        this.oTravels = this.travelsService.getTravels();
        this.suscription = this.oTravels.subscribe(travels => this.travels = travels);
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.suscription.unsuscribe();
    }
    accept(travelId) {
        this.alertsService.presentLoading('Aceptando...');
        this.travelsService.acceptTravel(travelId)
            .then(() => {
            this.alertsService.loading.dismiss();
            this.alertsService.presentAlertConfirmAccept('Solicitud aceptada', 'Por favor espere mientras el cliente realiza el pago.');
            //this.pushService.sendByUid('IGO','','','',this.user.uid)
        })
            .catch(() => {
            this.alertsService.loading.dismiss();
            this.alertsService.presentAlertWithHeader('Solicitudes', 'Lo sentimos, por favor intente nuevamente');
        });
    }
    toTravel(travelId) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.alertsService.presentLoading('Esperando...');
            const travels = this.travels.filter(travel => travel.travelId === travelId);
            if (travels[0].status === 'paid') {
                this.alertsService.loading.dismiss();
                this.router.navigate(['dashboard/travel', travelId, 'driver']);
            }
            else {
                this.alertsService.loading.dismiss();
                this.alertsService.presentAlertConfirmAccept('Esperando pago...', 'Por favor espere mientras el cliente realiza el pago.');
            }
        });
    }
    getWholePart(cost) {
        return Math.trunc(cost);
    }
    getDecimalPart(cost) {
        return (cost.toFixed(2)).split('.')[1];
    }
};
RequestsPage.ctorParameters = () => [
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_4__["TravelsService"] },
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__["AlertsService"] },
    { type: src_app_shared_services_push_service__WEBPACK_IMPORTED_MODULE_7__["PushService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] }
];
RequestsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-requests',
        template: _raw_loader_requests_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_requests_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RequestsPage);



/***/ }),

/***/ "Lq2P":
/*!********************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-stop-modal/select-stop-modal.page.ts ***!
  \********************************************************************************************/
/*! exports provided: SelectStopModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopModalPage", function() { return SelectStopModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_stop_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-stop-modal.page.html */ "Szg1");
/* harmony import */ var _select_stop_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-stop-modal.page.scss */ "2fpJ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/storage.service */ "fbMX");
/* harmony import */ var _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/admin/stops.service */ "/+Y6");
/* harmony import */ var src_app_shared_services_maps_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared/services/maps.service */ "Y0mc");



/// <reference path="../../../../../../../node_modules/@types/googlemaps/index.d.ts" />






let SelectStopModalPage = class SelectStopModalPage {
    constructor(alertsService, router, storageService, stopsService, mapsService) {
        this.alertsService = alertsService;
        this.router = router;
        this.storageService = storageService;
        this.stopsService = stopsService;
        this.mapsService = mapsService;
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.markers = [];
        this.addressStop = '';
        this.inputDisabled = true;
        this.inputPlaceholder = 'Seleccione una parada';
        this.startLocation = {};
    }
    ngOnInit() {
        this.stops = this.stopsService.getStop();
        this.storageService.get('travel')
            .then(travel => {
            this.travel = JSON.parse(travel);
            console.log(this.travel);
            if (this.travel.vehicleType == 'Standard') {
            }
            else {
                this.inputDisabled = false;
                this.inputPlaceholder = 'Seleccione o ingrese una parada';
            }
        })
            .catch(error => console.log(error));
    }
    ionViewDidEnter() {
        this.loadAutocomplete();
    }
    chooseStop(stop) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.deleteMarkers();
            this.addressStop = stop.address;
            this.startLocation.lat = stop.lat;
            this.startLocation.lng = stop.lng;
            this.startLocation.address = stop.address;
            this.marker.position.lat = stop.lat;
            this.marker.position.lng = stop.lng;
            this.marker.title = stop.address;
            this.addMarker(this.marker, this.marker.title, 'stops');
        });
    }
    addMarker(marker, name, typeMarket) {
        let marke;
        let icon;
        if (typeMarket == 'myLocation') {
            icon = {
                url: '../../assets/img/ic_loc.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(90, 90),
            };
            /*     const image = '../../assets/img/Pineapplemenu.png'; */
        }
        else if (typeMarket == 'stops') {
            icon = {
                url: '../../assets/img/ic_dropoff.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(50, 50),
            };
        }
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            //  draggable: true,
            // animation: google.maps.Animation.DROP,
            map: this.mapsService.map,
            icon: icon,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                    console.log('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
            this.mapsService.map.setCenter(this.marker.position);
        });
        const infoWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
                        if (infoWindow !== null) {
                        }
                        infoWindow.open(this.mapsService.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
    }
    selectUbication(pageName) {
        this.storageService.get('travel').then((travel) => {
            this.travel = JSON.parse(travel);
            this.travel.startLocation = this.startLocation;
            const sTravel = JSON.stringify(this.travel);
            this.storageService.set('travel', sTravel);
        });
        if (this.addressStop == '') {
            this.alertsService.presentAlertWithHeader('Elige tu parada.', 'Por favor escoja o ingrese una parada.');
        }
        else {
            this.alertsService.closeModal();
            this.router.navigate(['dashboard/' + pageName]);
            this.deleteMarkers();
        }
    }
    deleteMarkers() {
        this.markers = [];
        this.setMapOnAll(null);
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    loadAutocomplete() {
        const autocomplete = new google.maps.places.Autocomplete(document.querySelector('#autocomplete-input').getElementsByTagName('input')[0]);
        google.maps.event.addListener(autocomplete, 'place_changed', event => {
            const place = autocomplete.getPlace();
            this.startLocation.lat = place.geometry.location.lat();
            this.startLocation.lng = place.geometry.location.lng();
            this.startLocation.address = place.name + " " + place.formatted_address;
            this.startLocation.address = this.startLocation.address.replace('\\', '');
            this.mapsService.map.setCenter(place.geometry.location);
            const market = new google.maps.Marker({
                position: place.geometry.location,
                //animation: google.maps.Animation.DROP,
                icon: {
                    url: '../../../../../../assets/img/ic_loc.png',
                    scaledSize: new google.maps.Size(60, 60)
                }
            });
            this.mapsService.deleteMarkers();
            market.setMap(this.mapsService.map);
        });
    }
};
SelectStopModalPage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
    { type: _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_7__["StopsService"] },
    { type: src_app_shared_services_maps_service__WEBPACK_IMPORTED_MODULE_8__["MapsService"] }
];
SelectStopModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-stop-modal',
        template: _raw_loader_select_stop_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_stop_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectStopModalPage);



/***/ }),

/***/ "Mc/L":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-payment-modal/select-payment-modal.page.ts ***!
  \**************************************************************************************************/
/*! exports provided: SelectPaymentModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectPaymentModalPage", function() { return SelectPaymentModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_payment_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-payment-modal.page.html */ "nyxl");
/* harmony import */ var _select_payment_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-payment-modal.page.scss */ "854H");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _shared_services_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../shared/services/storage.service */ "fbMX");
/* harmony import */ var _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../shared/services/maps.service */ "Y0mc");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/admin/travels.service */ "KlPZ");










let SelectPaymentModalPage = class SelectPaymentModalPage {
    constructor(renderer, alertsService, storageService, mapsService, router, travelsService, activatedRoute) {
        this.renderer = renderer;
        this.alertsService = alertsService;
        this.storageService = storageService;
        this.mapsService = mapsService;
        this.router = router;
        this.travelsService = travelsService;
        this.activatedRoute = activatedRoute;
        // inicio de la ruta
        this.origin = { lat: 0, lng: 0 };
        // Fin d e  la ruta
        this.destination = { lat: 0, lng: 0 };
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.getTravel();
            this.origin.lat = this.travel.startLocation.lat;
            this.origin.lng = this.travel.startLocation.lng;
            this.destination.lat = this.travel.destinyLocation.lat;
            this.destination.lng = this.travel.destinyLocation.lng;
            this.calculateRoute();
        });
    }
    calculateRoute() {
        this.mapsService.directionsService.route({
            // paramentros de la ruta  inicial y final
            origin: this.origin,
            destination: this.destination,
            // Que  vehiculo se usa  para realizar el viaje
            travelMode: google.maps.TravelMode.DRIVING,
        }, (response, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                this.mapsService.directionsDisplay.setDirections(response);
            }
            else {
                //  alert('Could not display directions due to: ' + status);
            }
        });
    }
    confirmPaymentMethod() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.alertsService.presentLoading('Confirmando...');
            if (this.numberCard === undefined) {
                this.alertsService.loading.dismiss();
                this.alertsService.presentAlertWithHeader("Método de pago", "Por favor seleccion un método de pago");
            }
            else {
                if (this.numberCard === 1) {
                    this.travelsService.updatePayMethod(this.travelId, 'card')
                        .then(() => {
                        this.alertsService.loading.dismiss();
                    })
                        .catch((error) => {
                        this.alertsService.loading.dismiss();
                        console.log(error);
                    });
                }
                else if (this.numberCard === 2) {
                    this.travelsService.updatePayMethod(this.travelId, 'transfer')
                        .then(() => {
                        this.alertsService.loading.dismiss();
                        this.alertsService.closeModal();
                        this.router.navigate(['dashboard/transfer/' + this.travelId]);
                    })
                        .catch((error) => {
                        this.alertsService.loading.dismiss();
                        console.log(error);
                    });
                }
                else if (this.numberCard === 3) {
                    this.travelsService.updatePayMethod(this.travelId, 'cash')
                        .then(() => {
                        this.alertsService.loading.dismiss();
                        this.alertsService.closeModal();
                        this.router.navigate(['dashboard/travel', this.travelId, 'client'])
                            .then(nav => console.log(nav))
                            .catch(error => console.log(error));
                    })
                        .catch((error) => {
                        this.alertsService.loading.dismiss();
                        console.log(error);
                    });
                }
            }
        });
    }
    getTravel() {
        return new Promise((resolve) => {
            this.storageService.get('travel').then((travel) => {
                this.travel = JSON.parse(travel);
                resolve(this.travel);
            });
        });
    }
    select(cardNumber) {
        this.numberCard = cardNumber;
        const card = this[`card${cardNumber}`].nativeElement.getElementsByTagName('ion-card')[0];
        this.renderer.addClass(card, 'select');
        for (let indexCard = 1; indexCard <= 3; indexCard++) {
            if (indexCard !== cardNumber) {
                this.renderer.removeClass(this[`card${indexCard}`].nativeElement.getElementsByTagName('ion-card')[0], 'select');
            }
        }
    }
};
SelectPaymentModalPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_7__["Renderer2"] },
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_3__["AlertsService"] },
    { type: _shared_services_storage_service__WEBPACK_IMPORTED_MODULE_5__["StorageService"] },
    { type: _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__["MapsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_8__["TravelsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
];
SelectPaymentModalPage.propDecorators = {
    card1: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__["ViewChild"], args: ['card1', { read: _angular_core__WEBPACK_IMPORTED_MODULE_7__["ElementRef"] },] }],
    card2: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__["ViewChild"], args: ['card2', { read: _angular_core__WEBPACK_IMPORTED_MODULE_7__["ElementRef"] },] }],
    card3: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__["ViewChild"], args: ['card3', { read: _angular_core__WEBPACK_IMPORTED_MODULE_7__["ElementRef"] },] }],
    travelId: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__["Input"] }]
};
SelectPaymentModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_7__["Component"])({
        selector: 'app-select-payment-modal',
        template: _raw_loader_select_payment_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_payment_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectPaymentModalPage);



/***/ }),

/***/ "R9cC":
/*!*************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/users/users.page.ts ***!
  \*************************************************************/
/*! exports provided: UsersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPage", function() { return UsersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_users_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./users.page.html */ "3GtE");
/* harmony import */ var _users_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./users.page.scss */ "mfuq");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/modules/home/services/users.service */ "6JOU");






let UsersPage = class UsersPage {
    constructor(usersService, route) {
        this.usersService = usersService;
        this.route = route;
        this.buttonPressed = this.route.snapshot.paramMap.get('buttonPressed');
        this.titlePage = (this.buttonPressed == 'users') ? 'Usuarios' : 'Conductores';
    }
    ngOnInit() {
        if (this.buttonPressed == 'users') {
            this.users = this.usersService.read();
        }
        else {
            this.users = this.usersService.readDrivers();
        }
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    greeting() {
        console.log('Hola...');
    }
};
UsersPage.ctorParameters = () => [
    { type: src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UsersService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
];
UsersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-users',
        template: _raw_loader_users_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_users_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UsersPage);



/***/ }),

/***/ "SHym":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/requests/requests.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header title=\"Solicitudes\"></app-header>\n<ion-content class=\"ion-padding\">\n  <ion-searchbar\n  placeholder=\"Buscar Solicitud\" \n  inputmode=\"text\"\n  (ionChange)=\"onSearchChange($event)\"\n>\n</ion-searchbar>\n\n  \n  <app-request-card *ngFor=\"let travel of oTravels | async\"\n    [imagen]=\"travel.client.imagen\"\n    [title]=\"travel.client.name\" \n    [date]=\"travel.createdAt.toDate() | date: 'dd/MM/yyyy'\" \n    [dollars]=\"getWholePart(travel.cost)\" \n    [cents]=\"getDecimalPart(travel.cost)\" \n    [address]=\"'DESDE ' + travel.startLocation.address + ' A ' + travel.destinyLocation.address\"\n    [schedulingDate]=\"(travel.schedulingDate != 'undefined') ? 'AHORA' : travel.schedulingDate\"\n    [stop]=\"travel.startLocation.address\"\n    [status]=\"travel.status\"\n    (accept)=\"accept(travel.travelId)\"\n    (toTravel)=\"toTravel(travel.travelId)\">\n  </app-request-card>\n\n</ion-content>\n");

/***/ }),

/***/ "Szg1":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/select-stop-modal/select-stop-modal.page.html ***!
  \************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<app-close-modal-button (closeModal)=\"alertsService.closeModal()\"></app-close-modal-button>\n\n<ion-content class=\"ion-padding-horizontal\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"12\">\n        <p class=\"title-modal\">Elige tu parada de Origen</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n      <app-address-input *ngIf=\"travel?.vehicleType == 'VIP'\" [disabled]=\"inputDisabled\" [placeholder]=\"inputPlaceholder\" [(ngModel)]=\"addressStop\"></app-address-input>\n      <!--<ion-input type=\"text\" id=\"autocomplete-input\"></ion-input>-->\n      <!--<input type=\"text\" id=\"autocomplete-input\" />-->\n      <ion-searchbar *ngIf=\"travel?.vehicleType == 'Standard'\" class=\"searchbar\" inputmode=\"text\" placeholder=\"Buscar parada\" (ionChange)=\"onSearchChange($event)\">\n      </ion-searchbar>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf=\"travel?.vehicleType == 'Standard'\">\n      <ion-col size=\"12\">\n        <p class=\"subtitle-modal\">PARADAS CERCANAS</p>\n      </ion-col>\n    </ion-row>\n    <ng-container *ngIf=\"travel?.vehicleType == 'Standard'\">\n      <ion-row *ngFor=\"let stop of stops | async | filter:searchText\">\n        <ion-col size=\"12\">\n          <app-item-map city=\"{{ stop.city }}\" classes=\"icon-container green-item\" name=\"{{ stop.name }}\" description=\"{{ stop.description }}\" (chooseStop)=\"chooseStop(stop)\"></app-item-map>\n        </ion-col>\n      </ion-row>\n    </ng-container>\n    <ion-row>\n      <ion-col size=\"12\">\n        <app-button title=\"Seleccionar ubicacion\" (pressButton)=\"selectUbication('select-destination')\"></app-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "UWbE":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/request-menu/request-menu.page.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header title=\"Bienvenido a iGO\"></app-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col >\n        <div class=\"container\">\n          <div class=\"welcome-container\">\n            <p>Bienvenido, Persona</p>\n          </div>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <app-request-button title=\"Solicitudes Ahora\" iconName=\"person\" backgroundColorCard=\"green\" [routerLink]=\"['requests']\"></app-request-button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <app-request-button title=\"Solicitudes Agendadas\" iconName=\"car\" backgroundColorCard=\"blue\"></app-request-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "UhSo":
/*!*********************************************!*\
  !*** ./src/app/shared/pipes/filter.pipe.ts ***!
  \*********************************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");


let FilterPipe = class FilterPipe {
    transform(array, text = '', field = 'name') {
        if (text === '') {
            return array;
        }
        if (!array) {
            return array;
        }
        text = text.toLocaleLowerCase();
        return array.filter(item => item[field].toLowerCase().includes(text));
    }
};
FilterPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'filter'
    })
], FilterPipe);



/***/ }),

/***/ "W3XF":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-route-modal/select-route-modal.page.ts ***!
  \**********************************************************************************************/
/*! exports provided: SelectRouteModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectRouteModalPage", function() { return SelectRouteModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_route_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-route-modal.page.html */ "DXXd");
/* harmony import */ var _select_route_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-route-modal.page.scss */ "3fK3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/storage.service */ "fbMX");
/* harmony import */ var _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../shared/services/maps.service */ "Y0mc");








let SelectRouteModalPage = class SelectRouteModalPage {
    constructor(alertsService, router, storageService, mapsService) {
        this.alertsService = alertsService;
        this.router = router;
        this.storageService = storageService;
        this.mapsService = mapsService;
        // inicio de la ruta
        this.origin = { lat: 0, lng: 0 };
        // Fin d e  la ruta
        this.destination = { lat: 0, lng: 0 };
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.getTravel();
            this.origin.lat = this.travel.startLocation.lat;
            this.origin.lng = this.travel.startLocation.lng;
            this.destination.lat = this.travel.destinyLocation.lat;
            this.destination.lng = this.travel.destinyLocation.lng;
            this.calculateRoute();
        });
    }
    getTravel() {
        return new Promise((resolve) => {
            this.storageService.get('travel').then((travel) => {
                this.travel = JSON.parse(travel);
                resolve(this.travel);
            });
        });
    }
    confirmRoute(pageName) {
        this.alertsService.closeModal();
        this.router.navigate(['dashboard/' + pageName]);
    }
    calculateRoute() {
        this.mapsService.directionsService.route({
            // paramentros de la ruta  inicial y final
            origin: this.origin,
            destination: this.destination,
            // Que  vehiculo se usa  para realizar el viaje
            travelMode: google.maps.TravelMode.DRIVING,
        }, (response, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                this.mapsService.directionsDisplay.setDirections(response);
            }
            else {
                //  alert('Could not display directions due to: ' + status);
            }
        });
    }
    ionViewWillLeave() { }
};
SelectRouteModalPage.ctorParameters = () => [
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
    { type: _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_7__["MapsService"] }
];
SelectRouteModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-route-modal',
        template: _raw_loader_select_route_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_route_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectRouteModalPage);



/***/ }),

/***/ "acej":
/*!**************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/framework-delegate-4392cd63.js ***!
  \**************************************************************************/
/*! exports provided: a, d */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return attachComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return detachComponent; });
/* harmony import */ var _helpers_dd7e4b7b_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers-dd7e4b7b.js */ "1vRN");


const attachComponent = async (delegate, container, component, cssClasses, componentProps) => {
  if (delegate) {
    return delegate.attachViewToDom(container, component, componentProps, cssClasses);
  }
  if (typeof component !== 'string' && !(component instanceof HTMLElement)) {
    throw new Error('framework delegate is missing');
  }
  const el = (typeof component === 'string')
    ? container.ownerDocument && container.ownerDocument.createElement(component)
    : component;
  if (cssClasses) {
    cssClasses.forEach(c => el.classList.add(c));
  }
  if (componentProps) {
    Object.assign(el, componentProps);
  }
  container.appendChild(el);
  await new Promise(resolve => Object(_helpers_dd7e4b7b_js__WEBPACK_IMPORTED_MODULE_0__["c"])(el, resolve));
  return el;
};
const detachComponent = (delegate, element) => {
  if (element) {
    if (delegate) {
      const container = element.parentElement;
      return delegate.removeViewFromDom(container, element);
    }
    element.remove();
  }
  return Promise.resolve();
};




/***/ }),

/***/ "bQXX":
/*!****************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/destination-address-modal/destination-address-modal.page.html ***!
  \****************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-close-modal-button (closeModal)=\"alertsService.closeModal()\"></app-close-modal-button>\n<ion-content class=\"ion-padding\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"12\">\n        <p class=\"title-modal\">Ruta de viaje</p>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"border\">\n      <ion-col size=\"12\" class=\"ion-no-padding ion-no-margin border\">\n        <app-travel-card></app-travel-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <app-button title=\"Comfirmar ruta\" (pressButton)=\"redirectTo('nextPage')\"></app-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "dkpw":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/request-menu/request-menu.page.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".welcome-container {\n  display: flex;\n  justify-content: flex-start;\n  width: 100%;\n  margin-left: 16px;\n  margin-top: 50px;\n}\n.welcome-container p {\n  color: #3E4958;\n  font-size: 20px;\n  font-weight: bold;\n}\n.container {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3JlcXVlc3QtbWVudS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxhQUFBO0VBQ0EsMkJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQUNEO0FBQUM7RUFDQyxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBRUY7QUFFQTtFQUNDLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUFDRCIsImZpbGUiOiJyZXF1ZXN0LW1lbnUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndlbGNvbWUtY29udGFpbmVyIHtcblx0ZGlzcGxheTogZmxleDtcblx0anVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuXHR3aWR0aDogMTAwJTtcblx0bWFyZ2luLWxlZnQ6IDE2cHg7XG5cdG1hcmdpbi10b3A6IDUwcHg7XG5cdHAge1xuXHRcdGNvbG9yOiAjM0U0OTU4O1xuXHRcdGZvbnQtc2l6ZTogMjBweDtcblx0XHRmb250LXdlaWdodDogYm9sZDtcblx0fVxufVxuXG4uY29udGFpbmVyIHtcblx0ZGlzcGxheTogZmxleDtcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4iXX0= */");

/***/ }),

/***/ "fn7c":
/*!**************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-stop-schedule-modal/select-stop-schedule-modal.page.ts ***!
  \**************************************************************************************************************/
/*! exports provided: SelectStopScheduleModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopScheduleModalPage", function() { return SelectStopScheduleModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_stop_schedule_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-stop-schedule-modal.page.html */ "4Q2i");
/* harmony import */ var _select_stop_schedule_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-stop-schedule-modal.page.scss */ "/gzB");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");






let SelectStopScheduleModalPage = class SelectStopScheduleModalPage {
    constructor(alertsService, router) {
        this.alertsService = alertsService;
        this.router = router;
    }
    ngOnInit() {
    }
    redirectTo(pageName) {
        this.alertsService.closeModal();
        this.router.navigate(['dashboard/' + pageName]);
    }
};
SelectStopScheduleModalPage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
SelectStopScheduleModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-stop-schedule-modal',
        template: _raw_loader_select_stop_schedule_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_stop_schedule_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectStopScheduleModalPage);



/***/ }),

/***/ "h3R7":
/*!***********************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/spinner-configs-cd7845af.js ***!
  \***********************************************************************/
/*! exports provided: S */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S", function() { return SPINNERS; });
const spinners = {
  'bubbles': {
    dur: 1000,
    circles: 9,
    fn: (dur, index, total) => {
      const animationDelay = `${(dur * index / total) - dur}ms`;
      const angle = 2 * Math.PI * index / total;
      return {
        r: 5,
        style: {
          'top': `${9 * Math.sin(angle)}px`,
          'left': `${9 * Math.cos(angle)}px`,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'circles': {
    dur: 1000,
    circles: 8,
    fn: (dur, index, total) => {
      const step = index / total;
      const animationDelay = `${(dur * step) - dur}ms`;
      const angle = 2 * Math.PI * step;
      return {
        r: 5,
        style: {
          'top': `${9 * Math.sin(angle)}px`,
          'left': `${9 * Math.cos(angle)}px`,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'circular': {
    dur: 1400,
    elmDuration: true,
    circles: 1,
    fn: () => {
      return {
        r: 20,
        cx: 48,
        cy: 48,
        fill: 'none',
        viewBox: '24 24 48 48',
        transform: 'translate(0,0)',
        style: {}
      };
    }
  },
  'crescent': {
    dur: 750,
    circles: 1,
    fn: () => {
      return {
        r: 26,
        style: {}
      };
    }
  },
  'dots': {
    dur: 750,
    circles: 3,
    fn: (_, index) => {
      const animationDelay = -(110 * index) + 'ms';
      return {
        r: 6,
        style: {
          'left': `${9 - (9 * index)}px`,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'lines': {
    dur: 1000,
    lines: 12,
    fn: (dur, index, total) => {
      const transform = `rotate(${30 * index + (index < 6 ? 180 : -180)}deg)`;
      const animationDelay = `${(dur * index / total) - dur}ms`;
      return {
        y1: 17,
        y2: 29,
        style: {
          'transform': transform,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'lines-small': {
    dur: 1000,
    lines: 12,
    fn: (dur, index, total) => {
      const transform = `rotate(${30 * index + (index < 6 ? 180 : -180)}deg)`;
      const animationDelay = `${(dur * index / total) - dur}ms`;
      return {
        y1: 12,
        y2: 20,
        style: {
          'transform': transform,
          'animation-delay': animationDelay,
        }
      };
    }
  }
};
const SPINNERS = spinners;




/***/ }),

/***/ "mfuq":
/*!***************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/users/users.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n/* Estilos del header */\nion-toolbar ion-row {\n  align-items: center;\n  display: flex;\n  justify-content: center;\n  width: 85%;\n}\nion-toolbar ion-row ion-title {\n  font-size: 20px;\n  font-weight: bold;\n  --color: #3E4958;\n  text-align: center;\n}\n/* Estilo para colocar un margen izquierdo a cualquier elemento */\n.margin-left {\n  margin-left: 5%;\n}\n/* Estilos de la barra de busqueda */\nion-searchbar {\n  --box-shadow: inset 0px 4px 15px rgba(0, 0, 0, 0.14);\n  --border-radius: 15px;\n  --icon-color: #008D36;\n  --background:#ffff;\n}\n/* Contenedor de todas las tarjetas de navegación */\n.container {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  padding: 15px;\n  margin-top: 10%;\n}\n/* Estilos de las tarjetas */\nion-card {\n  border-radius: 15px;\n  box-shadow: 0px 2px 15px rgba(0, 0, 0, 0.15);\n  margin-top: 10%;\n  width: 90%;\n}\nion-card ion-card-header ion-card-title {\n  color: #333333;\n  font-weight: bold;\n}\nion-card ion-card-header ion-card-subtitle {\n  color: #3E4958;\n  font-size: 13.5px;\n  font-weight: 500;\n  margin: 8px 0px;\n}\nion-card ion-card-header .container-starts ion-icon {\n  font-size: 20px;\n  margin-right: 9px;\n}\n/* Contenedor de imagen del usuario */\n.container-img {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.container-img .img {\n  width: 82px;\n  height: 80px;\n  border-radius: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n/* Color verde para algunos elementos */\n.green {\n  color: #008D36;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3VzZXJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUFBaEIsdUJBQUE7QUFFSTtFQUNFLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtBQUNOO0FBQ007RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBQ1I7QUFNRSxpRUFBQTtBQUNBO0VBQ0UsZUFBQTtBQUhKO0FBT0Usb0NBQUE7QUFDRjtFQUNJLG9EQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNFLGtCQUFBO0FBSk47QUFPRSxtREFBQTtBQUNBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtBQUpOO0FBUUUsNEJBQUE7QUFDQTtFQUNJLG1CQUFBO0VBQ0EsNENBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtBQUxOO0FBUVU7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7QUFOZDtBQVFVO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBTmQ7QUFVYztFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQVJsQjtBQWNFLHFDQUFBO0FBQ0E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQVhOO0FBWU07RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQVZWO0FBY0UsdUNBQUE7QUFDQTtFQUNJLGNBQUE7QUFYTiIsImZpbGUiOiJ1c2Vycy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBFc3RpbG9zIGRlbCBoZWFkZXIgKi9cbmlvbi10b29sYmFyIHtcbiAgICBpb24tcm93IHtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICB3aWR0aDogODUlO1xuICBcbiAgICAgIGlvbi10aXRsZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIC0tY29sb3I6ICMzRTQ5NTg7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgXG4gICAgICB9XG5cbiAgICB9XG4gIH1cbiAgXG4gIC8qIEVzdGlsbyBwYXJhIGNvbG9jYXIgdW4gbWFyZ2VuIGl6cXVpZXJkbyBhIGN1YWxxdWllciBlbGVtZW50byAqL1xuICAubWFyZ2luLWxlZnQge1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgfVxuXG5cbiAgLyogRXN0aWxvcyBkZSBsYSBiYXJyYSBkZSBidXNxdWVkYSAqL1xuaW9uLXNlYXJjaGJhciB7XG4gICAgLS1ib3gtc2hhZG93OiBpbnNldCAwcHggNHB4IDE1cHggcmdiYSgwLCAwLCAwLCAwLjE0KTtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgLS1pY29uLWNvbG9yOiAjMDA4RDM2O1xuICAgICAgLS1iYWNrZ3JvdW5kOiNmZmZmO1xuICB9XG4gIFxuICAvKiBDb250ZW5lZG9yIGRlIHRvZGFzIGxhcyB0YXJqZXRhcyBkZSBuYXZlZ2FjacOzbiAqL1xuICAuY29udGFpbmVyIHtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgICBtYXJnaW4tdG9wOiAxMCU7IFxuICAgIFxuICB9XG4gIFxuICAvKiBFc3RpbG9zIGRlIGxhcyB0YXJqZXRhcyAqL1xuICBpb24tY2FyZHtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgICBib3gtc2hhZG93OiAwcHggMnB4IDE1cHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcbiAgICAgIG1hcmdpbi10b3A6MTAlO1xuICAgICAgd2lkdGg6IDkwJTtcbiAgICBcbiAgICAgIGlvbi1jYXJkLWhlYWRlcntcbiAgICAgICAgICBpb24tY2FyZC10aXRsZXtcbiAgICAgICAgICAgICAgY29sb3I6ICMzMzMzMzM7XG4gICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpb24tY2FyZC1zdWJ0aXRsZXtcbiAgICAgICAgICAgICAgY29sb3I6ICMzRTQ5NTg7XG4gICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTMuNXB4O1xuICAgICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICAgICAgICBtYXJnaW46IDhweCAwcHg7XG4gICAgICAgICAgfVxuICBcbiAgICAgICAgICAuY29udGFpbmVyLXN0YXJ0c3tcbiAgICAgICAgICAgICAgaW9uLWljb257XG4gICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDlweDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgIH1cbiAgfVxuICBcbiAgLyogQ29udGVuZWRvciBkZSBpbWFnZW4gZGVsIHVzdWFyaW8gKi9cbiAgLmNvbnRhaW5lci1pbWd7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgLmltZ3tcbiAgICAgICAgICB3aWR0aDogODJweDtcbiAgICAgICAgICBoZWlnaHQ6IDgwcHg7XG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICB9XG4gIH1cbiAgXG4gIC8qIENvbG9yIHZlcmRlIHBhcmEgYWxndW5vcyBlbGVtZW50b3MgKi9cbiAgLmdyZWVue1xuICAgICAgY29sb3I6ICMwMDhEMzY7XG4gIH0iXX0= */");

/***/ }),

/***/ "nyxl":
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/select-payment-modal/select-payment-modal.page.html ***!
  \******************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n  <app-close-modal-button\n    (click)=\"alertsService.closeModal()\"\n  ></app-close-modal-button>\n\n<ion-content class=\"ion-padding-horizontal\">\n  <ion-row>\n    <ion-col size=\"12\">\n      <p class=\"title-modal\">Seleccionar tipo de pago</p>\n    </ion-col>\n  </ion-row>\n  <app-payment-type text=\"*** 8295\" icon=\"card-outline\" #card1 (click)=\"select(1)\"></app-payment-type>\n  <app-payment-type text=\"Transferencia\" #card2 (click)=\"select(2)\" icon=\"no-icon\" ></app-payment-type>\n  <app-payment-type text=\"Efectivo\" #card3  (click)=\"select(3)\" icon=\"cash-outline\" ></app-payment-type>\n\n  <ion-row>\n    <ion-col size=\"12\">\n      <app-button title=\"Confirmar forma de pago\" (pressButton)=\"confirmPaymentMethod()\"></app-button>\n    </ion-col>\n  </ion-row>\n</ion-content>\n");

/***/ }),

/***/ "qULd":
/*!**************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/haptic-27b3f981.js ***!
  \**************************************************************/
/*! exports provided: a, b, c, d, h */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return hapticSelectionStart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return hapticSelectionChanged; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return hapticSelection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return hapticImpact; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return hapticSelectionEnd; });
const HapticEngine = {
  getEngine() {
    const win = window;
    return (win.TapticEngine) || (win.Capacitor && win.Capacitor.isPluginAvailable('Haptics') && win.Capacitor.Plugins.Haptics);
  },
  available() {
    return !!this.getEngine();
  },
  isCordova() {
    return !!window.TapticEngine;
  },
  isCapacitor() {
    const win = window;
    return !!win.Capacitor;
  },
  impact(options) {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    const style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
    engine.impact({ style });
  },
  notification(options) {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    const style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
    engine.notification({ style });
  },
  selection() {
    this.impact({ style: 'light' });
  },
  selectionStart() {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    if (this.isCapacitor()) {
      engine.selectionStart();
    }
    else {
      engine.gestureSelectionStart();
    }
  },
  selectionChanged() {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    if (this.isCapacitor()) {
      engine.selectionChanged();
    }
    else {
      engine.gestureSelectionChanged();
    }
  },
  selectionEnd() {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    if (this.isCapacitor()) {
      engine.selectionEnd();
    }
    else {
      engine.gestureSelectionEnd();
    }
  }
};
/**
 * Trigger a selection changed haptic event. Good for one-time events
 * (not for gestures)
 */
const hapticSelection = () => {
  HapticEngine.selection();
};
/**
 * Tell the haptic engine that a gesture for a selection change is starting.
 */
const hapticSelectionStart = () => {
  HapticEngine.selectionStart();
};
/**
 * Tell the haptic engine that a selection changed during a gesture.
 */
const hapticSelectionChanged = () => {
  HapticEngine.selectionChanged();
};
/**
 * Tell the haptic engine we are done with a gesture. This needs to be
 * called lest resources are not properly recycled.
 */
const hapticSelectionEnd = () => {
  HapticEngine.selectionEnd();
};
/**
 * Use this to indicate success/failure/warning to the user.
 * options should be of the type `{ style: 'light' }` (or `medium`/`heavy`)
 */
const hapticImpact = (options) => {
  HapticEngine.impact(options);
};




/***/ }),

/***/ "sVj7":
/*!************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/destination-address-modal/destination-address-modal.page.ts ***!
  \************************************************************************************************************/
/*! exports provided: DestinationAddressModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DestinationAddressModalPage", function() { return DestinationAddressModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_destination_address_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./destination-address-modal.page.html */ "bQXX");
/* harmony import */ var _destination_address_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./destination-address-modal.page.scss */ "BEzL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");





let DestinationAddressModalPage = class DestinationAddressModalPage {
    constructor(alertsService) {
        this.alertsService = alertsService;
    }
    ngOnInit() {
    }
    redirectTo(pageName) {
    }
};
DestinationAddressModalPage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] }
];
DestinationAddressModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-destination-address-modal',
        template: _raw_loader_destination_address_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_destination_address_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], DestinationAddressModalPage);



/***/ })

}]);
//# sourceMappingURL=common.js.map