(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-email-verification-email-verification-module"],{

/***/ "34oX":
/*!********************************************************************************************!*\
  !*** ./src/app/modules/home/pages/email-verification/email-verification-routing.module.ts ***!
  \********************************************************************************************/
/*! exports provided: EmailVerificationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailVerificationPageRoutingModule", function() { return EmailVerificationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _email_verification_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./email-verification.page */ "NkCz");




const routes = [
    {
        path: '',
        component: _email_verification_page__WEBPACK_IMPORTED_MODULE_3__["EmailVerificationPage"]
    }
];
let EmailVerificationPageRoutingModule = class EmailVerificationPageRoutingModule {
};
EmailVerificationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EmailVerificationPageRoutingModule);



/***/ }),

/***/ "4ISz":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/pages/email-verification/email-verification.page.html ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>emailVerification</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "NkCz":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/home/pages/email-verification/email-verification.page.ts ***!
  \**********************************************************************************/
/*! exports provided: EmailVerificationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailVerificationPage", function() { return EmailVerificationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_email_verification_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./email-verification.page.html */ "4ISz");
/* harmony import */ var _email_verification_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./email-verification.page.scss */ "qWpk");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let EmailVerificationPage = class EmailVerificationPage {
    constructor() { }
    ngOnInit() {
    }
};
EmailVerificationPage.ctorParameters = () => [];
EmailVerificationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-email-verification',
        template: _raw_loader_email_verification_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_email_verification_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], EmailVerificationPage);



/***/ }),

/***/ "VWH8":
/*!************************************************************************************!*\
  !*** ./src/app/modules/home/pages/email-verification/email-verification.module.ts ***!
  \************************************************************************************/
/*! exports provided: EmailVerificationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailVerificationPageModule", function() { return EmailVerificationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _email_verification_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./email-verification-routing.module */ "34oX");
/* harmony import */ var _email_verification_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./email-verification.page */ "NkCz");







let EmailVerificationPageModule = class EmailVerificationPageModule {
};
EmailVerificationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _email_verification_routing_module__WEBPACK_IMPORTED_MODULE_5__["EmailVerificationPageRoutingModule"]
        ],
        declarations: [_email_verification_page__WEBPACK_IMPORTED_MODULE_6__["EmailVerificationPage"]]
    })
], EmailVerificationPageModule);



/***/ }),

/***/ "qWpk":
/*!************************************************************************************!*\
  !*** ./src/app/modules/home/pages/email-verification/email-verification.page.scss ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlbWFpbC12ZXJpZmljYXRpb24ucGFnZS5zY3NzIn0= */");

/***/ })

}]);
//# sourceMappingURL=pages-email-verification-email-verification-module.js.map