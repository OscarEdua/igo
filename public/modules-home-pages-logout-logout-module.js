(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-home-pages-logout-logout-module"],{

/***/ "Dl9r":
/*!************************************************************!*\
  !*** ./src/app/modules/home/pages/logout/logout.page.scss ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".btn {\n  /* Green */\n  margin-top: 5%;\n  margin-left: 10%;\n  margin-right: 10%;\n  background: #008D36;\n  height: 60px;\n  width: 80%;\n  box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  border-radius: 15px;\n  color: #ffffff;\n  font-weight: bold;\n  margin-bottom: 5%;\n}\n\n.container-btn {\n  padding-top: 10%;\n  margin-left: 15%;\n}\n\n.img-logout {\n  height: 400px;\n  margin-bottom: 15%;\n  margin-top: 15%;\n}\n\nh1 {\n  font-weight: 600;\n}\n\nion-content {\n  --padding-start:30px;\n  --padding-end:30px;\n  --padding-top:30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2xvZ291dC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxVQUFBO0VBQ0QsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0RBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FBQUg7O0FBRUM7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0FBQ0w7O0FBR0M7RUFDSSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBQUw7O0FBSUM7RUFDSSxnQkFBQTtBQURMOztBQU1DO0VBQ0ksb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FBSEwiLCJmaWxlIjoibG9nb3V0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vRXN0aWxvcyBkZWwgYm90b24gZGUgc2FsaXJcbi5idG4ge1xuICAgIC8qIEdyZWVuICovXG4gICBtYXJnaW4tdG9wOiA1JTtcbiAgIG1hcmdpbi1sZWZ0OiAxMCU7XG4gICBtYXJnaW4tcmlnaHQ6IDEwJTtcbiAgIGJhY2tncm91bmQ6ICMwMDhEMzY7XG4gICBoZWlnaHQ6IDYwcHg7XG4gICB3aWR0aDogODAlO1xuICAgYm94LXNoYWRvdzogMHB4IDRweCAyMHB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yNSk7XG4gICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgY29sb3I6ICNmZmZmZmY7XG4gICBmb250LXdlaWdodDogYm9sZDtcbiAgIG1hcmdpbi1ib3R0b206IDUlO1xuIH1cbiAuY29udGFpbmVyLWJ0bntcbiAgICAgcGFkZGluZy10b3A6IDEwJTtcbiAgICAgbWFyZ2luLWxlZnQ6IDE1JTtcbiAgIH1cbiBcbiAvL0VzdGlsb3MgZGUgbGEgaW1hZ2VuXG4gLmltZy1sb2dvdXQge1xuICAgICBoZWlnaHQ6IDQwMHB4O1xuICAgICBtYXJnaW4tYm90dG9tOiAxNSU7XG4gICAgIG1hcmdpbi10b3A6IDE1JTtcbiB9XG4gXG4gLy9Fc3RpbG9zIGRlbCB0ZXh0byBwYXJhIHNhbGlyXG4gaDF7XG4gICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gfVxuIFxuIC8vUGFkZGluZyBhbCBjb250ZW5lZG9yXG4gXG4gaW9uLWNvbnRlbnR7XG4gICAgIC0tcGFkZGluZy1zdGFydDozMHB4O1xuICAgICAtLXBhZGRpbmctZW5kOjMwcHg7XG4gICAgIC0tcGFkZGluZy10b3A6MzBweDtcbiB9Il19 */");

/***/ }),

/***/ "Hf8d":
/*!********************************************************************!*\
  !*** ./src/app/modules/home/pages/logout/logout-routing.module.ts ***!
  \********************************************************************/
/*! exports provided: LogoutPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutPageRoutingModule", function() { return LogoutPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _logout_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./logout.page */ "VJkp");




const routes = [
    {
        path: '',
        component: _logout_page__WEBPACK_IMPORTED_MODULE_3__["LogoutPage"]
    }
];
let LogoutPageRoutingModule = class LogoutPageRoutingModule {
};
LogoutPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LogoutPageRoutingModule);



/***/ }),

/***/ "Sqph":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/pages/logout/logout.page.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<ion-header>\n  <ion-toolbar>\n    <ion-menu-button slot=\"start\"  menu=\"main menu\"></ion-menu-button>\n    <ion-title>Saliendo del Sistema</ion-title>\n    <ion-back-button defaultHref=\"home\"  slot=\"start\"></ion-back-button>\n\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content >\n\n  <ion-text  >\n  <div class=\"ion-text-center\">\n    <h1>¿Realmente quieres irte?</h1>\n  </div>\n  </ion-text>    \n  <ion-img src=\"assets/img/bye.png\" class=\"img-logout\"></ion-img>\n  <ion-row>\n    <ion-col>\n      <div>\n        <button class=\"btn\" (click)=\"authenticationService.signOut()\">\n          Salir\n        </button>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-content>\n");

/***/ }),

/***/ "UY9W":
/*!************************************************************!*\
  !*** ./src/app/modules/home/pages/logout/logout.module.ts ***!
  \************************************************************/
/*! exports provided: LogoutPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutPageModule", function() { return LogoutPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _logout_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./logout-routing.module */ "Hf8d");
/* harmony import */ var _logout_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./logout.page */ "VJkp");







let LogoutPageModule = class LogoutPageModule {
};
LogoutPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _logout_routing_module__WEBPACK_IMPORTED_MODULE_5__["LogoutPageRoutingModule"]
        ],
        declarations: [_logout_page__WEBPACK_IMPORTED_MODULE_6__["LogoutPage"]]
    })
], LogoutPageModule);



/***/ }),

/***/ "VJkp":
/*!**********************************************************!*\
  !*** ./src/app/modules/home/pages/logout/logout.page.ts ***!
  \**********************************************************/
/*! exports provided: LogoutPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutPage", function() { return LogoutPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_logout_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./logout.page.html */ "Sqph");
/* harmony import */ var _logout_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./logout.page.scss */ "Dl9r");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/authentication/authentication.service */ "6CRC");





let LogoutPage = class LogoutPage {
    constructor(authenticationService) {
        this.authenticationService = authenticationService;
    }
    ngOnInit() {
    }
};
LogoutPage.ctorParameters = () => [
    { type: src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] }
];
LogoutPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-logout',
        template: _raw_loader_logout_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_logout_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], LogoutPage);



/***/ })

}]);
//# sourceMappingURL=modules-home-pages-logout-logout-module.js.map