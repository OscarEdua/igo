(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-select-stop-modal-select-stop-modal-module"],{

/***/ "4pu2":
/*!******************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-stop-modal/select-stop-modal-routing.module.ts ***!
  \******************************************************************************************************/
/*! exports provided: SelectStopModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopModalPageRoutingModule", function() { return SelectStopModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_stop_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-stop-modal.page */ "Lq2P");




const routes = [
    {
        path: '',
        component: _select_stop_modal_page__WEBPACK_IMPORTED_MODULE_3__["SelectStopModalPage"]
    }
];
let SelectStopModalPageRoutingModule = class SelectStopModalPageRoutingModule {
};
SelectStopModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectStopModalPageRoutingModule);



/***/ }),

/***/ "s1vA":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-stop-modal/select-stop-modal.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: SelectStopModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopModalPageModule", function() { return SelectStopModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_stop_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-stop-modal-routing.module */ "4pu2");
/* harmony import */ var _select_stop_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-stop-modal.page */ "Lq2P");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");
/* harmony import */ var src_app_shared_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared/pipes/pipes.module */ "9Xeq");









let SelectStopModalPageModule = class SelectStopModalPageModule {
};
SelectStopModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_stop_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectStopModalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_shared_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"]
        ],
        declarations: [_select_stop_modal_page__WEBPACK_IMPORTED_MODULE_6__["SelectStopModalPage"]]
    })
], SelectStopModalPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-select-stop-modal-select-stop-modal-module.js.map