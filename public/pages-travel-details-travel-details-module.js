(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-travel-details-travel-details-module"],{

/***/ "4Kh5":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel-details/travel-details.page.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#map-travel-detail {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3RyYXZlbC1kZXRhaWxzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7QUFDSiIsImZpbGUiOiJ0cmF2ZWwtZGV0YWlscy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbWFwLXRyYXZlbC1kZXRhaWx7XG4gICAgaGVpZ2h0OiAxMDAlO1xufSJdfQ== */");

/***/ }),

/***/ "AtCJ":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/travel-details/travel-details.page.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content fullscreen>\n  <app-header title=\"Detalles del viaje\" slot=\"fixed\"></app-header>\n  <div id=\"map-travel-detail\"></div>\n  <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>  \n</ion-content>");

/***/ }),

/***/ "lab5":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel-details/travel-details.page.ts ***!
  \*******************************************************************************/
/*! exports provided: TravelDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelDetailsPage", function() { return TravelDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_travel_details_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./travel-details.page.html */ "AtCJ");
/* harmony import */ var _travel_details_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./travel-details.page.scss */ "4Kh5");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../shared/services/maps.service */ "Y0mc");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _modals_travel_details_modal_travel_details_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../modals/travel-details-modal/travel-details-modal.page */ "L3lH");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");








let TravelDetailsPage = class TravelDetailsPage {
    constructor(alertsService, mapsService, router) {
        this.alertsService = alertsService;
        this.mapsService = mapsService;
        this.router = router;
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.openModal();
        this.mapsService.loadmap('map-travel-detail');
    }
    openModal() {
        this.alertsService.presentModalWithoutTravelData(_modals_travel_details_modal_travel_details_modal_page__WEBPACK_IMPORTED_MODULE_6__["TravelDetailsModalPage"], 'select-type-vehicle-modal');
    }
};
TravelDetailsPage.ctorParameters = () => [
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__["AlertsService"] },
    { type: _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_4__["MapsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] }
];
TravelDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-travel-details',
        template: _raw_loader_travel_details_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_travel_details_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TravelDetailsPage);



/***/ }),

/***/ "nb7n":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel-details/travel-details.module.ts ***!
  \*********************************************************************************/
/*! exports provided: TravelDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelDetailsPageModule", function() { return TravelDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _travel_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./travel-details-routing.module */ "vMVc");
/* harmony import */ var _travel_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./travel-details.page */ "lab5");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let TravelDetailsPageModule = class TravelDetailsPageModule {
};
TravelDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _travel_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["TravelDetailsPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_travel_details_page__WEBPACK_IMPORTED_MODULE_6__["TravelDetailsPage"]]
    })
], TravelDetailsPageModule);



/***/ }),

/***/ "vMVc":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel-details/travel-details-routing.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: TravelDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelDetailsPageRoutingModule", function() { return TravelDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _travel_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./travel-details.page */ "lab5");




const routes = [
    {
        path: '',
        component: _travel_details_page__WEBPACK_IMPORTED_MODULE_3__["TravelDetailsPage"]
    }
];
let TravelDetailsPageRoutingModule = class TravelDetailsPageRoutingModule {
};
TravelDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TravelDetailsPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-travel-details-travel-details-module.js.map