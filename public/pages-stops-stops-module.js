(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-stops-stops-module"],{

/***/ "+aec":
/*!***************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/stops/stops.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilos del header */\nion-toolbar ion-row {\n  align-items: center;\n  display: flex;\n  justify-content: center;\n  width: 100%;\n}\nion-toolbar ion-row ion-title {\n  font-size: 20px;\n  font-weight: bold;\n  --color: #3E4958;\n  text-align: center;\n}\nion-toolbar ion-icon {\n  font-size: 30px;\n}\n/* Estilo para colocar un margen izquierdo a cualquier elemento */\n.margin-left {\n  margin-left: 5%;\n}\n/* Estilo para colocar un margen derecho a cualquier elemento */\n.margin-right {\n  margin-right: 5%;\n}\n/* Estilos de la barra de busqueda */\nion-searchbar {\n  --box-shadow: inset 0px 4px 15px rgba(0, 0, 0, 0.14);\n  --border-radius: 15px;\n  --icon-color: #008D36;\n  --background:#ffff;\n}\n/* Estilos de las tarjetas */\nion-card {\n  border-radius: 15px;\n  box-shadow: 0px 2px 15px rgba(0, 0, 0, 0.15);\n  margin-top: 10%;\n  width: 90%;\n}\nion-card ion-card-header ion-card-title {\n  color: #333333;\n  font-weight: bold;\n}\nion-card ion-card-header ion-card-subtitle {\n  color: #3E4958;\n  font-size: 13.5px;\n  font-weight: 500;\n  margin: 8px 0px;\n}\nion-card ion-card-header .container-starts ion-icon {\n  font-size: 20px;\n  margin-right: 9px;\n}\n/* Contenedor de imagen del usuario */\n.container-img {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.container-img .img {\n  width: 82px;\n  height: 80px;\n  border-radius: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n.container-btn {\n  display: flex;\n}\n.container-btn .btn {\n  width: 50%;\n  height: 30px;\n  margin-top: 3%;\n  --border-radius: 10px;\n  --box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  font-size: 17px;\n}\n/* Estilo del boton verde */\n.green-btn {\n  --background: #008D36;\n  --background-activated: #008D36;\n  --color: white;\n  font-weight: bold;\n}\n/* Estilo del boton rojo */\n.red {\n  --background: #CB2323;\n  --background-activated: #CB2323;\n  --color: white;\n  font-weight: bold;\n}\n/* Color verde para algunos elementos */\n.green {\n  color: #008D36;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3N0b3BzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx1QkFBQTtBQUVJO0VBQ0UsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0FBQU47QUFFTTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFBUjtBQUtJO0VBQ0UsZUFBQTtBQUhOO0FBT0UsaUVBQUE7QUFDQTtFQUNFLGVBQUE7QUFKSjtBQU9FLCtEQUFBO0FBQ0E7RUFDRSxnQkFBQTtBQUpKO0FBU0Usb0NBQUE7QUFDQTtFQUNFLG9EQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNFLGtCQUFBO0FBTk47QUFTRSw0QkFBQTtBQUNBO0VBQ0UsbUJBQUE7RUFDQSw0Q0FBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0FBTko7QUFRUTtFQUNJLGNBQUE7RUFDQSxpQkFBQTtBQU5aO0FBUVE7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUFOWjtBQVVZO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0FBUmhCO0FBY0UscUNBQUE7QUFDQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBWEo7QUFZSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FBVlI7QUFlRTtFQUNFLGFBQUE7QUFaSjtBQWNJO0VBQ0UsVUFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7RUFDQSxvREFBQTtFQUNBLGVBQUE7QUFaTjtBQWlCRSwyQkFBQTtBQUNBO0VBQ0UscUJBQUE7RUFDQSwrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQWRKO0FBaUJFLDBCQUFBO0FBQ0E7RUFDRSxxQkFBQTtFQUNBLCtCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FBZEo7QUFrQkUsdUNBQUE7QUFDQTtFQUNFLGNBQUE7QUFmSiIsImZpbGUiOiJzdG9wcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBFc3RpbG9zIGRlbCBoZWFkZXIgKi9cbmlvbi10b29sYmFyIHtcbiAgICBpb24tcm93IHtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgXG4gICAgICBpb24tdGl0bGUge1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAtLWNvbG9yOiAjM0U0OTU4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIFxuICAgICAgfVxuICAgIH1cbiAgXG4gICAgaW9uLWljb257XG4gICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgfVxuICB9XG4gIFxuICAvKiBFc3RpbG8gcGFyYSBjb2xvY2FyIHVuIG1hcmdlbiBpenF1aWVyZG8gYSBjdWFscXVpZXIgZWxlbWVudG8gKi9cbiAgLm1hcmdpbi1sZWZ0IHtcbiAgICBtYXJnaW4tbGVmdDogNSU7XG4gIH1cbiAgXG4gIC8qIEVzdGlsbyBwYXJhIGNvbG9jYXIgdW4gbWFyZ2VuIGRlcmVjaG8gYSBjdWFscXVpZXIgZWxlbWVudG8gKi9cbiAgLm1hcmdpbi1yaWdodCB7XG4gICAgbWFyZ2luLXJpZ2h0OiA1JTtcbiAgfVxuICBcbiAgXG4gIFxuICAvKiBFc3RpbG9zIGRlIGxhIGJhcnJhIGRlIGJ1c3F1ZWRhICovXG4gIGlvbi1zZWFyY2hiYXIge1xuICAgIC0tYm94LXNoYWRvdzogaW5zZXQgMHB4IDRweCAxNXB4IHJnYmEoMCwgMCwgMCwgMC4xNCk7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIC0taWNvbi1jb2xvcjogIzAwOEQzNjtcbiAgICAgIC0tYmFja2dyb3VuZDojZmZmZjtcbiAgfVxuICBcbiAgLyogRXN0aWxvcyBkZSBsYXMgdGFyamV0YXMgKi9cbiAgaW9uLWNhcmR7XG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDE1cHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcbiAgICBtYXJnaW4tdG9wOjEwJTtcbiAgICB3aWR0aDogOTAlO1xuICAgIGlvbi1jYXJkLWhlYWRlcntcbiAgICAgICAgaW9uLWNhcmQtdGl0bGV7XG4gICAgICAgICAgICBjb2xvcjogIzMzMzMzMztcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICB9XG4gICAgICAgIGlvbi1jYXJkLXN1YnRpdGxle1xuICAgICAgICAgICAgY29sb3I6ICMzRTQ5NTg7XG4gICAgICAgICAgICBmb250LXNpemU6IDEzLjVweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgICAgICBtYXJnaW46IDhweCAwcHg7XG4gICAgICAgIH1cbiAgXG4gICAgICAgIC5jb250YWluZXItc3RhcnRze1xuICAgICAgICAgICAgaW9uLWljb257XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogOXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICB9XG4gIFxuICAvKiBDb250ZW5lZG9yIGRlIGltYWdlbiBkZWwgdXN1YXJpbyAqL1xuICAuY29udGFpbmVyLWltZ3tcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgLmltZ3tcbiAgICAgICAgd2lkdGg6IDgycHg7XG4gICAgICAgIGhlaWdodDogODBweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICB9XG4gIH1cbiAgXG4gIC8vRXN0aWxvcyBkZSBsb3MgYm90b25lcyBib3RvbiBcbiAgLmNvbnRhaW5lci1idG57XG4gICAgZGlzcGxheTogZmxleDtcbiAgXG4gICAgLmJ0biB7XG4gICAgICB3aWR0aDogNTAlO1xuICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgbWFyZ2luLXRvcDogMyU7XG4gICAgICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICAtLWJveC1zaGFkb3c6IDBweCA0cHggMjBweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMjUpO1xuICAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgIH1cbiAgICBcbiAgfVxuICBcbiAgLyogRXN0aWxvIGRlbCBib3RvbiB2ZXJkZSAqL1xuICAuZ3JlZW4tYnRuIHtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDhEMzY7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzAwOEQzNjtcbiAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgfVxuICBcbiAgLyogRXN0aWxvIGRlbCBib3RvbiByb2pvICovXG4gIC5yZWQge1xuICAgIC0tYmFja2dyb3VuZDogI0NCMjMyMztcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjQ0IyMzIzO1xuICAgIC0tY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBcbiAgfVxuICBcbiAgLyogQ29sb3IgdmVyZGUgcGFyYSBhbGd1bm9zIGVsZW1lbnRvcyAqL1xuICAuZ3JlZW4ge1xuICAgIGNvbG9yOiAjMDA4RDM2O1xuICB9XG4gIFxuIl19 */");

/***/ }),

/***/ "XVPl":
/*!***************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/stops/stops.module.ts ***!
  \***************************************************************/
/*! exports provided: StopsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StopsPageModule", function() { return StopsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _stops_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./stops-routing.module */ "mvMU");
/* harmony import */ var _stops_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./stops.page */ "p4Ce");
/* harmony import */ var src_app_shared_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/pipes/pipes.module */ "9Xeq");








let StopsPageModule = class StopsPageModule {
};
StopsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _stops_routing_module__WEBPACK_IMPORTED_MODULE_5__["StopsPageRoutingModule"],
            src_app_shared_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]
        ],
        declarations: [_stops_page__WEBPACK_IMPORTED_MODULE_6__["StopsPage"]]
    })
], StopsPageModule);



/***/ }),

/***/ "YIJU":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/stops/stops.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar translucent>\n    <ion-buttons class=\"margin-left\" slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons>\n    <ion-row>\n      <ion-title>Lista de paradas</ion-title>\n    </ion-row>\n    <ion-icon [routerLink]=\"['/new-stop']\" slot=\"end\" class=\"margin-right green\" name=\"add-circle-outline\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-searchbar inputmode=\"text\" placeholder=\"Buscar por nombre\" (ionChange)=\"onSearchChange($event)\">\n  </ion-searchbar>\n\n\n  <ion-card *ngFor=\"let stop of stops \">\n    <ion-row>\n      <ion-col size=\"4\" class=\"container-img\">\n        <div style=\"background-image: url(../../assets/img/stops.png);\" class=\"img\">\n        </div>\n      </ion-col>\n      <ion-col size=\"8\">\n        <ion-card-header>\n          <ion-card-title>{{ stop.name }}</ion-card-title>\n          <ion-card-subtitle>{{ stop.description }}</ion-card-subtitle>\n          <ion-card-subtitle>{{ stop.address }}</ion-card-subtitle>\n          <ion-card-subtitle>{{ stop.state }}</ion-card-subtitle>\n          <div class=\"container-btn\">\n\n            <ion-button (click)=\"openModal(stop.uid, stop)\" class=\"btn green-btn\">\n              Editar\n            </ion-button>\n         \n            <ion-button (click)=\"delete(stop.uid)\" class=\"btn red\">\n              Eliminar\n            </ion-button>\n\n          </div>\n        \n        </ion-card-header>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n\n</ion-content>\n");

/***/ }),

/***/ "mvMU":
/*!***********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/stops/stops-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: StopsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StopsPageRoutingModule", function() { return StopsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _stops_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./stops.page */ "p4Ce");




const routes = [
    {
        path: '',
        component: _stops_page__WEBPACK_IMPORTED_MODULE_3__["StopsPage"]
    }
];
let StopsPageRoutingModule = class StopsPageRoutingModule {
};
StopsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], StopsPageRoutingModule);



/***/ }),

/***/ "p4Ce":
/*!*************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/stops/stops.page.ts ***!
  \*************************************************************/
/*! exports provided: StopsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StopsPage", function() { return StopsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_stops_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./stops.page.html */ "YIJU");
/* harmony import */ var _stops_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./stops.page.scss */ "+aec");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _components_stop_update_stop_update_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/stop-update/stop-update.component */ "OvAv");
/* harmony import */ var _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/admin/stops.service */ "/+Y6");



/// <reference path="../../../../../../node_modules/@types/googlemaps/index.d.ts" />




let StopsPage = class StopsPage {
    constructor(stopsService, alertService) {
        this.stopsService = stopsService;
        this.alertService = alertService;
        this.observableList = [];
        this.stops = [];
    }
    ngOnInit() {
        // this.stops = this.stopsService.read();
    }
    ionViewWillEnter() {
        this.getData();
    }
    getData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            /*     this.stops = (await this.getListStops()) as Stop[]; */
            yield this.getListStops();
        });
    }
    getListStops() {
        return new Promise((resolve) => {
            const observable = this.stopsService.getStop().subscribe((res) => {
                this.stops = res;
                resolve(this.stops);
            });
            this.observableList.push(observable);
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    delete(stopId) {
        this.alertService
            .presentAlertConfirm('Paradas!', 'Esta seguro de borrar la parada?')
            .then((result) => {
            if (result) {
                this.stopsService.deleteStop(stopId);
            }
        })
            .catch((result) => {
            this.alertService.presentAlert('Lo sentimos no se pudo borrar la parada!');
        });
    }
    openModal(stopId, stop) {
        this.alertService.presentModal(_components_stop_update_stop_update_component__WEBPACK_IMPORTED_MODULE_5__["StopUpdateComponent"], {
            stopId: stopId,
            address: stop.address,
            name: stop.name,
            description: stop.description,
            city: stop.city,
            provinces: stop.provinces,
            lat: stop.lat,
            lng: stop.lng,
            state: stop.state,
            isActive: stop.isActive,
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
StopsPage.ctorParameters = () => [
    { type: _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_6__["StopsService"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] }
];
StopsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-stops',
        template: _raw_loader_stops_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_stops_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], StopsPage);



/***/ })

}]);
//# sourceMappingURL=pages-stops-stops-module.js.map