(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-modals-travel-details-modal-travel-details-modal-module~pages-travel-details-travel-de~ec48b730"],{

/***/ "6vdA":
/*!****************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/travel-details-modal/travel-details-modal.page.scss ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card {\n  padding: 10px;\n  margin: 10px 5px;\n  border-radius: 15px;\n}\nion-card ion-radio-group {\n  display: flex;\n}\nion-card ion-radio-group ion-checkbox {\n  --background:#F2F2F2;\n  --box-shadow: inset 0px 4px 15px rgba(0, 0, 0, 0.14);\n  --border-radius: 8px;\n  width: 33.33px;\n  height: 30px;\n  --background-checked:#F2F2F2;\n  --border-color-checked:#008D36;\n  --checkmark-width:2;\n  --checkmark-color:#008D36;\n}\n.container-info {\n  padding: 0px 12px;\n}\n.container-info p {\n  margin: 3px 0px;\n}\n.container-info .container-card {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n}\n.container-info .container-card div {\n  border: 1px solid #DAE0E6;\n  border-radius: 6px;\n}\n.container-info .time {\n  color: #008D36;\n  font-weight: bold;\n}\n.container-info .time-title {\n  color: #97ADB6;\n}\n.container-check {\n  display: flex;\n  justify-content: space-between;\n}\n.container-check div {\n  display: flex;\n  align-items: center;\n}\n.container-check div img {\n  margin-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3RyYXZlbC1kZXRhaWxzLW1vZGFsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBQ0o7QUFBSTtFQUNJLGFBQUE7QUFFUjtBQURRO0VBQ0ksb0JBQUE7RUFDQSxvREFBQTtFQUNBLG9CQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSw0QkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtBQUdaO0FBQ0E7RUFDSSxpQkFBQTtBQUVKO0FBREk7RUFDSSxlQUFBO0FBR1I7QUFESTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FBR1I7QUFGUTtFQUNJLHlCQUFBO0VBQ0Esa0JBQUE7QUFJWjtBQURJO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0FBR1I7QUFBSTtFQUNJLGNBQUE7QUFFUjtBQUdBO0VBQ0ksYUFBQTtFQUNBLDhCQUFBO0FBQUo7QUFDSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQUNSO0FBQVE7RUFDSSxrQkFBQTtBQUVaIiwiZmlsZSI6InRyYXZlbC1kZXRhaWxzLW1vZGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJke1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgbWFyZ2luOiAxMHB4IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIGlvbi1yYWRpby1ncm91cHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgaW9uLWNoZWNrYm94e1xuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiNGMkYyRjI7XG4gICAgICAgICAgICAtLWJveC1zaGFkb3c6IGluc2V0IDBweCA0cHggMTVweCByZ2JhKDAsIDAsIDAsIDAuMTQpO1xuICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICAgICAgICB3aWR0aDogMzMuMzNweDtcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZC1jaGVja2VkOiNGMkYyRjI7XG4gICAgICAgICAgICAtLWJvcmRlci1jb2xvci1jaGVja2VkOiMwMDhEMzY7XG4gICAgICAgICAgICAtLWNoZWNrbWFyay13aWR0aDoyO1xuICAgICAgICAgICAgLS1jaGVja21hcmstY29sb3I6IzAwOEQzNjtcbiAgICAgICAgfVxuICAgIH1cbn1cbi5jb250YWluZXItaW5mb3tcbiAgICBwYWRkaW5nOiAwcHggMTJweDtcbiAgICBwe1xuICAgICAgICBtYXJnaW46IDNweCAwcHg7XG4gICAgfVxuICAgIC5jb250YWluZXItY2FyZHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBkaXZ7XG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjREFFMEU2O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNnB4O1xuICAgICAgICB9XG4gICAgfVxuICAgIC50aW1le1xuICAgICAgICBjb2xvcjojMDA4RDM2OyBcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgfVxuXG4gICAgLnRpbWUtdGl0bGV7XG4gICAgICAgIGNvbG9yOiAjOTdBREI2O1xuICAgIH1cbn1cblxuXG4uY29udGFpbmVyLWNoZWNre1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIGRpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgaW1ne1xuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgICAgICB9XG4gICAgfVxuIFxufSJdfQ== */");

/***/ }),

/***/ "8kji":
/*!*************************************************!*\
  !*** ./src/app/shared/services/push.service.ts ***!
  \*************************************************/
/*! exports provided: PushService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PushService", function() { return PushService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "wljF");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





let PushService = class PushService {
    constructor(oneSignal, http, router) {
        this.oneSignal = oneSignal;
        this.http = http;
        this.router = router;
    }
    configuracionInicial(uid) {
        /*     this.oneSignal. */
        this.oneSignal.setExternalUserId(uid);
        this.oneSignal.startInit('4f3b79de-e102-47ff-a1ac-f7753796d478', '796178968721');
        this.oneSignal.enableSound(true);
        this.oneSignal.enableVibrate(true);
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
        this.oneSignal.handleNotificationReceived().subscribe((noti) => {
            // Cuando recibe
            this.ext = noti.payload.additionalData['ruta'] + '';
        });
        this.oneSignal.handleNotificationOpened().subscribe((noti) => {
            // Cuando abre una notifiacion
            // this.router.navigate(['/notice/'+this.ext]);
            this.router.navigate([this.ext]);
        });
        //obtener uid de onsignal
        this.oneSignal.getIds().then((info) => {
            this.useId = info.userId;
        });
        this.oneSignal.endInit();
    }
    //Enviar  notificaciones individuales
    sendByUid(title, subtitle, detalle, ruta, uiduser) {
        //dato a mandar
        const datos = {
            app_id: '4f3b79de-e102-47ff-a1ac-f7753796d478',
            included_segments: ['Active User', 'Inactive User'],
            headings: { en: title },
            subtitle: { en: subtitle },
            data: { ruta: ruta, fecha: '100-210-250' },
            contents: { en: detalle },
            channel_for_external_user_ids: 'push',
            include_external_user_ids: [uiduser],
        };
        this.post(datos);
    }
    deleteuser() {
        this.oneSignal.removeExternalUserId();
    }
    //Enviar  notificaciones Global
    sendGlobal(title, subtitle, detalle, ruta) {
        //dato a mandar
        const datos = {
            app_id: '4f3b79de-e102-47ff-a1ac-f7753796d478',
            included_segments: ['Active Users', 'Inactive Users'],
            headings: { en: title },
            subtitle: { en: subtitle },
            data: { ruta: ruta, fecha: '100-210-250' },
            contents: { en: detalle },
        };
        this.post(datos);
    }
    post(datos) {
        const headers = {
            Authorization: 'Basic ODM0ODMzNzQtZjIzNi00NWU1LTk3ZDMtZmI1NmI3ZDhjZTFk',
        };
        var url = 'https://onesignal.com/api/v1/notifications';
        return new Promise((resolve) => {
            this.http.post(url, datos, { headers }).subscribe((data) => {
                resolve(data);
            });
        });
    }
};
PushService.ctorParameters = () => [
    { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_2__["OneSignal"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
PushService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], PushService);



/***/ }),

/***/ "L3lH":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/travel-details-modal/travel-details-modal.page.ts ***!
  \**************************************************************************************************/
/*! exports provided: TravelDetailsModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelDetailsModalPage", function() { return TravelDetailsModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_travel_details_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./travel-details-modal.page.html */ "X7vM");
/* harmony import */ var _travel_details_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./travel-details-modal.page.scss */ "6vdA");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../shared/services/maps.service */ "Y0mc");
/* harmony import */ var _shared_services_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../shared/services/storage.service */ "fbMX");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/admin/travels.service */ "KlPZ");
/* harmony import */ var _core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../core/authentication/authentication.service */ "6CRC");
/* harmony import */ var _shared_services_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../shared/services/shipping-parameters.service */ "jNID");
/* harmony import */ var src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/modules/home/services/users.service */ "6JOU");
/* harmony import */ var src_app_shared_services_push_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/shared/services/push.service */ "8kji");













let TravelDetailsModalPage = class TravelDetailsModalPage {
    constructor(ShippingParametersService, alertsService, router, mapsService, travelsService, authenticationService, pushService, userService, storageService) {
        this.ShippingParametersService = ShippingParametersService;
        this.alertsService = alertsService;
        this.router = router;
        this.mapsService = mapsService;
        this.travelsService = travelsService;
        this.authenticationService = authenticationService;
        this.pushService = pushService;
        this.userService = userService;
        this.storageService = storageService;
        this.origin = { lat: 0, lng: 0 };
        this.destination = { lat: 0, lng: 0 };
        this.numbersEmergency = [];
        this.observableList = [];
    }
    ngOnInit() {
        this.getParameters();
    }
    ionViewWillEnter() {
        this.loadData();
    }
    getParameters() {
        this.ShippingParametersService.getCollection().subscribe((res) => {
            this.numbersEmergency = res;
        });
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.getTravel();
            this.origin.lat = this.travel.startLocation.lat;
            this.origin.lng = this.travel.startLocation.lng;
            this.destination.lat = this.travel.destinyLocation.lat;
            this.destination.lng = this.travel.destinyLocation.lng;
            this.calculateRoute();
            this.travel.allowPets = false;
            this.travel.allowLuggage = false;
            this.travel.allowSmok = false;
            this.getUserDriver();
        });
    }
    calculateRoute() {
        this.mapsService.directionsService.route({
            // paramentros de la ruta  inicial y final
            origin: this.origin,
            destination: this.destination,
            // Que  vehiculo se usa  para realizar el viaje
            travelMode: google.maps.TravelMode.DRIVING,
        }, (response, status) => {
            const sDistance = response.routes[0].legs[0].distance.text;
            this.duration = response.routes[0].legs[0].duration.text;
            this.price =
                this.numbersEmergency[0].baseCost +
                    this.getExtraPrice(parseFloat(sDistance), +this.numbersEmergency[0].baseKilometers, +this.numbersEmergency[0].extraKilometerCost);
            this.percentageVip = this.numbersEmergency[0].extraVip * this.price / 100;
            this.travel.cost = (this.travel.vehicleType === 'VIP') ? (this.price + this.percentageVip) : this.price;
            this.wholePartPrice = Math.trunc(this.travel.cost);
            this.decimalPartPrice = (this.travel.cost.toFixed(2)).split('.')[1];
            this.travel.clientId = this.authenticationService.getCurrentUserId();
            this.travel.status = 'pending';
            if (status === google.maps.DirectionsStatus.OK) {
                this.mapsService.directionsDisplay.setDirections(response);
            }
            else {
                //  alert('Could not display directions due to: ' + status);
            }
        });
    }
    getUserDriver() {
        const observable = this.userService
            .getUsersByRole('driver')
            .subscribe((res) => {
            this.users = res;
            console.log(this.users);
        });
        this.observableList.push(observable);
    }
    getExtraPrice(kilometers, baseKilometers, price) {
        return (kilometers - baseKilometers) * price;
    }
    getTravel() {
        return new Promise((resolve) => {
            this.storageService.get('travel').then((travel) => {
                this.travel = JSON.parse(travel);
                resolve(this.travel);
            });
        });
    }
    goToSearchDriver(page) {
        this.alertsService.presentLoading('Confirmando viaje...');
        this.travelsService.add(this.travel)
            .then((result) => {
            this.alertsService.dismissLoading();
            this.router.navigate([`/dashboard/${page}/${result.id}`]);
            this.users.map((res) => {
                this.pushService.sendByUid('IGO', 'Se ha generado una solicitud por parte de un cliente.', 'Se ha generado una solicitud por parte de un cliente.', 'dashboard/requests', res.uid);
            });
            this.alertsService.closeModal();
        })
            .catch((error) => {
            this.alertsService.dismissLoading();
            this.alertsService.presentAlertWithHeader('Detalles del viaje', 'Lo sentimos vuelva a intentarlo');
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
TravelDetailsModalPage.ctorParameters = () => [
    { type: _shared_services_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_10__["ShippingParametersService"] },
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__["MapsService"] },
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_8__["TravelsService"] },
    { type: _core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_9__["AuthenticationService"] },
    { type: src_app_shared_services_push_service__WEBPACK_IMPORTED_MODULE_12__["PushService"] },
    { type: src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_11__["UsersService"] },
    { type: _shared_services_storage_service__WEBPACK_IMPORTED_MODULE_7__["StorageService"] }
];
TravelDetailsModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-travel-details-modal',
        template: _raw_loader_travel_details_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_travel_details_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TravelDetailsModalPage);



/***/ }),

/***/ "X7vM":
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/travel-details-modal/travel-details-modal.page.html ***!
  \******************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-close-modal-button (click)=\"alertsService.closeModal()\"></app-close-modal-button> <ion-content class=\"ion-padding\">\n  <ion-grid>\n    <app-travel-detail-card [wholePartPrice]=\"wholePartPrice\" [decimalPartPrice]=\"decimalPartPrice\" vehicleType=\"{{ travel?.vehicleType }}\" [duration]=\"duration\"></app-travel-detail-card>\n     <ion-card *ngIf=\"travel?.vehicleType === 'VIP'\">\n      <ion-list>\n        <ion-radio-group value=\"biff\" allow-empty-selection=true class=\"container-check\"> \n          <div>\n            <img src=\"../../../../../../assets/img/icon-pets.svg\" alt=\"\">\n            <ion-checkbox slot=\"end\" [(ngModel)]=\"travel.allowPets\"></ion-checkbox>\n          </div>\n        \n          <div>\n            <img src=\"../../../../../../assets/img/icon-back.svg\" alt=\"\">\n            <ion-checkbox slot=\"end\" [(ngModel)]=\"travel.allowLuggage\"></ion-checkbox>\n          </div>\n     \n          <div>\n            <img src=\"../../../../../../assets/img/icon-smoking.svg\" alt=\"\">\n            <ion-checkbox slot=\"end\" [(ngModel)]=\"travel.allowSmok\"></ion-checkbox>\n          </div>\n\n         \n        </ion-radio-group>\n      </ion-list>\n     </ion-card>\n     <ion-row class=\"container-info\">\n       <ion-col size=\"8\">\n         <p class=\"time-title\">Tiempo de viaje estimado</p>\n         <p class=\"time\">{{ duration }}</p>\n       </ion-col>\n       <!--<ion-col size=\"4\" class=\"container-card\">-->\n         <!--<div class=\"container-img\">-->\n          <!--<img src=\"../../../../../../assets/img/card.svg\" alt=\"\">-->\n         <!--</div>-->\n         <!--<p>**** 8295</p>-->\n      <!--</ion-col>-->\n     </ion-row>\n     <ion-row>\n      <ion-col size=\"12\">\n        <app-button title=\"Confirmar Viaje\" (pressButton)=\"goToSearchDriver('search-driver')\"></app-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=default~pages-modals-travel-details-modal-travel-details-modal-module~pages-travel-details-travel-de~ec48b730.js.map