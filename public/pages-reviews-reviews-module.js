(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-reviews-reviews-module"],{

/***/ "DlSe":
/*!*******************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/reviews/reviews.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n/* Estilos del header */\nion-toolbar ion-row {\n  align-items: center;\n  display: flex;\n  justify-content: center;\n  width: 85%;\n}\nion-toolbar ion-row ion-title {\n  font-size: 20px;\n  font-weight: bold;\n  --color: #3E4958;\n  text-align: center;\n}\n/* Estilos del contenedor de toda la pagina */\n.container {\n  margin-top: 10%;\n  padding: 0px 25px;\n}\n/* Contenedor de imagen del usuario */\n.container-img {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.container-img .img {\n  width: 40px;\n  height: 40px;\n  border-radius: 100%;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n}\n/* Contenedor del nombre de usuario y estrellas de calificación */\n.container-user-name p {\n  margin: 5px 0px;\n}\n.container-user-name .container-starts ion-icon {\n  font-size: 20px;\n  margin-right: 9px;\n}\n.container-time {\n  display: flex;\n  justify-content: flex-end;\n}\n.container-time p {\n  margin: 5px 0px;\n  font-size: 12px;\n  color: #7F7F7F;\n}\n.commentary {\n  margin: 5px 0px;\n  font-size: 15px;\n}\n/* Color verde para algunos elementos */\n.green {\n  color: #008D36;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Jldmlld3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQUFoQix1QkFBQTtBQUVJO0VBQ0UsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxVQUFBO0FBQ047QUFDTTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFDUjtBQUtFLDZDQUFBO0FBRUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUFISjtBQU9FLHFDQUFBO0FBQ0E7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQUpKO0FBTUk7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0FBSk47QUFRRSxpRUFBQTtBQUVFO0VBQ0UsZUFBQTtBQU5OO0FBVU07RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUFSUjtBQWFFO0VBQ0UsYUFBQTtFQUNBLHlCQUFBO0FBVko7QUFZSTtFQUNFLGVBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQVZOO0FBY0U7RUFFRSxlQUFBO0VBQ0EsZUFBQTtBQVpKO0FBZ0JFLHVDQUFBO0FBQ0E7RUFDRSxjQUFBO0FBYkoiLCJmaWxlIjoicmV2aWV3cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBFc3RpbG9zIGRlbCBoZWFkZXIgKi9cbmlvbi10b29sYmFyIHtcbiAgICBpb24tcm93IHtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICB3aWR0aDogODUlO1xuICBcbiAgICAgIGlvbi10aXRsZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIC0tY29sb3I6ICMzRTQ5NTg7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgXG4gICAgICB9XG4gICAgfVxuICB9XG4gIFxuICAvKiBFc3RpbG9zIGRlbCBjb250ZW5lZG9yIGRlIHRvZGEgbGEgcGFnaW5hICovXG4gIFxuICAuY29udGFpbmVyIHtcbiAgICBtYXJnaW4tdG9wOiAxMCU7XG4gICAgcGFkZGluZzogMHB4IDI1cHg7XG4gIH1cbiAgXG4gIFxuICAvKiBDb250ZW5lZG9yIGRlIGltYWdlbiBkZWwgdXN1YXJpbyAqL1xuICAuY29udGFpbmVyLWltZyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBcbiAgICAuaW1nIHtcbiAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIH1cbiAgfVxuICBcbiAgLyogQ29udGVuZWRvciBkZWwgbm9tYnJlIGRlIHVzdWFyaW8geSBlc3RyZWxsYXMgZGUgY2FsaWZpY2FjacOzbiAqL1xuICAuY29udGFpbmVyLXVzZXItbmFtZSB7XG4gICAgcCB7XG4gICAgICBtYXJnaW46IDVweCAwcHg7XG4gICAgfVxuICBcbiAgICAuY29udGFpbmVyLXN0YXJ0cyB7XG4gICAgICBpb24taWNvbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA5cHg7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIFxuICAuY29udGFpbmVyLXRpbWUge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgXG4gICAgcCB7XG4gICAgICBtYXJnaW46IDVweCAwcHg7XG4gICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICBjb2xvcjogIzdGN0Y3RjtcbiAgICB9XG4gIH1cbiAgXG4gIC5jb21tZW50YXJ5IHtcbiAgXG4gICAgbWFyZ2luOiA1cHggMHB4O1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgfVxuICBcbiAgXG4gIC8qIENvbG9yIHZlcmRlIHBhcmEgYWxndW5vcyBlbGVtZW50b3MgKi9cbiAgLmdyZWVuIHtcbiAgICBjb2xvcjogIzAwOEQzNjtcbiAgfVxuICAiXX0= */");

/***/ }),

/***/ "IKiX":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/reviews/reviews.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons class=\"margin-left\" slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons>\n    <ion-row>\n      <ion-title>Reseñas</ion-title>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n\n  <div class=\"container\">\n    <ion-row>\n      <ion-col size=\"2\" class=\"container-img\">\n        <div class=\"img\" style=\"background-image: url(../../assets/img/user.jpg);\">\n        </div>\n      </ion-col>\n      <ion-col size=6 class=\"container-user-name\">\n        <p>Nombre del usuario</p>\n        <div class=\"container-starts\">\n          <ion-icon class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon name=\"star-outline\"></ion-icon>\n        </div>\n\n      </ion-col>\n      <ion-col class=\"container-time\" size=4>\n        <p>1 Month ago</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"2\">\n\n      </ion-col>\n      <ion-col size=\"10\">\n        <p class=\"commentary\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro quam, laboriosam libero ab, \n          dolore qui illo cum accusantium magni modi labore architecto veniam, \n          omnis quas magnam perspiciatis ex sed blanditiis.</p>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"2\" class=\"container-img\">\n        <div class=\"img\" style=\"background-image: url(../../assets/img/user.jpg);\">\n        </div>\n      </ion-col>\n      <ion-col size=6 class=\"container-user-name\">\n        <p>Nombre del usuario</p>\n        <div class=\"container-starts\">\n          <ion-icon class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon name=\"star-outline\"></ion-icon>\n        </div>\n\n      </ion-col>\n      <ion-col class=\"container-time\" size=4>\n        <p>1 Month ago</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"2\">\n\n      </ion-col>\n      <ion-col size=\"10\">\n        <p class=\"commentary\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro quam, laboriosam libero ab, \n          dolore qui illo cum accusantium magni modi labore architecto veniam, \n          omnis quas magnam perspiciatis ex sed blanditiis.</p>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"2\" class=\"container-img\">\n        <div class=\"img\" style=\"background-image: url(../../assets/img/user.jpg);\">\n        </div>\n      </ion-col>\n      <ion-col size=6 class=\"container-user-name\">\n        <p>Nombre del usuario</p>\n        <div class=\"container-starts\">\n          <ion-icon class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n          <ion-icon name=\"star-outline\"></ion-icon>\n        </div>\n\n      </ion-col>\n      <ion-col class=\"container-time\" size=4>\n        <p>1 Month ago</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"2\">\n\n      </ion-col>\n      <ion-col size=\"10\">\n        <p class=\"commentary\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro quam, laboriosam libero ab, \n          dolore qui illo cum accusantium magni modi labore architecto veniam, \n          omnis quas magnam perspiciatis ex sed blanditiis.</p>\n      </ion-col>\n    </ion-row>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "NQsq":
/*!*******************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/reviews/reviews.module.ts ***!
  \*******************************************************************/
/*! exports provided: ReviewsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReviewsPageModule", function() { return ReviewsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _reviews_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./reviews-routing.module */ "lFOn");
/* harmony import */ var _reviews_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reviews.page */ "wv6V");







let ReviewsPageModule = class ReviewsPageModule {
};
ReviewsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _reviews_routing_module__WEBPACK_IMPORTED_MODULE_5__["ReviewsPageRoutingModule"]
        ],
        declarations: [_reviews_page__WEBPACK_IMPORTED_MODULE_6__["ReviewsPage"]]
    })
], ReviewsPageModule);



/***/ }),

/***/ "lFOn":
/*!***************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/reviews/reviews-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: ReviewsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReviewsPageRoutingModule", function() { return ReviewsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _reviews_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./reviews.page */ "wv6V");




const routes = [
    {
        path: '',
        component: _reviews_page__WEBPACK_IMPORTED_MODULE_3__["ReviewsPage"]
    }
];
let ReviewsPageRoutingModule = class ReviewsPageRoutingModule {
};
ReviewsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ReviewsPageRoutingModule);



/***/ }),

/***/ "wv6V":
/*!*****************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/reviews/reviews.page.ts ***!
  \*****************************************************************/
/*! exports provided: ReviewsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReviewsPage", function() { return ReviewsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_reviews_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./reviews.page.html */ "IKiX");
/* harmony import */ var _reviews_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./reviews.page.scss */ "DlSe");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let ReviewsPage = class ReviewsPage {
    constructor() { }
    ngOnInit() {
    }
};
ReviewsPage.ctorParameters = () => [];
ReviewsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-reviews',
        template: _raw_loader_reviews_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_reviews_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ReviewsPage);



/***/ })

}]);
//# sourceMappingURL=pages-reviews-reviews-module.js.map