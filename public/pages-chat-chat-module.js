(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-chat-chat-module"],{

/***/ "0bPw":
/*!*********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/chat/chat-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: ChatPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageRoutingModule", function() { return ChatPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chat.page */ "wlap");




const routes = [
    {
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_3__["ChatPage"]
    }
];
let ChatPageRoutingModule = class ChatPageRoutingModule {
};
ChatPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ChatPageRoutingModule);



/***/ }),

/***/ "KfNg":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/chat/chat.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header  title=\"{{ (user | async)?.name }}\"></app-header>\n\n   <!-- <ion-content class=\"ion-padding\">\n    <div class=\"container\">\n      <div class=\"chat-container\">\n        <ion-icon name=\"chatbubbles\"></ion-icon>\n        <p>No hay mensajes aun</p>\n      </div>\n    </div>\n  </ion-content>\n  <ion-footer>\n    <ion-toolbar color=\"light\">\n      <ion-row class=\"ion-align-items-center\">\n        <ion-col size=\"10\">\n          <ion-textarea autoGrow=\"true\" class=\"message-input\" rows=\"1\" maxLength=\"500\">\n          </ion-textarea>\n        </ion-col>\n        <ion-col size=\"2\">\n          <ion-button expand=\"block\" fill=\"clear\" color=\"primary\" class=\"msg-btn\">\n            <ion-icon name=\"send\" slot=\"icon-only\"></ion-icon>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-toolbar>\n  </ion-footer>  -->\n  \n<ion-content class=\"ion-padding\">\n  <ion-grid>\n    <ion-row *ngFor=\"let message of messages | async\">\n      <ion-col size=\"9\" class=\"message\"\n        [offset]=\"message.myMsg ? 3 : 0\"\n        [ngClass]=\"{ 'my-message': message.myMsg, 'other-message': !message.myMsg }\">\n        <span>{{ message.msg }}</span>\n        <div class=\"time\"><br>{{ message.createdAt?.toMillis() | date:'short' }}</div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar color=\"light\">\n    <ion-row class=\"ion-align-items-center\">\n      <ion-col size=\"10\">\n        <ion-textarea autoGrow=\"true\" class=\"message-input\" rows=\"1\" maxLength=\"500\" [(ngModel)]=\"newMsg\" >\n        </ion-textarea>\n      </ion-col>\n      <ion-col size=\"2\">\n        <ion-button expand=\"block\" fill=\"clear\" color=\"primary\" [disabled]=\"newMsg === ''\"\n          class=\"msg-btn\" (click)=\"sendMessage()\">\n          <ion-icon name=\"send\" slot=\"icon-only\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>\n\n\n");

/***/ }),

/***/ "UBAY":
/*!*************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/chat/chat.module.ts ***!
  \*************************************************************/
/*! exports provided: ChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chat-routing.module */ "0bPw");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat.page */ "wlap");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let ChatPageModule = class ChatPageModule {
};
ChatPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChatPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]]
    })
], ChatPageModule);



/***/ }),

/***/ "ZREo":
/*!*************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/chat/chat.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\nion-icon {\n  width: 213px;\n  height: 184px;\n  color: #008D36;\n}\n\np {\n  text-align: center;\n  font-weight: normal;\n  font-size: 15px;\n  color: #008D36;\n  line-height: 136%;\n}\n\n.message-input {\n  width: 100%;\n  border: 1px solid var(--ion-color-medium);\n  border-radius: 6px;\n  background: #fff;\n  resize: none;\n  margin-top: 0px;\n  --padding-start: 8px;\n}\n\nion-content, ion-toolbar {\n  --background: #ffffff;\n}\n\n.style-messenge {\n  left: 100%;\n  right: -100%;\n  top: 0%;\n  bottom: 33.33%;\n  background: #F7F8F9;\n  border-radius: 30px;\n  border: solid #F7F8F9 1em;\n}\n\n.style-messenge-send {\n  background: #008D36;\n  border-radius: 30px 0px 30px 30px;\n  border: solid #008D36 1em;\n}\n\n.message-input {\n  width: 100%;\n  border: 1px solid var(--ion-color-medium);\n  border-radius: 6px;\n  background: #fff;\n  resize: none;\n  margin-top: 0px;\n  --padding-start: 8px;\n}\n\n.my-message {\n  background: #008D36;\n  color: #fff;\n  padding: 10px !important;\n  /* border-radius: 1pc; */\n  border-radius: 20px 0px 20px 20px;\n  margin-bottom: 4px !important;\n  white-space: pre-wrap;\n}\n\n.other-message {\n  background: #F7F8F9;\n  color: #3E4958;\n  padding: 10px !important;\n  /* border-radius: 1pc; */\n  border-radius: 20px 20px 20px 0px;\n  margin-bottom: 4px !important;\n  white-space: pre-wrap;\n}\n\n.time {\n  color: #dfdfdf;\n  float: right;\n  font-size: small;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2NoYXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxZQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUFDRDs7QUFFQTtFQUNDLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FBQ0Q7O0FBQ0E7RUFDSSxXQUFBO0VBQ0EseUNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtBQUVKOztBQUdBO0VBQ0kscUJBQUE7QUFBSjs7QUFHQTtFQUNDLFVBQUE7RUFDQSxZQUFBO0VBQ0EsT0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7QUFBRDs7QUFHQTtFQUNDLG1CQUFBO0VBQ0EsaUNBQUE7RUFDQSx5QkFBQTtBQUFEOztBQUdBO0VBQ0ksV0FBQTtFQUNBLHlDQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7QUFBSjs7QUFHQTtFQUNDLG1CQUFBO0VBQ0csV0FBQTtFQUNBLHdCQUFBO0VBQ0Esd0JBQUE7RUFDQSxpQ0FBQTtFQUNBLDZCQUFBO0VBQ0EscUJBQUE7QUFBSjs7QUFHQTtFQUNDLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLHdCQUFBO0VBQ0csd0JBQUE7RUFDSCxpQ0FBQTtFQUNBLDZCQUFBO0VBQ0cscUJBQUE7QUFBSjs7QUFHQTtFQUNJLGNBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFBSiIsImZpbGUiOiJjaGF0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXIge1xuXHRoZWlnaHQ6IDEwMCU7XG5cdGRpc3BsYXk6IGZsZXg7XG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG5pb24taWNvbiB7XG5cdHdpZHRoOiAyMTNweDtcblx0aGVpZ2h0OiAxODRweDtcblx0Y29sb3I6ICMwMDhEMzY7XG59XG5cbnAge1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdGZvbnQtd2VpZ2h0OiBub3JtYWw7XG5cdGZvbnQtc2l6ZTogMTVweDtcblx0Y29sb3I6ICMwMDhEMzY7XG5cdGxpbmUtaGVpZ2h0OiAxMzYlO1xufVxuLm1lc3NhZ2UtaW5wdXQge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIHJlc2l6ZTogbm9uZTtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiA4cHg7XG59XG5cblxuXG5pb24tY29udGVudCxpb24tdG9vbGJhciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4uc3R5bGUtbWVzc2VuZ2Uge1xuXHRsZWZ0OiAxMDAlO1xuXHRyaWdodDogLTEwMCU7XG5cdHRvcDogMCU7XG5cdGJvdHRvbTogMzMuMzMlO1xuXHRiYWNrZ3JvdW5kOiAjRjdGOEY5O1xuXHRib3JkZXItcmFkaXVzOiAzMHB4O1xuXHRib3JkZXI6IHNvbGlkICNGN0Y4RjkgMWVtO1xufVxuXG4uc3R5bGUtbWVzc2VuZ2Utc2VuZCB7XG5cdGJhY2tncm91bmQ6ICMwMDhEMzY7XG5cdGJvcmRlci1yYWRpdXM6IDMwcHggMHB4IDMwcHggMzBweDtcdFxuXHRib3JkZXI6IHNvbGlkICMwMDhEMzYgMWVtO1xufVxuXG4ubWVzc2FnZS1pbnB1dCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG4gICAgYm9yZGVyLXJhZGl1czogNnB4O1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgcmVzaXplOiBub25lO1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDhweDtcbn1cbiBcbi5teS1tZXNzYWdlIHtcblx0YmFja2dyb3VuZDogIzAwOEQzNjtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7XG4gICAgLyogYm9yZGVyLXJhZGl1czogMXBjOyAqL1xuICAgIGJvcmRlci1yYWRpdXM6IDIwcHggMHB4IDIwcHggMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiA0cHggIWltcG9ydGFudDtcbiAgICB3aGl0ZS1zcGFjZTogcHJlLXdyYXA7XG59XG4gXG4ub3RoZXItbWVzc2FnZSB7XG5cdGJhY2tncm91bmQ6ICNGN0Y4Rjk7XG5cdGNvbG9yOiAjM0U0OTU4O1xuXHRwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7XG4gICAgLyogYm9yZGVyLXJhZGl1czogMXBjOyAqL1xuXHRib3JkZXItcmFkaXVzOiAyMHB4IDIwcHggMjBweCAwcHg7XG5cdG1hcmdpbi1ib3R0b206IDRweCAhaW1wb3J0YW50O1xuICAgIHdoaXRlLXNwYWNlOiBwcmUtd3JhcDtcbn1cbiBcbi50aW1lIHtcbiAgICBjb2xvcjogI2RmZGZkZjtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgZm9udC1zaXplOiBzbWFsbDtcbn1cblxuIl19 */");

/***/ }),

/***/ "g3ch":
/*!******************************************************************!*\
  !*** ./src/app/modules/dashboard/services/admin/chat.service.ts ***!
  \******************************************************************/
/*! exports provided: ChatService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatService", function() { return ChatService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "UbJi");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase/app */ "Jgta");






let ChatService = class ChatService {
    constructor(afAuth, afs) {
        this.afAuth = afAuth;
        this.afs = afs;
        this.currentUser = null;
        this.afAuth.onAuthStateChanged((user) => {
            this.currentUser = user;
        });
    }
    addChatMessage(msg, transmitterEmail, receiberEmail) {
        return this.afs.collection('messages').add({
            msg: msg,
            from: this.currentUser.uid,
            createdAt: firebase_app__WEBPACK_IMPORTED_MODULE_5__["default"].firestore.FieldValue.serverTimestamp(),
            canal: transmitterEmail + '&' + receiberEmail
        });
    }
    getChatMessages(transmitterEmail, receiberEmail) {
        let users = [];
        return this.getUsers().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(res => {
            users = res;
            return this.afs.collection('messages', ref => ref.where('canal', '==', transmitterEmail + '&' + receiberEmail).orderBy('createdAt')).valueChanges({ idField: 'id' });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(messages => {
            // Get the real name for each user
            for (let m of messages) {
                m.fromName = this.getUserForMsg(m.from, users);
                m.myMsg = this.currentUser.uid === m.from;
            }
            return messages;
        }));
    }
    getUsers() {
        return this.afs.collection('users').valueChanges({ idField: 'uid' });
    }
    getUserForMsg(msgFromId, users) {
        for (let usr of users) {
            if (usr.uid == msgFromId) {
                return usr.email;
            }
        }
        return 'Deleted';
    }
};
ChatService.ctorParameters = () => [
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"] }
];
ChatService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ChatService);



/***/ }),

/***/ "wlap":
/*!***********************************************************!*\
  !*** ./src/app/modules/dashboard/pages/chat/chat.page.ts ***!
  \***********************************************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_chat_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./chat.page.html */ "KfNg");
/* harmony import */ var _chat_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chat.page.scss */ "ZREo");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/auth */ "UbJi");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_admin_chat_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/admin/chat.service */ "g3ch");









let ChatPage = class ChatPage {
    constructor(chatService, afAuth, afs, activatedRoute) {
        this.chatService = chatService;
        this.afAuth = afAuth;
        this.afs = afs;
        this.activatedRoute = activatedRoute;
        this.newMsg = '';
        this.currentUser = null;
    }
    ngOnInit() {
        this.afAuth.onAuthStateChanged((user) => {
            this.currentUser = user;
            this.userDoc = this.afs.doc('users/' + user.uid);
            this.user = this.userDoc.valueChanges();
            this.user.subscribe((currentUser) => {
                switch (currentUser.role) {
                    case 'client':
                        this.transmitterEmail = currentUser.email;
                        this.receiberEmail = this.activatedRoute.snapshot.paramMap.get('email');
                        break;
                    case 'driver':
                        this.transmitterEmail = this.activatedRoute.snapshot.paramMap.get('email');
                        this.receiberEmail = currentUser.email;
                        break;
                }
                this.messages = this.chatService.getChatMessages(this.transmitterEmail, this.receiberEmail);
                this.currentEmail = currentUser.email;
            });
        });
    }
    sendMessage() {
        this.chatService.addChatMessage(this.newMsg, this.transmitterEmail, this.receiberEmail).then(() => {
            this.newMsg = '';
            this.content.scrollToBottom();
        });
    }
};
ChatPage.ctorParameters = () => [
    { type: _services_admin_chat_service__WEBPACK_IMPORTED_MODULE_8__["ChatService"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__["AngularFireAuth"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] }
];
ChatPage.propDecorators = {
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonContent"],] }]
};
ChatPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-chat',
        template: _raw_loader_chat_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_chat_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ChatPage);



/***/ })

}]);
//# sourceMappingURL=pages-chat-chat-module.js.map