(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-select-route-modal-select-route-modal-module"],{

/***/ "BTSe":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-route-modal/select-route-modal.module.ts ***!
  \************************************************************************************************/
/*! exports provided: SelectRouteModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectRouteModalPageModule", function() { return SelectRouteModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_route_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-route-modal-routing.module */ "fzqR");
/* harmony import */ var _select_route_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-route-modal.page */ "W3XF");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");








let SelectRouteModalPageModule = class SelectRouteModalPageModule {
};
SelectRouteModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_route_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectRouteModalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_select_route_modal_page__WEBPACK_IMPORTED_MODULE_6__["SelectRouteModalPage"]]
    })
], SelectRouteModalPageModule);



/***/ }),

/***/ "fzqR":
/*!********************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-route-modal/select-route-modal-routing.module.ts ***!
  \********************************************************************************************************/
/*! exports provided: SelectRouteModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectRouteModalPageRoutingModule", function() { return SelectRouteModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_route_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-route-modal.page */ "W3XF");




const routes = [
    {
        path: '',
        component: _select_route_modal_page__WEBPACK_IMPORTED_MODULE_3__["SelectRouteModalPage"]
    }
];
let SelectRouteModalPageRoutingModule = class SelectRouteModalPageRoutingModule {
};
SelectRouteModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectRouteModalPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-select-route-modal-select-route-modal-module.js.map