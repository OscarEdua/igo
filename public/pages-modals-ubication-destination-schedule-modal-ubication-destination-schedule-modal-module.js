(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-ubication-destination-schedule-modal-ubication-destination-schedule-modal-module"],{

/***/ "cBYm":
/*!********************************************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal-routing.module.ts ***!
  \********************************************************************************************************************************************/
/*! exports provided: UbicationDestinationScheduleModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UbicationDestinationScheduleModalPageRoutingModule", function() { return UbicationDestinationScheduleModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ubication_destination_schedule_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ubication-destination-schedule-modal.page */ "IhZq");




const routes = [
    {
        path: '',
        component: _ubication_destination_schedule_modal_page__WEBPACK_IMPORTED_MODULE_3__["UbicationDestinationScheduleModalPage"]
    }
];
let UbicationDestinationScheduleModalPageRoutingModule = class UbicationDestinationScheduleModalPageRoutingModule {
};
UbicationDestinationScheduleModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UbicationDestinationScheduleModalPageRoutingModule);



/***/ }),

/***/ "gfJs":
/*!************************************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal.module.ts ***!
  \************************************************************************************************************************************/
/*! exports provided: UbicationDestinationScheduleModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UbicationDestinationScheduleModalPageModule", function() { return UbicationDestinationScheduleModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _ubication_destination_schedule_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ubication-destination-schedule-modal-routing.module */ "cBYm");
/* harmony import */ var _ubication_destination_schedule_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ubication-destination-schedule-modal.page */ "IhZq");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");








let UbicationDestinationScheduleModalPageModule = class UbicationDestinationScheduleModalPageModule {
};
UbicationDestinationScheduleModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ubication_destination_schedule_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["UbicationDestinationScheduleModalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_ubication_destination_schedule_modal_page__WEBPACK_IMPORTED_MODULE_6__["UbicationDestinationScheduleModalPage"]]
    })
], UbicationDestinationScheduleModalPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-ubication-destination-schedule-modal-ubication-destination-schedule-modal-module.js.map