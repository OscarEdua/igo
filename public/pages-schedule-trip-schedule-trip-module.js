(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-schedule-trip-schedule-trip-module"],{

/***/ "/i3L":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/schedule-trip/schedule-trip.page.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <app-principal-header title=\"Agenda tu viaje\" (openMenu)=\"openMenu()\" slot=\"fixed\"></app-principal-header>\n  <div class=\"map-container\">\n    <div #schedulemap\n    id=\"schedulemap\"></div>\n  </div>\n  <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>\n</ion-content>\n");

/***/ }),

/***/ "3Wkb":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/schedule-trip/schedule-trip.page.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilo del contenedor de la linea que cierra el modal */\n.open-modal {\n  box-shadow: 0px 0px 60px rgba(0, 0, 0, 0.2);\n  border-radius: 14px 14px 0px 0px;\n}\n.open-modal ion-row {\n  display: flex;\n  justify-content: center;\n}\n/* Estilo de la linea del modal */\n.open-modal-line {\n  width: 30px;\n  height: 5px;\n  background-color: #D5DDE0;\n  border-radius: 50px;\n}\n.map-container {\n  height: 100%;\n}\n/* Estilos de la linea que cierra el modal */\n.close-modal-line {\n  width: 30px;\n  height: 5px;\n  background-color: #D5DDE0;\n  border-radius: 50px;\n}\n#schedulemap {\n  width: 100%;\n  height: 100%;\n  opacity: 0;\n  border-radius: 5px;\n  border: 2px solid rgba(0, 0, 0, 0.288);\n  transition: opacity 150ms ease-in;\n  display: block;\n}\n#schedulemap.show-map {\n  opacity: 1;\n}\nion-toolbar {\n  --background: transparent;\n}\napp-principal-header {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NjaGVkdWxlLXRyaXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLDBEQUFBO0FBQ0E7RUFDRSwyQ0FBQTtFQUNBLGdDQUFBO0FBQUY7QUFDRTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtBQUNKO0FBR0EsaUNBQUE7QUFDQTtFQUNFLFdBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtBQUFGO0FBSUE7RUFDRSxZQUFBO0FBREY7QUFLQSw0Q0FBQTtBQUNBO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FBRkY7QUFNQztFQUNDLFdBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0Esc0NBQUE7RUFDQSxpQ0FBQTtFQUNBLGNBQUE7QUFIRjtBQUlFO0VBQ0UsVUFBQTtBQUZKO0FBTUE7RUFDRSx5QkFBQTtBQUhGO0FBTUE7RUFDRSxXQUFBO0FBSEYiLCJmaWxlIjoic2NoZWR1bGUtdHJpcC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qIEVzdGlsbyBkZWwgY29udGVuZWRvciBkZSBsYSBsaW5lYSBxdWUgY2llcnJhIGVsIG1vZGFsICovXG4ub3Blbi1tb2RhbCB7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggNjBweCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gIGJvcmRlci1yYWRpdXM6IDE0cHggMTRweCAwcHggMHB4O1xuICBpb24tcm93IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB9XG59XG5cbi8qIEVzdGlsbyBkZSBsYSBsaW5lYSBkZWwgbW9kYWwgKi9cbi5vcGVuLW1vZGFsLWxpbmUge1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNENURERTA7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG59XG5cblxuLm1hcC1jb250YWluZXIge1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cblxuLyogRXN0aWxvcyBkZSBsYSBsaW5lYSBxdWUgY2llcnJhIGVsIG1vZGFsICovXG4uY2xvc2UtbW9kYWwtbGluZSB7XG4gIHdpZHRoOiAzMHB4O1xuICBoZWlnaHQ6IDVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0Q1RERFMCA7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG59XG5cbiAvL0VzdGlsbyBkZSBtYXBhXG4gI3NjaGVkdWxlbWFwIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgb3BhY2l0eTogMDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBib3JkZXI6IDJweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMjg4KTtcbiAgdHJhbnNpdGlvbjogb3BhY2l0eSAxNTBtcyBlYXNlLWluO1xuICBkaXNwbGF5OiBibG9jaztcbiAgJi5zaG93LW1hcHtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuYXBwLXByaW5jaXBhbC1oZWFkZXIge1xuICB3aWR0aDogMTAwJTtcbn1cbiJdfQ== */");

/***/ }),

/***/ "h6Oe":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/schedule-trip/schedule-trip.page.ts ***!
  \*****************************************************************************/
/*! exports provided: ScheduleTripPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScheduleTripPage", function() { return ScheduleTripPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_schedule_trip_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./schedule-trip.page.html */ "/i3L");
/* harmony import */ var _schedule_trip_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./schedule-trip.page.scss */ "3Wkb");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _modals_schedule_trip_modal_schedule_trip_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modals/schedule-trip-modal/schedule-trip-modal.page */ "c5KJ");
/* harmony import */ var src_app_shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/maps.service */ "Y0mc");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @capacitor/core */ "gcOT");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ "TEn/");









const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_7__["Plugins"];
let ScheduleTripPage = class ScheduleTripPage {
    constructor(menuController, mapsService, alertsService) {
        this.menuController = menuController;
        this.mapsService = mapsService;
        this.alertsService = alertsService;
        this.observableList = [];
        this.markers = [];
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.loadDataPageDashboard();
    }
    loadDataPageDashboard() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.mapsService.loadmap('schedulemap');
            this.generateMarksMyLocation();
            this.openModal();
        });
    }
    generateMarksMyLocation() {
        Geolocation.getCurrentPosition().then((res) => {
            this.marker.position.lat = res.coords.latitude;
            this.marker.position.lng = res.coords.longitude;
            this.marker.title = 'Mi Ubicación';
            this.addMarker(this.marker, this.marker.title, 'myLocation');
        });
    }
    addMarker(marker, name, typeMarket) {
        let marke;
        let icon;
        if (typeMarket == 'myLocation') {
            icon = {
                url: '../../assets/img/ic_loc.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(60, 60),
            };
            /*     const image = '../../assets/img/Pineapplemenu.png'; */
        }
        else if (typeMarket == 'stops') {
            icon = {
                url: '../../assets/img/flag.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(25, 25),
            };
        }
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            //  draggable: true,
            // animation: google.maps.Animation.DROP,
            map: this.mapsService.map,
            icon: icon,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                    console.log('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
        const infoWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
                        if (infoWindow !== null) {
                        }
                        infoWindow.open(this.mapsService.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
    }
    openMenu() {
        this.menuController.open('principal');
    }
    openModal() {
        this.alertsService.presentModalWithoutTravelData(_modals_schedule_trip_modal_schedule_trip_modal_page__WEBPACK_IMPORTED_MODULE_5__["ScheduleTripModalPage"], 'medium-modal');
    }
};
ScheduleTripPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["MenuController"] },
    { type: src_app_shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__["MapsService"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] }
];
ScheduleTripPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-schedule-trip',
        template: _raw_loader_schedule_trip_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_schedule_trip_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ScheduleTripPage);



/***/ }),

/***/ "nBi3":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/schedule-trip/schedule-trip-routing.module.ts ***!
  \***************************************************************************************/
/*! exports provided: ScheduleTripPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScheduleTripPageRoutingModule", function() { return ScheduleTripPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _schedule_trip_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./schedule-trip.page */ "h6Oe");




const routes = [
    {
        path: '',
        component: _schedule_trip_page__WEBPACK_IMPORTED_MODULE_3__["ScheduleTripPage"]
    }
];
let ScheduleTripPageRoutingModule = class ScheduleTripPageRoutingModule {
};
ScheduleTripPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ScheduleTripPageRoutingModule);



/***/ }),

/***/ "qUjs":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/schedule-trip/schedule-trip.module.ts ***!
  \*******************************************************************************/
/*! exports provided: ScheduleTripPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScheduleTripPageModule", function() { return ScheduleTripPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _schedule_trip_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./schedule-trip-routing.module */ "nBi3");
/* harmony import */ var _schedule_trip_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./schedule-trip.page */ "h6Oe");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let ScheduleTripPageModule = class ScheduleTripPageModule {
};
ScheduleTripPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _schedule_trip_routing_module__WEBPACK_IMPORTED_MODULE_5__["ScheduleTripPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_schedule_trip_page__WEBPACK_IMPORTED_MODULE_6__["ScheduleTripPage"]]
    })
], ScheduleTripPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-schedule-trip-schedule-trip-module.js.map