(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-modals-select-destination-modal-select-destination-modal-module~pages-select-destinati~7073e38f"],{

/***/ "B7/t":
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/select-destination-modal/select-destination-modal.page.html ***!
  \**************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n  <app-close-modal-button (click)=\"alertsService.closeModal()\"></app-close-modal-button>\n\n<ion-content class=\"ion-padding-horizontal\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"12\">\n        <p class=\"title-modal\">Ubicacion del Destino</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <app-address-input *ngIf=\"travel?.vehicleType == 'VIP'\" [disabled]=\"inputDisabled\" [placeholder]=\"inputPlaceholder\" [(ngModel)]=\"addressStop\"></app-address-input>\n        <!--<app-address-input [text]=\"address\" [disabled]=\"inputDisabled\" [placeholder]=\"inputPlaceholder\" [(ngModel)]=\"addressStop\"></app-address-input>-->\n        <ion-searchbar *ngIf=\"travel?.vehicleType == 'Standard'\" class=\"searchbar\" inputmode=\"text\" placeholder=\"Buscar parada\" (ionChange)=\"onSearchChange($event)\">\n        </ion-searchbar>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf=\"travel?.vehicleType == 'Standard'\">\n      <ion-col size=\"12\">\n        <p class=\"subtitle-modal\">PARADAS CERCANAS</p>\n      </ion-col>\n    </ion-row>\n    <ng-container *ngIf=\"travel?.vehicleType == 'Standard'\">\n      <ion-row *ngFor=\"let stop of stops | async | filter:searchText\">\n        <ion-col size=\"12\">\n          <app-item-map city=\"{{ stop.city }}\" classes=\"icon-container green-item\" name=\"{{ stop.name }}\" description=\"{{ stop.description }}\" (chooseStop)=\"chooseStop(stop)\"></app-item-map>\n        </ion-col>\n      </ion-row>\n    </ng-container>\n    <ion-row>\n      <ion-col size=\"12\">\n        <app-button title=\"Seleccionar destino\" (pressButton)=\"selectDestination('select-route')\"></app-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n");

/***/ }),

/***/ "K+g4":
/*!************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-destination-modal/select-destination-modal.page.scss ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".subtitle-modal {\n  margin-bottom: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3NlbGVjdC1kZXN0aW5hdGlvbi1tb2RhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxrQkFBQTtBQUNEIiwiZmlsZSI6InNlbGVjdC1kZXN0aW5hdGlvbi1tb2RhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3VidGl0bGUtbW9kYWwge1xuXHRtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4iXX0= */");

/***/ }),

/***/ "mXHT":
/*!**********************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-destination-modal/select-destination-modal.page.ts ***!
  \**********************************************************************************************************/
/*! exports provided: SelectDestinationModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectDestinationModalPage", function() { return SelectDestinationModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_destination_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-destination-modal.page.html */ "B7/t");
/* harmony import */ var _select_destination_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-destination-modal.page.scss */ "K+g4");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/admin/stops.service */ "/+Y6");
/* harmony import */ var src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/services/storage.service */ "fbMX");
/* harmony import */ var _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../shared/services/maps.service */ "Y0mc");









let SelectDestinationModalPage = class SelectDestinationModalPage {
    constructor(router, alertsService, stopsService, storageService, mapsService) {
        this.router = router;
        this.alertsService = alertsService;
        this.stopsService = stopsService;
        this.storageService = storageService;
        this.mapsService = mapsService;
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.markers = [];
        this.addressStop = '';
        this.inputDisabled = true;
        this.inputPlaceholder = 'Seleccione una parada';
        this.destinityLocation = {};
        this.addressDestination = '';
    }
    ngOnInit() {
        this.stops = this.stopsService.getStop();
        this.storageService.get('travel')
            .then(travel => {
            this.travel = JSON.parse(travel);
            if (this.travel.vehicleType == 'Standard') {
            }
            else {
                this.inputDisabled = false;
                this.inputPlaceholder = 'Seleccione o ingrese una parada';
            }
        })
            .catch(error => console.log(error));
    }
    ionViewDidEnter() {
        this.loadAutocomplete();
    }
    chooseStop(stop) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.deleteMarkers();
            this.addressStop = stop.address;
            this.marker.position.lat = stop.lat;
            this.marker.position.lng = stop.lng;
            this.marker.title = stop.address;
            this.addMarker(this.marker, this.marker.title, 'stops');
            this.destinityLocation.address = stop.address;
            this.destinityLocation.lat = stop.lat;
            this.destinityLocation.lng = stop.lng;
        });
    }
    addMarker(marker, name, typeMarket) {
        let marke;
        let icon;
        if (typeMarket == 'myLocation') {
            icon = {
                url: '../../assets/img/ic_loc.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(90, 90),
            };
            /*     const image = '../../assets/img/Pineapplemenu.png'; */
        }
        else if (typeMarket == 'stops') {
            icon = {
                url: '../../assets/img/ic_dropoff.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(50, 50),
            };
        }
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            //  draggable: true,
            // animation: google.maps.Animation.DROP,
            map: this.mapsService.map,
            icon: icon,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                    console.log('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
            this.mapsService.map.setCenter(this.marker.position);
        });
        const infoWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
                        if (infoWindow !== null) {
                        }
                        infoWindow.open(this.mapsService.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
    }
    selectDestination(pageName) {
        this.storageService.get('travel')
            .then(travel => {
            this.travel = JSON.parse(travel);
            this.travel.destinyLocation = this.destinityLocation;
            const sTravel = JSON.stringify(this.travel);
            this.storageService.set('travel', sTravel);
            if (this.addressStop === '') {
                this.alertsService.presentAlertWithHeader('Ubicacion del Destino.', 'Por favor escoja o ingrese una parada de destino.');
            }
            else {
                if (this.travel.startLocation.address === this.travel.destinyLocation.address) {
                    this.alertsService.presentAlertWithHeader('Ruta de viaje', 'Por favor seleccione una parada de destino diferente a la parada de inicio.');
                }
                else {
                    this.alertsService.closeModal();
                    this.router.navigate(['dashboard/' + pageName]);
                    this.deleteMarkers();
                }
            }
        });
    }
    deleteMarkers() {
        this.markers = [];
        this.setMapOnAll(null);
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    loadAutocomplete() {
        const autocomplete = new google.maps.places.Autocomplete(document.querySelector('#autocomplete-input').getElementsByTagName('input')[0]);
        google.maps.event.addListener(autocomplete, 'place_changed', event => {
            const place = autocomplete.getPlace();
            this.destinityLocation.lat = place.geometry.location.lat();
            this.destinityLocation.lng = place.geometry.location.lng();
            this.destinityLocation.address = place.name + " " + place.formatted_address;
            this.destinityLocation.address = this.destinityLocation.address.replace('\\', '');
            this.mapsService.map.setCenter(place.geometry.location);
            const market = new google.maps.Marker({
                position: place.geometry.location,
                //animation: google.maps.Animation.DROP,
                icon: {
                    url: '../../../../../../assets/img/ic_loc.png',
                    scaledSize: new google.maps.Size(60, 60)
                }
            });
            this.mapsService.deleteMarkers();
            market.setMap(this.mapsService.map);
        });
    }
};
SelectDestinationModalPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_5__["AlertsService"] },
    { type: _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_6__["StopsService"] },
    { type: src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_7__["StorageService"] },
    { type: _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_8__["MapsService"] }
];
SelectDestinationModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-destination-modal',
        template: _raw_loader_select_destination_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_destination_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectDestinationModalPage);



/***/ })

}]);
//# sourceMappingURL=default~pages-modals-select-destination-modal-select-destination-modal-module~pages-select-destinati~7073e38f.js.map