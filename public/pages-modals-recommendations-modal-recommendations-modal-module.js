(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-recommendations-modal-recommendations-modal-module"],{

/***/ "1SQ7":
/*!**************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/recommendations-modal/recommendations-modal-routing.module.ts ***!
  \**************************************************************************************************************/
/*! exports provided: RecommendationsModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecommendationsModalPageRoutingModule", function() { return RecommendationsModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _recommendations_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./recommendations-modal.page */ "UvKr");




const routes = [
    {
        path: '',
        component: _recommendations_modal_page__WEBPACK_IMPORTED_MODULE_3__["RecommendationsModalPage"]
    }
];
let RecommendationsModalPageRoutingModule = class RecommendationsModalPageRoutingModule {
};
RecommendationsModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RecommendationsModalPageRoutingModule);



/***/ }),

/***/ "z28r":
/*!******************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/recommendations-modal/recommendations-modal.module.ts ***!
  \******************************************************************************************************/
/*! exports provided: RecommendationsModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecommendationsModalPageModule", function() { return RecommendationsModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _recommendations_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./recommendations-modal-routing.module */ "1SQ7");
/* harmony import */ var _recommendations_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./recommendations-modal.page */ "UvKr");







let RecommendationsModalPageModule = class RecommendationsModalPageModule {
};
RecommendationsModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _recommendations_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["RecommendationsModalPageRoutingModule"]
        ],
        declarations: [_recommendations_modal_page__WEBPACK_IMPORTED_MODULE_6__["RecommendationsModalPage"]]
    })
], RecommendationsModalPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-recommendations-modal-recommendations-modal-module.js.map