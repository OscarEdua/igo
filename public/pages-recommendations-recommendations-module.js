(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-recommendations-recommendations-module"],{

/***/ "AIoj":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/recommendations/recommendations.page.html ***!
  \*************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content fullscreen>\n    <app-principal-header (openMenu)=\"openMenu()\" title=\"Recomendaciones\" slot=\"fixed\"></app-principal-header>\n    <div class=\"map-container\">\n        <div #recommendations\n        id=\"recommendations-map\"></div>\n    </div>\n    <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>\n</ion-content>\n");

/***/ }),

/***/ "K+2D":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/recommendations/recommendations.page.scss ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#map {\n  height: 40%;\n}\n\n.data-container {\n  height: 60%;\n  border-radius: 0px 20px 0px 0px;\n}\n\n.avatar-container {\n  border-radius: 50%;\n  width: 116px;\n  height: 116px;\n  position: relative;\n  top: -60px;\n  vertical-align: middle;\n  display: flex;\n  align-items: center;\n}\n\nion-avatar {\n  margin: auto;\n  vertical-align: center;\n  width: 75px;\n  height: 75px;\n}\n\nh2 {\n  margin-top: 10px;\n  margin-bottom: 23px;\n}\n\n.data-details {\n  position: relative;\n  top: -70px;\n  padding-left: 24px;\n  padding-right: 24px;\n}\n\nh2 {\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 28px;\n  color: #3E4958;\n}\n\nion-icon {\n  color: #214E9D;\n  border-radius: 2px;\n  width: 36px;\n  height: 36px;\n}\n\n.stars {\n  display: flex;\n  justify-content: center;\n}\n\n.no-color {\n  color: #D5DDE0;\n}\n\np {\n  font-weight: normal;\n  font-size: 15px;\n  color: #3E4958;\n  text-align: center;\n}\n\nion-textarea {\n  border: 0.5px solid #D5DDE0;\n  border-radius: 15px;\n  background: #F7F8F9;\n  height: 134px;\n  width: 310px;\n}\n\nion-button {\n  width: 310px;\n  height: 60px;\n  --background: #008D36;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n  --border-radius: 15px;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 24px;\n  color: #FFFFFF;\n  margin-top: 24px;\n  margin-bottom: 34px;\n}\n\nion-content {\n  --background:rgba(255,255,255,0);\n}\n\n.map-container {\n  height: 100%;\n}\n\n#recommendations-map {\n  width: 100%;\n  height: 100%;\n  opacity: 0;\n  border-radius: 5px;\n  border: 2px solid rgba(0, 0, 0, 0.288);\n  transition: opacity 150ms ease-in;\n  display: block;\n}\n\n#recommendations-map.show-map {\n  opacity: 1;\n}\n\napp-principal-header {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3JlY29tbWVuZGF0aW9ucy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxXQUFBO0FBQ0Q7O0FBRUE7RUFDQyxXQUFBO0VBQ0EsK0JBQUE7QUFDRDs7QUFFQTtFQUNDLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxzQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQUNEOztBQUVBO0VBQ0MsWUFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFDRDs7QUFFQTtFQUNDLGdCQUFBO0VBQ0EsbUJBQUE7QUFDRDs7QUFFQTtFQUNDLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUFDRDs7QUFFQTtFQUNDLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUNEOztBQUVBO0VBQ0MsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFDRDs7QUFFQTtFQUNDLGFBQUE7RUFDQSx1QkFBQTtBQUNEOztBQUVBO0VBQ0MsY0FBQTtBQUNEOztBQUVBO0VBQ0MsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FBQ0Q7O0FBRUE7RUFDQywyQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQUNEOztBQUVBO0VBQ0MsWUFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLGlEQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBQ0Q7O0FBRUE7RUFDSSxnQ0FBQTtBQUNKOztBQUVBO0VBQ0MsWUFBQTtBQUNEOztBQUVDO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsY0FBQTtBQUNEOztBQUFDO0VBQ0UsVUFBQTtBQUVIOztBQUNBO0VBQ0MsV0FBQTtBQUVEIiwiZmlsZSI6InJlY29tbWVuZGF0aW9ucy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbWFwIHtcblx0aGVpZ2h0OiA0MCU7XG59XG5cbi5kYXRhLWNvbnRhaW5lciB7XG5cdGhlaWdodDogNjAlO1xuXHRib3JkZXItcmFkaXVzOiAwcHggMjBweCAwcHggMHB4O1xufVxuXG4uYXZhdGFyLWNvbnRhaW5lciB7XG5cdGJvcmRlci1yYWRpdXM6IDUwJTtcblx0d2lkdGg6IDExNnB4O1xuXHRoZWlnaHQ6IDExNnB4O1xuXHRwb3NpdGlvbjogcmVsYXRpdmU7XG5cdHRvcDogLTYwcHg7XG5cdHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG5cdGRpc3BsYXk6IGZsZXg7XG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbmlvbi1hdmF0YXIgeyBcblx0bWFyZ2luOiBhdXRvO1xuXHR2ZXJ0aWNhbC1hbGlnbjogY2VudGVyO1xuXHR3aWR0aDogNzVweDtcblx0aGVpZ2h0OiA3NXB4O1xufVxuXG5oMiB7XG5cdG1hcmdpbi10b3A6IDEwcHg7XG5cdG1hcmdpbi1ib3R0b206IDIzcHg7XG59XG5cbi5kYXRhLWRldGFpbHMge1xuXHRwb3NpdGlvbjogcmVsYXRpdmU7XG5cdHRvcDogLTcwcHg7XG5cdHBhZGRpbmctbGVmdDogMjRweDtcblx0cGFkZGluZy1yaWdodDogMjRweDtcbn1cblxuaDIge1xuXHRmb250LXdlaWdodDogYm9sZDtcblx0Zm9udC1zaXplOiAxOHB4O1xuXHRsaW5lLWhlaWdodDogMjhweDtcblx0Y29sb3I6ICMzRTQ5NTg7XG59XG5cbmlvbi1pY29uIHtcblx0Y29sb3I6ICMyMTRFOUQ7XG5cdGJvcmRlci1yYWRpdXM6IDJweDtcblx0d2lkdGg6IDM2cHg7XG5cdGhlaWdodDogMzZweDtcbn1cblxuLnN0YXJzIHsgXG5cdGRpc3BsYXk6IGZsZXg7XG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4ubm8tY29sb3Ige1xuXHRjb2xvcjogI0Q1RERFMDtcbn1cblxucCB7XG5cdGZvbnQtd2VpZ2h0OiBub3JtYWw7XG5cdGZvbnQtc2l6ZTogMTVweDtcblx0Y29sb3I6ICMzRTQ5NTg7XG5cdHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaW9uLXRleHRhcmVhIHtcblx0Ym9yZGVyOiAwLjVweCBzb2xpZCAjRDVEREUwO1xuXHRib3JkZXItcmFkaXVzOiAxNXB4O1xuXHRiYWNrZ3JvdW5kOiAjRjdGOEY5O1xuXHRoZWlnaHQ6IDEzNHB4O1xuXHR3aWR0aDogMzEwcHg7XG59XG5cbmlvbi1idXR0b24ge1xuXHR3aWR0aDogMzEwcHg7XG5cdGhlaWdodDogNjBweDtcblx0LS1iYWNrZ3JvdW5kOiAjMDA4RDM2O1xuXHQtLWJveC1zaGFkb3c6IDBweCAycHggOHB4IHJnYmEoMTYsIDEwNSwgMjI3LCAwLjMpO1xuXHQtLWJvcmRlci1yYWRpdXM6IDE1cHg7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xuXHRmb250LXNpemU6IDE4cHg7XG5cdGxpbmUtaGVpZ2h0OiAyNHB4O1xuXHRjb2xvcjogI0ZGRkZGRjtcblx0bWFyZ2luLXRvcDogMjRweDtcblx0bWFyZ2luLWJvdHRvbTogMzRweDtcbn1cblxuaW9uLWNvbnRlbnR7XG4gICAgLS1iYWNrZ3JvdW5kOnJnYmEoMjU1LDI1NSwyNTUsMCk7XG4gIH1cbiAgXG4ubWFwLWNvbnRhaW5lciB7XG5cdGhlaWdodDogMTAwJTtcbiAgfVxuIC8vRXN0aWxvIGRlIG1hcGFcbiAjcmVjb21tZW5kYXRpb25zLW1hcCB7XG5cdHdpZHRoOiAxMDAlO1xuXHRoZWlnaHQ6IDEwMCU7XG5cdG9wYWNpdHk6IDA7XG5cdGJvcmRlci1yYWRpdXM6IDVweDtcblx0Ym9yZGVyOiAycHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjI4OCk7XG5cdHRyYW5zaXRpb246IG9wYWNpdHkgMTUwbXMgZWFzZS1pbjtcblx0ZGlzcGxheTogYmxvY2s7XG5cdCYuc2hvdy1tYXB7XG5cdCAgb3BhY2l0eTogMTtcblx0fVxuICB9XG5hcHAtcHJpbmNpcGFsLWhlYWRlciB7XG5cdHdpZHRoOiAxMDAlO1xuICB9ICJdfQ== */");

/***/ }),

/***/ "OBnO":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/recommendations/recommendations.page.ts ***!
  \*********************************************************************************/
/*! exports provided: RecommendationsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecommendationsPage", function() { return RecommendationsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_recommendations_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./recommendations.page.html */ "AIoj");
/* harmony import */ var _recommendations_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./recommendations.page.scss */ "K+2D");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _modals_recommendations_modal_recommendations_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modals/recommendations-modal/recommendations-modal.page */ "UvKr");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../shared/services/maps.service */ "Y0mc");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @capacitor/core */ "gcOT");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "tyNb");








const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_8__["Plugins"];


let RecommendationsPage = class RecommendationsPage {
    constructor(menuController, alertsService, mapsService, activatedRoute) {
        this.menuController = menuController;
        this.alertsService = alertsService;
        this.mapsService = mapsService;
        this.activatedRoute = activatedRoute;
        this.markers = [];
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.loadDataPageDashboard();
    }
    loadDataPageDashboard() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                yield this.mapsService.loadmap('recommendations-map');
                this.generateMarksMyLocation();
                this.openModal();
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    generateMarksMyLocation() {
        Geolocation.getCurrentPosition().then((res) => {
            this.marker.position.lat = res.coords.latitude;
            this.marker.position.lng = res.coords.longitude;
            this.marker.title = 'Mi Ubicación';
            this.addMarker(this.marker, this.marker.title, 'myLocation');
        });
    }
    addMarker(marker, name, typeMarket) {
        let marke;
        let icon;
        if (typeMarket == 'myLocation') {
            icon = {
                url: '../../assets/img/ic_loc.png',
                //ic_loc size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(60, 60),
            };
            /*     const image = '../../assets/img/Pineapplemenu.png'; */
        }
        else if (typeMarket == 'stops') {
            icon = {
                url: '../../assets/img/flag.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(25, 25),
            };
        }
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            //  draggable: true,
            // animation: google.maps.Animation.DROP,
            map: this.mapsService.map,
            icon: icon,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                    console.log('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
        const infoWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
                        if (infoWindow !== null) {
                        }
                        infoWindow.open(this.mapsService.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
    }
    openModal() {
        this.alertsService.presentModalWithData(_modals_recommendations_modal_recommendations_modal_page__WEBPACK_IMPORTED_MODULE_5__["RecommendationsModalPage"], { travelId: this.travelId }, 'my-custom-class-recommendations');
    }
    openMenu() {
        this.menuController.open('principal');
    }
};
RecommendationsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] },
    { type: _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_7__["MapsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__["ActivatedRoute"] }
];
RecommendationsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-recommendations',
        template: _raw_loader_recommendations_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_recommendations_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RecommendationsPage);



/***/ }),

/***/ "Y1SK":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/recommendations/recommendations.module.ts ***!
  \***********************************************************************************/
/*! exports provided: RecommendationsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecommendationsPageModule", function() { return RecommendationsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _recommendations_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./recommendations-routing.module */ "d/g/");
/* harmony import */ var _recommendations_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./recommendations.page */ "OBnO");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let RecommendationsPageModule = class RecommendationsPageModule {
};
RecommendationsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _recommendations_routing_module__WEBPACK_IMPORTED_MODULE_5__["RecommendationsPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_recommendations_page__WEBPACK_IMPORTED_MODULE_6__["RecommendationsPage"]]
    })
], RecommendationsPageModule);



/***/ }),

/***/ "d/g/":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/recommendations/recommendations-routing.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: RecommendationsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecommendationsPageRoutingModule", function() { return RecommendationsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _recommendations_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./recommendations.page */ "OBnO");




const routes = [
    {
        path: '',
        component: _recommendations_page__WEBPACK_IMPORTED_MODULE_3__["RecommendationsPage"]
    }
];
let RecommendationsPageRoutingModule = class RecommendationsPageRoutingModule {
};
RecommendationsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RecommendationsPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-recommendations-recommendations-module.js.map