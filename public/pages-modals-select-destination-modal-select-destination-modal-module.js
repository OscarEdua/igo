(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-select-destination-modal-select-destination-modal-module"],{

/***/ "WsMI":
/*!************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-destination-modal/select-destination-modal.module.ts ***!
  \************************************************************************************************************/
/*! exports provided: SelectDestinationModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectDestinationModalPageModule", function() { return SelectDestinationModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_destination_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-destination-modal-routing.module */ "YCFA");
/* harmony import */ var _select_destination_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-destination-modal.page */ "mXHT");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");
/* harmony import */ var src_app_shared_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared/pipes/pipes.module */ "9Xeq");









let SelectDestinationModalPageModule = class SelectDestinationModalPageModule {
};
SelectDestinationModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_destination_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectDestinationModalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_shared_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"]
        ],
        declarations: [_select_destination_modal_page__WEBPACK_IMPORTED_MODULE_6__["SelectDestinationModalPage"]]
    })
], SelectDestinationModalPageModule);



/***/ }),

/***/ "YCFA":
/*!********************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-destination-modal/select-destination-modal-routing.module.ts ***!
  \********************************************************************************************************************/
/*! exports provided: SelectDestinationModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectDestinationModalPageRoutingModule", function() { return SelectDestinationModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_destination_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-destination-modal.page */ "mXHT");




const routes = [
    {
        path: '',
        component: _select_destination_modal_page__WEBPACK_IMPORTED_MODULE_3__["SelectDestinationModalPage"]
    }
];
let SelectDestinationModalPageRoutingModule = class SelectDestinationModalPageRoutingModule {
};
SelectDestinationModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectDestinationModalPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-select-destination-modal-select-destination-modal-module.js.map