(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-travel-travel-module"],{

/***/ "Arcy":
/*!***************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel/travel.page.ts ***!
  \***************************************************************/
/*! exports provided: TravelPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelPage", function() { return TravelPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_travel_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./travel.page.html */ "cFAm");
/* harmony import */ var _travel_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./travel.page.scss */ "nvm8");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _modals_in_travel_modal_in_travel_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modals/in-travel-modal/in-travel-modal.page */ "kCj7");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../shared/services/maps.service */ "Y0mc");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @capacitor/core */ "gcOT");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/admin/travels.service */ "KlPZ");









const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_9__["Plugins"];


let marke;
let TravelPage = class TravelPage {
    constructor(alertsService, menuController, mapsService, activatedRoute, travelsService) {
        this.alertsService = alertsService;
        this.menuController = menuController;
        this.mapsService = mapsService;
        this.activatedRoute = activatedRoute;
        this.travelsService = travelsService;
        this.markers = [];
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.current = {
            address: '',
            lat: 0,
            lng: 0
        };
        this.isTracking = true;
        this.origin = { lat: 0, lng: 0 };
        this.destination = { lat: 0, lng: 0 };
        this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
        this.roleCurrentUser = activatedRoute.snapshot.paramMap.get('roleCurrentUser');
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.loadDataPageDashboard();
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.getTravel();
            this.origin.lat = this.travel.startLocation.lat;
            this.origin.lng = this.travel.startLocation.lng;
            this.destination.lat = this.travel.destinyLocation.lat;
            this.destination.lng = this.travel.destinyLocation.lng;
            this.calculateRoute();
        });
    }
    getTravel() {
        return new Promise((resolve) => {
            this.oTravel = this.travelsService.getTravelAndUserById(this.travelId, 'client');
            this.oTravel.subscribe(travel => {
                this.travel = travel;
                resolve(this.travel);
            });
        });
    }
    calculateRoute() {
        this.mapsService.directionsService.route({
            // paramentros de la ruta  inicial y final
            origin: this.origin,
            destination: this.destination,
            // Que  vehiculo se usa  para realizar el viaje
            travelMode: google.maps.TravelMode.DRIVING,
        }, (response, status) => {
            const sDistance = response.routes[0].legs[0].distance.text;
            if (status === google.maps.DirectionsStatus.OK) {
                this.mapsService.directionsDisplay.setDirections(response);
            }
            else {
                //  alert('Could not display directions due to: ' + status);
            }
        });
    }
    loadDataPageDashboard() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.mapsService.loadmap('travelmap');
            this.generateMarksMyLocation();
            this.openModal();
        });
    }
    generateMarksMyLocation() {
        Geolocation.getCurrentPosition().then((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.marker.position.lat = res.coords.latitude;
            this.marker.position.lng = res.coords.longitude;
            this.marker.title = 'Mi Ubicación';
            yield this.addMarker(this.marker, this.marker.title, 'myLocation');
            this.traking();
        }));
    }
    traking() {
        //let market;
        if (this.markers[0] != undefined) {
            this.watch = Geolocation.watchPosition({}, (position, err) => {
                let result = [position.coords.latitude, position.coords.longitude];
                var latlng = new google.maps.LatLng(result[0], result[1]);
                marke.setPosition(latlng); // this.transition(result);
                /* this.mapsService.map.setCenter(latlng); */
                this.current.lat = position.coords.latitude;
                this.current.lng = position.coords.longitude;
                // this.travelsService.updateCurrentLocation(this.travelId, this.current)
            });
        }
    }
    // Unsubscribe from the geolocation watch using the initial ID
    stopTracking() {
        Geolocation.clearWatch({ id: this.watch }).then(() => {
            this.isTracking = false;
        });
    }
    addMarker(marker, name, typeMarket) {
        return new Promise((resolve) => {
            let icon;
            if (typeMarket == 'myLocation') {
                icon = {
                    url: '../../assets/img/auto.png',
                    //ic_loc size: new google.maps.Size(100, 100),
                    origin: new google.maps.Point(0, 0),
                    //    anchor: new google.maps.Point(34, 60),
                    scaledSize: new google.maps.Size(40, 65),
                };
                /*     const image = '../../assets/img/Pineapplemenu.png'; */
            }
            else if (typeMarket == 'stops') {
                icon = {
                    url: '../../assets/img/flag.png',
                    //size: new google.maps.Size(100, 100),
                    origin: new google.maps.Point(0, 0),
                    //    anchor: new google.maps.Point(34, 60),
                    scaledSize: new google.maps.Size(25, 25),
                };
            }
            const geocoder = new google.maps.Geocoder();
            marke = new google.maps.Marker({
                position: marker.position,
                //  draggable: true,
                // animation: google.maps.Animation.DROP,
                map: this.mapsService.map,
                icon: icon,
                title: marker.title,
            });
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        marke.getPosition().lat();
                        marke.getPosition().lng();
                        results[0].formatted_address;
                    }
                    else {
                        console.log('No results found');
                    }
                }
                else {
                    console.log('Geocoder failed due to: ' + status);
                }
            });
            const infoWindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marke, 'click', () => {
                geocoder.geocode({ location: marke.position }, (results, status) => {
                    if (status === 'OK') {
                        if (results[0]) {
                            infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
                            if (infoWindow !== null) {
                            }
                            infoWindow.open(this.mapsService.map, marke);
                        }
                        else {
                        }
                    }
                    else {
                    }
                });
            });
            this.markers.push(marke);
            resolve(marke);
        });
        this.traking();
    }
    openModal() {
        //this.alertsService.presentModalWithoutTravelData(
        //InTravelModalPage,
        //'my-custom-class-complete'
        //);
        this.alertsService.presentModalWithData(_modals_in_travel_modal_in_travel_modal_page__WEBPACK_IMPORTED_MODULE_5__["InTravelModalPage"], {
            travelId: this.travelId,
            roleCurrentUser: this.roleCurrentUser
        }, 'my-custom-class-complete');
    }
    ionViewDidLeave() {
        this.stopTracking();
    }
    openMenu() {
        this.menuController.open('principal');
    }
};
TravelPage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"] },
    { type: _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_7__["MapsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"] },
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_10__["TravelsService"] }
];
TravelPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-travel',
        template: _raw_loader_travel_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_travel_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TravelPage);



/***/ }),

/***/ "EBK7":
/*!*****************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel/travel.module.ts ***!
  \*****************************************************************/
/*! exports provided: TravelPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelPageModule", function() { return TravelPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _travel_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./travel-routing.module */ "ku3w");
/* harmony import */ var _travel_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./travel.page */ "Arcy");
/* harmony import */ var src_app_modules_dashboard_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modules/dashboard/components/components.module */ "KQTD");








let TravelPageModule = class TravelPageModule {
};
TravelPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _travel_routing_module__WEBPACK_IMPORTED_MODULE_5__["TravelPageRoutingModule"],
            src_app_modules_dashboard_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_travel_page__WEBPACK_IMPORTED_MODULE_6__["TravelPage"]]
    })
], TravelPageModule);



/***/ }),

/***/ "cFAm":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/travel/travel.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content fullscreen>\n    <app-principal-header (openMenu)=\"openMenu()\" title=\"Llegando\" slot=\"fixed\"></app-principal-header>\n    <div class=\"map-container\">\n        <div #travelmap\n        id=\"travelmap\"></div>\n    </div>\n    <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>\n</ion-content>\n");

/***/ }),

/***/ "ku3w":
/*!*************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel/travel-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: TravelPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelPageRoutingModule", function() { return TravelPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _travel_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./travel.page */ "Arcy");




const routes = [
    {
        path: '',
        component: _travel_page__WEBPACK_IMPORTED_MODULE_3__["TravelPage"]
    }
];
let TravelPageRoutingModule = class TravelPageRoutingModule {
};
TravelPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TravelPageRoutingModule);



/***/ }),

/***/ "nvm8":
/*!*****************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/travel/travel.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p.dollars, p.cents {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  color: #3E4958;\n  font-weight: bold;\n}\n\n.dollars {\n  font-size: 26px;\n  line-height: 31px;\n  position: relative;\n  top: -5px;\n  font-size: 26px;\n}\n\n.cents {\n  font-size: 13px;\n  line-height: 16px;\n}\n\n.price {\n  display: flex;\n  justify-content: flex-end;\n  position: relative;\n  font-size: 13px;\n}\n\n.details > p {\n  margin-top: 0px;\n  text-align: right;\n  color: #3E4958;\n  font-weight: normal;\n}\n\nion-button {\n  --background: #008D36;\n  --box-shadow: 0px 2px 8px rgb(16, 105, 227, 0.3);\n  --border-radius: 15px;\n  width: 264px;\n  height: 60px;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 28px;\n}\n\n.buttons {\n  display: flex;\n  justify-content: flex-start;\n  align-items: center;\n}\n\n.close-btn {\n  font-size: 40px;\n  color: #3E4958;\n  margin-left: 32px;\n}\n\n.text {\n  font-weight: bold;\n  color: #3E4958;\n}\n\nion-grid {\n  position: absolute;\n  bottom: 0px;\n}\n\nion-content {\n  position: relative;\n}\n\n#map {\n  height: 65%;\n}\n\nion-grid {\n  height: 35%;\n  width: 100%;\n}\n\n.img-container {\n  position: relative;\n}\n\nion-avatar {\n  position: absolute;\n  top: -50px;\n}\n\n.second-row {\n  padding-top: 10px;\n}\n\n.map-container {\n  height: 100%;\n}\n\n#travelmap {\n  width: 100%;\n  height: 100%;\n  opacity: 0;\n  border-radius: 5px;\n  border: 2px solid rgba(0, 0, 0, 0.288);\n  transition: opacity 150ms ease-in;\n  display: block;\n}\n\n#travelmap.show-map {\n  opacity: 1;\n}\n\napp-principal-header {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3RyYXZlbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUFDRDs7QUFFQTtFQUNDLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUFDRDs7QUFFQTtFQUNDLGVBQUE7RUFDQSxpQkFBQTtBQUNEOztBQUVBO0VBQ0MsYUFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBQ0Q7O0FBRUE7RUFDQyxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUFDRDs7QUFFQTtFQUNDLHFCQUFBO0VBQ0EsZ0RBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFDRDs7QUFFQTtFQUNDLGFBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FBQ0Q7O0FBRUE7RUFDQyxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxpQkFBQTtFQUNBLGNBQUE7QUFDRDs7QUFFQTtFQUNDLGtCQUFBO0VBQ0EsV0FBQTtBQUNEOztBQUVBO0VBQ0Msa0JBQUE7QUFDRDs7QUFFQTtFQUNDLFdBQUE7QUFDRDs7QUFFQTtFQUNDLFdBQUE7RUFDQSxXQUFBO0FBQ0Q7O0FBRUE7RUFDQyxrQkFBQTtBQUNEOztBQUVBO0VBQ0Msa0JBQUE7RUFDQSxVQUFBO0FBQ0Q7O0FBRUE7RUFDQyxpQkFBQTtBQUNEOztBQUVBO0VBQ0MsWUFBQTtBQUNEOztBQUVDO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsY0FBQTtBQUNEOztBQUFDO0VBQ0UsVUFBQTtBQUVIOztBQUNBO0VBQ0MsV0FBQTtBQUVEIiwiZmlsZSI6InRyYXZlbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJwLmRvbGxhcnMsIHAuY2VudHMge1xuXHRtYXJnaW4tdG9wOiAwcHg7XG5cdG1hcmdpbi1ib3R0b206IDBweDtcblx0Y29sb3I6ICMzRTQ5NTg7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uZG9sbGFycyB7XG5cdGZvbnQtc2l6ZTogMjZweDtcblx0bGluZS1oZWlnaHQ6IDMxcHg7XG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcblx0dG9wOiAtNXB4O1xuXHRmb250LXNpemU6IDI2cHg7XG59XG5cbi5jZW50cyB7XG5cdGZvbnQtc2l6ZTogMTNweDtcblx0bGluZS1oZWlnaHQ6IDE2cHg7XG59XG5cbi5wcmljZSB7XG5cdGRpc3BsYXk6IGZsZXg7XG5cdGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcblx0Zm9udC1zaXplOiAxM3B4O1xufVxuXG4uZGV0YWlscyA+IHAge1xuXHRtYXJnaW4tdG9wOiAwcHg7XG5cdHRleHQtYWxpZ246IHJpZ2h0O1xuXHRjb2xvcjogIzNFNDk1ODtcblx0Zm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cblxuaW9uLWJ1dHRvbiB7XG5cdC0tYmFja2dyb3VuZDogIzAwOEQzNjtcblx0LS1ib3gtc2hhZG93OiAwcHggMnB4IDhweCByZ2IoMTYsIDEwNSwgMjI3LCAwLjMpO1xuXHQtLWJvcmRlci1yYWRpdXM6IDE1cHg7XG5cdHdpZHRoOiAyNjRweDtcblx0aGVpZ2h0OiA2MHB4O1xuXHRmb250LXdlaWdodDogYm9sZDtcblx0Zm9udC1zaXplOiAxOHB4O1xuXHRsaW5lLWhlaWdodDogMjhweDtcbn1cblxuLmJ1dHRvbnMge1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5jbG9zZS1idG4ge1xuXHRmb250LXNpemU6IDQwcHg7XG5cdGNvbG9yOiAjM0U0OTU4O1xuXHRtYXJnaW4tbGVmdDogMzJweDtcbn1cblxuLnRleHQge1xuXHRmb250LXdlaWdodDogYm9sZDtcblx0Y29sb3I6ICMzRTQ5NTg7XG59XG5cbmlvbi1ncmlkIHtcblx0cG9zaXRpb246IGFic29sdXRlO1xuXHRib3R0b206IDBweDtcbn1cblxuaW9uLWNvbnRlbnQge1xuXHRwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbiNtYXAge1xuXHRoZWlnaHQ6IDY1JTtcbn1cblxuaW9uLWdyaWQge1xuXHRoZWlnaHQ6IDM1JTtcblx0d2lkdGg6IDEwMCU7XG59XG5cbi5pbWctY29udGFpbmVyIHtcblx0cG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG5pb24tYXZhdGFyIHtcblx0cG9zaXRpb246IGFic29sdXRlO1xuXHR0b3A6IC01MHB4O1xufVxuXG4uc2Vjb25kLXJvdyB7XG5cdHBhZGRpbmctdG9wOiAxMHB4O1xufVxuXG4ubWFwLWNvbnRhaW5lciB7XG5cdGhlaWdodDogMTAwJTtcbiAgfVxuIC8vRXN0aWxvIGRlIG1hcGFcbiAjdHJhdmVsbWFwIHtcblx0d2lkdGg6IDEwMCU7XG5cdGhlaWdodDogMTAwJTtcblx0b3BhY2l0eTogMDtcblx0Ym9yZGVyLXJhZGl1czogNXB4O1xuXHRib3JkZXI6IDJweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMjg4KTtcblx0dHJhbnNpdGlvbjogb3BhY2l0eSAxNTBtcyBlYXNlLWluO1xuXHRkaXNwbGF5OiBibG9jaztcblx0Ji5zaG93LW1hcHtcblx0ICBvcGFjaXR5OiAxO1xuXHR9XG4gIH1cbmFwcC1wcmluY2lwYWwtaGVhZGVyIHtcblx0d2lkdGg6IDEwMCU7XG4gIH1cbiAgIl19 */");

/***/ })

}]);
//# sourceMappingURL=pages-travel-travel-module.js.map