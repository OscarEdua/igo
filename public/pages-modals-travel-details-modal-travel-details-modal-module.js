(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-travel-details-modal-travel-details-modal-module"],{

/***/ "H4No":
/*!****************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/travel-details-modal/travel-details-modal.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: TravelDetailsModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelDetailsModalPageModule", function() { return TravelDetailsModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _travel_details_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./travel-details-modal-routing.module */ "mb9W");
/* harmony import */ var _travel_details_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./travel-details-modal.page */ "L3lH");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");








let TravelDetailsModalPageModule = class TravelDetailsModalPageModule {
};
TravelDetailsModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _travel_details_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["TravelDetailsModalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_travel_details_modal_page__WEBPACK_IMPORTED_MODULE_6__["TravelDetailsModalPage"]]
    })
], TravelDetailsModalPageModule);



/***/ }),

/***/ "mb9W":
/*!************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/travel-details-modal/travel-details-modal-routing.module.ts ***!
  \************************************************************************************************************/
/*! exports provided: TravelDetailsModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelDetailsModalPageRoutingModule", function() { return TravelDetailsModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _travel_details_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./travel-details-modal.page */ "L3lH");




const routes = [
    {
        path: '',
        component: _travel_details_modal_page__WEBPACK_IMPORTED_MODULE_3__["TravelDetailsModalPage"]
    }
];
let TravelDetailsModalPageRoutingModule = class TravelDetailsModalPageRoutingModule {
};
TravelDetailsModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TravelDetailsModalPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-travel-details-modal-travel-details-modal-module.js.map