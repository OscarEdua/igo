(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-modals-recommendations-modal-recommendations-modal-module~pages-recommendations-recomm~657c8d3c"],{

/***/ "6ywd":
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/recommendations-modal/recommendations-modal.page.html ***!
  \********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content scrollY=\"true\">\n\n  <div class=\"relative\">\n    <ion-row class=\"container-close-modal\">\n      <div class=\"close-modal-line\" (click)=\"alertsService.closeModal()\">\n      </div>\n    </ion-row>\n    <div id=\"map\"></div>\n    <div class=\"data-container\">\n      <div class=\"absolute\">\n        <div class=\"avatar-container\">\n          <ion-avatar *ngIf=\"driver?.imagen == ''\">\n            <img src=\"../../../../../assets/img/user.png\">\n          </ion-avatar>\n        </div>\n        <div class=\"avatar-container\">\n          <ion-avatar *ngIf=\"driver?.imagen != ''\">\n            <img src=\"{{driver?.imagen}}\">\n          </ion-avatar>\n        </div>\n      </div>\n        <div class=\"data-details\">\n          <h2>Conductor : {{driver?.name}}</h2>\n          <div class=\"stars\">\n            <ion-row class=\"rating\">\n              <h2>Califica nuestro servicio</h2>\n              <ion-col size=\"12\">\n                <ion-icon class=\"white-star\" (click)=\"addStar(1)\" #star1 name=\"star\"></ion-icon>\n                <ion-icon class=\"white-star\" (click)=\"addStar(2)\" #star2 name=\"star\"></ion-icon>\n                <ion-icon class=\"white-star\" (click)=\"addStar(3)\" #star3 name=\"star\"></ion-icon>\n                <ion-icon class=\"white-star\" (click)=\"addStar(4)\" #star4 name=\"star\"></ion-icon>\n                <ion-icon class=\"white-star\" (click)=\"addStar(5)\" #star5 name=\"star\"></ion-icon>\n              </ion-col>\n            </ion-row>\n          </div>\n          <p></p>\n          <ion-textarea formControlName=\"comment\" required rows=\"14\" ngDefaultControl autoGrow=\"true\"\n            placeholder=\"Escribe tu comentario aquí...\"></ion-textarea>\n          <div class=\"error\" *ngIf=\"form.controls.commentary.errors && form.controls.commentary.touched\"></div>\n          <ion-button class=\"btn ion-text-capitalize\" (click)=\"rateTravel()\">Calificar</ion-button>\n        </div>\n    </div>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "C4ym":
/*!*********************************************************!*\
  !*** ./src/app/modules/home/services/rating.service.ts ***!
  \*********************************************************/
/*! exports provided: RatingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingService", function() { return RatingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");




let RatingService = class RatingService {
    constructor(afs) {
        this.afs = afs;
    }
    addRating(rating) {
        rating.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        rating.updateAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        return this.afs.collection('rating').add(rating);
    }
    getRatingByOrder(uidOrder) {
        return this.afs
            .collection('rating', (ref) => ref.where('travelId', '==', uidOrder))
            .valueChanges({ idField: 'uid' });
    }
};
RatingService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
RatingService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], RatingService);



/***/ }),

/***/ "JF+T":
/*!******************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/recommendations-modal/recommendations-modal.page.scss ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background:rgba(255,255,255,0);\n}\n\ndiv.relative {\n  position: relative;\n  width: 100%;\n  height: 100%;\n  top: 8%;\n  background: white;\n  right: 0%;\n  box-shadow: 0px 0px 60px rgba(0, 0, 0, 0.2);\n  border-radius: 14px 14px 0px 0px;\n}\n\ndiv.absolute {\n  border: solid #ffff 20px;\n  position: absolute;\n  top: -35px;\n  margin-left: -3%;\n  margin: -4px;\n  border-radius: 160px;\n  background-color: #ffffff;\n}\n\n.rating ion-col {\n  display: flex;\n  justify-content: space-around;\n}\n\n.rating ion-col ion-icon {\n  font-size: 45px;\n}\n\n.blue-star {\n  color: #214E9D;\n}\n\n.white-star {\n  color: rgba(0, 0, 0, 0.2);\n}\n\n.data-container {\n  height: 60%;\n  border-radius: 0px 20px 0px 0px;\n}\n\n.avatar-container {\n  border-radius: 50%;\n  top: -60px;\n  vertical-align: middle;\n  display: flex;\n  align-items: center;\n}\n\nion-avatar {\n  margin: auto;\n  vertical-align: center;\n  width: 75px;\n  height: 75px;\n}\n\nh2 {\n  margin-top: 10px;\n  margin-bottom: 23px;\n}\n\n/* Estilos de la linea que cierra el modal */\n\n.close-modal-line {\n  width: 30px;\n  height: 5px;\n  background-color: #D5DDE0;\n  border-radius: 50px;\n}\n\n/* Estilos del contenedor de la linea que cierra el modal */\n\n.container-close-modal {\n  padding: 3%;\n  justify-content: center;\n}\n\n.data-details {\n  margin-top: 2em;\n  position: relative;\n  top: 0px;\n  padding-left: 24px;\n  padding-right: 24px;\n}\n\nh2 {\n  margin-bottom: 6%;\n  margin-left: 5px;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 30px;\n  color: #3E4958;\n}\n\nion-icon {\n  color: #214E9D;\n  border-radius: 2px;\n  width: 36px;\n  height: 36px;\n}\n\n.stars {\n  display: flex;\n  justify-content: center;\n}\n\n.no-color {\n  color: #D5DDE0;\n}\n\np {\n  font-weight: normal;\n  font-size: 15px;\n  color: #3E4958;\n  text-align: center;\n}\n\nion-textarea {\n  border: 0.5px solid #D5DDE0;\n  border-radius: 15px;\n  background: #F7F8F9;\n  height: 180px;\n  width: 100%;\n}\n\nion-button {\n  width: 40em;\n  height: 60px;\n  --background: #008D36;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n  --border-radius: 15px;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 24px;\n  color: #FFFFFF;\n  margin-top: 24px;\n  margin-bottom: 34px;\n}\n\n.btn {\n  width: 100%;\n  height: 60px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3JlY29tbWVuZGF0aW9ucy1tb2RhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQ0FBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLE9BQUE7RUFDQSxpQkFBQTtFQUNBLFNBQUE7RUFDQSwyQ0FBQTtFQUNBLGdDQUFBO0FBRUo7O0FBQ0E7RUFDRSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0VBQ0EseUJBQUE7QUFFRjs7QUFNRTtFQUNFLGFBQUE7RUFDQSw2QkFBQTtBQUhKOztBQUtJO0VBQ0UsZUFBQTtBQUhOOztBQVFBO0VBQ0UsY0FBQTtBQUxGOztBQVFBO0VBQ0UseUJBQUE7QUFMRjs7QUFTQTtFQUNDLFdBQUE7RUFDQSwrQkFBQTtBQU5EOztBQVNBO0VBQ0Msa0JBQUE7RUFDQSxVQUFBO0VBQ0Esc0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUFORDs7QUFVQTtFQUNDLFlBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBUEQ7O0FBVUE7RUFFQyxnQkFBQTtFQUNBLG1CQUFBO0FBUkQ7O0FBV0UsNENBQUE7O0FBQ0E7RUFDRSxXQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUFSSjs7QUFXRSwyREFBQTs7QUFDQTtFQUNFLFdBQUE7RUFDQSx1QkFBQTtBQVJKOztBQVVBO0VBQ0UsZUFBQTtFQUNELGtCQUFBO0VBQ0EsUUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUFQRDs7QUFVQTtFQUNHLGlCQUFBO0VBQ0MsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFQSjs7QUFVQTtFQUNDLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBUEQ7O0FBVUE7RUFDQyxhQUFBO0VBQ0EsdUJBQUE7QUFQRDs7QUFVQTtFQUNDLGNBQUE7QUFQRDs7QUFVQTtFQUNDLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQVBEOztBQVVBO0VBQ0MsMkJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNDLFdBQUE7QUFQRjs7QUFVQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0gscUJBQUE7RUFDQSxpREFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQVBEOztBQVVBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFQSiIsImZpbGUiOiJyZWNvbW1lbmRhdGlvbnMtbW9kYWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnR7XG4gICAgLS1iYWNrZ3JvdW5kOnJnYmEoMjU1LDI1NSwyNTUsMCk7XG4gIH1cbmRpdi5yZWxhdGl2ZSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB0b3A6IDglO1xuICAgIGJhY2tncm91bmQ6IHJnYmEoIDI1NSwgMjU1LCAyNTUsIDEuMDAgKTtcbiAgICByaWdodDogMCU7XG4gICAgYm94LXNoYWRvdzogMHB4IDBweCA2MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgICBib3JkZXItcmFkaXVzOiAxNHB4IDE0cHggMHB4IDBweDtcbiAgfSBcblxuZGl2LmFic29sdXRlIHtcbiAgYm9yZGVyOiBzb2xpZCAjZmZmZiAyMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogLTM1cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMyU7XG4gIG1hcmdpbjogLTRweDtcbiAgYm9yZGVyLXJhZGl1czogMTYwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG59XG5cbiNtYXAge1xufVxuXG4ucmF0aW5nIHtcblxuICBpb24tY29sIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuXG4gICAgaW9uLWljb24ge1xuICAgICAgZm9udC1zaXplOiA0NXB4O1xuICAgIH1cbiAgfVxufVxuXG4uYmx1ZS1zdGFyIHtcbiAgY29sb3I6ICMyMTRFOUQ7XG59XG5cbi53aGl0ZS1zdGFyIHtcbiAgY29sb3I6IHJnYmEoJGNvbG9yOiAjMDAwMDAwLCAkYWxwaGE6IDAuMik7XG59XG5cblxuLmRhdGEtY29udGFpbmVyIHtcblx0aGVpZ2h0OiA2MCU7XG5cdGJvcmRlci1yYWRpdXM6IDBweCAyMHB4IDBweCAwcHg7XG59XG5cbi5hdmF0YXItY29udGFpbmVyIHtcblx0Ym9yZGVyLXJhZGl1czogNTAlO1xuXHR0b3A6IC02MHB4O1xuXHR2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG5cbmlvbi1hdmF0YXIgeyBcblx0bWFyZ2luOiBhdXRvO1xuXHR2ZXJ0aWNhbC1hbGlnbjogY2VudGVyO1xuXHR3aWR0aDogNzVweDtcblx0aGVpZ2h0OiA3NXB4O1xufVxuXG5oMiB7XG5cblx0bWFyZ2luLXRvcDogMTBweDtcblx0bWFyZ2luLWJvdHRvbTogMjNweDtcbn1cblxuICAvKiBFc3RpbG9zIGRlIGxhIGxpbmVhIHF1ZSBjaWVycmEgZWwgbW9kYWwgKi9cbiAgLmNsb3NlLW1vZGFsLWxpbmUge1xuICAgIHdpZHRoOiAzMHB4O1xuICAgIGhlaWdodDogNXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNENURERTAgO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIH1cbiAgXG4gIC8qIEVzdGlsb3MgZGVsIGNvbnRlbmVkb3IgZGUgbGEgbGluZWEgcXVlIGNpZXJyYSBlbCBtb2RhbCAqL1xuICAuY29udGFpbmVyLWNsb3NlLW1vZGFsIHtcbiAgICBwYWRkaW5nOiAzJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgfVxuLmRhdGEtZGV0YWlscyB7XG4gIG1hcmdpbi10b3A6IDJlbTtcblx0cG9zaXRpb246IHJlbGF0aXZlO1xuXHR0b3A6IDBweDtcblx0cGFkZGluZy1sZWZ0OiAyNHB4O1xuXHRwYWRkaW5nLXJpZ2h0OiAyNHB4O1xufVxuXG5oMiB7XG5cdCAgbWFyZ2luLWJvdHRvbTogNiU7XG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgY29sb3I6ICMzRTQ5NTg7XG59XG5cbmlvbi1pY29uIHtcblx0Y29sb3I6ICMyMTRFOUQ7XG5cdGJvcmRlci1yYWRpdXM6IDJweDtcblx0d2lkdGg6IDM2cHg7XG5cdGhlaWdodDogMzZweDtcbn1cblxuLnN0YXJzIHsgXG5cdGRpc3BsYXk6IGZsZXg7XG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4ubm8tY29sb3Ige1xuXHRjb2xvcjogI0Q1RERFMDtcbn1cblxucCB7XG5cdGZvbnQtd2VpZ2h0OiBub3JtYWw7XG5cdGZvbnQtc2l6ZTogMTVweDtcblx0Y29sb3I6ICMzRTQ5NTg7XG5cdHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaW9uLXRleHRhcmVhIHtcblx0Ym9yZGVyOiAwLjVweCBzb2xpZCAjRDVEREUwO1xuXHRib3JkZXItcmFkaXVzOiAxNXB4O1xuXHRiYWNrZ3JvdW5kOiAjRjdGOEY5O1xuXHRoZWlnaHQ6IDE4MHB4O1xuICB3aWR0aDogMTAwJTtcbn1cblxuaW9uLWJ1dHRvbiB7XG4gICAgd2lkdGg6IDQwZW07XHRcbiAgICBoZWlnaHQ6IDYwcHg7XG5cdC0tYmFja2dyb3VuZDogIzAwOEQzNjtcblx0LS1ib3gtc2hhZG93OiAwcHggMnB4IDhweCByZ2JhKDE2LCAxMDUsIDIyNywgMC4zKTtcblx0LS1ib3JkZXItcmFkaXVzOiAxNXB4O1xuXHRmb250LXdlaWdodDogYm9sZDtcblx0Zm9udC1zaXplOiAxOHB4O1xuXHRsaW5lLWhlaWdodDogMjRweDtcblx0Y29sb3I6ICNGRkZGRkY7XG5cdG1hcmdpbi10b3A6IDI0cHg7XG5cdG1hcmdpbi1ib3R0b206IDM0cHg7XG59XG4vL0VzdGlsb3MgZGUgbG9zIGJvdG9uZXMgYm90b24gXG4uYnRuIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gIH1cbiAgIl19 */");

/***/ }),

/***/ "UvKr":
/*!****************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/recommendations-modal/recommendations-modal.page.ts ***!
  \****************************************************************************************************/
/*! exports provided: RecommendationsModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecommendationsModalPage", function() { return RecommendationsModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_recommendations_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./recommendations-modal.page.html */ "6ywd");
/* harmony import */ var _recommendations_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./recommendations-modal.page.scss */ "JF+T");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_modules_home_services_rating_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/modules/home/services/rating.service */ "C4ym");
/* harmony import */ var src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/modules/home/services/users.service */ "6JOU");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/admin/travels.service */ "KlPZ");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "3Pt+");









let RecommendationsModalPage = class RecommendationsModalPage {
    constructor(alertsService, userService, travelService, ratingService, renderer, alertService) {
        this.alertsService = alertsService;
        this.userService = userService;
        this.travelService = travelService;
        this.ratingService = ratingService;
        this.renderer = renderer;
        this.alertService = alertService;
        this.observables = [];
        this.rating = {
            comment: '',
            rate: 0,
        };
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadData();
        this.getRatingByOrder();
    }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroup"]({
            commentary: new _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["Validators"].required]),
        });
    }
    getRatingByOrder() {
        const observable = this.ratingService
            .getRatingByOrder(this.travel.travelId)
            .subscribe((res) => {
            this.ratingQuery = res[0];
            if (this.ratingQuery !== undefined) {
                this.addStar(this.ratingQuery.rate);
                this.form.controls.commentary.setValue(this.ratingQuery.comment);
            }
        });
        this.observables.push(observable);
    }
    loadData() {
        this.travelService.getTravelId(this.travelId).subscribe((traveldData) => {
            this.travel = traveldData;
            this.driverId = this.travel.driverId;
            console.log(this.driverId);
            this.userService.getUserById(this.driverId).subscribe((userData) => {
                this.driver = userData;
                console.log(this.driver, this.travel);
            });
        });
    }
    rateTravel() {
        if (this.rating.rate === 0 || this.rating.comment === '') {
            this.alertService.presentAlert('Por favor ingrese una calificación y un comentario!');
        }
        else {
            this.rating.travelId = this.travelId;
            this.rating.isQualified = true;
            if (this.rating.rate > 0 || this.rating.rate <= 2) {
                this.rating.status = 'bad';
            }
            if (this.rating.rate > 2 || this.rating.rate <= 4) {
                this.rating.status = 'good';
            }
            if (this.rating.rate > 4) {
                this.rating.status = 'excelent';
            }
            this.rating.isQualified = true;
            this.rating.clientId = this.travel.clientId;
            this.rating.travelId = this.travel.travelId;
            this.rating.driverId = this.travel.driverId;
            this.ratingService.addRating(this.rating).then((res) => {
                this.travelService.updateTravel(this.travel, this.travelId).then((res) => {
                    console.log('yeah');
                    this.alertService.presentAlert('Proceso de calificación Exitoso 😁');
                });
            });
        }
    }
    addStar(numberStar) {
        this.switchToWhite();
        this.switchToBlue(numberStar);
    }
    switchToBlue(numberStar) {
        this.rating.rate = numberStar;
        switch (numberStar) {
            case 1:
                this.renderer.removeClass(this.star1.nativeElement, 'white-star');
                this.renderer.addClass(this.star1.nativeElement, 'blue-star');
                break;
            case 2:
                this.fillWithBlue(numberStar);
                break;
            case 3:
                this.fillWithBlue(numberStar);
                break;
            case 4:
                this.fillWithBlue(numberStar);
                break;
            case 5:
                this.fillWithBlue(numberStar);
                break;
            default:
                break;
        }
    }
    switchToWhite() {
        for (let star = 1; star < 6; star = star + 1) {
            this.renderer.removeClass(this[`star${star}`].nativeElement, 'blue-star');
            this.renderer.addClass(this[`star${star}`].nativeElement, 'white-star');
        }
    }
    fillWithBlue(numberStar) {
        for (let star = 1; star <= numberStar; star = star + 1) {
            this.renderer.removeClass(this[`star${star}`].nativeElement, 'white-star');
            this.renderer.addClass(this[`star${star}`].nativeElement, 'blue-star');
        }
    }
};
RecommendationsModalPage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] },
    { type: src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UsersService"] },
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_7__["TravelsService"] },
    { type: src_app_modules_home_services_rating_service__WEBPACK_IMPORTED_MODULE_4__["RatingService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Renderer2"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] }
];
RecommendationsModalPage.propDecorators = {
    travelId: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    star1: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['star1',] }],
    star2: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['star2',] }],
    star3: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['star3',] }],
    star4: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['star4',] }],
    star5: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['star5',] }]
};
RecommendationsModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-recommendations-modal',
        template: _raw_loader_recommendations_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_recommendations_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RecommendationsModalPage);



/***/ })

}]);
//# sourceMappingURL=default~pages-modals-recommendations-modal-recommendations-modal-module~pages-recommendations-recomm~657c8d3c.js.map