(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-ubication-destination-schedule-ubication-destination-schedule-module"],{

/***/ "/VMZ":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/ubication-destination-schedule/ubication-destination-schedule-routing.module.ts ***!
  \*************************************************************************************************************************/
/*! exports provided: UbicationDestinationSchedulePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UbicationDestinationSchedulePageRoutingModule", function() { return UbicationDestinationSchedulePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ubication_destination_schedule_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ubication-destination-schedule.page */ "FaeM");




const routes = [
    {
        path: '',
        component: _ubication_destination_schedule_page__WEBPACK_IMPORTED_MODULE_3__["UbicationDestinationSchedulePage"]
    }
];
let UbicationDestinationSchedulePageRoutingModule = class UbicationDestinationSchedulePageRoutingModule {
};
UbicationDestinationSchedulePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UbicationDestinationSchedulePageRoutingModule);



/***/ }),

/***/ "3Koi":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/ubication-destination-schedule/ubication-destination-schedule.page.scss ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1YmljYXRpb24tZGVzdGluYXRpb24tc2NoZWR1bGUucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "FaeM":
/*!***************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/ubication-destination-schedule/ubication-destination-schedule.page.ts ***!
  \***************************************************************************************************************/
/*! exports provided: UbicationDestinationSchedulePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UbicationDestinationSchedulePage", function() { return UbicationDestinationSchedulePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_ubication_destination_schedule_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./ubication-destination-schedule.page.html */ "v7tV");
/* harmony import */ var _ubication_destination_schedule_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ubication-destination-schedule.page.scss */ "3Koi");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _modals_ubication_destination_schedule_modal_ubication_destination_schedule_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal.page */ "IhZq");






let UbicationDestinationSchedulePage = class UbicationDestinationSchedulePage {
    constructor(alertsService) {
        this.alertsService = alertsService;
    }
    ngOnInit() {
    }
    openModal() {
        this.alertsService.presentModalWithoutTravelData(_modals_ubication_destination_schedule_modal_ubication_destination_schedule_modal_page__WEBPACK_IMPORTED_MODULE_5__["UbicationDestinationScheduleModalPage"], 'ubication-destination-schedula-modal');
    }
};
UbicationDestinationSchedulePage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] }
];
UbicationDestinationSchedulePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-ubication-destination-schedule',
        template: _raw_loader_ubication_destination_schedule_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_ubication_destination_schedule_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UbicationDestinationSchedulePage);



/***/ }),

/***/ "lZDK":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/ubication-destination-schedule/ubication-destination-schedule.module.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: UbicationDestinationSchedulePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UbicationDestinationSchedulePageModule", function() { return UbicationDestinationSchedulePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _ubication_destination_schedule_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ubication-destination-schedule-routing.module */ "/VMZ");
/* harmony import */ var _ubication_destination_schedule_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ubication-destination-schedule.page */ "FaeM");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let UbicationDestinationSchedulePageModule = class UbicationDestinationSchedulePageModule {
};
UbicationDestinationSchedulePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ubication_destination_schedule_routing_module__WEBPACK_IMPORTED_MODULE_5__["UbicationDestinationSchedulePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_ubication_destination_schedule_page__WEBPACK_IMPORTED_MODULE_6__["UbicationDestinationSchedulePage"]]
    })
], UbicationDestinationSchedulePageModule);



/***/ }),

/***/ "v7tV":
/*!*******************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/ubication-destination-schedule/ubication-destination-schedule.page.html ***!
  \*******************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header></app-header>\n<ion-content>\n  <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=pages-ubication-destination-schedule-ubication-destination-schedule-module.js.map