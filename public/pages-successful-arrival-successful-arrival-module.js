(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-successful-arrival-successful-arrival-module"],{

/***/ "08g/":
/*!*************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/successful-arrival/successful-arrival-routing.module.ts ***!
  \*************************************************************************************************/
/*! exports provided: SuccessfulArrivalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuccessfulArrivalPageRoutingModule", function() { return SuccessfulArrivalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _successful_arrival_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./successful-arrival.page */ "rj1E");




const routes = [
    {
        path: '',
        component: _successful_arrival_page__WEBPACK_IMPORTED_MODULE_3__["SuccessfulArrivalPage"]
    }
];
let SuccessfulArrivalPageRoutingModule = class SuccessfulArrivalPageRoutingModule {
};
SuccessfulArrivalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SuccessfulArrivalPageRoutingModule);



/***/ }),

/***/ "8Bun":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/successful-arrival/successful-arrival.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: SuccessfulArrivalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuccessfulArrivalPageModule", function() { return SuccessfulArrivalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _successful_arrival_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./successful-arrival-routing.module */ "08g/");
/* harmony import */ var _successful_arrival_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./successful-arrival.page */ "rj1E");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let SuccessfulArrivalPageModule = class SuccessfulArrivalPageModule {
};
SuccessfulArrivalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _successful_arrival_routing_module__WEBPACK_IMPORTED_MODULE_5__["SuccessfulArrivalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_successful_arrival_page__WEBPACK_IMPORTED_MODULE_6__["SuccessfulArrivalPage"]]
    })
], SuccessfulArrivalPageModule);



/***/ }),

/***/ "RwmX":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/successful-arrival/successful-arrival.page.scss ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-col, ion-content {\n  display: flex;\n  justify-content: center;\n}\n\nion-grid {\n  height: 100%;\n}\n\n.button-container {\n  display: flex;\n  justify-content: center;\n  position: absolute;\n  width: 100%;\n  bottom: 4%;\n}\n\n.img-container {\n  width: 75%;\n  height: 75%;\n}\n\n.message {\n  font-size: 40px;\n  line-height: 50px;\n  font-weight: bold;\n  text-align: center;\n  width: 60%;\n  margin-bottom: 0px;\n  margin-left: -30%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3N1Y2Nlc3NmdWwtYXJyaXZhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxhQUFBO0VBQ0EsdUJBQUE7QUFDRDs7QUFFQTtFQUNDLFlBQUE7QUFDRDs7QUFFQTtFQUNDLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7QUFDRDs7QUFFQTtFQUNDLFVBQUE7RUFDQSxXQUFBO0FBQ0Q7O0FBRUE7RUFDQyxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUFDRCIsImZpbGUiOiJzdWNjZXNzZnVsLWFycml2YWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbCwgaW9uLWNvbnRlbnQge1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuaW9uLWdyaWQge1xuXHRoZWlnaHQ6IDEwMCU7XG59XG5cbi5idXR0b24tY29udGFpbmVyIHtcblx0ZGlzcGxheTogZmxleDtcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0d2lkdGg6IDEwMCU7XG5cdGJvdHRvbTogNCU7XG59XG5cbi5pbWctY29udGFpbmVyIHtcblx0d2lkdGg6IDc1JTtcblx0aGVpZ2h0OiA3NSU7XG59XG5cbi5tZXNzYWdlIHtcblx0Zm9udC1zaXplOiA0MHB4O1xuXHRsaW5lLWhlaWdodDogNTBweDtcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG5cdHRleHQtYWxpZ246IGNlbnRlcjtcblx0d2lkdGg6IDYwJTtcblx0bWFyZ2luLWJvdHRvbTogMHB4O1xuXHRtYXJnaW4tbGVmdDogLTMwJTtcbn1cbiJdfQ== */");

/***/ }),

/***/ "VBKM":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/successful-arrival/successful-arrival.page.html ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"12\" class=\"img-container\">\n        <img src=\"../../../../../assets/img/carrito1.png\">\n      </ion-col>\n      <ion-col size=\"17\">\n        <h1 class=\"message\">Ruta Completada</h1>\n\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <div class=\"button-container\" routerLink=\"../recommendations\">\n    <app-button title=\"Calificar Cliente\"></app-button>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "rj1E":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/successful-arrival/successful-arrival.page.ts ***!
  \***************************************************************************************/
/*! exports provided: SuccessfulArrivalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuccessfulArrivalPage", function() { return SuccessfulArrivalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_successful_arrival_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./successful-arrival.page.html */ "VBKM");
/* harmony import */ var _successful_arrival_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./successful-arrival.page.scss */ "RwmX");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let SuccessfulArrivalPage = class SuccessfulArrivalPage {
    constructor() { }
    ngOnInit() {
    }
};
SuccessfulArrivalPage.ctorParameters = () => [];
SuccessfulArrivalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-successful-arrival',
        template: _raw_loader_successful_arrival_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_successful_arrival_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SuccessfulArrivalPage);



/***/ })

}]);
//# sourceMappingURL=pages-successful-arrival-successful-arrival-module.js.map