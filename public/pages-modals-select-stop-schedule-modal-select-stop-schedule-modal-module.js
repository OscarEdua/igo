(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-select-stop-schedule-modal-select-stop-schedule-modal-module"],{

/***/ "RiRr":
/*!****************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-stop-schedule-modal/select-stop-schedule-modal.module.ts ***!
  \****************************************************************************************************************/
/*! exports provided: SelectStopScheduleModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopScheduleModalPageModule", function() { return SelectStopScheduleModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_stop_schedule_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-stop-schedule-modal-routing.module */ "oqiJ");
/* harmony import */ var _select_stop_schedule_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-stop-schedule-modal.page */ "fn7c");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");








let SelectStopScheduleModalPageModule = class SelectStopScheduleModalPageModule {
};
SelectStopScheduleModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_stop_schedule_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectStopScheduleModalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_select_stop_schedule_modal_page__WEBPACK_IMPORTED_MODULE_6__["SelectStopScheduleModalPage"]]
    })
], SelectStopScheduleModalPageModule);



/***/ }),

/***/ "oqiJ":
/*!************************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-stop-schedule-modal/select-stop-schedule-modal-routing.module.ts ***!
  \************************************************************************************************************************/
/*! exports provided: SelectStopScheduleModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopScheduleModalPageRoutingModule", function() { return SelectStopScheduleModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_stop_schedule_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-stop-schedule-modal.page */ "fn7c");




const routes = [
    {
        path: '',
        component: _select_stop_schedule_modal_page__WEBPACK_IMPORTED_MODULE_3__["SelectStopScheduleModalPage"]
    }
];
let SelectStopScheduleModalPageRoutingModule = class SelectStopScheduleModalPageRoutingModule {
};
SelectStopScheduleModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectStopScheduleModalPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-select-stop-schedule-modal-select-stop-schedule-modal-module.js.map