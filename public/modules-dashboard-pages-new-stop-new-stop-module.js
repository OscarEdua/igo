(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-dashboard-pages-new-stop-new-stop-module"],{

/***/ "Hy62":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/new-stop/new-stop.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <app-principal-header (openMenu)=\"openMenu()\" slot=\"fixed\"></app-principal-header>\n  <div #mapstops id=\"mapstops\"></div>\n  <ion-grid>\n    <ion-row class=\"container-title\">\n      <h5>Crear una nueva parada</h5>\n    </ion-row>\n    <ion-row class=\"container\">\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">DIRECCIÓN</ion-label>\n        <ion-item class=\"input-ionItem\">\n          <ion-input [(ngModel)]=\"stop.address\" id=\"stop_address\" autocomplete=\"off\" (keyup)=\"keyUpHandler()\"></ion-input>\n        </ion-item>\n      </div>\n    </ion-row>\n    \n    <ion-row class=\"container\">\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">NOMBRE</ion-label>\n        <ion-item class=\"input-ionItem\">\n          <ion-input class=\"text-datails\" type=\"text\" [(ngModel)]=\"stop.name\"></ion-input>\n        </ion-item>\n      </div>\n    </ion-row>\n\n    <ion-row class=\"container\">\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">DESCRIPCIÓN</ion-label>\n        <ion-item class=\"input-ionItem\">\n          <ion-textarea [(ngModel)]=\"stop.description\"></ion-textarea>\n        </ion-item>\n      </div>\n    </ion-row>\n\n    <ion-row class=\"container\">\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">ESTADO</ion-label>\n\n        <ion-select value=\"publico\" [(ngModel)]=\"stop.state\">\n          <ion-select-option value=\"publico\">Publico</ion-select-option>\n          <ion-select-option value=\"oculto\">Oculto</ion-select-option>\n        </ion-select>\n      </div>\n    </ion-row>\n  </ion-grid>\n\n  <ion-row class=\"container-btn\">\n    <ion-button class=\"btn\" (click)=\"addstop()\"> Crear Parada </ion-button>\n  </ion-row>\n\n</ion-content>\n");

/***/ }),

/***/ "eTcf":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/new-stop/new-stop-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: NewStopPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewStopPageRoutingModule", function() { return NewStopPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _new_stop_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-stop.page */ "u6W7");




const routes = [
    {
        path: '',
        component: _new_stop_page__WEBPACK_IMPORTED_MODULE_3__["NewStopPage"]
    }
];
let NewStopPageRoutingModule = class NewStopPageRoutingModule {
};
NewStopPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NewStopPageRoutingModule);



/***/ }),

/***/ "kbHH":
/*!*********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/new-stop/new-stop.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilos del header */\nion-toolbar ion-row {\n  align-items: center;\n  display: flex;\n  justify-content: center;\n  width: 100%;\n}\nion-toolbar ion-row ion-title {\n  font-size: 20px;\n  font-weight: bold;\n  --color: #3E4958;\n  text-align: center;\n}\nh1 {\n  font-weight: bold;\n  color: #2f363f;\n  text-align: center;\n  font-size: 18px;\n  line-height: 28px;\n}\n#mapstops {\n  width: 100%;\n  height: 45%;\n  opacity: 0;\n  border-radius: 15px;\n  transition: opacity 150ms ease-in;\n  display: block;\n}\n#mapstops.show-map {\n  opacity: 1;\n}\n.container {\n  display: flex;\n  justify-content: center;\n}\n.container .container-inputs {\n  margin-bottom: 3%;\n  padding: 10px;\n  width: 90%;\n  text-align: left;\n}\n.container .container-inputs ion-select {\n  width: 100%;\n  justify-content: center;\n}\n.container .container-inputs .label {\n  color: #3E4958;\n  font-weight: bold;\n  padding-left: 10px;\n  font-size: 13px;\n  line-height: 20px;\n}\n.container .container-inputs .input-ionItem {\n  --background: #F7F8F9;\n  --highlight-color-focused: #008D36;\n  --highlight-color-valid: #008D36;\n  border: 0.5px solid #D5DDE0;\n  border-radius: 15px;\n  margin-top: 3%;\n}\n.container .container-select {\n  text-align: left;\n  padding: 10px;\n  width: 90%;\n  margin: 0px;\n}\n/* Contenedor del titulo */\n.container-title {\n  margin-left: 10%;\n  margin-bottom: 2%;\n}\n.container-title h5 {\n  font-weight: bold;\n  font-size: 18px;\n  color: #3E4958;\n}\n/* Estilos del boton de agregar una nueva parada */\n.container-btn {\n  justify-content: center;\n}\n.container-btn .btn {\n  width: 75%;\n  height: 60px;\n  --background: #008D36;\n  --border-radius: 15px;\n  --box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  color: white;\n  font-size: 17px;\n  margin-bottom: 10%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL25ldy1zdG9wLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx1QkFBQTtBQUVFO0VBQ0UsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0FBQUo7QUFFSTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFBTjtBQUtBO0VBQ0UsaUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFGRjtBQUtDO0VBQ0MsV0FBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQ0FBQTtFQUNBLGNBQUE7QUFGRjtBQUdFO0VBQ0UsVUFBQTtBQURKO0FBS0E7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7QUFGRjtBQUlFO0VBQ0UsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0FBRko7QUFJSTtFQUNFLFdBQUE7RUFDQSx1QkFBQTtBQUZOO0FBTUU7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUpKO0FBT0k7RUFDRSxxQkFBQTtFQUNBLGtDQUFBO0VBQ0EsZ0NBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQUxOO0FBVUU7RUFDRSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQVJKO0FBWUUsMEJBQUE7QUFDQTtFQUNFLGdCQUFBO0VBQ0EsaUJBQUE7QUFUSjtBQVdJO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQVROO0FBYUUsa0RBQUE7QUFDQTtFQUNFLHVCQUFBO0FBVko7QUFZSTtFQUNFLFVBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNBLG9EQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQVZOIiwiZmlsZSI6Im5ldy1zdG9wLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIEVzdGlsb3MgZGVsIGhlYWRlciAqL1xuaW9uLXRvb2xiYXIge1xuICBpb24tcm93IHtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICBpb24tdGl0bGUge1xuICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAtLWNvbG9yOiAjM0U0OTU4O1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgfVxuICB9XG59XG5oMSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzJmMzYzZjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiAyOHB4O1xufVxuIC8vRXN0aWxvIGRlIG1hcGFcbiAjbWFwc3RvcHMge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA0NSU7XG4gIG9wYWNpdHk6IDA7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMTUwbXMgZWFzZS1pbjtcbiAgZGlzcGxheTogYmxvY2s7XG4gICYuc2hvdy1tYXB7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxufVxuXG4uY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cbiAgLmNvbnRhaW5lci1pbnB1dHMge1xuICAgIG1hcmdpbi1ib3R0b206IDMlO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgd2lkdGg6IDkwJTtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBcbiAgICBpb24tc2VsZWN0IHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgfVxuICBcbiAgICBcbiAgLmxhYmVsIHtcbiAgICBjb2xvcjogIzNFNDk1ODtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICB9XG5cbiAgICAuaW5wdXQtaW9uSXRlbSB7XG4gICAgICAtLWJhY2tncm91bmQ6ICNGN0Y4Rjk7XG4gICAgICAtLWhpZ2hsaWdodC1jb2xvci1mb2N1c2VkOiAjMDA4RDM2O1xuICAgICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6ICMwMDhEMzY7XG4gICAgICBib3JkZXI6IDAuNXB4IHNvbGlkICNENURERTA7XG4gICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgICAgbWFyZ2luLXRvcDogMyU7XG4gICAgfVxuICAgIFxuICB9XG5cbiAgLmNvbnRhaW5lci1zZWxlY3R7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIHdpZHRoOiA5MCU7XG4gICAgbWFyZ2luOiAwcHg7XG4gIH1cbn1cbiAgXG4gIC8qIENvbnRlbmVkb3IgZGVsIHRpdHVsbyAqL1xuICAuY29udGFpbmVyLXRpdGxlIHtcbiAgICBtYXJnaW4tbGVmdDogMTAlO1xuICAgIG1hcmdpbi1ib3R0b206IDIlO1xuICBcbiAgICBoNSB7XG4gICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIGNvbG9yOiAjM0U0OTU4O1xuICAgIH1cbiAgfVxuXG4gIC8qIEVzdGlsb3MgZGVsIGJvdG9uIGRlIGFncmVnYXIgdW5hIG51ZXZhIHBhcmFkYSAqL1xuICAuY29udGFpbmVyLWJ0biB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIFxuICAgIC5idG4ge1xuICAgICAgd2lkdGg6IDc1JTtcbiAgICAgIGhlaWdodDogNjBweDtcbiAgICAgIC0tYmFja2dyb3VuZDogIzAwOEQzNjtcbiAgICAgIC0tYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgIC0tYm94LXNoYWRvdzogMHB4IDRweCAyMHB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yNSk7XG4gICAgICBjb2xvcjogd2hpdGU7XG4gICAgICBmb250LXNpemU6IDE3cHg7XG4gICAgICBtYXJnaW4tYm90dG9tOiAxMCU7XG4gICAgfVxuICBcbiAgXG4gIH1cbiAgIl19 */");

/***/ }),

/***/ "u6W7":
/*!*******************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/new-stop/new-stop.page.ts ***!
  \*******************************************************************/
/*! exports provided: NewStopPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewStopPage", function() { return NewStopPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_new_stop_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./new-stop.page.html */ "Hy62");
/* harmony import */ var _new_stop_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-stop.page.scss */ "kbHH");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/admin/stops.service */ "/+Y6");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @capacitor/core */ "gcOT");










const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_8__["Plugins"];
let NewStopPage = class NewStopPage {
    constructor(router, loadingController, stopsService, alertsService, menuController) {
        this.router = router;
        this.loadingController = loadingController;
        this.stopsService = stopsService;
        this.alertsService = alertsService;
        this.menuController = menuController;
        this.directionsService = new google.maps.DirectionsService();
        this.directionsDisplay = new google.maps.DirectionsRenderer();
        this.markers = [];
        this.stop = {
            name: '',
            description: '',
            city: '',
            provinces: '',
            address: '',
            lat: 0,
            lng: 0,
            state: '',
            isActive: true,
        };
        this.sessionToken = new google.maps.places.AutocompleteSessionToken();
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadmap();
        this.GoogleAutocomplete = new google.maps.places.Autocomplete(document.getElementById('stop_address').getElementsByTagName('input')[0], {
            types: ['address'],
            sessionToken: this.sessionToken,
            componentRestrictions: { country: 'ec' },
            fields: [
                'address_components',
                'geometry',
                'icon',
                'name',
                'formatted_address',
            ],
        });
    }
    //subir datos
    addstop() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.stop.name != '' &&
                this.stop.description != '' &&
                this.stop.address != '' &&
                this.stop.lat != 0 &&
                this.stop.lng != 0 &&
                this.stop.state != '') {
                this.stopsService.addStop(this.stop);
                this.alertsService.presentAlert('Parada Agregada');
                this.router.navigate(['dashboard/stops']);
            }
            else {
                this.alertsService.presentAlert('Debe ingresar todos los datos');
            }
        });
    }
    ////////////////////////////////API DE GOOGLE MAPS/////////////////////////////////////////////////////////////
    //Cargar el mapa
    loadmap() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let latLng;
            let market;
            const coordinates = yield Geolocation.getCurrentPosition();
            latLng = new google.maps.LatLng(coordinates.coords.latitude, coordinates.coords.longitude);
            market = {
                position: {
                    lat: coordinates.coords.latitude,
                    lng: coordinates.coords.longitude,
                },
                title: 'Mi Ubicación',
            };
            //CUANDO TENEMOS LAS COORDENADAS SIMPLEMENTE NECESITAMOS PASAR AL MAPA DE GOOGLE TODOS LOS PARAMETROS.
            const mapEle = document.getElementById('mapstops');
            // this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
            // Crear el mapa
            this.map = new google.maps.Map(mapEle, {
                center: latLng,
                zoom: 14,
                //estilo del mapa (color gris)
                styles: [
                    {
                        elementType: 'geometry',
                        stylers: [
                            {
                                color: '#f5f5f5',
                            },
                        ],
                    },
                    {
                        elementType: 'labels.icon',
                        stylers: [
                            {
                                visibility: 'off',
                            },
                        ],
                    },
                    {
                        elementType: 'labels.text.fill',
                        stylers: [
                            {
                                color: '#616161',
                            },
                        ],
                    },
                    {
                        elementType: 'labels.text.stroke',
                        stylers: [
                            {
                                color: '#f5f5f5',
                            },
                        ],
                    },
                    {
                        featureType: 'administrative.land_parcel',
                        elementType: 'labels.text.fill',
                        stylers: [
                            {
                                color: '#bdbdbd',
                            },
                        ],
                    },
                    {
                        featureType: 'poi',
                        elementType: 'geometry',
                        stylers: [
                            {
                                color: '#eeeeee',
                            },
                        ],
                    },
                    {
                        featureType: 'poi',
                        elementType: 'labels.text.fill',
                        stylers: [
                            {
                                color: '#757575',
                            },
                        ],
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'geometry',
                        stylers: [
                            {
                                color: '#e5e5e5',
                            },
                        ],
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'labels.text.fill',
                        stylers: [
                            {
                                color: '#9e9e9e',
                            },
                        ],
                    },
                    {
                        featureType: 'road',
                        elementType: 'geometry',
                        stylers: [
                            {
                                color: '#ffffff',
                            },
                        ],
                    },
                    {
                        featureType: 'road.arterial',
                        elementType: 'labels.text.fill',
                        stylers: [
                            {
                                color: '#757575',
                            },
                        ],
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'geometry',
                        stylers: [
                            {
                                color: '#dadada',
                            },
                        ],
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'labels.text.fill',
                        stylers: [
                            {
                                color: '#616161',
                            },
                        ],
                    },
                    {
                        featureType: 'road.local',
                        elementType: 'labels.text.fill',
                        stylers: [
                            {
                                color: '#9e9e9e',
                            },
                        ],
                    },
                    {
                        featureType: 'transit.line',
                        elementType: 'geometry',
                        stylers: [
                            {
                                color: '#e5e5e5',
                            },
                        ],
                    },
                    {
                        featureType: 'transit.station',
                        elementType: 'geometry',
                        stylers: [
                            {
                                color: '#eeeeee',
                            },
                        ],
                    },
                    {
                        featureType: 'water',
                        elementType: 'geometry',
                        stylers: [
                            {
                                color: '#c9c9c9',
                            },
                        ],
                    },
                    {
                        featureType: 'water',
                        elementType: 'labels.text.fill',
                        stylers: [
                            {
                                color: '#9e9e9e',
                            },
                        ],
                    },
                ],
            });
            //  this.addMarker(market);
            //colocar el mapa con los parametros
            this.directionsDisplay.setMap(this.map);
            mapEle.classList.add('show-map');
        });
    }
    //Auto completado
    //busqueda de datos por me dio de un input
    keyUpHandler() {
        if (this.stop.address.length > 0) {
            google.maps.event.addListener(this.GoogleAutocomplete, `place_changed`, () => {
                const place = this.GoogleAutocomplete.getPlace();
                let market = {
                    position: {
                        lat: place.geometry.location.lat(),
                        lng: place.geometry.location.lng(),
                    },
                    title: 'Mi Ubicación',
                };
                var centro = {
                    lat: place.geometry.location.lat(),
                    lng: place.geometry.location.lng(),
                };
                this.map.setCenter(centro);
                this.addMarker(market);
            });
        }
    }
    //añadir marcador
    addMarker(marker) {
        this.deleteMarkers();
        let marke;
        const meta = '../../assets/img/stops.png';
        const icon2 = {
            url: meta,
            /*     size: new google.maps.Size(100, 100),
              origin: new google.maps.Point(0, 0),
               anchor: new google.maps.Point(40, 60), */
            scaledSize: new google.maps.Size(100, 100),
        };
        marke = new google.maps.Marker({
            position: marker.position,
            draggable: true,
            animation: google.maps.Animation.DROP,
            map: this.map,
            icon: icon2,
            title: marker.title,
        });
        const geocoder = new google.maps.Geocoder();
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    //Info Windows de google
                    var content = results[0].formatted_address;
                    var infoWindow = new google.maps.InfoWindow({
                        content: content,
                    });
                    //   infoWindow.close(this.map, marke);
                    this.stop.lat = marke.getPosition().lat();
                    this.stop.lng = marke.getPosition().lng();
                    this.stop.address = results[0].formatted_address;
                    this.stop.city = results[4].formatted_address;
                    this.stop.provinces = results[7].formatted_address;
                }
                else {
                    window.alert('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
        google.maps.event.addListener(marke, 'dragend', () => {
            var currentInfoWindow = null;
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        //Info Windows de google
                        var content = results[0].formatted_address;
                        var infoWindow = new google.maps.InfoWindow({
                            content: content,
                        });
                        //   infoWindow.close(this.map, marke);
                        this.stop.lat = marke.getPosition().lat();
                        this.stop.lng = marke.getPosition().lng();
                        this.stop.address = results[0].formatted_address;
                        this.stop.city = results[4].formatted_address;
                        this.stop.provinces = results[7].formatted_address;
                    }
                    else {
                        window.alert('No results found');
                    }
                }
                else {
                    console.log('Geocoder failed due to: ' + status);
                }
            });
        });
        this.markers.push(marke);
    }
    //funciones para eliminar las marcas de mapa de google
    //desde
    deleteMarkers() {
        this.clearMarkers();
        this.markers = [];
    }
    clearMarkers() {
        this.setMapOnAll(null);
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
    //hasta
    openMenu() {
        this.menuController.open('principal');
    }
};
NewStopPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_6__["StopsService"] },
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_7__["AlertsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"] }
];
NewStopPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-new-stop',
        template: _raw_loader_new_stop_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_new_stop_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], NewStopPage);



/***/ }),

/***/ "wepa":
/*!*********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/new-stop/new-stop.module.ts ***!
  \*********************************************************************/
/*! exports provided: NewStopPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewStopPageModule", function() { return NewStopPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _new_stop_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./new-stop-routing.module */ "eTcf");
/* harmony import */ var _new_stop_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./new-stop.page */ "u6W7");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let NewStopPageModule = class NewStopPageModule {
};
NewStopPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _new_stop_routing_module__WEBPACK_IMPORTED_MODULE_5__["NewStopPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_new_stop_page__WEBPACK_IMPORTED_MODULE_6__["NewStopPage"]]
    })
], NewStopPageModule);



/***/ })

}]);
//# sourceMappingURL=modules-dashboard-pages-new-stop-new-stop-module.js.map