(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "2GcX":
/*!******************************************************!*\
  !*** ./src/app/modules/home/pages/home/home.page.ts ***!
  \******************************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./home.page.html */ "eTM1");
/* harmony import */ var _home_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home.page.scss */ "dCgX");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let HomePage = class HomePage {
    constructor() { }
    ngOnInit() {
    }
};
HomePage.ctorParameters = () => [];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-home',
        template: _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_home_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], HomePage);



/***/ }),

/***/ "HiyI":
/*!********************************************************!*\
  !*** ./src/app/modules/home/pages/home/home.module.ts ***!
  \********************************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-routing.module */ "t95I");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "2GcX");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "dCgX":
/*!********************************************************!*\
  !*** ./src/app/modules/home/pages/home/home.page.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-slide {\n  padding: 40px;\n}\n\nimg {\n  width: 220px;\n  height: 200px;\n  margin: 60px 0 40px;\n}\n\n.img-cuadrito {\n  width: 290px;\n  height: 290px;\n}\n\n:host ::ng-deep .swiper-pagination {\n  bottom: 10%;\n}\n\n:host ::ng-deep .swiper-pagination .swiper-pagination-bullet {\n  --bullet-background: #000000;\n  margin-bottom: 7%;\n  width: 10px;\n  height: 10px;\n}\n\n:host ::ng-deep .swiper-pagination .swiper-pagination-bullet-active {\n  --bullet-background-active: #4C75B9;\n  width: 10px;\n  height: 10px;\n}\n\nh2 {\n  font-weight: 500;\n  margin: 0px;\n}\n\n.container-btn {\n  width: 100%;\n  display: flex;\n  justify-content: flex-end;\n}\n\n.btn {\n  border-radius: 10px;\n  width: 90%;\n  height: 48px;\n  position: absolute;\n  background: #008D36;\n  top: 88%;\n  color: white;\n  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.3);\n}\n\n.btn-omitir {\n  position: absolute;\n  padding: 30px 40px 0px 0px;\n  z-index: 2;\n  color: #14386A;\n  background-color: rgba(255, 255, 255, 0);\n  font-size: 16px;\n}\n\nion-slides {\n  height: 100%;\n  z-index: 1;\n}\n\n.swiper-slide {\n  display: block;\n}\n\n@media only screen and (min-width: 768px) {\n  img {\n    width: 380px;\n    height: 360px;\n    margin: 60px 0 40px;\n  }\n\n  h2 {\n    font-size: 40px;\n  }\n\n  ion-slide p {\n    font-size: 20px;\n  }\n\n  button {\n    font-size: 20px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsYUFBQTtBQUNEOztBQUVBO0VBQ0UsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQUNGOztBQUVBO0VBQ0UsWUFBQTtFQUNBLGFBQUE7QUFDRjs7QUFFQTtFQUNFLFdBQUE7QUFDRjs7QUFFRTtFQUNFLDRCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUFKOztBQUdFO0VBQ0UsbUNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQURKOztBQUtBO0VBQ0UsZ0JBQUE7RUFDQSxXQUFBO0FBRkY7O0FBS0E7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0FBRkY7O0FBS0E7RUFDRSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsMENBQUE7QUFGRjs7QUFNQTtFQUNFLGtCQUFBO0VBQ0EsMEJBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLHdDQUFBO0VBQ0EsZUFBQTtBQUhGOztBQU9BO0VBQ0UsWUFBQTtFQUNBLFVBQUE7QUFKRjs7QUFRQTtFQUNFLGNBQUE7QUFMRjs7QUFVQTtFQUNFO0lBQ0UsWUFBQTtJQUNBLGFBQUE7SUFDQSxtQkFBQTtFQVBGOztFQVVBO0lBQ0UsZUFBQTtFQVBGOztFQVdFO0lBQ0UsZUFBQTtFQVJKOztFQVlBO0lBQ0UsZUFBQTtFQVRGO0FBQ0YiLCJmaWxlIjoiaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tc2xpZGV7XG5cdHBhZGRpbmc6IDQwcHg7XG59XG5cbmltZyB7XG4gIHdpZHRoOiAyMjBweDtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgbWFyZ2luOiA2MHB4IDAgNDBweDtcbn1cblxuLmltZy1jdWFkcml0b3tcbiAgd2lkdGg6IDI5MHB4O1xuICBoZWlnaHQ6IDI5MHB4O1xufVxuXG46aG9zdCA6Om5nLWRlZXAgLnN3aXBlci1wYWdpbmF0aW9uIHtcbiAgYm90dG9tOiAxMCU7XG5cblxuICAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0IHtcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kOiAjMDAwMDAwO1xuICAgIG1hcmdpbi1ib3R0b206IDclO1xuICAgIHdpZHRoOiAxMHB4O1xuICAgIGhlaWdodDogMTBweDtcbiAgfVxuXG4gIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXQtYWN0aXZlIHtcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTogIzRDNzVCOTtcbiAgICB3aWR0aDogMTBweDtcbiAgICBoZWlnaHQ6IDEwcHg7XG4gIH1cbn1cblxuaDIge1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDBweDtcbn1cblxuLmNvbnRhaW5lci1idG57XG4gIHdpZHRoOiAxMDAlIDsgXG4gIGRpc3BsYXk6IGZsZXg7IFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uYnRuIHtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgd2lkdGg6IDkwJTtcbiAgaGVpZ2h0OiA0OHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6ICMwMDhEMzY7XG4gIHRvcDogODglO1xuICBjb2xvcjogd2hpdGU7XG4gIGJveC1zaGFkb3c6IDBweCAzcHggM3B4IHJnYmEoJGNvbG9yOiMwMDAwMDAsICRhbHBoYTogMC4zKTtcbn1cblxuXG4uYnRuLW9taXRpciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcGFkZGluZzozMHB4IDQwcHggMHB4IDBweDtcbiAgei1pbmRleDogMjtcbiAgY29sb3I6ICMxNDM4NkE7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoJGNvbG9yOiAjZmZmZmZmLCAkYWxwaGE6IDApO1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cblxuaW9uLXNsaWRlcyB7XG4gIGhlaWdodDogMTAwJTtcbiAgei1pbmRleDogMTtcbn1cblxuXG4uc3dpcGVyLXNsaWRlIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cblxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc2OHB4KXtcbiAgaW1nIHtcbiAgICB3aWR0aDogMzgwcHg7XG4gICAgaGVpZ2h0OiAzNjBweDtcbiAgICBtYXJnaW46IDYwcHggMCA0MHB4O1xuICB9XG5cbiAgaDJ7XG4gICAgZm9udC1zaXplOiA0MHB4O1xuICB9XG5cbiAgaW9uLXNsaWRle1xuICAgIHB7XG4gICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgfVxuICB9XG5cbiAgYnV0dG9ue1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgfVxuICBcbn1cblxuIl19 */");

/***/ }),

/***/ "eTM1":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/pages/home/home.page.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\n  <ion-content fullscreen scroll-y=\"false\">\n   \n     <div class=\"container-btn\">\n      <button class=\"btn-omitir\" [routerLink]=\"['login']\">Omitir</button>\n     </div> \n    <ion-slides pager=\"true\" >\n      <ion-slide>\n        <img class=\"img-cuadrito\" src=\"../../assets/img/carrito1.png\" />\n        <h2>Elije una parada</h2>\n        <p>Elige entre paradas fijas o una parada personalizada</p>\n        <div class=\"product-image-overlay\"></div>\n      </ion-slide>\n\n      <ion-slide >\n        <img class=\"img-cuadrito\" src=\"../../assets/img/metododepago.png\"    />\n        <h2>Elige tu método de pago</h2>\n        <p>\n          Selecciona una forma de pago preferida o paga una suscripcion para acceder a viajes premium\n        </p>\n        <div class=\"product-image-overlay\"></div>\n      </ion-slide>\n\n      <ion-slide >\n        <img class=\"img-cuadrito\" src=\"../../assets/img/carroavios.png\"  />\n        <h2>Disfruta de tu viaje</h2>\n        <p>Relajate y disfruta de un viaje seguro con iGO</p>\n\n        <div class=\"product-image-overlay\"></div>\n      </ion-slide>\n\n      <ion-slide>\n        <img class=\"img-cuadrito\" src=\"../../assets/img/igoLogo.png\" />\n        <h2>Empecemos Ya!</h2>\n        <p></p>\n        <div style=\"width: 100%; display: flex; justify-content: center;\">\n          <button routerLink=\"login\" class=\"btn\">iGO!</button>\n        </div>\n        <div class=\"product-image-overlay\"></div>\n      </ion-slide>\n    </ion-slides>\n  </ion-content>\n\n  <ion-footer>\n    <ion-toolbar>\n    <div class=\"container-circles\">\n        <div class=\"circle activated\"></div>\n        <div class=\"circle\"></div>\n        <div class=\"circle\"></div>\n    </div>\n    </ion-toolbar>\n  </ion-footer>\n</ion-app>\n");

/***/ }),

/***/ "t95I":
/*!****************************************************************!*\
  !*** ./src/app/modules/home/pages/home/home-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "2GcX");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomePageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module.js.map