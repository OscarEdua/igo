(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-history-history-module"],{

/***/ "4CY+":
/*!*******************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/history/history.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card {\n  border-radius: 15px;\n  box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.1);\n  background: #FFFFFF;\n}\n\np {\n  font-weight: normal;\n  font-size: 15px;\n  line-height: 18px;\n  color: #000000;\n}\n\n.container {\n  margin: auto;\n}\n\n.img-container {\n  text-align: center;\n}\n\n.type-vehicle {\n  text-align: center;\n}\n\n.point {\n  border-radius: 100%;\n  width: 8px;\n  height: 8px;\n  background: blue;\n  margin-bottom: 5px;\n}\n\n.stick {\n  width: 1px;\n  height: 40px;\n  background: black;\n  margin-top: 5px;\n  margin-left: 3px;\n}\n\n.point-container {\n  text-align: center;\n}\n\n.icons-container {\n  position: relative;\n  top: 5px;\n  left: 15px;\n}\n\nion-content > ion-card {\n  margin-top: 60px;\n}\n\n/* Contenedor de imagen del usuario */\n\n.container-img {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.container-img .img {\n  width: 82px;\n  height: 80px;\n  border-radius: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2hpc3RvcnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsbUJBQUE7RUFDQSwyQ0FBQTtFQUNBLG1CQUFBO0FBQ0Q7O0FBRUE7RUFDQyxtQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFDRDs7QUFFQTtFQUNDLFlBQUE7QUFDRDs7QUFFQTtFQUNDLGtCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxrQkFBQTtBQUNEOztBQUVBO0VBQ0MsbUJBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFDRDs7QUFFQTtFQUNDLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFDRDs7QUFFQTtFQUNDLGtCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxrQkFBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0FBQ0Q7O0FBRUE7RUFDQyxnQkFBQTtBQUNEOztBQUVBLHFDQUFBOztBQUNBO0VBQ0MsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFDRDs7QUFBQztFQUNDLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FBRUYiLCJmaWxlIjoiaGlzdG9yeS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCB7XG5cdGJvcmRlci1yYWRpdXM6IDE1cHg7XG5cdGJveC1zaGFkb3c6IDBweCAwcHggMzBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG5cdGJhY2tncm91bmQ6ICNGRkZGRkY7XG59XG5cbnAge1xuXHRmb250LXdlaWdodDogbm9ybWFsO1xuXHRmb250LXNpemU6IDE1cHg7XG5cdGxpbmUtaGVpZ2h0OiAxOHB4O1xuXHRjb2xvcjogIzAwMDAwMDtcbn1cblxuLmNvbnRhaW5lciB7XG5cdG1hcmdpbjogYXV0bztcbn1cblxuLmltZy1jb250YWluZXIge1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi50eXBlLXZlaGljbGUge1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5wb2ludCB7XG5cdGJvcmRlci1yYWRpdXM6IDEwMCU7XG5cdHdpZHRoOiA4cHg7XG5cdGhlaWdodDogOHB4O1xuXHRiYWNrZ3JvdW5kOiBibHVlO1xuXHRtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbi5zdGljayB7XG5cdHdpZHRoOiAxcHg7XG5cdGhlaWdodDogNDBweDtcblx0YmFja2dyb3VuZDogYmxhY2s7XG5cdG1hcmdpbi10b3A6IDVweDtcblx0bWFyZ2luLWxlZnQ6IDNweDtcbn1cblxuLnBvaW50LWNvbnRhaW5lciB7XG5cdHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmljb25zLWNvbnRhaW5lciB7XG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcblx0dG9wOiA1cHg7XG5cdGxlZnQ6IDE1cHg7XG59XG5cbmlvbi1jb250ZW50ID4gaW9uLWNhcmQge1xuXHRtYXJnaW4tdG9wOiA2MHB4O1xufVxuXG4vKiBDb250ZW5lZG9yIGRlIGltYWdlbiBkZWwgdXN1YXJpbyAqL1xuLmNvbnRhaW5lci1pbWd7XG5cdGRpc3BsYXk6IGZsZXg7XG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXHQuaW1ne1xuXHRcdHdpZHRoOiA4MnB4O1xuXHRcdGhlaWdodDogODBweDtcblx0XHRib3JkZXItcmFkaXVzOiAxMHB4O1xuXHRcdGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG5cdFx0YmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcblx0fVxufSJdfQ== */");

/***/ }),

/***/ "Bua2":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/history/history.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header title=\"Historial\"></app-header>\n<ion-content>\n    <div class=\"container\">\n  <!--     <ion-searchbar inputmode=\"text\" placeholder=\"Buscar por  nombre\" (ionChange)=\"onSearchChange($event)\">\n      </ion-searchbar> -->\n\n     <div *ngFor=\"let travel of travels | async \">\n      <ion-card *ngIf=\"uUser?.role == 'admin'\">\n        <ion-grid>\n          <ion-row class=\"ion-justify-content-center ion-padding-top\">\n            <ion-col size=\"4\" class=\"container-img\">\n              <div *ngIf=\"travel.client.imagen != ''\" style=\"background-image: url({{ travel.client.imagen }});\" class=\"img\">\n              </div>\n              <div *ngIf=\"travel.client.imagen == ''\" style=\"background-image: url('../../../../../assets/img/user.png');\" class=\"img\">\n              </div>\n            </ion-col>\n            <ion-col size=\"6\">\n              <p>Número de Pasajeros: {{travel.numberPassengers}} </p>\n              <p *ngIf=\"travel.payMethod == 'cash'\">Tipo de Pago: Efectivo</p>\n              <p *ngIf=\"travel.payMethod == 'transfer'\">Tipo de Pago: Transferencia</p>\n              <p>Tipo de Vehículo: {{travel.vehicleType}} </p>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col size=\"12\">\n              <ion-card>\n                <ion-row>\n                  <ion-col size=\"1\">\n                    <div class=\"icons-container\">\n                      <div class=\"point-container\">\n                        <p class=\"point\"></p>\n                      </div>\n                      <div class=\"stick\"></div>\n                    </div>\n                  </ion-col>\n                  <ion-col size=\"11\">\n                    <ion-item>  {{travel.destinyLocation.address}} </ion-item>  \n                    <ion-item lines=\"none\">{{travel.startLocation.address}} </ion-item>\n                  </ion-col>\n                </ion-row>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card>\n\n      <ion-card *ngIf=\"userId == travel.driverId\">\n        <ion-grid>\n          <ion-row class=\"ion-justify-content-center ion-padding-top\">\n            <ion-col size=\"4\" class=\"container-img\">\n              <div *ngIf=\"travel.client.imagen != ''\" style=\"background-image: url({{ travel.client.imagen }});\" class=\"img\">\n              </div>\n              <div *ngIf=\"travel.client.imagen == ''\" style=\"background-image: url('../../../../../assets/img/user.png');\" class=\"img\">\n              </div>\n            </ion-col>\n            <ion-col size=\"6\">\n              <p>Número de Pasajeros: {{travel.numberPassengers}} </p>\n              <p *ngIf=\"travel.payMethod == 'cash'\">Tipo de Pago: Efectivo</p>\n              <p *ngIf=\"travel.payMethod == 'transfer'\">Tipo de Pago: Transferencia</p>\n              <p>Tipo de Vehículo: {{travel.vehicleType}} </p>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col size=\"12\">\n              <ion-card>\n                <ion-row>\n                  <ion-col size=\"1\">\n                    <div class=\"icons-container\">\n                      <div class=\"point-container\">\n                        <p class=\"point\"></p>\n                      </div>\n                      <div class=\"stick\"></div>\n                    </div>\n                  </ion-col>\n                  <ion-col size=\"11\">\n                    <ion-item>  {{travel.destinyLocation.address}} </ion-item>  \n                    <ion-item lines=\"none\">{{travel.startLocation.address}} </ion-item>\n                  </ion-col>\n                </ion-row>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card>\n      \n      <ion-card *ngIf=\"userId == travel.clientId\">\n        <ion-grid>\n          <ion-row class=\"ion-justify-content-center ion-padding-top\">\n            <ion-col size=\"4\" class=\"container-img\">\n              <div *ngIf=\"travel.client.imagen != ''\" style=\"background-image: url({{ travel.client.imagen }});\" class=\"img\">\n              </div>\n              <div *ngIf=\"travel.client.imagen == ''\" style=\"background-image: url('../../../../../assets/img/user.png');\" class=\"img\">\n              </div>\n            </ion-col>\n            <ion-col size=\"6\">\n              <p>Número de Pasajeros: {{travel.numberPassengers}} </p>\n              <p *ngIf=\"travel.payMethod == 'cash'\">Tipo de Pago: Efectivo</p>\n              <p *ngIf=\"travel.payMethod == 'transfer'\">Tipo de Pago: Transferencia</p>\n              <p>Tipo de Vehículo: {{travel.vehicleType}} </p>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col size=\"12\">\n              <ion-card>\n                <ion-row>\n                  <ion-col size=\"1\">\n                    <div class=\"icons-container\">\n                      <div class=\"point-container\">\n                        <p class=\"point\"></p>\n                      </div>\n                      <div class=\"stick\"></div>\n                    </div>\n                  </ion-col>\n                  <ion-col size=\"11\">\n                    <ion-item>  {{travel.destinyLocation.address}} </ion-item>  \n                    <ion-item lines=\"none\">{{travel.startLocation.address}} </ion-item>\n                  </ion-col>\n                </ion-row>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card>\n    </div> \n    </div>\n</ion-content>\n");

/***/ }),

/***/ "N7kH":
/*!*******************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/history/history.module.ts ***!
  \*******************************************************************/
/*! exports provided: HistoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPageModule", function() { return HistoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _history_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./history-routing.module */ "Pdn0");
/* harmony import */ var _history_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./history.page */ "tc5X");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let HistoryPageModule = class HistoryPageModule {
};
HistoryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _history_routing_module__WEBPACK_IMPORTED_MODULE_5__["HistoryPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_history_page__WEBPACK_IMPORTED_MODULE_6__["HistoryPage"]]
    })
], HistoryPageModule);



/***/ }),

/***/ "Pdn0":
/*!***************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/history/history-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: HistoryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPageRoutingModule", function() { return HistoryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _history_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./history.page */ "tc5X");




const routes = [
    {
        path: '',
        component: _history_page__WEBPACK_IMPORTED_MODULE_3__["HistoryPage"]
    }
];
let HistoryPageRoutingModule = class HistoryPageRoutingModule {
};
HistoryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HistoryPageRoutingModule);



/***/ }),

/***/ "tc5X":
/*!*****************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/history/history.page.ts ***!
  \*****************************************************************/
/*! exports provided: HistoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPage", function() { return HistoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_history_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./history.page.html */ "Bua2");
/* harmony import */ var _history_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./history.page.scss */ "4CY+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/admin/travels.service */ "KlPZ");
/* harmony import */ var _core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/authentication/authentication.service */ "6CRC");
/* harmony import */ var _home_services_users_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../home/services/users.service */ "6JOU");







let HistoryPage = class HistoryPage {
    constructor(travelsService, usersService, autenticationService) {
        this.travelsService = travelsService;
        this.usersService = usersService;
        this.autenticationService = autenticationService;
        this.userId = this.autenticationService.getCurrentUserId();
        this.oUser = this.usersService.getUserById(this.userId);
        this.oUser.subscribe(user => this.uUser = user);
    }
    ngOnInit() {
        this.travels = this.travelsService.getTravelsHistorial();
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
};
HistoryPage.ctorParameters = () => [
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_4__["TravelsService"] },
    { type: _home_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UsersService"] },
    { type: _core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"] }
];
HistoryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-history',
        template: _raw_loader_history_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_history_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], HistoryPage);



/***/ })

}]);
//# sourceMappingURL=pages-history-history-module.js.map