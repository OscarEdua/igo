(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-bank-data-bank-data-module"],{

/***/ "l/Tr":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/bank-data/bank-data-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: BankDataPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankDataPageRoutingModule", function() { return BankDataPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _bank_data_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bank-data.page */ "oaVf");




const routes = [
    {
        path: '',
        component: _bank_data_page__WEBPACK_IMPORTED_MODULE_3__["BankDataPage"]
    }
];
let BankDataPageRoutingModule = class BankDataPageRoutingModule {
};
BankDataPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BankDataPageRoutingModule);



/***/ }),

/***/ "l/ZI":
/*!***********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/bank-data/bank-data.module.ts ***!
  \***********************************************************************/
/*! exports provided: BankDataPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankDataPageModule", function() { return BankDataPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _bank_data_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bank-data-routing.module */ "l/Tr");
/* harmony import */ var _bank_data_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bank-data.page */ "oaVf");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let BankDataPageModule = class BankDataPageModule {
};
BankDataPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _bank_data_routing_module__WEBPACK_IMPORTED_MODULE_5__["BankDataPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_bank_data_page__WEBPACK_IMPORTED_MODULE_6__["BankDataPage"]]
    })
], BankDataPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-bank-data-bank-data-module.js.map