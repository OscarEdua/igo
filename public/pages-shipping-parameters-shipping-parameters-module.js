(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-shipping-parameters-shipping-parameters-module"],{

/***/ "+nW0":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/shipping-parameters/shipping-parameters.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: ShippingParametersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingParametersPageModule", function() { return ShippingParametersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _shipping_parameters_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shipping-parameters-routing.module */ "EbdO");
/* harmony import */ var _shipping_parameters_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shipping-parameters.page */ "xvIe");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let ShippingParametersPageModule = class ShippingParametersPageModule {
};
ShippingParametersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _shipping_parameters_routing_module__WEBPACK_IMPORTED_MODULE_5__["ShippingParametersPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_shipping_parameters_page__WEBPACK_IMPORTED_MODULE_6__["ShippingParametersPage"]]
    })
], ShippingParametersPageModule);



/***/ }),

/***/ "2Tq3":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/shipping-parameters/shipping-parameters.page.scss ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  display: flex;\n  justify-content: center;\n}\n.container .container-inputs {\n  margin-bottom: 3%;\n  padding: 10px;\n  width: 90%;\n  text-align: left;\n}\n.container .container-inputs ion-select {\n  width: 100%;\n  justify-content: center;\n}\n.container .container-inputs .label {\n  color: #3E4958;\n  font-weight: bold;\n  padding-left: 10px;\n  font-size: 18px;\n  line-height: 20px;\n}\n.container .container-inputs .input-ionItem {\n  --background: #F7F8F9;\n  --highlight-color-focused: #008D36;\n  --highlight-color-valid: #008D36;\n  border: 0.5px solid #D5DDE0;\n  border-radius: 15px;\n  margin-top: 3%;\n}\n.container .container-select {\n  text-align: left;\n  padding: 10px;\n  width: 90%;\n  margin: 0px;\n}\n/* Estilo del boton */\n.container-btn {\n  display: flex;\n  justify-content: center;\n}\n.error {\n  color: tomato;\n  font-size: 15px;\n  margin-left: 16px;\n}\n.margin {\n  margin-top: 5%;\n}\n.btn {\n  margin-top: 10%;\n  width: 78%;\n  height: 60px;\n}\n.green {\n  --background: #008D36;\n  --background-activated:#008D36;\n  color: white;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NoaXBwaW5nLXBhcmFtZXRlcnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0FBQUY7QUFFRTtFQUNFLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtBQUFKO0FBRUk7RUFDRSxXQUFBO0VBQ0EsdUJBQUE7QUFBTjtBQUlFO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFGSjtBQUtJO0VBQ0UscUJBQUE7RUFDQSxrQ0FBQTtFQUNBLGdDQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFITjtBQVFFO0VBQ0UsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QUFOSjtBQVVFLHFCQUFBO0FBQ0Y7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUFQSjtBQVNFO0VBQ0UsYUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQU5KO0FBUUE7RUFDSSxjQUFBO0FBTEo7QUFRQTtFQUNJLGVBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtBQUxKO0FBT0E7RUFDSSxxQkFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBSkoiLCJmaWxlIjoic2hpcHBpbmctcGFyYW1ldGVycy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAuY29udGFpbmVyLWlucHV0cyB7XG4gICAgbWFyZ2luLWJvdHRvbTogMyU7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICB3aWR0aDogOTAlO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIFxuICAgIGlvbi1zZWxlY3Qge1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICB9XG4gIFxuICAgIFxuICAubGFiZWwge1xuICAgIGNvbG9yOiAjM0U0OTU4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIH1cblxuICAgIC5pbnB1dC1pb25JdGVtIHtcbiAgICAgIC0tYmFja2dyb3VuZDogI0Y3RjhGOTtcbiAgICAgIC0taGlnaGxpZ2h0LWNvbG9yLWZvY3VzZWQ6ICMwMDhEMzY7XG4gICAgICAtLWhpZ2hsaWdodC1jb2xvci12YWxpZDogIzAwOEQzNjtcbiAgICAgIGJvcmRlcjogMC41cHggc29saWQgI0Q1RERFMDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgICBtYXJnaW4tdG9wOiAzJTtcbiAgICB9XG4gICAgXG4gIH1cblxuICAuY29udGFpbmVyLXNlbGVjdHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBtYXJnaW46IDBweDtcbiAgfVxufVxuICBcbiAgLyogRXN0aWxvIGRlbCBib3RvbiAqL1xuLmNvbnRhaW5lci1idG4ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cbiAgLmVycm9yIHtcbiAgICBjb2xvcjogdG9tYXRvO1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBtYXJnaW4tbGVmdDogMTZweDtcbn1cbi5tYXJnaW57XG4gICAgbWFyZ2luLXRvcDogNSU7XG59XG5cbi5idG4ge1xuICAgIG1hcmdpbi10b3A6IDEwJTtcbiAgICB3aWR0aDogNzglO1xuICAgIGhlaWdodDogNjBweDtcbiAgfVxuLmdyZWVuIHtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDhEMzY7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDojMDA4RDM2O1xuICAgIGNvbG9yOndoaXRlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufSJdfQ== */");

/***/ }),

/***/ "EbdO":
/*!***************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/shipping-parameters/shipping-parameters-routing.module.ts ***!
  \***************************************************************************************************/
/*! exports provided: ShippingParametersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingParametersPageRoutingModule", function() { return ShippingParametersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _shipping_parameters_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shipping-parameters.page */ "xvIe");




const routes = [
    {
        path: '',
        component: _shipping_parameters_page__WEBPACK_IMPORTED_MODULE_3__["ShippingParametersPage"]
    }
];
let ShippingParametersPageRoutingModule = class ShippingParametersPageRoutingModule {
};
ShippingParametersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ShippingParametersPageRoutingModule);



/***/ }),

/***/ "W6fD":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/shipping-parameters/shipping-parameters.page.html ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons class=\"margin-left\" slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-row>\n      <ion-title>Parámetros iGo</ion-title>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n<ion-content fullscreen>\n  <div class=\"margin\"> \n    <form [formGroup]=\"form\" (ngSubmit)=\"update()\">\n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">Kilómetros</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"number\" formControlName=\"baseKilometers\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div\n        class=\"error\"\n        *ngIf=\"form.controls.baseKilometers.errors && form.controls.baseKilometers.touched\">\n        El campo kilómetros es requerido.\n      </div>     \n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">Costo por kilómetros de inicio</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"number\" formControlName=\"baseCost\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div class=\"error\" *ngIf=\"form.controls.baseCost.errors && form.controls.baseCost.touched\">\n        El campo costo por kilómetros de inicio es requerido.\n      </div>\n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">Costo por kilómetro extra</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"number\" formControlName=\"extraKilometerCost\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div\n        class=\"error\"\n        *ngIf=\"form.controls.extraKilometerCost.errors && form.controls.extraKilometerCost.touched\">\n        El campo costo por kilómetro extra es requerido.\n      </div>\n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">Porcentaje adicional VIP</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"number\" formControlName=\"extraVip\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div class=\"error\" *ngIf=\"form.controls.extraVip.errors && form.controls.extraVip.touched\">\n        El campo porcentaje adicional VIP es requerido.\n      </div>\n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">Número WhatsApp</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"number\" formControlName=\"numWhatsApp\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div\n        class=\"error\"\n        *ngIf=\"form.controls.numWhatsApp.errors && form.controls.numWhatsApp.touched\">\n        El campo Número WhatsApp es requerido.\n      </div> \n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">Mensaje WhatsApp</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-textarea type=\"string\" formControlName=\"messageWhatsApp\"></ion-textarea>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div\n        class=\"error\"\n        *ngIf=\"form.controls.messageWhatsApp.errors && form.controls.messageWhatsApp.touched\">\n        El campo Mensaje WhatsApp es requerido.\n      </div> \n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">Número Emergencia</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"number\" formControlName=\"numEmergency\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div\n        class=\"error\"\n        *ngIf=\"form.controls.numEmergency.errors && form.controls.numEmergency.touched\">\n        El campo Número Emergencia es requerido.\n      </div>\n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">Número Administrador</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"number\" formControlName=\"numCall\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div\n        class=\"error\"\n        *ngIf=\"form.controls.numCall.errors && form.controls.numCall.touched\">\n        El campo Número Administrador es requerido.\n      </div>\n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">Mensaje Administrador</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-textarea type=\"string\" formControlName=\"messageCall\"></ion-textarea>\n          </ion-item>\n        </div>\n      </ion-row>\n      <div\n        class=\"error\"\n        *ngIf=\"form.controls.messageCall.errors && form.controls.messageCall.touched\">\n        El campo Mensaje Administrador es requerido.\n      </div> \n\n      <ion-row class=\"container-btn\">\n        <ion-button class=\"btn green\" (click)=\"update()\" [disabled]=\"!form.valid\"> Actualizar </ion-button>\n      </ion-row>\n    </form>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "xvIe":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/shipping-parameters/shipping-parameters.page.ts ***!
  \*****************************************************************************************/
/*! exports provided: ShippingParametersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingParametersPage", function() { return ShippingParametersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_shipping_parameters_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./shipping-parameters.page.html */ "W6fD");
/* harmony import */ var _shipping_parameters_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shipping-parameters.page.scss */ "2Tq3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _shared_services_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../shared/services/shipping-parameters.service */ "jNID");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "TEn/");








let ShippingParametersPage = class ShippingParametersPage {
    constructor(shippingParametersService, alertService, menuController) {
        this.shippingParametersService = shippingParametersService;
        this.alertService = alertService;
        this.menuController = menuController;
        this.observableList = [];
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            baseKilometers: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            baseCost: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            extraKilometerCost: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            extraVip: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            messageCall: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            messageWhatsApp: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            numCall: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            numEmergency: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            numWhatsApp: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
        });
    }
    ngOnInit() {
        this.getParametrers();
    }
    getParametrers() {
        const observable = this.shippingParametersService.getparametes().subscribe((res) => {
            this.getshippingParameters = res;
            this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
                baseKilometers: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.getshippingParameters['baseKilometers'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,]),
                baseCost: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.getshippingParameters['baseCost'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
                extraKilometerCost: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.getshippingParameters['extraKilometerCost'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,]),
                extraVip: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.getshippingParameters['extraVip'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
                messageCall: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.getshippingParameters['messageCall'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
                messageWhatsApp: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.getshippingParameters['messageWhatsApp'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
                numCall: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.getshippingParameters['numCall'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
                numEmergency: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.getshippingParameters['numEmergency'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
                numWhatsApp: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.getshippingParameters['numWhatsApp'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])
            });
        });
    }
    update() {
        this.setshippingParameters = {
            baseKilometers: this.form.controls.baseKilometers.value,
            baseCost: this.form.controls.baseCost.value,
            extraKilometerCost: this.form.controls.extraKilometerCost.value,
            extraVip: this.form.controls.extraVip.value,
            messageCall: this.form.controls.messageCall.value,
            messageWhatsApp: this.form.controls.messageWhatsApp.value,
            numCall: this.form.controls.numCall.value,
            numEmergency: this.form.controls.numEmergency.value,
            numWhatsApp: this.form.controls.numWhatsApp.value
        };
        this.shippingParametersService
            .update(this.setshippingParameters)
            .then(() => this.alertService.presentAlert('Parametros Actualizados ✔'))
            .catch((error) => console.log(error));
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
    openMenu() {
        this.menuController.open('principal');
    }
};
ShippingParametersPage.ctorParameters = () => [
    { type: _shared_services_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_5__["ShippingParametersService"] },
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"] }
];
ShippingParametersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-shipping-parameters',
        template: _raw_loader_shipping_parameters_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_shipping_parameters_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ShippingParametersPage);



/***/ })

}]);
//# sourceMappingURL=pages-shipping-parameters-shipping-parameters-module.js.map