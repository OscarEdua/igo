(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-requests-requests-module"],{

/***/ "7dfg":
/*!*********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/requests/requests.module.ts ***!
  \*********************************************************************/
/*! exports provided: RequestsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestsPageModule", function() { return RequestsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _requests_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./requests-routing.module */ "f9da");
/* harmony import */ var _requests_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./requests.page */ "LYy4");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let RequestsPageModule = class RequestsPageModule {
};
RequestsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _requests_routing_module__WEBPACK_IMPORTED_MODULE_5__["RequestsPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_requests_page__WEBPACK_IMPORTED_MODULE_6__["RequestsPage"]]
    })
], RequestsPageModule);



/***/ }),

/***/ "8kji":
/*!*************************************************!*\
  !*** ./src/app/shared/services/push.service.ts ***!
  \*************************************************/
/*! exports provided: PushService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PushService", function() { return PushService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "wljF");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





let PushService = class PushService {
    constructor(oneSignal, http, router) {
        this.oneSignal = oneSignal;
        this.http = http;
        this.router = router;
    }
    configuracionInicial(uid) {
        /*     this.oneSignal. */
        this.oneSignal.setExternalUserId(uid);
        this.oneSignal.startInit('4f3b79de-e102-47ff-a1ac-f7753796d478', '796178968721');
        this.oneSignal.enableSound(true);
        this.oneSignal.enableVibrate(true);
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
        this.oneSignal.handleNotificationReceived().subscribe((noti) => {
            // Cuando recibe
            this.ext = noti.payload.additionalData['ruta'] + '';
        });
        this.oneSignal.handleNotificationOpened().subscribe((noti) => {
            // Cuando abre una notifiacion
            // this.router.navigate(['/notice/'+this.ext]);
            this.router.navigate([this.ext]);
        });
        //obtener uid de onsignal
        this.oneSignal.getIds().then((info) => {
            this.useId = info.userId;
        });
        this.oneSignal.endInit();
    }
    //Enviar  notificaciones individuales
    sendByUid(title, subtitle, detalle, ruta, uiduser) {
        //dato a mandar
        const datos = {
            app_id: '4f3b79de-e102-47ff-a1ac-f7753796d478',
            included_segments: ['Active User', 'Inactive User'],
            headings: { en: title },
            subtitle: { en: subtitle },
            data: { ruta: ruta, fecha: '100-210-250' },
            contents: { en: detalle },
            channel_for_external_user_ids: 'push',
            include_external_user_ids: [uiduser],
        };
        this.post(datos);
    }
    deleteuser() {
        this.oneSignal.removeExternalUserId();
    }
    //Enviar  notificaciones Global
    sendGlobal(title, subtitle, detalle, ruta) {
        //dato a mandar
        const datos = {
            app_id: '4f3b79de-e102-47ff-a1ac-f7753796d478',
            included_segments: ['Active Users', 'Inactive Users'],
            headings: { en: title },
            subtitle: { en: subtitle },
            data: { ruta: ruta, fecha: '100-210-250' },
            contents: { en: detalle },
        };
        this.post(datos);
    }
    post(datos) {
        const headers = {
            Authorization: 'Basic ODM0ODMzNzQtZjIzNi00NWU1LTk3ZDMtZmI1NmI3ZDhjZTFk',
        };
        var url = 'https://onesignal.com/api/v1/notifications';
        return new Promise((resolve) => {
            this.http.post(url, datos, { headers }).subscribe((data) => {
                resolve(data);
            });
        });
    }
};
PushService.ctorParameters = () => [
    { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_2__["OneSignal"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
PushService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], PushService);



/***/ }),

/***/ "f9da":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/requests/requests-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: RequestsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestsPageRoutingModule", function() { return RequestsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _requests_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./requests.page */ "LYy4");




const routes = [
    {
        path: '',
        component: _requests_page__WEBPACK_IMPORTED_MODULE_3__["RequestsPage"]
    }
];
let RequestsPageRoutingModule = class RequestsPageRoutingModule {
};
RequestsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RequestsPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-requests-requests-module.js.map