(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-schedule-trip-modal-schedule-trip-modal-module"],{

/***/ "178Q":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/schedule-trip-modal/schedule-trip-modal.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: ScheduleTripModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScheduleTripModalPageModule", function() { return ScheduleTripModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _schedule_trip_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./schedule-trip-modal-routing.module */ "ZhVO");
/* harmony import */ var _schedule_trip_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./schedule-trip-modal.page */ "c5KJ");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");








let ScheduleTripModalPageModule = class ScheduleTripModalPageModule {
};
ScheduleTripModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _schedule_trip_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["ScheduleTripModalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_schedule_trip_modal_page__WEBPACK_IMPORTED_MODULE_6__["ScheduleTripModalPage"]]
    })
], ScheduleTripModalPageModule);



/***/ }),

/***/ "ZhVO":
/*!**********************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/schedule-trip-modal/schedule-trip-modal-routing.module.ts ***!
  \**********************************************************************************************************/
/*! exports provided: ScheduleTripModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScheduleTripModalPageRoutingModule", function() { return ScheduleTripModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _schedule_trip_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./schedule-trip-modal.page */ "c5KJ");




const routes = [
    {
        path: '',
        component: _schedule_trip_modal_page__WEBPACK_IMPORTED_MODULE_3__["ScheduleTripModalPage"]
    }
];
let ScheduleTripModalPageRoutingModule = class ScheduleTripModalPageRoutingModule {
};
ScheduleTripModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ScheduleTripModalPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-schedule-trip-modal-schedule-trip-modal-module.js.map