(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-destination-address-destination-address-module"],{

/***/ "2NHr":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/destination-address/destination-address.page.scss ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJkZXN0aW5hdGlvbi1hZGRyZXNzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "cX0g":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/destination-address/destination-address.page.html ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header title=\"Direccion de Destino\"></app-header>\n<ion-content>\n  <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>\n</ion-content>\n");

/***/ }),

/***/ "dgll":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/destination-address/destination-address.page.ts ***!
  \*****************************************************************************************/
/*! exports provided: DestinationAddressPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DestinationAddressPage", function() { return DestinationAddressPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_destination_address_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./destination-address.page.html */ "cX0g");
/* harmony import */ var _destination_address_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./destination-address.page.scss */ "2NHr");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _modals_destination_address_modal_destination_address_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modals/destination-address-modal/destination-address-modal.page */ "sVj7");






let DestinationAddressPage = class DestinationAddressPage {
    constructor(alertsService) {
        this.alertsService = alertsService;
    }
    ngOnInit() {
    }
    openModal() {
        this.alertsService.presentModalWithoutTravelData(_modals_destination_address_modal_destination_address_modal_page__WEBPACK_IMPORTED_MODULE_5__["DestinationAddressModalPage"], 'destination-address-modal');
    }
};
DestinationAddressPage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] }
];
DestinationAddressPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-destination-address',
        template: _raw_loader_destination_address_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_destination_address_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], DestinationAddressPage);



/***/ }),

/***/ "vwtk":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/destination-address/destination-address.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: DestinationAddressPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DestinationAddressPageModule", function() { return DestinationAddressPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _destination_address_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./destination-address-routing.module */ "xhXM");
/* harmony import */ var _destination_address_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./destination-address.page */ "dgll");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let DestinationAddressPageModule = class DestinationAddressPageModule {
};
DestinationAddressPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _destination_address_routing_module__WEBPACK_IMPORTED_MODULE_5__["DestinationAddressPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_destination_address_page__WEBPACK_IMPORTED_MODULE_6__["DestinationAddressPage"]]
    })
], DestinationAddressPageModule);



/***/ }),

/***/ "xhXM":
/*!***************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/destination-address/destination-address-routing.module.ts ***!
  \***************************************************************************************************/
/*! exports provided: DestinationAddressPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DestinationAddressPageRoutingModule", function() { return DestinationAddressPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _destination_address_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./destination-address.page */ "dgll");




const routes = [
    {
        path: '',
        component: _destination_address_page__WEBPACK_IMPORTED_MODULE_3__["DestinationAddressPage"]
    }
];
let DestinationAddressPageRoutingModule = class DestinationAddressPageRoutingModule {
};
DestinationAddressPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DestinationAddressPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-destination-address-destination-address-module.js.map