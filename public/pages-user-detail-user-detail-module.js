(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-detail-user-detail-module"],{

/***/ "9Qab":
/*!*************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/user-detail/user-detail.page.ts ***!
  \*************************************************************************/
/*! exports provided: UserDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDetailPage", function() { return UserDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_user_detail_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./user-detail.page.html */ "zNdB");
/* harmony import */ var _user_detail_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user-detail.page.scss */ "gB08");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/modules/home/services/users.service */ "6JOU");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");







let UserDetailPage = class UserDetailPage {
    constructor(usersService, activatedRoute, alertService) {
        this.usersService = usersService;
        this.activatedRoute = activatedRoute;
        this.alertService = alertService;
        this.userId = activatedRoute.snapshot.paramMap.get('userId');
    }
    ngOnInit() {
        this.user = this.usersService.getUserById(this.userId);
        this.user.subscribe((data) => {
            this.uUser = data;
        });
    }
    updateRoleByUid() {
        this.uUser.role = this.role;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Cambio de Rol satisfactorio');
    }
    unsuscribeUser() {
        this.uUser.isActive = false;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Usuario Desactivado');
    }
    suscribeUser() {
        this.uUser.isActive = true;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Usuario Activado');
    }
    checkRole(event) {
        this.role = event.detail.value;
    }
};
UserDetailPage.ctorParameters = () => [
    { type: src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UsersService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] }
];
UserDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-user-detail',
        template: _raw_loader_user_detail_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_user_detail_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UserDetailPage);



/***/ }),

/***/ "W1NF":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/user-detail/user-detail-routing.module.ts ***!
  \***********************************************************************************/
/*! exports provided: UserDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDetailPageRoutingModule", function() { return UserDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _user_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-detail.page */ "9Qab");




const routes = [
    {
        path: '',
        component: _user_detail_page__WEBPACK_IMPORTED_MODULE_3__["UserDetailPage"]
    }
];
let UserDetailPageRoutingModule = class UserDetailPageRoutingModule {
};
UserDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UserDetailPageRoutingModule);



/***/ }),

/***/ "gB08":
/*!***************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/user-detail/user-detail.page.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilos del header */\nion-toolbar ion-row {\n  align-items: center;\n  display: flex;\n  justify-content: center;\n  width: 85%;\n}\nion-toolbar ion-row ion-title {\n  font-size: 20px;\n  font-weight: bold;\n  --color: #3E4958;\n  text-align: center;\n}\n.container {\n  padding: 0px 25px;\n}\n/* Estilo para colocar un margen izquierdo a cualquier elemento */\n.margin-left {\n  margin-left: 5%;\n}\n/* Estilos de las tarjetas */\nion-card {\n  border-radius: 15px;\n  box-shadow: none;\n  margin-top: 10%;\n  width: 90%;\n  --background: rgba(0, 0, 0, 0);\n  border: none;\n}\nion-card ion-card-header ion-card-title {\n  color: #333333;\n  font-weight: bold;\n}\nion-card ion-card-header ion-card-subtitle {\n  color: #3E4958;\n  font-size: 13.5px;\n  font-weight: 500;\n  margin: 8px 0px;\n}\nion-card ion-card-header .container-starts ion-icon {\n  font-size: 20px;\n  margin-right: 9px;\n}\n/* Contenedor de imagen del usuario */\n.container-img {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.container-img .img {\n  width: 82px;\n  height: 80px;\n  border-radius: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n/* Color verde para algunos elementos */\n.green {\n  color: #008D36;\n}\n.btn {\n  width: 100%;\n  height: 60px;\n  margin-top: 3%;\n  --border-radius: 15px;\n  --box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  font-size: 17px;\n}\n/* Estilo del boton verde */\n.green-btn {\n  --background: #008D36;\n  --background-activated: #008D36;\n  --color: white;\n  font-weight: bold;\n}\n/* Estilo del boton blanco */\n.white {\n  --background: white;\n  --background-activated: white;\n  --color: #4B545A;\n  font-weight: bold;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n}\n/* Estilo del boton rojo */\n.red {\n  --background: #CB2323;\n  --background-activated: #CB2323;\n  --color: white;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3VzZXItZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx1QkFBQTtBQUVJO0VBQ0UsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxVQUFBO0FBQU47QUFFTTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFBUjtBQU1FO0VBQ0UsaUJBQUE7QUFISjtBQU1FLGlFQUFBO0FBQ0E7RUFDRSxlQUFBO0FBSEo7QUFNRSw0QkFBQTtBQUNBO0VBQ0UsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsOEJBQUE7RUFDQSxZQUFBO0FBSEo7QUFNTTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtBQUpSO0FBT007RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUFMUjtBQVNRO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBUFY7QUFjRSxxQ0FBQTtBQUNBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFYSjtBQWFJO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUFYTjtBQWVFLHVDQUFBO0FBQ0E7RUFDRSxjQUFBO0FBWko7QUFnQkU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxxQkFBQTtFQUNBLG9EQUFBO0VBQ0EsZUFBQTtBQWJKO0FBZ0JFLDJCQUFBO0FBQ0E7RUFDRSxxQkFBQTtFQUNBLCtCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FBYko7QUFnQkUsNEJBQUE7QUFDQTtFQUNFLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaURBQUE7QUFiSjtBQWdCRSwwQkFBQTtBQUNBO0VBQ0UscUJBQUE7RUFDQSwrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQWJKIiwiZmlsZSI6InVzZXItZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIEVzdGlsb3MgZGVsIGhlYWRlciAqL1xuaW9uLXRvb2xiYXIge1xuICAgIGlvbi1yb3cge1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIHdpZHRoOiA4NSU7XG4gIFxuICAgICAgaW9uLXRpdGxlIHtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgLS1jb2xvcjogIzNFNDk1ODtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgXG4gIC5jb250YWluZXIge1xuICAgIHBhZGRpbmc6IDBweCAyNXB4O1xuICB9XG4gIFxuICAvKiBFc3RpbG8gcGFyYSBjb2xvY2FyIHVuIG1hcmdlbiBpenF1aWVyZG8gYSBjdWFscXVpZXIgZWxlbWVudG8gKi9cbiAgLm1hcmdpbi1sZWZ0IHtcbiAgICBtYXJnaW4tbGVmdDogNSU7XG4gIH1cbiAgXG4gIC8qIEVzdGlsb3MgZGUgbGFzIHRhcmpldGFzICovXG4gIGlvbi1jYXJkIHtcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgLS1iYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDApO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgXG4gICAgaW9uLWNhcmQtaGVhZGVyIHtcbiAgICAgIGlvbi1jYXJkLXRpdGxlIHtcbiAgICAgICAgY29sb3I6ICMzMzMzMzM7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgfVxuICBcbiAgICAgIGlvbi1jYXJkLXN1YnRpdGxlIHtcbiAgICAgICAgY29sb3I6ICMzRTQ5NTg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTMuNXB4O1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICBtYXJnaW46IDhweCAwcHg7XG4gICAgICB9XG4gIFxuICAgICAgLmNvbnRhaW5lci1zdGFydHMge1xuICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgIG1hcmdpbi1yaWdodDogOXB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG4gIFxuICBcbiAgLyogQ29udGVuZWRvciBkZSBpbWFnZW4gZGVsIHVzdWFyaW8gKi9cbiAgLmNvbnRhaW5lci1pbWcge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgXG4gICAgLmltZyB7XG4gICAgICB3aWR0aDogODJweDtcbiAgICAgIGhlaWdodDogODBweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICB9XG4gIH1cbiAgXG4gIC8qIENvbG9yIHZlcmRlIHBhcmEgYWxndW5vcyBlbGVtZW50b3MgKi9cbiAgLmdyZWVuIHtcbiAgICBjb2xvcjogIzAwOEQzNjtcbiAgfVxuICBcbiAgLy9Fc3RpbG9zIGRlIGxvcyBib3RvbmVzIGJvdG9uIFxuICAuYnRuIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgbWFyZ2luLXRvcDogMyU7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIC0tYm94LXNoYWRvdzogMHB4IDRweCAyMHB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yNSk7XG4gICAgZm9udC1zaXplOiAxN3B4O1xuICB9XG4gIFxuICAvKiBFc3RpbG8gZGVsIGJvdG9uIHZlcmRlICovXG4gIC5ncmVlbi1idG4ge1xuICAgIC0tYmFja2dyb3VuZDogIzAwOEQzNjtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjMDA4RDM2O1xuICAgIC0tY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB9XG4gIFxuICAvKiBFc3RpbG8gZGVsIGJvdG9uIGJsYW5jbyAqL1xuICAud2hpdGUge1xuICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogd2hpdGU7XG4gICAgLS1jb2xvcjogIzRCNTQ1QTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAtLWJveC1zaGFkb3c6IDBweCAycHggOHB4IHJnYmEoMTYsIDEwNSwgMjI3LCAwLjMpO1xuICB9XG4gIFxuICAvKiBFc3RpbG8gZGVsIGJvdG9uIHJvam8gKi9cbiAgLnJlZCB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjQ0IyMzIzO1xuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICNDQjIzMjM7XG4gICAgLS1jb2xvcjogd2hpdGU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIFxuICB9XG4gICJdfQ== */");

/***/ }),

/***/ "tZh1":
/*!***************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/user-detail/user-detail.module.ts ***!
  \***************************************************************************/
/*! exports provided: UserDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDetailPageModule", function() { return UserDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _user_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-detail-routing.module */ "W1NF");
/* harmony import */ var _user_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-detail.page */ "9Qab");







let UserDetailPageModule = class UserDetailPageModule {
};
UserDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _user_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["UserDetailPageRoutingModule"]
        ],
        declarations: [_user_detail_page__WEBPACK_IMPORTED_MODULE_6__["UserDetailPage"]]
    })
], UserDetailPageModule);



/***/ }),

/***/ "zNdB":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/user-detail/user-detail.page.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons class=\"margin-left\" slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons>\n    <ion-row>\n      <ion-title>Usuario</ion-title>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n\n  <div class=\"container\">\n    <ion-card  >\n      <ion-row>\n        <ion-col size=\"4\" class=\"container-img\">\n          <div *ngIf=\"(user | async)?.imagen != ''\" style=\"background-image: url({{ (user | async)?.imagen }});\" class=\"img\">\n          </div>\n          <div *ngIf=\"(user | async)?.imagen == ''\" style=\"background-image: url('../../../../../assets//img/user.png');\" class=\"img\">\n          </div>\n        </ion-col>\n\n        <ion-col size=\"8\">\n          <ion-card-header>\n            <ion-card-title>{{ (user | async)?.name }}</ion-card-title>\n            <ion-card-subtitle>{{ (user | async)?.email }}</ion-card-subtitle>\n            <ion-card-subtitle>Telefono:{{ (user | async)?.telephoneNumber }}</ion-card-subtitle>\n            <ion-card-subtitle *ngIf=\"(user | async)?.role == 'driver'\" >Rol: Conductor</ion-card-subtitle>\n            <ion-card-subtitle *ngIf=\"(user | async)?.role == 'admin'\" >Rol: Administrador</ion-card-subtitle>\n            <ion-card-subtitle *ngIf=\"(user | async)?.role == 'client'\" >Rol: Cliente</ion-card-subtitle>\n            <div class=\"container-starts\" *ngIf=\"(user | async)?.role === 'driver'\">\n              <ion-icon class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon name=\"star-outline\"></ion-icon>\n            </div>\n          </ion-card-header>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-label *ngIf=\"(user | async)?.role === 'client'\">Rol Actual :<ion-note  color=\"success\"> Cliente</ion-note></ion-label>\n         <ion-label *ngIf=\"(user | async)?.role === 'driver'\">Rol Actual :<ion-note  color=\"medium\"> Conductor</ion-note> </ion-label>\n         <ion-label *ngIf=\"(user | async)?.role === 'admin'\">Rol Actual :<ion-note  color=\"primary\"> Administrador</ion-note></ion-label>\n         \n        <ion-select placeholder=\"Roles disponibles\" (ionChange)=\"checkRole($event)\" okText=\"Aceptar\"\n                cancelText=\"Cancelar\">\n                <ion-select-option  value=\"client\" name=\"role\">Cliente</ion-select-option>\n                <ion-select-option  value=\"driver\" name=\"role\">Conductor</ion-select-option>\n                <ion-select-option  value=\"admin\" name=\"role\">Administrador</ion-select-option>\n              </ion-select>\n      </ion-col>\n  \n      <ion-col size=\"12\">\n        <ion-button class=\"btn green-btn\" (click)=\"updateRoleByUid()\">\n          Guardar Cambios\n        </ion-button>\n      </ion-col>\n  \n      <ion-col size=\"12\">\n        <ion-button class=\"btn white\" [routerLink]=\"['../../reviews']\">\n          Reseñas\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"12\" *ngIf=\"(user | async)?.isActive != false\">\n        <ion-button class=\"btn red\" (click)=\"unsuscribeUser()\">\n          Desactivar Usuario\n         </ion-button>\n      </ion-col>\n      <ion-col size=\"12\" *ngIf=\"(user | async)?.isActive != true\">\n        <ion-button class=\"btn white\" (click)=\"suscribeUser()\">\n          Activar Usuario\n         </ion-button>\n      </ion-col>\n  \n    </ion-row>\n  \n   \n\n  </div>\n\n\n\n\n</ion-content>\n\n");

/***/ })

}]);
//# sourceMappingURL=pages-user-detail-user-detail-module.js.map