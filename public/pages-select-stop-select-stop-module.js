(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-select-stop-select-stop-module"],{

/***/ "CrfH":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-stop/select-stop-routing.module.ts ***!
  \***********************************************************************************/
/*! exports provided: SelectStopPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopPageRoutingModule", function() { return SelectStopPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_stop_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-stop.page */ "g0AF");




const routes = [
    {
        path: '',
        component: _select_stop_page__WEBPACK_IMPORTED_MODULE_3__["SelectStopPage"]
    }
];
let SelectStopPageRoutingModule = class SelectStopPageRoutingModule {
};
SelectStopPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectStopPageRoutingModule);



/***/ }),

/***/ "IesI":
/*!***************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-stop/select-stop.module.ts ***!
  \***************************************************************************/
/*! exports provided: SelectStopPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopPageModule", function() { return SelectStopPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_stop_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-stop-routing.module */ "CrfH");
/* harmony import */ var _select_stop_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-stop.page */ "g0AF");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let SelectStopPageModule = class SelectStopPageModule {
};
SelectStopPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_stop_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectStopPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_select_stop_page__WEBPACK_IMPORTED_MODULE_6__["SelectStopPage"]]
    })
], SelectStopPageModule);



/***/ }),

/***/ "g0AF":
/*!*************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-stop/select-stop.page.ts ***!
  \*************************************************************************/
/*! exports provided: SelectStopPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopPage", function() { return SelectStopPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_stop_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-stop.page.html */ "h+fP");
/* harmony import */ var _select_stop_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-stop.page.scss */ "nW7c");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _modals_select_stop_modal_select_stop_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modals/select-stop-modal/select-stop-modal.page */ "Lq2P");
/* harmony import */ var src_app_shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/maps.service */ "Y0mc");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @capacitor/core */ "gcOT");
/* harmony import */ var _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/admin/stops.service */ "/+Y6");







const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_7__["Plugins"];


let SelectStopPage = class SelectStopPage {
    constructor(alertsService, mapsService, stopsService) {
        this.alertsService = alertsService;
        this.mapsService = mapsService;
        this.stopsService = stopsService;
        this.markers = [];
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.markerStops = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
    }
    ngOnInit() {
    }
    openModal() {
        this.alertsService.presentModalWithoutTravelData(_modals_select_stop_modal_select_stop_modal_page__WEBPACK_IMPORTED_MODULE_5__["SelectStopModalPage"], 'select-stop-modal');
    }
    ionViewWillEnter() {
        this.loadDataPageDashboard();
        this.stops = this.stopsService.getStop();
        this.stops.subscribe((res) => {
            this.stopsMark = res;
            this.generateMarks();
        });
    }
    generateMarks() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.deleteMarkers();
            this.stopsMark.map((res) => {
                this.markerStops.position.lat = res.lat;
                this.markerStops.position.lng = res.lng;
                this.markerStops.title = res.name;
                this.addMarker(this.markerStops, this.markerStops.title, 'stops');
            });
        });
    }
    loadDataPageDashboard() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.mapsService.loadmap('map-select-stop-page');
            this.openModal();
            this.generateMarksMyLocation();
        });
    }
    generateMarksMyLocation() {
        Geolocation.getCurrentPosition().then((res) => {
            this.marker.position.lat = res.coords.latitude;
            this.marker.position.lng = res.coords.longitude;
            this.marker.title = 'Mi Ubicación';
            this.mapsService.addMarker(this.marker, this.marker.title, 'myLocation');
        });
    }
    addMarker(marker, name, typeMarket) {
        let marke;
        let icon;
        if (typeMarket == 'myLocation') {
            icon = {
                url: '../../assets/img/ic_loc.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(60, 60),
            };
            /*     const image = '../../assets/img/Pineapplemenu.png'; */
        }
        else if (typeMarket == 'stops') {
            icon = {
                url: '../../assets/img/flag.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(25, 25),
            };
        }
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            //  draggable: true,
            // animation: google.maps.Animation.DROP,
            map: this.mapsService.map,
            icon: icon,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                    console.log('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
        const infoWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
                        if (infoWindow !== null) {
                        }
                        infoWindow.open(this.mapsService.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
    }
    deleteMarkers() {
        this.setMapOnAll(null);
        this.markers = [];
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
    ionViewWillLeave() {
        this.deleteMarkers();
    }
};
SelectStopPage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: src_app_shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__["MapsService"] },
    { type: _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_8__["StopsService"] }
];
SelectStopPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-stop',
        template: _raw_loader_select_stop_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_stop_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectStopPage);



/***/ }),

/***/ "h+fP":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/select-stop/select-stop.page.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content fullscreen [scrollEvents]=\"true\">\n  <app-header title=\"Elije tu parada de Origen\" slot=\"fixed\"></app-header>\n  <div id=\"map-select-stop-page\"></div>\n  <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>\n</ion-content>\n");

/***/ }),

/***/ "nW7c":
/*!***************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-stop/select-stop.page.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZWxlY3Qtc3RvcC5wYWdlLnNjc3MifQ== */");

/***/ })

}]);
//# sourceMappingURL=pages-select-stop-select-stop-module.js.map