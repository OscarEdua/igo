(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-select-stop-schedule-select-stop-schedule-module"],{

/***/ "38pC":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-stop-schedule/select-stop-schedule-routing.module.ts ***!
  \*****************************************************************************************************/
/*! exports provided: SelectStopSchedulePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopSchedulePageRoutingModule", function() { return SelectStopSchedulePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_stop_schedule_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-stop-schedule.page */ "bjrl");




const routes = [
    {
        path: '',
        component: _select_stop_schedule_page__WEBPACK_IMPORTED_MODULE_3__["SelectStopSchedulePage"]
    }
];
let SelectStopSchedulePageRoutingModule = class SelectStopSchedulePageRoutingModule {
};
SelectStopSchedulePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectStopSchedulePageRoutingModule);



/***/ }),

/***/ "XKv/":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-stop-schedule/select-stop-schedule.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: SelectStopSchedulePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopSchedulePageModule", function() { return SelectStopSchedulePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_stop_schedule_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-stop-schedule-routing.module */ "38pC");
/* harmony import */ var _select_stop_schedule_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-stop-schedule.page */ "bjrl");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let SelectStopSchedulePageModule = class SelectStopSchedulePageModule {
};
SelectStopSchedulePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_stop_schedule_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectStopSchedulePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_select_stop_schedule_page__WEBPACK_IMPORTED_MODULE_6__["SelectStopSchedulePage"]]
    })
], SelectStopSchedulePageModule);



/***/ }),

/***/ "bjrl":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-stop-schedule/select-stop-schedule.page.ts ***!
  \*******************************************************************************************/
/*! exports provided: SelectStopSchedulePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectStopSchedulePage", function() { return SelectStopSchedulePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_stop_schedule_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-stop-schedule.page.html */ "nFcA");
/* harmony import */ var _select_stop_schedule_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-stop-schedule.page.scss */ "xKeR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _modals_select_stop_schedule_modal_select_stop_schedule_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modals/select-stop-schedule-modal/select-stop-schedule-modal.page */ "fn7c");






let SelectStopSchedulePage = class SelectStopSchedulePage {
    constructor(alertsService) {
        this.alertsService = alertsService;
    }
    ngOnInit() {
    }
    openModal() {
        this.alertsService.presentModalWithoutTravelData(_modals_select_stop_schedule_modal_select_stop_schedule_modal_page__WEBPACK_IMPORTED_MODULE_5__["SelectStopScheduleModalPage"], 'select-stop-schedule-modal');
    }
};
SelectStopSchedulePage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] }
];
SelectStopSchedulePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-stop-schedule',
        template: _raw_loader_select_stop_schedule_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_stop_schedule_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectStopSchedulePage);



/***/ }),

/***/ "nFcA":
/*!***********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/select-stop-schedule/select-stop-schedule.page.html ***!
  \***********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header title=\"Elige tu parada\"></app-header>\n<ion-content>\n  <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>\n</ion-content>\n");

/***/ }),

/***/ "xKeR":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-stop-schedule/select-stop-schedule.page.scss ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZWxlY3Qtc3RvcC1zY2hlZHVsZS5wYWdlLnNjc3MifQ== */");

/***/ })

}]);
//# sourceMappingURL=pages-select-stop-schedule-select-stop-schedule-module.js.map