(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-destination-address-modal-destination-address-modal-module"],{

/***/ "KvMJ":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/destination-address-modal/destination-address-modal-routing.module.ts ***!
  \**********************************************************************************************************************/
/*! exports provided: DestinationAddressModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DestinationAddressModalPageRoutingModule", function() { return DestinationAddressModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _destination_address_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./destination-address-modal.page */ "sVj7");




const routes = [
    {
        path: '',
        component: _destination_address_modal_page__WEBPACK_IMPORTED_MODULE_3__["DestinationAddressModalPage"]
    }
];
let DestinationAddressModalPageRoutingModule = class DestinationAddressModalPageRoutingModule {
};
DestinationAddressModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DestinationAddressModalPageRoutingModule);



/***/ }),

/***/ "mPKk":
/*!**************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/destination-address-modal/destination-address-modal.module.ts ***!
  \**************************************************************************************************************/
/*! exports provided: DestinationAddressModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DestinationAddressModalPageModule", function() { return DestinationAddressModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _destination_address_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./destination-address-modal-routing.module */ "KvMJ");
/* harmony import */ var _destination_address_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./destination-address-modal.page */ "sVj7");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");








let DestinationAddressModalPageModule = class DestinationAddressModalPageModule {
};
DestinationAddressModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _destination_address_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["DestinationAddressModalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_destination_address_modal_page__WEBPACK_IMPORTED_MODULE_6__["DestinationAddressModalPage"]]
    })
], DestinationAddressModalPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-destination-address-modal-destination-address-modal-module.js.map