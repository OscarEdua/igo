(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-transfer-transfer-module"],{

/***/ "5+zM":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/transfer/transfer.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content fullscreen>\n  <app-header title=\"Opciones de Pago\" slot=\"fixed\"></app-header>\n  <div id=\"trasfer-map\"></div>\n  <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>\n</ion-content>");

/***/ }),

/***/ "QZzT":
/*!*********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/transfer/transfer.module.ts ***!
  \*********************************************************************/
/*! exports provided: TransferPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferPageModule", function() { return TransferPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _transfer_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./transfer-routing.module */ "UyRw");
/* harmony import */ var _transfer_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./transfer.page */ "a6Qm");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let TransferPageModule = class TransferPageModule {
};
TransferPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _transfer_routing_module__WEBPACK_IMPORTED_MODULE_5__["TransferPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_transfer_page__WEBPACK_IMPORTED_MODULE_6__["TransferPage"]]
    })
], TransferPageModule);



/***/ }),

/***/ "UyRw":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/transfer/transfer-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: TransferPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferPageRoutingModule", function() { return TransferPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _transfer_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./transfer.page */ "a6Qm");




const routes = [
    {
        path: '',
        component: _transfer_page__WEBPACK_IMPORTED_MODULE_3__["TransferPage"]
    }
];
let TransferPageRoutingModule = class TransferPageRoutingModule {
};
TransferPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TransferPageRoutingModule);



/***/ }),

/***/ "a6Qm":
/*!*******************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/transfer/transfer.page.ts ***!
  \*******************************************************************/
/*! exports provided: TransferPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferPage", function() { return TransferPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_transfer_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./transfer.page.html */ "5+zM");
/* harmony import */ var _transfer_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./transfer.page.scss */ "cyx5");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _modals_transfer_modal_transfer_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modals/transfer-modal/transfer-modal.page */ "Y70h");
/* harmony import */ var _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../shared/services/maps.service */ "Y0mc");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");








let TransferPage = class TransferPage {
    constructor(alertsService, mapsService, activatedRoute) {
        this.alertsService = alertsService;
        this.mapsService = mapsService;
        this.activatedRoute = activatedRoute;
    }
    ngOnInit() {
        this.travelId = this.activatedRoute.snapshot.paramMap.get('travelId');
    }
    openModal() {
        //this.alertsService.presentModalWithoutTravelData(TransferModalPage, 'high-modal');
        this.alertsService.presentModalWithData(_modals_transfer_modal_transfer_modal_page__WEBPACK_IMPORTED_MODULE_5__["TransferModalPage"], { travelId: this.travelId }, 'high-modal');
    }
    ionViewWillEnter() {
        this.openModal();
        this.mapsService.loadmap('trasfer-map');
    }
};
TransferPage.ctorParameters = () => [
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__["MapsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] }
];
TransferPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-transfer',
        template: _raw_loader_transfer_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_transfer_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TransferPage);



/***/ }),

/***/ "cyx5":
/*!*********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/transfer/transfer.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#trasfer-map {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3RyYW5zZmVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLFlBQUE7QUFDRCIsImZpbGUiOiJ0cmFuc2Zlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjdHJhc2Zlci1tYXAge1xuXHRoZWlnaHQ6IDEwMCU7XG59XG4iXX0= */");

/***/ })

}]);
//# sourceMappingURL=pages-transfer-transfer-module.js.map