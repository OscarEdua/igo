(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-confirmed-trip-confirmed-trip-module"],{

/***/ "542/":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/confirmed-trip/confirmed-trip-routing.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: ConfirmedTripPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmedTripPageRoutingModule", function() { return ConfirmedTripPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _confirmed_trip_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./confirmed-trip.page */ "itYX");




const routes = [
    {
        path: '',
        component: _confirmed_trip_page__WEBPACK_IMPORTED_MODULE_3__["ConfirmedTripPage"]
    }
];
let ConfirmedTripPageRoutingModule = class ConfirmedTripPageRoutingModule {
};
ConfirmedTripPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ConfirmedTripPageRoutingModule);



/***/ }),

/***/ "itYX":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/confirmed-trip/confirmed-trip.page.ts ***!
  \*******************************************************************************/
/*! exports provided: ConfirmedTripPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmedTripPage", function() { return ConfirmedTripPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_confirmed_trip_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./confirmed-trip.page.html */ "mqcT");
/* harmony import */ var _confirmed_trip_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./confirmed-trip.page.scss */ "jr3Q");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");





let ConfirmedTripPage = class ConfirmedTripPage {
    constructor(animationCtrl, element) {
        this.animationCtrl = animationCtrl;
        this.element = element;
    }
    ngOnInit() { }
    ionViewDidEnter() {
        this.animationAllCircles();
    }
    animationAllCircles() {
        this.animationIncreaseCircle(this.circleVerySmall.nativeElement).play();
        this.animationIncreaseCircle(this.circleSmall.nativeElement).play();
        this.animationIncreaseCircle(this.circle.nativeElement).play();
    }
    animationIncreaseCircle(element) {
        const animation = this.animationCtrl.create();
        animation
            .addElement(element)
            .duration(2500)
            .iterations(Infinity)
            .fromTo('transform', 'scale(1)', 'scale(1.3)')
            .fromTo('opacity', '1', '0.7');
        return animation;
    }
};
ConfirmedTripPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AnimationController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"] }
];
ConfirmedTripPage.propDecorators = {
    circle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['circle',] }],
    circleSmall: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['circleSmall',] }],
    circleVerySmall: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['circleVerySmall',] }]
};
ConfirmedTripPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-confirmed-trip',
        template: _raw_loader_confirmed_trip_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_confirmed_trip_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ConfirmedTripPage);



/***/ }),

/***/ "jr3Q":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/confirmed-trip/confirmed-trip.page.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: rgba(9, 116, 39, 0.8);\n}\nion-content .search {\n  height: 90%;\n  width: 100%;\n}\nion-content .search ion-col {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\nion-content .search ion-col .circle {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background: rgba(255, 255, 255, 0.4);\n  width: 220px;\n  height: 220px;\n  border-radius: 100%;\n}\nion-content .search ion-col .circle .circle-small {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background: rgba(255, 255, 255, 0.4);\n  width: 120px;\n  height: 120px;\n  border-radius: 100%;\n}\nion-content .search ion-col .circle .circle-very-small {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background: rgba(255, 255, 255, 0.4);\n  width: 55px;\n  height: 55px;\n  border-radius: 100%;\n}\n.search-text {\n  color: white;\n  font-weight: bold;\n  display: flex;\n  justify-content: center;\n  height: 10%;\n}\n.img-car {\n  position: absolute;\n  width: 100px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2NvbmZpcm1lZC10cmlwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1DQUFBO0FBQ0o7QUFDSTtFQUNFLFdBQUE7RUFDQSxXQUFBO0FBQ047QUFDTTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBQ1I7QUFDUTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0NBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FBQ1Y7QUFDVTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0NBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FBQ2Q7QUFFVTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0NBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBQWQ7QUFRRTtFQUNFLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7QUFMSjtBQVFFO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0FBTE4iLCJmaWxlIjoiY29uZmlybWVkLXRyaXAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogcmdiYSg5LCAxMTYsIDM5LCAwLjgpO1xuICBcbiAgICAuc2VhcmNoIHtcbiAgICAgIGhlaWdodDogOTAlO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gIFxuICAgICAgaW9uLWNvbCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBcbiAgICAgICAgLmNpcmNsZSB7XG4gICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC40KTtcbiAgICAgICAgICB3aWR0aDogMjIwcHg7XG4gICAgICAgICAgaGVpZ2h0OiAyMjBweDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICBcbiAgICAgICAgICAuY2lyY2xlLXNtYWxsIHtcbiAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC40KTtcbiAgICAgICAgICAgICAgd2lkdGg6IDEyMHB4O1xuICAgICAgICAgICAgICBoZWlnaHQ6IDEyMHB4O1xuICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgICAgIH1cbiAgXG4gICAgICAgICAgLmNpcmNsZS12ZXJ5LXNtYWxse1xuICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgICAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjQpO1xuICAgICAgICAgICAgICB3aWR0aDogNTVweDtcbiAgICAgICAgICAgICAgaGVpZ2h0OiA1NXB4O1xuICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgICAgIH1cbiAgXG4gICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgLnNlYXJjaC10ZXh0e1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGhlaWdodDogMTAlO1xuICBcbiAgfVxuICAuaW1nLWNhcntcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHdpZHRoOiAxMDBweDtcbiAgICAgIFxuICB9XG4gICJdfQ== */");

/***/ }),

/***/ "mqcT":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/confirmed-trip/confirmed-trip.page.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <ion-row class=\"search\"> \n    <ion-col size=\"12\">\n      <div class=\"circle\" #circle>\n        <div class=\"circle-small\" #circleSmall>\n          <div class=\"circle-very-small\" #circleVerySmall>\n\n          </div>\n          \n        </div>\n      </div>\n      <img class=\"img-car\" src=\"../../../../../assets/img/search-car.png\" alt=\"\">\n\n    </ion-col>\n  </ion-row>\n  <ion-row class=\"search-text\">\n    <p>Viaje Confirmado</p>\n  </ion-row>\n  \n</ion-content>\n");

/***/ }),

/***/ "vKj2":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/confirmed-trip/confirmed-trip.module.ts ***!
  \*********************************************************************************/
/*! exports provided: ConfirmedTripPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmedTripPageModule", function() { return ConfirmedTripPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _confirmed_trip_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./confirmed-trip-routing.module */ "542/");
/* harmony import */ var _confirmed_trip_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./confirmed-trip.page */ "itYX");







let ConfirmedTripPageModule = class ConfirmedTripPageModule {
};
ConfirmedTripPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _confirmed_trip_routing_module__WEBPACK_IMPORTED_MODULE_5__["ConfirmedTripPageRoutingModule"]
        ],
        declarations: [_confirmed_trip_page__WEBPACK_IMPORTED_MODULE_6__["ConfirmedTripPage"]]
    })
], ConfirmedTripPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-confirmed-trip-confirmed-trip-module.js.map