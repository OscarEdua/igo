(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-dashboard-dashboard-module"],{

/***/ "XoyV":
/*!*******************************************************!*\
  !*** ./src/app/modules/dashboard/dashboard.module.ts ***!
  \*******************************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard-routing.module */ "vH6C");



let DashboardModule = class DashboardModule {
};
DashboardModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [
            _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_2__["DashboardRoutingModule"],
        ]
    })
], DashboardModule);



/***/ }),

/***/ "vH6C":
/*!***************************************************************!*\
  !*** ./src/app/modules/dashboard/dashboard-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");



const routes = [
    {
        path: '',
        loadChildren: () => Promise.all(/*! import() | pages-dashboard-dashboard-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-start-travel-start-travel-module"), __webpack_require__.e("common"), __webpack_require__.e("pages-dashboard-dashboard-module")]).then(__webpack_require__.bind(null, /*! ./pages/dashboard/dashboard.module */ "HoTW")).then(m => m.DashboardPageModule)
    },
    {
        path: 'users',
        loadChildren: () => Promise.all(/*! import() | pages-users-users-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-users-users-module")]).then(__webpack_require__.bind(null, /*! ./pages/users/users.module */ "edmr")).then(m => m.UsersPageModule)
    },
    {
        path: 'stops',
        loadChildren: () => Promise.all(/*! import() | pages-stops-stops-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-stops-stops-module")]).then(__webpack_require__.bind(null, /*! ./pages/stops/stops.module */ "XVPl")).then(m => m.StopsPageModule)
    },
    {
        path: 'user-detail/:userId',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-user-detail-user-detail-module */ "pages-user-detail-user-detail-module").then(__webpack_require__.bind(null, /*! ./pages/user-detail/user-detail.module */ "tZh1")).then(m => m.UserDetailPageModule)
    },
    {
        path: 'reviews',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-reviews-reviews-module */ "pages-reviews-reviews-module").then(__webpack_require__.bind(null, /*! ./pages/reviews/reviews.module */ "NQsq")).then(m => m.ReviewsPageModule)
    },
    {
        path: 'profile',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-profile-profile-module */ "pages-profile-profile-module").then(__webpack_require__.bind(null, /*! ./pages/profile/profile.module */ "OwGE")).then(m => m.ProfilePageModule)
    },
    {
        path: 'driver-detail/:driverId',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-driver-detail-driver-detail-module */ "pages-driver-detail-driver-detail-module").then(__webpack_require__.bind(null, /*! ./pages/driver-detail/driver-detail.module */ "yPFg")).then(m => m.DriverDetailPageModule)
    },
    {
        path: 'request-menu',
        loadChildren: () => Promise.all(/*! import() | pages-request-menu-request-menu-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("common"), __webpack_require__.e("pages-request-menu-request-menu-module")]).then(__webpack_require__.bind(null, /*! ./pages/request-menu/request-menu.module */ "WCqY")).then(m => m.RequestMenuPageModule)
    },
    {
        path: 'requests',
        loadChildren: () => Promise.all(/*! import() | pages-requests-requests-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("common"), __webpack_require__.e("pages-requests-requests-module")]).then(__webpack_require__.bind(null, /*! ./pages/requests/requests.module */ "7dfg")).then(m => m.RequestsPageModule)
    },
    {
        path: 'travel/:travelId/:roleCurrentUser',
        loadChildren: () => Promise.all(/*! import() | pages-travel-travel-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-in-travel-modal-in-travel-modal-module~pages-travel-travel-module"), __webpack_require__.e("pages-travel-travel-module")]).then(__webpack_require__.bind(null, /*! ./pages/travel/travel.module */ "EBK7")).then(m => m.TravelPageModule)
    },
    {
        path: 'successful-arrival',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-successful-arrival-successful-arrival-module */ "pages-successful-arrival-successful-arrival-module").then(__webpack_require__.bind(null, /*! ./pages/successful-arrival/successful-arrival.module */ "8Bun")).then(m => m.SuccessfulArrivalPageModule)
    },
    {
        path: 'recommendations/:travelId',
        loadChildren: () => Promise.all(/*! import() | pages-recommendations-recommendations-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-recommendations-modal-recommendations-modal-module~pages-recommendations-recomm~657c8d3c"), __webpack_require__.e("pages-recommendations-recommendations-module")]).then(__webpack_require__.bind(null, /*! ./pages/recommendations/recommendations.module */ "Y1SK")).then(m => m.RecommendationsPageModule)
    },
    {
        path: 'chat/:email',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-chat-chat-module */ "pages-chat-chat-module").then(__webpack_require__.bind(null, /*! ./pages/chat/chat.module */ "UBAY")).then(m => m.ChatPageModule)
    },
    {
        path: 'start-travel',
        loadChildren: () => Promise.all(/*! import() | pages-modals-start-travel-start-travel-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-start-travel-start-travel-module"), __webpack_require__.e("pages-modals-start-travel-start-travel-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/start-travel/start-travel.module */ "geE7")).then(m => m.StartTravelPageModule)
    },
    {
        path: 'select-vehicle-type',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-modals-select-vehicle-type-select-vehicle-type-module */ "pages-modals-select-vehicle-type-select-vehicle-type-module").then(__webpack_require__.bind(null, /*! ./pages/modals/select-vehicle-type/select-vehicle-type.module */ "SKZj")).then(m => m.SelectVehicleTypePageModule)
    },
    {
        path: 'select-vehicle/:isTravelNow',
        loadChildren: () => Promise.all(/*! import() | pages-select-vehicle-select-vehicle-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("pages-select-vehicle-select-vehicle-module")]).then(__webpack_require__.bind(null, /*! ./pages/select-vehicle/select-vehicle.module */ "EEsZ")).then(m => m.SelectVehiclePageModule)
    },
    {
        path: 'history',
        loadChildren: () => Promise.all(/*! import() | pages-history-history-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("pages-history-history-module")]).then(__webpack_require__.bind(null, /*! ./pages/history/history.module */ "N7kH")).then(m => m.HistoryPageModule)
    },
    {
        path: 'select-stop',
        loadChildren: () => Promise.all(/*! import() | pages-select-stop-select-stop-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("common"), __webpack_require__.e("pages-select-stop-select-stop-module")]).then(__webpack_require__.bind(null, /*! ./pages/select-stop/select-stop.module */ "IesI")).then(m => m.SelectStopPageModule)
    },
    {
        path: 'select-stop-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-select-stop-modal-select-stop-modal-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("common"), __webpack_require__.e("pages-modals-select-stop-modal-select-stop-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/select-stop-modal/select-stop-modal.module */ "s1vA")).then(m => m.SelectStopModalPageModule)
    },
    {
        path: 'select-destination',
        loadChildren: () => Promise.all(/*! import() | pages-select-destination-select-destination-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-select-destination-modal-select-destination-modal-module~pages-select-destinati~7073e38f"), __webpack_require__.e("pages-select-destination-select-destination-module")]).then(__webpack_require__.bind(null, /*! ./pages/select-destination/select-destination.module */ "Hu7B")).then(m => m.SelectDestinationPageModule)
    },
    {
        path: 'select-destination-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-select-destination-modal-select-destination-modal-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-select-destination-modal-select-destination-modal-module~pages-select-destinati~7073e38f"), __webpack_require__.e("common"), __webpack_require__.e("pages-modals-select-destination-modal-select-destination-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/select-destination-modal/select-destination-modal.module */ "WsMI")).then(m => m.SelectDestinationModalPageModule)
    },
    {
        path: 'schedule-trip',
        loadChildren: () => Promise.all(/*! import() | pages-schedule-trip-schedule-trip-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-schedule-trip-modal-schedule-trip-modal-module~pages-schedule-trip-schedule-trip-module"), __webpack_require__.e("pages-schedule-trip-schedule-trip-module")]).then(__webpack_require__.bind(null, /*! ./pages/schedule-trip/schedule-trip.module */ "qUjs")).then(m => m.ScheduleTripPageModule)
    },
    {
        path: 'schedule-trip-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-schedule-trip-modal-schedule-trip-modal-module */[__webpack_require__.e("default~pages-modals-schedule-trip-modal-schedule-trip-modal-module~pages-schedule-trip-schedule-trip-module"), __webpack_require__.e("pages-modals-schedule-trip-modal-schedule-trip-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/schedule-trip-modal/schedule-trip-modal.module */ "178Q")).then(m => m.ScheduleTripModalPageModule)
    },
    {
        path: 'select-stop-schedule',
        loadChildren: () => Promise.all(/*! import() | pages-select-stop-schedule-select-stop-schedule-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-select-stop-schedule-select-stop-schedule-module")]).then(__webpack_require__.bind(null, /*! ./pages/select-stop-schedule/select-stop-schedule.module */ "XKv/")).then(m => m.SelectStopSchedulePageModule)
    },
    {
        path: 'select-stop-schedule-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-select-stop-schedule-modal-select-stop-schedule-modal-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-modals-select-stop-schedule-modal-select-stop-schedule-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/select-stop-schedule-modal/select-stop-schedule-modal.module */ "RiRr")).then(m => m.SelectStopScheduleModalPageModule)
    },
    {
        path: 'ubication-destination-schedule',
        loadChildren: () => Promise.all(/*! import() | pages-ubication-destination-schedule-ubication-destination-schedule-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-ubication-destination-schedule-ubication-destination-schedule-module")]).then(__webpack_require__.bind(null, /*! ./pages/ubication-destination-schedule/ubication-destination-schedule.module */ "lZDK")).then(m => m.UbicationDestinationSchedulePageModule)
    },
    {
        path: 'ubication-destination-schedule-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-ubication-destination-schedule-modal-ubication-destination-schedule-modal-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-modals-ubication-destination-schedule-modal-ubication-destination-schedule-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal.module */ "gfJs")).then(m => m.UbicationDestinationScheduleModalPageModule)
    },
    {
        path: 'destination-address',
        loadChildren: () => Promise.all(/*! import() | pages-destination-address-destination-address-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-destination-address-destination-address-module")]).then(__webpack_require__.bind(null, /*! ./pages/destination-address/destination-address.module */ "vwtk")).then(m => m.DestinationAddressPageModule)
    },
    {
        path: 'destination-address-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-destination-address-modal-destination-address-modal-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-modals-destination-address-modal-destination-address-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/destination-address-modal/destination-address-modal.module */ "mPKk")).then(m => m.DestinationAddressModalPageModule)
    },
    {
        path: 'edit-vehicle-data',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-edit-vehicle-data-edit-vehicle-data-module */ "pages-edit-vehicle-data-edit-vehicle-data-module").then(__webpack_require__.bind(null, /*! ./pages/edit-vehicle-data/edit-vehicle-data.module */ "hLyZ")).then(m => m.EditVehicleDataPageModule)
    },
    {
        path: 'select-route',
        loadChildren: () => Promise.all(/*! import() | pages-select-route-select-route-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("common"), __webpack_require__.e("pages-select-route-select-route-module")]).then(__webpack_require__.bind(null, /*! ./pages/select-route/select-route.module */ "5gjn")).then(m => m.SelectRoutePageModule)
    },
    {
        path: 'select-route-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-select-route-modal-select-route-modal-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("common"), __webpack_require__.e("pages-modals-select-route-modal-select-route-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/select-route-modal/select-route-modal.module */ "BTSe")).then(m => m.SelectRouteModalPageModule)
    },
    {
        path: 'select-payment-type/:travelId',
        loadChildren: () => Promise.all(/*! import() | pages-select-payment-type-select-payment-type-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("common"), __webpack_require__.e("pages-select-payment-type-select-payment-type-module")]).then(__webpack_require__.bind(null, /*! ./pages/select-payment-type/select-payment-type.module */ "Hi2B")).then(m => m.SelectPaymentTypePageModule)
    },
    {
        path: 'select-payment-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-select-payment-modal-select-payment-modal-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("common"), __webpack_require__.e("pages-modals-select-payment-modal-select-payment-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/select-payment-modal/select-payment-modal.module */ "zZI3")).then(m => m.SelectPaymentModalPageModule)
    },
    {
        path: 'transfer',
        loadChildren: () => Promise.all(/*! import() | pages-transfer-transfer-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-transfer-modal-transfer-modal-module~pages-transfer-transfer-module"), __webpack_require__.e("pages-transfer-transfer-module")]).then(__webpack_require__.bind(null, /*! ./pages/transfer/transfer.module */ "QZzT")).then(m => m.TransferPageModule)
    },
    {
        path: 'transfer-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-transfer-modal-transfer-modal-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-transfer-modal-transfer-modal-module~pages-transfer-transfer-module"), __webpack_require__.e("pages-modals-transfer-modal-transfer-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/transfer-modal/transfer-modal.module */ "CGbD")).then(m => m.TransferModalPageModule)
    },
    {
        path: 'search-driver/:travelId',
        loadChildren: () => Promise.all(/*! import() | pages-search-driver-search-driver-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("pages-search-driver-search-driver-module")]).then(__webpack_require__.bind(null, /*! ./pages/search-driver/search-driver.module */ "RxX9")).then(m => m.SearchDriverPageModule)
    },
    {
        path: 'confirmed-trip',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-confirmed-trip-confirmed-trip-module */ "pages-confirmed-trip-confirmed-trip-module").then(__webpack_require__.bind(null, /*! ./pages/confirmed-trip/confirmed-trip.module */ "vKj2")).then(m => m.ConfirmedTripPageModule)
    },
    {
        path: 'travel-details',
        loadChildren: () => Promise.all(/*! import() | pages-travel-details-travel-details-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-travel-details-modal-travel-details-modal-module~pages-travel-details-travel-de~ec48b730"), __webpack_require__.e("pages-travel-details-travel-details-module")]).then(__webpack_require__.bind(null, /*! ./pages/travel-details/travel-details.module */ "nb7n")).then(m => m.TravelDetailsPageModule)
    },
    {
        path: 'travel-details-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-travel-details-modal-travel-details-modal-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-travel-details-modal-travel-details-modal-module~pages-travel-details-travel-de~ec48b730"), __webpack_require__.e("pages-modals-travel-details-modal-travel-details-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/travel-details-modal/travel-details-modal.module */ "H4No")).then(m => m.TravelDetailsModalPageModule)
    },
    {
        path: 'travel-complete/:travelId',
        loadChildren: () => Promise.all(/*! import() | pages-travel-complete-travel-complete-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("pages-travel-complete-travel-complete-module")]).then(__webpack_require__.bind(null, /*! ./pages/travel-complete/travel-complete.module */ "CuF/")).then(m => m.TravelCompletePageModule)
    },
    {
        path: 'in-travel-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-in-travel-modal-in-travel-modal-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-in-travel-modal-in-travel-modal-module~pages-travel-travel-module"), __webpack_require__.e("pages-modals-in-travel-modal-in-travel-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/in-travel-modal/in-travel-modal.module */ "9zrF")).then(m => m.InTravelModalPageModule)
    },
    {
        path: 'recommendations-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-recommendations-modal-recommendations-modal-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-modals-recommendations-modal-recommendations-modal-module~pages-recommendations-recomm~657c8d3c"), __webpack_require__.e("pages-modals-recommendations-modal-recommendations-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/recommendations-modal/recommendations-modal.module */ "z28r")).then(m => m.RecommendationsModalPageModule)
    },
    {
        path: 'search-complete',
        loadChildren: () => Promise.all(/*! import() | pages-search-complete-search-complete-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("pages-search-complete-search-complete-module")]).then(__webpack_require__.bind(null, /*! ./pages/search-complete/search-complete.module */ "Q11+")).then(m => m.SearchCompletePageModule)
    },
    {
        path: 'shipping-parameters',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-shipping-parameters-shipping-parameters-module */ "pages-shipping-parameters-shipping-parameters-module").then(__webpack_require__.bind(null, /*! ./pages/shipping-parameters/shipping-parameters.module */ "+nW0")).then(m => m.ShippingParametersPageModule)
    },
    {
        path: 'review-transfer',
        loadChildren: () => Promise.all(/*! import() | pages-review-transfer-review-transfer-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("common"), __webpack_require__.e("pages-review-transfer-review-transfer-module")]).then(__webpack_require__.bind(null, /*! ./pages/review-transfer/review-transfer.module */ "gFNC")).then(m => m.ReviewTransferPageModule)
    },
    {
        path: 'select-stop-schedule-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-select-stop-schedule-modal-select-stop-schedule-modal-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-modals-select-stop-schedule-modal-select-stop-schedule-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/select-stop-schedule-modal/select-stop-schedule-modal.module */ "RiRr")).then(m => m.SelectStopScheduleModalPageModule)
    },
    {
        path: 'ubication-destination-schedule',
        loadChildren: () => Promise.all(/*! import() | pages-ubication-destination-schedule-ubication-destination-schedule-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-ubication-destination-schedule-ubication-destination-schedule-module")]).then(__webpack_require__.bind(null, /*! ./pages/ubication-destination-schedule/ubication-destination-schedule.module */ "lZDK")).then(m => m.UbicationDestinationSchedulePageModule)
    },
    {
        path: 'ubication-destination-schedule-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-ubication-destination-schedule-modal-ubication-destination-schedule-modal-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-modals-ubication-destination-schedule-modal-ubication-destination-schedule-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal.module */ "gfJs")).then(m => m.UbicationDestinationScheduleModalPageModule)
    },
    {
        path: 'destination-address',
        loadChildren: () => Promise.all(/*! import() | pages-destination-address-destination-address-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-destination-address-destination-address-module")]).then(__webpack_require__.bind(null, /*! ./pages/destination-address/destination-address.module */ "vwtk")).then(m => m.DestinationAddressPageModule)
    },
    {
        path: 'destination-address-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-destination-address-modal-destination-address-modal-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-modals-destination-address-modal-destination-address-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/destination-address-modal/destination-address-modal.module */ "mPKk")).then(m => m.DestinationAddressModalPageModule)
    },
    {
        path: 'edit-vehicle-data',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-edit-vehicle-data-edit-vehicle-data-module */ "pages-edit-vehicle-data-edit-vehicle-data-module").then(__webpack_require__.bind(null, /*! ./pages/edit-vehicle-data/edit-vehicle-data.module */ "hLyZ")).then(m => m.EditVehicleDataPageModule)
    },
    {
        path: 'select-route',
        loadChildren: () => Promise.all(/*! import() | pages-select-route-select-route-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("common"), __webpack_require__.e("pages-select-route-select-route-module")]).then(__webpack_require__.bind(null, /*! ./pages/select-route/select-route.module */ "5gjn")).then(m => m.SelectRoutePageModule)
    },
    {
        path: 'select-route-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-select-route-modal-select-route-modal-module */[__webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("common"), __webpack_require__.e("pages-modals-select-route-modal-select-route-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/select-route-modal/select-route-modal.module */ "BTSe")).then(m => m.SelectRouteModalPageModule)
    },
    {
        path: 'select-payment-type/:travelId',
        loadChildren: () => Promise.all(/*! import() | pages-select-payment-type-select-payment-type-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("common"), __webpack_require__.e("pages-select-payment-type-select-payment-type-module")]).then(__webpack_require__.bind(null, /*! ./pages/select-payment-type/select-payment-type.module */ "Hi2B")).then(m => m.SelectPaymentTypePageModule)
    },
    {
        path: 'select-payment-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-select-payment-modal-select-payment-modal-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("common"), __webpack_require__.e("pages-modals-select-payment-modal-select-payment-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/select-payment-modal/select-payment-modal.module */ "zZI3")).then(m => m.SelectPaymentModalPageModule)
    },
    {
        path: 'transfer/:travelId',
        loadChildren: () => Promise.all(/*! import() | pages-transfer-transfer-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-transfer-modal-transfer-modal-module~pages-transfer-transfer-module"), __webpack_require__.e("pages-transfer-transfer-module")]).then(__webpack_require__.bind(null, /*! ./pages/transfer/transfer.module */ "QZzT")).then(m => m.TransferPageModule)
    },
    {
        path: 'transfer-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-transfer-modal-transfer-modal-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-transfer-modal-transfer-modal-module~pages-transfer-transfer-module"), __webpack_require__.e("pages-modals-transfer-modal-transfer-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/transfer-modal/transfer-modal.module */ "CGbD")).then(m => m.TransferModalPageModule)
    },
    {
        path: 'search-driver/:travelId',
        loadChildren: () => Promise.all(/*! import() | pages-search-driver-search-driver-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("pages-search-driver-search-driver-module")]).then(__webpack_require__.bind(null, /*! ./pages/search-driver/search-driver.module */ "RxX9")).then(m => m.SearchDriverPageModule)
    },
    {
        path: 'confirmed-trip',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-confirmed-trip-confirmed-trip-module */ "pages-confirmed-trip-confirmed-trip-module").then(__webpack_require__.bind(null, /*! ./pages/confirmed-trip/confirmed-trip.module */ "vKj2")).then(m => m.ConfirmedTripPageModule)
    },
    {
        path: 'travel-details',
        loadChildren: () => Promise.all(/*! import() | pages-travel-details-travel-details-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-travel-details-modal-travel-details-modal-module~pages-travel-details-travel-de~ec48b730"), __webpack_require__.e("pages-travel-details-travel-details-module")]).then(__webpack_require__.bind(null, /*! ./pages/travel-details/travel-details.module */ "nb7n")).then(m => m.TravelDetailsPageModule)
    },
    {
        path: 'travel-details-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-travel-details-modal-travel-details-modal-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-travel-details-modal-travel-details-modal-module~pages-travel-details-travel-de~ec48b730"), __webpack_require__.e("pages-modals-travel-details-modal-travel-details-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/travel-details-modal/travel-details-modal.module */ "H4No")).then(m => m.TravelDetailsModalPageModule)
    },
    {
        path: 'travel-complete/:travelId',
        loadChildren: () => Promise.all(/*! import() | pages-travel-complete-travel-complete-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("pages-travel-complete-travel-complete-module")]).then(__webpack_require__.bind(null, /*! ./pages/travel-complete/travel-complete.module */ "CuF/")).then(m => m.TravelCompletePageModule)
    },
    {
        path: 'in-travel-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-in-travel-modal-in-travel-modal-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-dashboard-dashboard-module~pages-modals-in-travel-modal-in-travel-modal-module~pages-m~9330d6cd"), __webpack_require__.e("default~pages-modals-in-travel-modal-in-travel-modal-module~pages-travel-travel-module"), __webpack_require__.e("pages-modals-in-travel-modal-in-travel-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/in-travel-modal/in-travel-modal.module */ "9zrF")).then(m => m.InTravelModalPageModule)
    },
    {
        path: 'recommendations-modal',
        loadChildren: () => Promise.all(/*! import() | pages-modals-recommendations-modal-recommendations-modal-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("default~pages-modals-recommendations-modal-recommendations-modal-module~pages-recommendations-recomm~657c8d3c"), __webpack_require__.e("pages-modals-recommendations-modal-recommendations-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/modals/recommendations-modal/recommendations-modal.module */ "z28r")).then(m => m.RecommendationsModalPageModule)
    },
    {
        path: 'search-complete/:travelId',
        loadChildren: () => Promise.all(/*! import() | pages-search-complete-search-complete-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("pages-search-complete-search-complete-module")]).then(__webpack_require__.bind(null, /*! ./pages/search-complete/search-complete.module */ "Q11+")).then(m => m.SearchCompletePageModule)
    },
    {
        path: 'shipping-parameters',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-shipping-parameters-shipping-parameters-module */ "pages-shipping-parameters-shipping-parameters-module").then(__webpack_require__.bind(null, /*! ./pages/shipping-parameters/shipping-parameters.module */ "+nW0")).then(m => m.ShippingParametersPageModule)
    },
    {
        path: 'review-transfer',
        loadChildren: () => Promise.all(/*! import() | pages-review-transfer-review-transfer-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("common"), __webpack_require__.e("pages-review-transfer-review-transfer-module")]).then(__webpack_require__.bind(null, /*! ./pages/review-transfer/review-transfer.module */ "gFNC")).then(m => m.ReviewTransferPageModule)
    },
    {
        path: 'confirm-transfers/:travelId',
        loadChildren: () => Promise.all(/*! import() | pages-confirm-transfers-confirm-transfers-module */[__webpack_require__.e("default~pages-confirm-transfers-confirm-transfers-module~pages-history-history-module~pages-modals-i~d44f8de8"), __webpack_require__.e("pages-confirm-transfers-confirm-transfers-module")]).then(__webpack_require__.bind(null, /*! ./pages/confirm-transfers/confirm-transfers.module */ "S4Co")).then(m => m.ConfirmTransfersPageModule)
    },
    {
        path: 'bank-data',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-bank-data-bank-data-module */ "pages-bank-data-bank-data-module").then(__webpack_require__.bind(null, /*! ./pages/bank-data/bank-data.module */ "l/ZI")).then(m => m.BankDataPageModule)
    }
];
let DashboardRoutingModule = class DashboardRoutingModule {
};
DashboardRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
        ]
    })
], DashboardRoutingModule);



/***/ })

}]);
//# sourceMappingURL=modules-dashboard-dashboard-module.js.map