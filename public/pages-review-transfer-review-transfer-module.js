(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-review-transfer-review-transfer-module"],{

/***/ "3M3X":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/review-transfer/review-transfer-routing.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: ReviewTransferPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReviewTransferPageRoutingModule", function() { return ReviewTransferPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _review_transfer_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./review-transfer.page */ "9Nxq");




const routes = [
    {
        path: '',
        component: _review_transfer_page__WEBPACK_IMPORTED_MODULE_3__["ReviewTransferPage"]
    }
];
let ReviewTransferPageRoutingModule = class ReviewTransferPageRoutingModule {
};
ReviewTransferPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ReviewTransferPageRoutingModule);



/***/ }),

/***/ "9Nxq":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/review-transfer/review-transfer.page.ts ***!
  \*********************************************************************************/
/*! exports provided: ReviewTransferPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReviewTransferPage", function() { return ReviewTransferPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_review_transfer_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./review-transfer.page.html */ "IpE2");
/* harmony import */ var _review_transfer_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./review-transfer.page.scss */ "HljI");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _home_services_users_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../home/services/users.service */ "6JOU");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/admin/travels.service */ "KlPZ");







let ReviewTransferPage = class ReviewTransferPage {
    constructor(usersService, route, travelsService) {
        this.usersService = usersService;
        this.route = route;
        this.travelsService = travelsService;
        this.buttonPressed = this.route.snapshot.paramMap.get('buttonPressed');
        this.titlePage = (this.buttonPressed == 'users') ? 'Usuarios' : 'Conductores';
    }
    ngOnInit() {
        this.users = this.usersService.read();
        this.travels = this.travelsService.getTravelsTransfer();
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    greeting() {
        console.log('Hola...');
    }
};
ReviewTransferPage.ctorParameters = () => [
    { type: _home_services_users_service__WEBPACK_IMPORTED_MODULE_4__["UsersService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_6__["TravelsService"] }
];
ReviewTransferPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-review-transfer',
        template: _raw_loader_review_transfer_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_review_transfer_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ReviewTransferPage);



/***/ }),

/***/ "HljI":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/review-transfer/review-transfer.page.scss ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n/* Estilos del header */\nion-toolbar ion-row {\n  align-items: center;\n  display: flex;\n  justify-content: center;\n  width: 85%;\n}\nion-toolbar ion-row ion-title {\n  font-size: 20px;\n  font-weight: bold;\n  --color: #3E4958;\n  text-align: center;\n}\n/* Estilo para colocar un margen izquierdo a cualquier elemento */\n.margin-left {\n  margin-left: 5%;\n}\n/* Estilos de la barra de busqueda */\nion-searchbar {\n  --box-shadow: inset 0px 4px 15px rgba(0, 0, 0, 0.14);\n  --border-radius: 15px;\n  --icon-color: #008D36;\n  --background:#ffff;\n}\n/* Contenedor de todas las tarjetas de navegación */\n.container {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  padding: 15px;\n  margin-top: 10%;\n}\n/* Estilos de las tarjetas */\nion-card {\n  border-radius: 15px;\n  box-shadow: 0px 2px 15px rgba(0, 0, 0, 0.15);\n  margin-top: 10%;\n  width: 90%;\n}\nion-card ion-card-header ion-card-title {\n  color: #333333;\n  font-weight: bold;\n}\nion-card ion-card-header ion-card-subtitle {\n  color: #3E4958;\n  font-size: 13.5px;\n  font-weight: 500;\n  margin: 8px 0px;\n}\nion-card ion-card-header .container-starts ion-icon {\n  font-size: 20px;\n  margin-right: 9px;\n}\n/* Contenedor de imagen del usuario */\n.container-img {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.container-img .img {\n  width: 82px;\n  height: 80px;\n  border-radius: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n/* Color verde para algunos elementos */\n.green {\n  color: #008D36;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Jldmlldy10cmFuc2Zlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FBQWhCLHVCQUFBO0FBRUk7RUFDRSxtQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUFDTjtBQUNNO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQUNSO0FBTUUsaUVBQUE7QUFDQTtFQUNFLGVBQUE7QUFISjtBQU9FLG9DQUFBO0FBQ0Y7RUFDSSxvREFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDRSxrQkFBQTtBQUpOO0FBT0UsbURBQUE7QUFDQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7QUFKTjtBQVFFLDRCQUFBO0FBQ0E7RUFDSSxtQkFBQTtFQUNBLDRDQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7QUFMTjtBQVFVO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0FBTmQ7QUFRVTtFQUNJLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQU5kO0FBVWM7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7QUFSbEI7QUFjRSxxQ0FBQTtBQUNBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFYTjtBQVlNO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUFWVjtBQWNFLHVDQUFBO0FBQ0E7RUFDSSxjQUFBO0FBWE4iLCJmaWxlIjoicmV2aWV3LXRyYW5zZmVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIEVzdGlsb3MgZGVsIGhlYWRlciAqL1xuaW9uLXRvb2xiYXIge1xuICAgIGlvbi1yb3cge1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIHdpZHRoOiA4NSU7XG4gIFxuICAgICAgaW9uLXRpdGxlIHtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgLS1jb2xvcjogIzNFNDk1ODtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBcbiAgICAgIH1cblxuICAgIH1cbiAgfVxuICBcbiAgLyogRXN0aWxvIHBhcmEgY29sb2NhciB1biBtYXJnZW4gaXpxdWllcmRvIGEgY3VhbHF1aWVyIGVsZW1lbnRvICovXG4gIC5tYXJnaW4tbGVmdCB7XG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xuICB9XG5cblxuICAvKiBFc3RpbG9zIGRlIGxhIGJhcnJhIGRlIGJ1c3F1ZWRhICovXG5pb24tc2VhcmNoYmFyIHtcbiAgICAtLWJveC1zaGFkb3c6IGluc2V0IDBweCA0cHggMTVweCByZ2JhKDAsIDAsIDAsIDAuMTQpO1xuICAgIC0tYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAtLWljb24tY29sb3I6ICMwMDhEMzY7XG4gICAgICAtLWJhY2tncm91bmQ6I2ZmZmY7XG4gIH1cbiAgXG4gIC8qIENvbnRlbmVkb3IgZGUgdG9kYXMgbGFzIHRhcmpldGFzIGRlIG5hdmVnYWNpw7NuICovXG4gIC5jb250YWluZXIge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgcGFkZGluZzogMTVweDtcbiAgICAgIG1hcmdpbi10b3A6IDEwJTsgXG4gICAgXG4gIH1cbiAgXG4gIC8qIEVzdGlsb3MgZGUgbGFzIHRhcmpldGFzICovXG4gIGlvbi1jYXJke1xuICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMTVweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xuICAgICAgbWFyZ2luLXRvcDoxMCU7XG4gICAgICB3aWR0aDogOTAlO1xuICAgIFxuICAgICAgaW9uLWNhcmQtaGVhZGVye1xuICAgICAgICAgIGlvbi1jYXJkLXRpdGxle1xuICAgICAgICAgICAgICBjb2xvcjogIzMzMzMzMztcbiAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlvbi1jYXJkLXN1YnRpdGxle1xuICAgICAgICAgICAgICBjb2xvcjogIzNFNDk1ODtcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAxMy41cHg7XG4gICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgICAgICAgIG1hcmdpbjogOHB4IDBweDtcbiAgICAgICAgICB9XG4gIFxuICAgICAgICAgIC5jb250YWluZXItc3RhcnRze1xuICAgICAgICAgICAgICBpb24taWNvbntcbiAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogOXB4O1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgfVxuICB9XG4gIFxuICAvKiBDb250ZW5lZG9yIGRlIGltYWdlbiBkZWwgdXN1YXJpbyAqL1xuICAuY29udGFpbmVyLWltZ3tcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAuaW1ne1xuICAgICAgICAgIHdpZHRoOiA4MnB4O1xuICAgICAgICAgIGhlaWdodDogODBweDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgIH1cbiAgfVxuICBcbiAgLyogQ29sb3IgdmVyZGUgcGFyYSBhbGd1bm9zIGVsZW1lbnRvcyAqL1xuICAuZ3JlZW57XG4gICAgICBjb2xvcjogIzAwOEQzNjtcbiAgfSJdfQ== */");

/***/ }),

/***/ "IpE2":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/review-transfer/review-transfer.page.html ***!
  \*************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons class=\"margin-left\" slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons>\n    <ion-row>\n      <ion-title>Transferencias</ion-title>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div class=\"container\">\n    <ion-searchbar inputmode=\"text\" placeholder=\"Buscar por  nombre\" (ionChange)=\"onSearchChange($event)\">\n    </ion-searchbar>\n    <ion-card *ngFor=\"let travel of travels | async | filter:searchText:'status'\">\n      <ion-row [routerLink]=\"['../confirm-transfers/' + travel.travelId]\">\n        <ion-col size=\"4\" class=\"container-img\">\n          <div *ngIf=\"travel.client.imagen != ''\" style=\"background-image: url({{ travel.client.imagen }});\" class=\"img\">\n          </div>\n          <div *ngIf=\"travel.client.imagen == ''\" style=\"background-image: url('../../../../../assets/img/user.png');\" class=\"img\">\n          </div>\n        </ion-col>\n        <ion-col size=\"8\">\n          <ion-card-header>\n            <ion-card-title>{{travel.client.name}}</ion-card-title>\n            <ion-card-subtitle>{{travel.startLocation.address }}</ion-card-subtitle>\n          </ion-card-header>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "gFNC":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/review-transfer/review-transfer.module.ts ***!
  \***********************************************************************************/
/*! exports provided: ReviewTransferPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReviewTransferPageModule", function() { return ReviewTransferPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _review_transfer_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./review-transfer-routing.module */ "3M3X");
/* harmony import */ var _review_transfer_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./review-transfer.page */ "9Nxq");
/* harmony import */ var _shared_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../shared/pipes/pipes.module */ "9Xeq");








let ReviewTransferPageModule = class ReviewTransferPageModule {
};
ReviewTransferPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _review_transfer_routing_module__WEBPACK_IMPORTED_MODULE_5__["ReviewTransferPageRoutingModule"],
            _shared_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]
        ],
        declarations: [_review_transfer_page__WEBPACK_IMPORTED_MODULE_6__["ReviewTransferPage"]]
    })
], ReviewTransferPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-review-transfer-review-transfer-module.js.map