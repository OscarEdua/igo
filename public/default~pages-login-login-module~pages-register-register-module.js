(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-login-login-module~pages-register-register-module"],{

/***/ "8kji":
/*!*************************************************!*\
  !*** ./src/app/shared/services/push.service.ts ***!
  \*************************************************/
/*! exports provided: PushService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PushService", function() { return PushService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "wljF");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





let PushService = class PushService {
    constructor(oneSignal, http, router) {
        this.oneSignal = oneSignal;
        this.http = http;
        this.router = router;
    }
    configuracionInicial(uid) {
        /*     this.oneSignal. */
        this.oneSignal.setExternalUserId(uid);
        this.oneSignal.startInit('4f3b79de-e102-47ff-a1ac-f7753796d478', '796178968721');
        this.oneSignal.enableSound(true);
        this.oneSignal.enableVibrate(true);
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
        this.oneSignal.handleNotificationReceived().subscribe((noti) => {
            // Cuando recibe
            this.ext = noti.payload.additionalData['ruta'] + '';
        });
        this.oneSignal.handleNotificationOpened().subscribe((noti) => {
            // Cuando abre una notifiacion
            // this.router.navigate(['/notice/'+this.ext]);
            this.router.navigate([this.ext]);
        });
        //obtener uid de onsignal
        this.oneSignal.getIds().then((info) => {
            this.useId = info.userId;
        });
        this.oneSignal.endInit();
    }
    //Enviar  notificaciones individuales
    sendByUid(title, subtitle, detalle, ruta, uiduser) {
        //dato a mandar
        const datos = {
            app_id: '4f3b79de-e102-47ff-a1ac-f7753796d478',
            included_segments: ['Active User', 'Inactive User'],
            headings: { en: title },
            subtitle: { en: subtitle },
            data: { ruta: ruta, fecha: '100-210-250' },
            contents: { en: detalle },
            channel_for_external_user_ids: 'push',
            include_external_user_ids: [uiduser],
        };
        this.post(datos);
    }
    deleteuser() {
        this.oneSignal.removeExternalUserId();
    }
    //Enviar  notificaciones Global
    sendGlobal(title, subtitle, detalle, ruta) {
        //dato a mandar
        const datos = {
            app_id: '4f3b79de-e102-47ff-a1ac-f7753796d478',
            included_segments: ['Active Users', 'Inactive Users'],
            headings: { en: title },
            subtitle: { en: subtitle },
            data: { ruta: ruta, fecha: '100-210-250' },
            contents: { en: detalle },
        };
        this.post(datos);
    }
    post(datos) {
        const headers = {
            Authorization: 'Basic ODM0ODMzNzQtZjIzNi00NWU1LTk3ZDMtZmI1NmI3ZDhjZTFk',
        };
        var url = 'https://onesignal.com/api/v1/notifications';
        return new Promise((resolve) => {
            this.http.post(url, datos, { headers }).subscribe((data) => {
                resolve(data);
            });
        });
    }
};
PushService.ctorParameters = () => [
    { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_2__["OneSignal"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
PushService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], PushService);



/***/ }),

/***/ "ARc/":
/*!****************************************************************!*\
  !*** ./src/app/modules/home/pages/register/register.page.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilos para centrar el titulo del header solo en android */\n.md .toolbar-header {\n  text-align: center;\n}\n.background-slide {\n  background-image: url('fondo-1.png');\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.background-slide-client {\n  background-image: url('fondo-2.png');\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.background-slide-drivers {\n  background-image: url('fondo-3.png');\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n/* Estilos del titulo de la pantalla */\nion-title {\n  color: #3E4958;\n  font-size: 20px;\n  font-weight: bold;\n}\n/* Estilos del contenedor de la imagen para centrar la imagen */\n.container-img {\n  justify-content: center;\n  margin-top: 12%;\n}\n/* Estilos para el texto de  elegir un tipo de usuario */\n.container-item {\n  justify-content: center;\n}\n.container-item h5 {\n  font-size: 18px;\n  color: #3E4958;\n  font-weight: bold;\n}\n/* Estilos del contenedor del select */\n.container-select-role {\n  justify-content: center;\n  margin-top: 10%;\n}\n.container-select-role p {\n  color: #97ADB6;\n  font-size: 15px;\n}\n.container-select-role p span {\n  color: #008D36;\n  text-decoration: underline;\n}\n/* Estilos del ion-item que contiene el ion-select */\n.select-ionItem {\n  width: 90%;\n  --background: #F7F8F9;\n  --border-radius: 15px;\n  --color: #008D36;\n  --border-color: #D5DDE0;\n  --border-style: solid;\n}\n.select-ionItem ion-select {\n  justify-content: center;\n  width: 100%;\n}\n.select-ionItem ion-select ion-select-option span {\n  color: #3E4958;\n}\n/* Contenedor de los circulos */\n.container-circles {\n  justify-content: center;\n  align-items: center;\n  display: flex;\n}\n/* Estilos del boton de seguir pantalla */\n/* ion-button {\n  --background: #BFE6CA;\n  --color: black;\n  --border-radius: 100%;\n    height: 36px;\n    width: 36px;\n    font-size:11px;\n    color: #3E4958;\n\n\n} */\n/* Estilos del slide */\nion-slides {\n  height: 100%;\n  --bullet-background: #008D36;\n  --bullet-background-active: #008D36;\n}\n/* Set the text color */\nion-select::part(text) {\n  color: #3E4958;\n}\n/* Set the icon color and opacity */\nion-select::part(icon) {\n  color: #008D36;\n  opacity: 1;\n}\n.btn-see-img {\n  --background: #F7F8F9;\n  --background-activated: #F7F8F9;\n  --color:#3E4958;\n  font-size: 13px;\n  font-weight: bold;\n  --border-color:rgba(255,255,255,0);\n}\n.gender {\n  padding: 10px;\n  display: block;\n  text-align: left;\n}\n.container {\n  justify-content: center;\n}\n.container .container-inputs {\n  margin-bottom: 3%;\n  padding: 10px;\n  width: 90%;\n  text-align: left;\n}\n.container .container-inputs ion-select {\n  width: 100%;\n  justify-content: center;\n}\n.container .container-inputs .label {\n  color: #3E4958;\n  opacity: 0.8;\n  font-weight: bold;\n  padding-left: 10px;\n}\n.container .container-inputs .input-ionItem {\n  --background: #F7F8F9;\n  --highlight-color-focused: #008D36;\n  --highlight-color-valid: #008D36;\n  border: 0.5px solid #D5DDE0;\n  border-radius: 15px;\n  margin-top: 3%;\n}\n.container .container-select {\n  text-align: left;\n  padding: 10px;\n  width: 90%;\n  margin: 0px;\n}\n.btn {\n  width: 75%;\n  height: 60px;\n  --background: #008D36;\n  --border-radius:15px;\n  --box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  color: white;\n  font-size: 17px;\n}\n.container-btn {\n  justify-content: center;\n  margin-top: 35%;\n}\n.file-input {\n  opacity: 0;\n  position: absolute;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  z-index: 999;\n}\n.btn-subir {\n  background-color: #008D36;\n  color: white;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 45px;\n  height: 45px;\n  border-radius: 100%;\n  padding: 5%;\n  font-size: 25px;\n}\n.container-inputs-vehicle {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n}\n.container-title {\n  margin-left: 10%;\n  margin-bottom: 8%;\n}\n.container-title h5 {\n  font-weight: bold;\n  font-size: 18px;\n  color: #3E4958;\n}\n.container-img-vehicle {\n  display: flex;\n  justify-content: center;\n  width: 75%;\n}\n.container-img-vehicle img {\n  width: 20%;\n  height: 20%;\n}\nion-slides {\n  overflow-y: scroll;\n}\n.scroll-vertically {\n  overflow: auto;\n  height: 100%;\n}\nion-checkbox {\n  --background: #F2F2F2;\n  --border-radius: 8px;\n  --size: 30px;\n  --border-width: 1px;\n}\n.checkbox-text {\n  font-style: normal;\n  font-weight: normal;\n  color: #3E4958;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3JlZ2lzdGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSw4REFBQTtBQUNBO0VBQ0Usa0JBQUE7QUFDRjtBQUVBO0VBQ0Usb0NBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUFDRjtBQUdBO0VBQ0Usb0NBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUFBRjtBQUdBO0VBQ0Usb0NBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUFBRjtBQUlBLHNDQUFBO0FBQ0E7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBREY7QUFLQSwrREFBQTtBQUNBO0VBQ0UsdUJBQUE7RUFDQSxlQUFBO0FBRkY7QUFLQSx3REFBQTtBQUNBO0VBQ0UsdUJBQUE7QUFGRjtBQUlFO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUZKO0FBTUEsc0NBQUE7QUFDQTtFQUNFLHVCQUFBO0VBQ0EsZUFBQTtBQUhGO0FBS0U7RUFDRSxjQUFBO0VBQ0EsZUFBQTtBQUhKO0FBS0k7RUFDRSxjQUFBO0VBQ0EsMEJBQUE7QUFITjtBQVFBLG9EQUFBO0FBQ0E7RUFDRSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQkFBQTtBQUxGO0FBT0U7RUFDRSx1QkFBQTtFQUNBLFdBQUE7QUFMSjtBQVFNO0VBQ0UsY0FBQTtBQU5SO0FBZ0JBLCtCQUFBO0FBQ0E7RUFDRSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtBQWJGO0FBa0JBLHlDQUFBO0FBQ0E7Ozs7Ozs7Ozs7R0FBQTtBQVlBLHNCQUFBO0FBQ0E7RUFDRSxZQUFBO0VBQ0EsNEJBQUE7RUFDQSxtQ0FBQTtBQWhCRjtBQW1CQSx1QkFBQTtBQUNBO0VBQ0UsY0FBQTtBQWhCRjtBQW1CQSxtQ0FBQTtBQUNBO0VBQ0UsY0FBQTtFQUNBLFVBQUE7QUFoQkY7QUFxQkE7RUFDRSxxQkFBQTtFQUNBLCtCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtDQUFBO0FBbEJGO0FBc0JBO0VBQ0UsYUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQW5CRjtBQXdCQTtFQUNFLHVCQUFBO0FBckJGO0FBc0JFO0VBQ0UsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0FBcEJKO0FBc0JJO0VBQ0UsV0FBQTtFQUNBLHVCQUFBO0FBcEJOO0FBd0JFO0VBQ0UsY0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBdEJKO0FBMEJJO0VBQ0UscUJBQUE7RUFDQSxrQ0FBQTtFQUNBLGdDQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUF4Qk47QUE2QkU7RUFDRSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQTNCSjtBQW9DQTtFQUNFLFVBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLG9EQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFqQ0Y7QUFvQ0E7RUFDRSx1QkFBQTtFQUNBLGVBQUE7QUFqQ0Y7QUFzQ0E7RUFDRSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtBQW5DRjtBQTJDQTtFQUNFLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBeENGO0FBMkNFO0VBQ0UsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUF4Q0o7QUEyQ0U7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0FBeENKO0FBeUNJO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQXZDTjtBQTJDRTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUF4Q0o7QUF5Q0k7RUFDRSxVQUFBO0VBQ0EsV0FBQTtBQXZDTjtBQTJDRTtFQUNFLGtCQUFBO0FBeENKO0FBMkNFO0VBQ0UsY0FBQTtFQUNBLFlBQUE7QUF4Q0o7QUEyQ0E7RUFDRSxxQkFBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBeENGO0FBMkNBO0VBQ0Usa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUF4Q0YiLCJmaWxlIjoicmVnaXN0ZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogRXN0aWxvcyBwYXJhIGNlbnRyYXIgZWwgdGl0dWxvIGRlbCBoZWFkZXIgc29sbyBlbiBhbmRyb2lkICovXG4ubWQgLnRvb2xiYXItaGVhZGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uYmFja2dyb3VuZC1zbGlkZXtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vLi4vLi4vLi4vYXNzZXRzL2ltZy9mb25kby0xLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBcbn1cblxuLmJhY2tncm91bmQtc2xpZGUtY2xpZW50e1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi8uLi8uLi8uLi9hc3NldHMvaW1nL2ZvbmRvLTIucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIFxufVxuLmJhY2tncm91bmQtc2xpZGUtZHJpdmVyc3tcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vLi4vLi4vLi4vYXNzZXRzL2ltZy9mb25kby0zLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBcbn1cblxuLyogRXN0aWxvcyBkZWwgdGl0dWxvIGRlIGxhIHBhbnRhbGxhICovXG5pb24tdGl0bGUge1xuICBjb2xvcjogIzNFNDk1ODtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuXG4vKiBFc3RpbG9zIGRlbCBjb250ZW5lZG9yIGRlIGxhIGltYWdlbiBwYXJhIGNlbnRyYXIgbGEgaW1hZ2VuICovXG4uY29udGFpbmVyLWltZyB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAxMiU7XG59XG5cbi8qIEVzdGlsb3MgcGFyYSBlbCB0ZXh0byBkZSAgZWxlZ2lyIHVuIHRpcG8gZGUgdXN1YXJpbyAqL1xuLmNvbnRhaW5lci1pdGVtIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cbiAgaDUge1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBjb2xvcjogIzNFNDk1ODtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgfVxufVxuXG4vKiBFc3RpbG9zIGRlbCBjb250ZW5lZG9yIGRlbCBzZWxlY3QgKi9cbi5jb250YWluZXItc2VsZWN0LXJvbGUge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMTAlO1xuXG4gIHAge1xuICAgIGNvbG9yOiAjOTdBREI2O1xuICAgIGZvbnQtc2l6ZTogMTVweDtcblxuICAgIHNwYW4ge1xuICAgICAgY29sb3I6ICMwMDhEMzY7XG4gICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgICB9XG4gIH1cbn1cblxuLyogRXN0aWxvcyBkZWwgaW9uLWl0ZW0gcXVlIGNvbnRpZW5lIGVsIGlvbi1zZWxlY3QgKi9cbi5zZWxlY3QtaW9uSXRlbSB7XG4gIHdpZHRoOiA5MCU7XG4gIC0tYmFja2dyb3VuZDogI0Y3RjhGOTtcbiAgLS1ib3JkZXItcmFkaXVzOiAxNXB4O1xuICAtLWNvbG9yOiAjMDA4RDM2O1xuICAtLWJvcmRlci1jb2xvcjogI0Q1RERFMDtcbiAgLS1ib3JkZXItc3R5bGU6IHNvbGlkO1xuXG4gIGlvbi1zZWxlY3Qge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgaW9uLXNlbGVjdC1vcHRpb24ge1xuICAgICAgc3BhbiB7XG4gICAgICAgIGNvbG9yOiAjM0U0OTU4XG4gICAgICB9XG4gICAgfVxuICB9XG5cblxufVxuXG5cblxuLyogQ29udGVuZWRvciBkZSBsb3MgY2lyY3Vsb3MgKi9cbi5jb250YWluZXItY2lyY2xlcyB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuXG5cbn1cblxuLyogRXN0aWxvcyBkZWwgYm90b24gZGUgc2VndWlyIHBhbnRhbGxhICovXG4vKiBpb24tYnV0dG9uIHtcbiAgLS1iYWNrZ3JvdW5kOiAjQkZFNkNBO1xuICAtLWNvbG9yOiBibGFjaztcbiAgLS1ib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgIGhlaWdodDogMzZweDtcbiAgICB3aWR0aDogMzZweDtcbiAgICBmb250LXNpemU6MTFweDtcbiAgICBjb2xvcjogIzNFNDk1ODtcbiBcbiBcbn0gKi9cblxuLyogRXN0aWxvcyBkZWwgc2xpZGUgKi9cbmlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDEwMCU7XG4gIC0tYnVsbGV0LWJhY2tncm91bmQ6ICMwMDhEMzY7XG4gIC0tYnVsbGV0LWJhY2tncm91bmQtYWN0aXZlOiAjMDA4RDM2O1xufVxuXG4vKiBTZXQgdGhlIHRleHQgY29sb3IgKi9cbmlvbi1zZWxlY3Q6OnBhcnQodGV4dCkge1xuICBjb2xvcjogIzNFNDk1ODtcbn1cblxuLyogU2V0IHRoZSBpY29uIGNvbG9yIGFuZCBvcGFjaXR5ICovXG5pb24tc2VsZWN0OjpwYXJ0KGljb24pIHtcbiAgY29sb3I6ICMwMDhEMzY7XG4gIG9wYWNpdHk6IDE7XG59XG5cblxuXG4uYnRuLXNlZS1pbWd7XG4gIC0tYmFja2dyb3VuZDogI0Y3RjhGOTtcbiAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogI0Y3RjhGOTtcbiAgLS1jb2xvcjojM0U0OTU4O1xuICBmb250LXNpemU6IDEzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAtLWJvcmRlci1jb2xvcjpyZ2JhKDI1NSwyNTUsMjU1LDApO1xufVxuXG5cbi5nZW5kZXIge1xuICBwYWRkaW5nOiAxMHB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cblxuXG5cbi5jb250YWluZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgLmNvbnRhaW5lci1pbnB1dHMge1xuICAgIG1hcmdpbi1ib3R0b206IDMlO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgd2lkdGg6IDkwJTtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBcbiAgICBpb24tc2VsZWN0IHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgfVxuICBcbiAgICBcbiAgLmxhYmVsIHtcbiAgICBjb2xvcjogIzNFNDk1ODtcbiAgICBvcGFjaXR5OiAwLjg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICB9XG4gIFxuICBcbiAgICAuaW5wdXQtaW9uSXRlbSB7XG4gICAgICAtLWJhY2tncm91bmQ6ICNGN0Y4Rjk7XG4gICAgICAtLWhpZ2hsaWdodC1jb2xvci1mb2N1c2VkOiAjMDA4RDM2O1xuICAgICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6ICMwMDhEMzY7XG4gICAgICBib3JkZXI6IDAuNXB4IHNvbGlkICNENURERTA7XG4gICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgICAgbWFyZ2luLXRvcDogMyU7XG4gICAgfVxuICAgIFxuICB9XG5cbiAgLmNvbnRhaW5lci1zZWxlY3R7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIHdpZHRoOiA5MCU7XG4gICAgbWFyZ2luOiAwcHg7XG4gIH1cbiAgXG5cbn1cblxuXG5cblxuLmJ0biB7XG4gIHdpZHRoOiA3NSU7XG4gIGhlaWdodDogNjBweDtcbiAgLS1iYWNrZ3JvdW5kOiAjMDA4RDM2O1xuICAtLWJvcmRlci1yYWRpdXM6MTVweDtcbiAgLS1ib3gtc2hhZG93OiAwcHggNHB4IDIwcHggcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjI1KTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE3cHg7XG59XG5cbi5jb250YWluZXItYnRue1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMzUlO1xufVxuXG5cblxuLmZpbGUtaW5wdXQge1xuICBvcGFjaXR5OiAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgbGVmdDogMDtcbiAgei1pbmRleDogOTk5O1xufVxuXG5cblxuXG5cbi8vRXN0aWxvcyBkZSBib3RvbiBzdWJpciBpbWdcbi5idG4tc3ViaXJ7XG4gIGJhY2tncm91bmQtY29sb3I6IzAwOEQzNjtcbiAgY29sb3I6d2hpdGU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB3aWR0aDogNDVweDtcbiAgaGVpZ2h0OiA0NXB4O1xuICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICBwYWRkaW5nOiA1JTtcbiAgZm9udC1zaXplOiAyNXB4O1xuICB9XG5cbiAgLmNvbnRhaW5lci1pbnB1dHMtdmVoaWNsZXtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG5cbiAgLmNvbnRhaW5lci10aXRsZXtcbiAgICBtYXJnaW4tbGVmdDogMTAlO1xuICAgIG1hcmdpbi1ib3R0b206IDglO1xuICAgIGg1e1xuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICBjb2xvcjogIzNFNDk1ODtcbiAgICB9XG4gIH1cblxuICAuY29udGFpbmVyLWltZy12ZWhpY2xle1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgd2lkdGg6IDc1JTtcbiAgICBpbWd7XG4gICAgICB3aWR0aDogMjAlO1xuICAgICAgaGVpZ2h0OiAyMCU7XG4gICAgfVxuICB9XG5cbiAgaW9uLXNsaWRlcyB7XG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICB9XG5cbiAgLnNjcm9sbC12ZXJ0aWNhbGx5IHtcbiAgICBvdmVyZmxvdzogYXV0bztcbiAgICBoZWlnaHQ6IDEwMCU7XG4gIH1cblxuaW9uLWNoZWNrYm94IHtcbiAgLS1iYWNrZ3JvdW5kOiAjRjJGMkYyO1xuICAtLWJvcmRlci1yYWRpdXM6IDhweDtcbiAgLS1zaXplOiAzMHB4O1xuICAtLWJvcmRlci13aWR0aDogMXB4O1xufVxuXG4uY2hlY2tib3gtdGV4dCB7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgY29sb3I6ICMzRTQ5NTg7XG59XG4iXX0= */");

/***/ }),

/***/ "CDR2":
/*!**************************************************************!*\
  !*** ./src/app/modules/home/pages/register/register.page.ts ***!
  \**************************************************************/
/*! exports provided: RegisterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPage", function() { return RegisterPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_register_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./register.page.html */ "TFk7");
/* harmony import */ var _register_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./register.page.scss */ "ARc/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var src_app_shared_services_images_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/services/images.service */ "gx82");
/* harmony import */ var src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/core/authentication/authentication.service */ "6CRC");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @capacitor/core */ "gcOT");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @agm/core */ "pxUr");
/* harmony import */ var src_app_shared_services_push_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/shared/services/push.service */ "8kji");
/* harmony import */ var _services_users_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../services/users.service */ "6JOU");












const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_8__["Plugins"];
let RegisterPage = class RegisterPage {
    constructor(alertsService, imagesService, authenticationService, router, pushService, userService, mapsAPILoader) {
        this.alertsService = alertsService;
        this.imagesService = imagesService;
        this.authenticationService = authenticationService;
        this.router = router;
        this.pushService = pushService;
        this.userService = userService;
        this.mapsAPILoader = mapsAPILoader;
        this.observableList = [];
        this.user = {
            name: '',
            email: '',
            password: '',
            address: '',
            telephoneNumber: '',
            role: 'client',
            imagen: ''
        };
        this.inputType = 'password';
        this.iconName = 'eye';
        this.vehicle = {
            licenseImagen: '',
            brand: '',
            plateImagen: '',
            imagen: '',
            color: '',
            accepPets: false,
            luggageSpace: false,
            acceptSmok: false
        };
        this.plateImagenFile = null;
        this.vehicleImagenFile = null;
        this.licenseImagenFile = null;
    }
    ngOnInit() {
        this.mapsAPILoader.load()
            .then(() => {
            this.setAddressWithCurrentPosition();
            this.geoCoder = new google.maps.Geocoder;
        });
    }
    ionViewWillEnter() {
        this.getUserAdmin();
    }
    changeFileImagen($event, inputFile) {
        if ($event.target.files && $event.target.files[0]) {
            let reader = new FileReader();
            reader.readAsDataURL($event.target.files[0]);
            reader.onload = (event) => {
                switch (inputFile) {
                    case 'plateImagen':
                        this.plateImagenFile = $event.target.files[0];
                        console.log(this.plateImagenFile);
                        this.vehicle.plateImagen = event.target.result;
                        console.log(this.vehicleImagenFile);
                        break;
                    case 'vehicleImagen':
                        this.vehicleImagenFile = $event.target.files[0];
                        this.vehicle.imagen = event.target.result;
                        break;
                    case 'licenseImagen':
                        this.licenseImagenFile = $event.target.files[0];
                        this.vehicle.licenseImagen = event.target.result;
                        break;
                }
            };
        }
    }
    showHiddenPassword() {
        this.iconName = this.iconName === 'eye' ? 'eye-off' : 'eye';
        this.inputType = this.inputType === 'text' ? 'password' : 'text';
    }
    signIn() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.alertsService.presentLoading('Registrando...');
            if (this.user.role == 'client') {
                if (!this.thereAreEmptyFieldsClient()) {
                    this.authenticationService.signUp(this.user)
                        .then(() => {
                        this.alertsService.loading.dismiss();
                        this.cleanClientFields();
                        this.alertsService.presentAlertConfirmAccept('Ya casi!', 'Por favor revise su correo electronico para activar su cuenta.')
                            .then(result => {
                            this.router.navigate(['home/login']);
                        });
                    })
                        .catch((error) => {
                        this.alertsService.loading.dismiss();
                        if (error.message = 'The email address is already in use by another account.')
                            this.alertsService.presentAlert('La direccion de correo ya esta siendo usado por otra cuenta.');
                    });
                }
                else {
                    this.alertsService.loading.dismiss();
                    this.alertsService.presentAlertWithHeader('Registro!', 'Por favor complete los campos requeridos!');
                }
            }
            else if (this.user.role == 'driver') {
                if (!this.thereAreEmptyFieldsDriver()) {
                    if (!this.thereAreEmptyImagensFields()) {
                        this.vehicle.plateImagen = yield this.imagesService.uploadImage('vehicles', this.plateImagenFile);
                        this.vehicle.imagen = yield this.imagesService.uploadImage('vehicles', this.vehicleImagenFile);
                        this.vehicle.licenseImagen = yield this.imagesService.uploadImage('vehicles', this.licenseImagenFile);
                        this.user.vehicle = this.vehicle;
                        this.user.vehicle.type = '';
                        this.authenticationService.signUp(this.user)
                            .then(() => {
                            this.alertsService.loading.dismiss();
                            this.alertsService.presentAlertConfirmAccept('Ya casi!', 'Por favor revise su correo electronico para activar su cuenta.')
                                .then(() => {
                                // notificación a admins
                                this.users.map((res) => {
                                    this.pushService.sendByUid('IGO', 'Se ha registrado un conductor en el sistema, revisa su información para aprobarlo', 'Se ha registrado un conductor en el sistema, revisa su información para aprobarlo', 'dashboard/users/drivers', res.uid);
                                });
                                this.router.navigate(['home/login']);
                            });
                        })
                            .catch((error) => {
                            this.alertsService.loading.dismiss();
                            if (error.message = 'The email address is already in use by another account.')
                                this.alertsService.presentAlert('La direccion de correo ya esta siendo usado por otra cuenta.');
                        });
                    }
                    else {
                        this.alertsService.loading.dismiss();
                        this.alertsService.presentAlert('Por favor seleccione las imagenes requeridas!');
                    }
                }
                else {
                    this.alertsService.loading.dismiss();
                    this.alertsService.presentAlertWithHeader('Register!', 'Por favor complete los campos requeridos!');
                }
            }
        });
    }
    thereAreEmptyFieldsClient() {
        if (this.user.name == '' || this.user.email == '' || this.user.password == '' || this.user.gender == '')
            return true;
        else
            return false;
    }
    thereAreEmptyFieldsDriver() {
        if (this.user.name == '' || this.user.email == '' || this.user.password == '' || this.user.telephoneNumber == '' || this.user.gender == '' || this.vehicle.model == '' || this.vehicle.color == '')
            return true;
        else
            return false;
    }
    thereAreEmptyImagensFields() {
        if (this.vehicle.plateImagen == '' || this.vehicle.imagen == '' || this.vehicle.licenseImagen == '')
            return true;
        else
            return false;
    }
    cleanClientFields() {
        this.user = {
            name: '',
            email: '',
            password: '',
            address: '',
            telephoneNumber: '',
            role: 'client',
            imagen: ''
        };
    }
    setAddressWithCurrentPosition() {
        const options = {
            enableHighAccuracy: false,
            timeout: Infinity,
            maximumAge: 0
        };
        const wait = Geolocation.watchPosition(options, (position, error) => {
            this.latitude = position.coords.latitude;
            this.longitude = position.coords.longitude;
            this.zoom = 8;
            this.setAddres(this.latitude, this.longitude);
        });
    }
    getUserAdmin() {
        const observable = this.userService
            .getUsersByRole('admin')
            .subscribe((res) => {
            this.users = res;
            console.log(this.users);
        });
        this.observableList.push(observable);
    }
    setAddres(latitude, longitude) {
        this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    this.zoom = 12;
                    this.user.address = results[0].formatted_address;
                }
                else {
                    console.log('No results found...');
                }
            }
            else {
                console.log('Geocode failed due to: ' + status);
            }
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
RegisterPage.ctorParameters = () => [
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: src_app_shared_services_images_service__WEBPACK_IMPORTED_MODULE_5__["ImagesService"] },
    { type: src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: src_app_shared_services_push_service__WEBPACK_IMPORTED_MODULE_10__["PushService"] },
    { type: _services_users_service__WEBPACK_IMPORTED_MODULE_11__["UsersService"] },
    { type: _agm_core__WEBPACK_IMPORTED_MODULE_9__["MapsAPILoader"] }
];
RegisterPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-register',
        template: _raw_loader_register_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_register_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RegisterPage);



/***/ }),

/***/ "Kp8P":
/*!**********************************************************!*\
  !*** ./src/app/modules/home/pages/login/login.page.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".img-signIn {\n  width: 50%;\n  margin-left: 20%;\n  margin-right: 20%;\n  margin-top: 0%;\n  border-radius: 50%;\n  background-color: #fff;\n}\n\nh4 {\n  color: #3E4958;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 20px;\n  line-height: 28px;\n  margin-top: 10%;\n}\n\n.btn-registro {\n  position: absolute;\n  color: #008D36;\n  background-color: rgba(255, 255, 255, 0);\n  font-size: 17px;\n}\n\n.container-inputs {\n  margin-bottom: 0%;\n  padding: 10px;\n}\n\nion-item {\n  --background: #F2F2F2;\n  --highlight-color-focused:#008D36;\n  --highlight-color-valid:#008D36;\n  border: 0.5px solid #D5DDE0;\n  border-radius: 15px;\n  margin-top: 3%;\n}\n\n.label {\n  color: #3E4958;\n  font-size: 14px;\n  opacity: 0.8;\n  font-weight: bold;\n  padding-left: 10px;\n}\n\nh5 {\n  color: #3E4958;\n}\n\n.my_buttton {\n  /* Green */\n  margin-top: 5%;\n  margin-left: 10%;\n  margin-right: 10%;\n  background: #008D36;\n  height: 60px;\n  width: 80%;\n  box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  border-radius: 15px;\n  color: #ffffff;\n  font-size: 18px;\n  font-weight: bold;\n  margin-bottom: 5%;\n}\n\n.container-btn {\n  padding-top: 10%;\n  margin-left: 15%;\n  color: #97ADB6;\n}\n\n/* Solid border */\n\nhr.solid {\n  border-top: 3px solid #bbb;\n  padding: 5px;\n  margin-left: 5%;\n  margin-right: 5%;\n}\n\nh5 {\n  color: #3E4958;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 17px;\n  line-height: 28px;\n}\n\n.img-fb {\n  margin-top: 0%;\n}\n\nion-row {\n  margin-left: 20%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUFDSjs7QUFFRTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUlBO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0Esd0NBQUE7RUFDQSxlQUFBO0FBREY7O0FBSUE7RUFDRSxpQkFBQTtFQUNBLGFBQUE7QUFERjs7QUFLQTtFQUNJLHFCQUFBO0VBQ0EsaUNBQUE7RUFDQSwrQkFBQTtFQUNBLDJCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FBRko7O0FBS0E7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBRkY7O0FBS0E7RUFDRSxjQUFBO0FBRkY7O0FBS0E7RUFDRSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0RBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQUZGOztBQUtBO0VBQ0UsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFGRjs7QUFLQSxpQkFBQTs7QUFDQTtFQUNFLDBCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUZGOztBQUlBO0VBQ0UsY0FBQTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFESjs7QUFJQTtFQUNFLGNBQUE7QUFERjs7QUFHQTtFQUNFLGdCQUFBO0FBQUYiLCJmaWxlIjoibG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltZy1zaWduSW57XG4gICAgd2lkdGg6IDUwJTtcbiAgICBtYXJnaW4tbGVmdDogMjAlO1xuICAgIG1hcmdpbi1yaWdodDogMjAlO1xuICAgIG1hcmdpbi10b3A6IDAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICB9XG5cbiAgaDQge1xuICAgIGNvbG9yOiAjM0U0OTU4O1xuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDI4cHg7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xuXG4gIH1cblxuICBcbi5idG4tcmVnaXN0cm8ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGNvbG9yOiAjMDA4RDM2O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKCRjb2xvcjogI2ZmZmZmZiwgJGFscGhhOiAwKTtcbiAgZm9udC1zaXplOiAxN3B4O1xufVxuXG4uY29udGFpbmVyLWlucHV0cyB7XG4gIG1hcmdpbi1ib3R0b206MCU7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG5cblxuaW9uLWl0ZW0ge1xuICAgIC0tYmFja2dyb3VuZDogI0YyRjJGMjtcbiAgICAtLWhpZ2hsaWdodC1jb2xvci1mb2N1c2VkOiMwMDhEMzY7XG4gICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6IzAwOEQzNjtcbiAgICBib3JkZXI6IDAuNXB4IHNvbGlkICNENURERTA7XG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICBtYXJnaW4tdG9wOiAzJTtcbn1cblxuLmxhYmVsIHtcbiAgY29sb3I6ICMzRTQ5NTg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgb3BhY2l0eTogMC44O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xufVxuXG5oNXtcbiAgY29sb3I6IzNFNDk1ODtcbn1cblxuLm15X2J1dHR0b257XG4gIC8qIEdyZWVuICovXG4gIG1hcmdpbi10b3A6IDUlO1xuICBtYXJnaW4tbGVmdDogMTAlO1xuICBtYXJnaW4tcmlnaHQ6IDEwJTtcbiAgYmFja2dyb3VuZDogIzAwOEQzNjtcbiAgaGVpZ2h0OiA2MHB4O1xuICB3aWR0aDogODAlO1xuICBib3gtc2hhZG93OiAwcHggNHB4IDIwcHggcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjI1KTtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG1hcmdpbi1ib3R0b206IDUlO1xufVxuXG4uY29udGFpbmVyLWJ0bntcbiAgcGFkZGluZy10b3A6IDEwJTtcbiAgbWFyZ2luLWxlZnQ6IDE1JTtcbiAgY29sb3I6ICM5N0FEQjY7XG59XG5cbi8qIFNvbGlkIGJvcmRlciAqL1xuaHIuc29saWQge1xuICBib3JkZXItdG9wOiAzcHggc29saWQgI2JiYjtcbiAgcGFkZGluZzogNXB4O1xuICBtYXJnaW4tbGVmdDogNSU7XG4gIG1hcmdpbi1yaWdodDogNSU7XG59XG5oNSB7XG4gIGNvbG9yOiAjM0U0OTU4O1xuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgbGluZS1oZWlnaHQ6IDI4cHg7XG59XG5cbi5pbWctZmJ7ICBcbiAgbWFyZ2luLXRvcDogMCU7XG59XG5pb24tcm93e1xuICBtYXJnaW4tbGVmdDogMjAlO1xufSJdfQ== */");

/***/ }),

/***/ "TFk7":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/pages/register/register.page.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"toolbar-header\">\n    <ion-title>Registro</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content scrollable=\"true\">\n  <ion-slides pager=\"true\">\n    <ion-slide>\n      <ion-grid fixed>\n        <ion-row class=\"container-img\">\n          <img src=\"../../assets/img/vehicle-register.png\" alt=\"\" />\n        </ion-row>\n        <ion-row class=\"container-item\">\n          <h5>Elige tu tipo de usuario</h5>\n        </ion-row>\n        <ion-row class=\"container-select-role\">\n          <ion-item class=\"select-ionItem\">\n            <ion-select cancelText=\"Cancelar\" name=\"role\" [(ngModel)]=\"user.role\" value=\"client\" okText=\"Aceptar\">\n              <ion-select-option value=\"client\"><span>Cliente</span></ion-select-option>\n              <ion-select-option value=\"driver\"><span>Conductor</span></ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-row>\n        <ion-row class=\"container-select-role\" [routerLink]=\"['../../login']\">\n          <p>Tienes una cuenta? <span>Ingresa aquí </span></p>\n        </ion-row>\n      </ion-grid>\n    </ion-slide>\n\n\n    <ion-slide *ngIf=\"user.role != 'client'\" class=\"background-slide-drivers\">\n      <ion-grid>\n        <ion-row class=\"container-title\">\n          <h5>Cuéntanos de ti</h5>\n        </ion-row>\n        <ion-row class=\"container\">\n          <div class=\"container-inputs\">\n            <ion-label class=\"label\">NOMBRE*</ion-label>\n            <ion-item class=\"input-ionItem\">\n              <ion-input type=\"text\" [(ngModel)]=\"user.name\"></ion-input>\n            </ion-item>\n          </div>\n          <div class=\"container-inputs\">\n            <ion-label class=\"label\">EMAIL*</ion-label>\n            <ion-item class=\"input-ionItem\">\n              <ion-input type=\"email\" [(ngModel)]=\"user.email\"></ion-input>\n            </ion-item>\n          </div>\n          <div class=\"container-inputs\">\n            <ion-label class=\"label\">CONTRASEÑA*</ion-label>\n            <ion-item class=\"input-ionItem\">\n              <ion-input type=\"password\" [(ngModel)]=\"user.password\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-row>\n      </ion-grid>\n    </ion-slide>\n\n    <ion-slide *ngIf=\"user.role != 'client'\" class=\"background-slide\">\n      <ion-grid>\n        <ion-row class=\"container-title\">\n          <h5>Cuéntanos de ti</h5>\n        </ion-row>\n        <ion-row class=\"container\">\n          <div class=\"container-inputs\">\n            <ion-label class=\"label\">DIRECCIÓN*</ion-label> \n            <ion-item class=\"input-ionItem\">\n              <ion-input placeholder=\"Se obtiene desde el GPS\" type=\"text\" disabled=\"true\" [value]=\"user.address\"></ion-input>\n            </ion-item> <br>\n            <ion-label class=\"label\">NUMERO DE CELULAR*</ion-label>\n            <ion-item class=\"input-ionItem\">\n              <ion-input type=\"number\" [(ngModel)]=\"user.telephoneNumber\"></ion-input>\n            </ion-item>\n          </div>\n          <div class=\"container-select\">\n            <ion-label class=\"label gender\">GENERO</ion-label>\n            <ion-item class=\"select-ionItem\" style=\"width: 100%;\">\n              <ion-select [(ngModel)]=\"user.gender\" name=\"gender\" cancelText=\"Cancelar\" name=\"role\" value=\"client\"\n                okText=\"Aceptar\">\n                <ion-select-option value=\"Femenino\"><span>Femenino</span></ion-select-option>\n                <ion-select-option value=\"Masculino\"><span>Masculino</span></ion-select-option>\n                <ion-select-option value=\"Otros\"><span>Otros</span></ion-select-option>\n              </ion-select>\n            </ion-item>\n          </div>\n\n        </ion-row>\n<!--         <ion-row class=\"container-btn\">\n          <ion-button class=\"btn\" (click)=\"signin()\"> Registrarse </ion-button>\n        </ion-row> -->\n      </ion-grid>\n    </ion-slide>\n\n    <ion-slide *ngIf=\"user.role != 'client'\" class=\"background-slide-client\">\n      <ion-grid class=\"scroll-vertically\">\n        <ion-row class=\"container-title\">\n          <h5>Describe tu vehículo</h5>\n        </ion-row>\n        <ion-row class=\"container\">\n          <div class=\"container-inputs\">\n            <ion-label class=\"label\">PLACA*</ion-label>\n            <div class=\"container-inputs-vehicle\">\n              <ion-item *ngIf=\"vehicle.plateImagen == ''\" class=\"input-ionItem\" style=\"width: 75%\">\n                <ion-input type=\"text\" value=\"Imagen no subida\" disabled=\"true\"></ion-input>\n              </ion-item>\n              <ion-item *ngIf=\"vehicle.plateImagen != ''\" class=\"input-ionItem\" style=\"width: 75%\"\n                >\n                <ion-button class=\"btn-see-img\">\n                  Tocar para ver imagen\n                </ion-button>\n              </ion-item>\n              <div class=\"container-icon\">\n                <ion-item lines=\"none\">\n                  <ion-input type=\"file\" accept=\"image/*\" id=\"file-input-license-imagen\" class=\"file-input\"\n                    (change)=\"changeFileImagen($event, 'plateImagen')\"></ion-input>\n                  <button class=\"btn-subir\">\n                    <ion-icon name=\"arrow-up-outline\"></ion-icon>\n                  </button>\n                </ion-item>\n              </div>\n            </div>\n          </div>\n          <div class=\"container-inputs\">\n            <ion-label class=\"label\">MODELO DEL VEHÍCULO*</ion-label>\n            <div class=\"container-inputs-vehicle\">\n              <ion-item class=\"input-ionItem\" style=\"width: 75%\">\n                <ion-input type=\"text\" [(ngModel)]=\"vehicle.brand\"></ion-input>\n              </ion-item>\n            </div>\n          </div>\n          <div class=\"container-inputs\">\n            <ion-label class=\"label\">FOTO DEL VEHICULO*</ion-label>\n            <div class=\"container-inputs-vehicle\">\n              <ion-item class=\"input-ionItem\" *ngIf=\"vehicle.imagen == ''\" style=\"width: 75%\">\n                <ion-input type=\"text\" value=\"Imagen no subida\" disabled=\"true\"></ion-input>\n              </ion-item>\n              <ion-item *ngIf=\"vehicle.imagen != ''\" class=\"input-ionItem\" style=\"width: 75%\"\n                >\n                <ion-button class=\"btn-see-img\">\n                  Tocar para ver imagen\n                </ion-button>\n              </ion-item>\n              <ion-item lines=\"none\">\n                <ion-input type=\"file\" accept=\"image/*\" id=\"flicenseImagenile-input\" #fileInp class=\"file-input\"\n                  (change)=\"changeFileImagen($event, 'vehicleImagen')\"></ion-input>\n                <button class=\"btn-subir\">\n                  <ion-icon name=\"arrow-up-outline\"></ion-icon>\n                </button>\n              </ion-item>\n            </div>\n          </div>\n\n          <div class=\"container-inputs\">\n            <ion-label class=\"label\">LICENCIA*</ion-label>\n            <div class=\"container-inputs-vehicle\">\n              <ion-item class=\"input-ionItem\" *ngIf=\"vehicle.licenseImagen == ''\" style=\"width: 75%\">\n                <ion-input type=\"text\" value=\"Imagen no subida\" disabled=\"true\"></ion-input>\n              </ion-item>\n              <ion-item *ngIf=\"vehicle.licenseImagen != ''\" class=\"input-ionItem\" style=\"width: 75%\"\n                >\n                <ion-button class=\"btn-see-img\">\n                  Tocar para ver imagen\n                </ion-button>\n              </ion-item>\n              <ion-item lines=\"none\">\n                <ion-input type=\"file\" accept=\"image/*\" class=\"file-input\" #fileInp\n                  (change)=\"changeFileImagen($event, 'licenseImagen')\"></ion-input>\n                <button class=\"btn-subir\">\n                  <ion-icon name=\"arrow-up-outline\"></ion-icon>\n                </button>\n              </ion-item>\n            </div>\n          </div>\n          <div class=\"container-inputs\">\n            <ion-label class=\"label\">COLOR*</ion-label>\n            <ion-item class=\"input-ionItem\" style=\"width: 75%\">\n              <ion-input type=\"text\" [(ngModel)]=\"vehicle.color\"></ion-input>\n            </ion-item>\n          </div>\n          <div class=\"container-inputs\">\n            <ion-label class=\"label\">NUMERO DE ASIENTOS DISPONIBLES*</ion-label>\n            <ion-item class=\"input-ionItem\" style=\"width: 75%\">\n              <ion-input type=\"number\" [(ngModel)]=\"vehicle.availableSeats\"></ion-input>\n            </ion-item>\n          </div>\n          <div class=\"container-inputs\">\n            <ion-list lines=\"none\">\n              <ion-item>\n                <ion-label class=\"checkbox-text\">ACEPTA MASCOTAS</ion-label>\n                <ion-checkbox slot=\"start\" [(ngModel)]=\"vehicle.accepPets\"></ion-checkbox>\n              </ion-item>\n              <ion-item>\n                <ion-label class=\"ion-text-wrap checkbox-text\">ESPACIO PARA EQUIPAJE</ion-label>\n                <ion-checkbox slot=\"start\" [(ngModel)]=\"vehicle.luggageSpace\"></ion-checkbox>\n              </ion-item>\n              <ion-item>\n                <ion-label class=\"checkbox-text\">ACEPTA FUMAR</ion-label>\n                <ion-checkbox slot=\"start\" [(ngModel)]=\"vehicle.acceptSmok\"></ion-checkbox>\n              </ion-item>\n            </ion-list>\n          </div>\n          <ion-button class=\"btn\" (click)=\"signIn()\"> Registrarse </ion-button> </ion-row>\n      </ion-grid>\n    </ion-slide>\n\n    <ion-slide *ngIf=\"user.role == 'client'\" class=\"background-slide\">\n      <ion-row class=\"container\">\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">NOMBRE* </ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input [(ngModel)]=\"user.name\" name=\"name\" type=\"text\"></ion-input>\n          </ion-item>\n        </div>\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">EMAIL*</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input [(ngModel)]=\"user.email\" name=\"email\" type=\"email\"></ion-input>\n          </ion-item>\n        </div>\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">CONTRASEÑA*</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input [type]=\"inputType\" name=\"password\" [(ngModel)]=\"user.password\"></ion-input>\n            <ion-icon [name]=\"iconName\" item-right (click)=\"showHiddenPassword()\"></ion-icon>\n          </ion-item>\n        </div>\n        <div class=\"container-inputs\">\n          <ion-label class=\"label\">DIRECCIÓN*</ion-label>\n          <ion-item class=\"input-ionItem\">\n            <ion-input type=\"text\" placeholder=\"Se obtiene desde el GPS\" disabled=\"true\" [value]=\"user.address\"\n              name=\"address\"></ion-input>\n          </ion-item>\n        </div>\n      </ion-row>\n    </ion-slide>\n\n    <ion-slide *ngIf=\"user.role == 'client'\" class=\"background-slide-client\">\n      <ion-grid>\n        <ion-row class=\"container\">\n          <div class=\"container-select\">\n            <ion-label class=\"label gender\">GENERO</ion-label>\n            <ion-item class=\"select-ionItem\" style=\"width: 100%\">\n              <ion-select cancelText=\"Cancelar\" name=\"role\" value=\"client\" [(ngModel)]=\"user.gender\" okText=\"Aceptar\">\n                <ion-select-option value=\"Femenino\"><span>Femenino</span></ion-select-option>\n                <ion-select-option value=\"Masculino\"><span>Masculino</span></ion-select-option>\n                <ion-select-option value=\"Otros\"><span>Otros</span></ion-select-option>\n              </ion-select>\n            </ion-item>\n          </div>\n          <div class=\"container-inputs\">\n            <ion-label class=\"label\">NÚMERO DE CELULAR</ion-label>\n            <ion-item class=\"input-ionItem\">\n              <ion-input type=\"number\" maxlength=\"10\" [(ngModel)]=\"user.telephoneNumber\" name=\"number\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-row>\n        <ion-row class=\"container-btn\">\n          <ion-button class=\"btn\" (click)=\"signIn()\"> Registrarse </ion-button>\n        </ion-row>\n      </ion-grid>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n");

/***/ }),

/***/ "gx82":
/*!***************************************************!*\
  !*** ./src/app/shared/services/images.service.ts ***!
  \***************************************************/
/*! exports provided: ImagesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImagesService", function() { return ImagesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "Jgta");



let ImagesService = class ImagesService {
    constructor() { }
    uploadImage(storageFolder, image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const uploadTask = yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].storage().ref().child(`${storageFolder}/${image.name}`).put(image);
                return yield uploadTask.ref.getDownloadURL();
            }
            catch (error) {
                console.log(error);
                return false;
            }
        });
    }
    deleteImage(storageFolder, image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].storage().ref().child(`${storageFolder} / ${image.name}`).delete();
        });
    }
};
ImagesService.ctorParameters = () => [];
ImagesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ImagesService);



/***/ }),

/***/ "iJRm":
/*!********************************************************!*\
  !*** ./src/app/modules/home/pages/login/login.page.ts ***!
  \********************************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./login.page.html */ "u3wP");
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.page.scss */ "Kp8P");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/authentication/authentication.service */ "6CRC");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @capacitor/core */ "gcOT");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");









const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_7__["Plugins"];
let LoginPage = class LoginPage {
    constructor(alertsService, router, authenticationService, firestore) {
        this.alertsService = alertsService;
        this.router = router;
        this.authenticationService = authenticationService;
        this.firestore = firestore;
        this.inputType = 'password';
        this.iconName = 'eye';
        this.user = {
            email: '',
            password: ''
        };
    }
    ngOnInit() {
    }
    showHiddenPassword() {
        this.iconName = this.iconName === 'eye' ? 'eye-off' : 'eye';
        this.inputType = this.inputType === 'text' ? 'password' : 'text';
    }
    signIn(email, password) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.alertsService.presentLoading('Cargando...');
            if (this.isEmptyFields()) {
                this.alertsService.loading.dismiss();
                this.alertsService.presentAlertWithHeader('Lo sentimos!', 'Por favor ingrese todos los campos!');
            }
            else {
                this.authenticationService.signIn(email, password)
                    .then((userFirebase) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    if (userFirebase && !userFirebase.user.emailVerified) {
                        this.alertsService.presentAlert('Su cuenta no ha sido verificada. Por favor revise su correo.');
                        this.alertsService.loading.dismiss();
                        return;
                    }
                    else {
                        this.firestore.collection('/users').doc(userFirebase.user.uid).get().subscribe((result) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                            const userData = result.data();
                            if (!userData.isActive) {
                                yield this.authenticationService.logoutUser();
                                this.alertsService.loading.dismiss();
                                this.alertsService.presentAlert('Su cuenta está desactivada, contáctese con iGo por favor!');
                            }
                            else {
                                this.cleanFields();
                                this.alertsService.loading.dismiss();
                                this.router.navigate(['dashboard']);
                            }
                        }));
                    }
                }))
                    .catch(error => {
                    this.alertsService.loading.dismiss();
                    this.alertsService.presentAlert('Usuario y/o contraseña invalidos.');
                });
            }
        });
    }
    cleanFields() {
        this.user = {
            email: '',
            password: ''
        };
    }
    isEmptyFields() {
        if (this.user.email == '' || this.user.password == '')
            return true;
        else
            return false;
    }
    assingAddres() {
        const geocoder = new google.maps.Geocoder();
        Geolocation.watchPosition({}, (position, error) => {
            let pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            geocoder.geocode({ location: pos }, (results, status) => {
                if (status == "OK") {
                    if (results[0]) {
                        this.user.address = results[0].formatted_address;
                        console.log(this.user.address);
                    }
                }
            });
        });
    }
    goToRegister() {
        this.cleanFields();
        this.router.navigate(['home/login/register']);
    }
};
LoginPage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__["AngularFirestore"] }
];
LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], LoginPage);



/***/ }),

/***/ "u3wP":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/pages/login/login.page.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n<ion-content>\n    <h4 align=\"center\">Iniciar Sesión</h4>\n    <img class=\"img-signIn\" src=\"../../../../../assets/img/igoLogo.png\" />\n    <div class=\"container-inputs\">\n        <ion-label class=\"label\" position=\"stacked\">EMAIL</ion-label>\n        <ion-item>            \n            <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"user.email\"></ion-input>\n        </ion-item>        \n    </div>\n    <div class=\"container-inputs\">\n        <ion-label class=\"label\" position=\"stacked\">CONTRASEÑA</ion-label>\n        <ion-item>            \n            <ion-input [type]=\"inputType\" name=\"password\" [(ngModel)]=\"user.password\"></ion-input>\n            <ion-icon [name]=\"iconName\" item-right (click)=\"showHiddenPassword()\"></ion-icon>\n        </ion-item>        \n    </div>\n    <button class=\"my_buttton\" (click)=\"signIn(user.email, user.password)\">Iniciar Sesion</button>\n    \n    \n    <hr class=\"solid\">\n    <h5 align=\"center\">O prueba También con</h5>\n\n    <ion-row>\n        <ion-col>\n            <img class=\"img-fb\" src=\"../../../../../assets/img/fb.png\" (click)=\"authenticationService.facebookAuth()\">\n        </ion-col>\n        <ion-col>\n            <img class=\"img-fb\" src=\"../../../../../assets/img/google.png\"  (click)=\"authenticationService.googleAuth()\">\n        </ion-col>\n    </ion-row>\n\n    <div class=\"container-btn\">\n        No tienes una cuenta ?<button class=\"btn-registro\" (click)=\"goToRegister()\">Registrarse</button>\n    </div>\n\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=default~pages-login-login-module~pages-register-register-module.js.map