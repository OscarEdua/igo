(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-select-payment-modal-select-payment-modal-module"],{

/***/ "fCMU":
/*!************************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-payment-modal/select-payment-modal-routing.module.ts ***!
  \************************************************************************************************************/
/*! exports provided: SelectPaymentModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectPaymentModalPageRoutingModule", function() { return SelectPaymentModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_payment_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-payment-modal.page */ "Mc/L");




const routes = [
    {
        path: '',
        component: _select_payment_modal_page__WEBPACK_IMPORTED_MODULE_3__["SelectPaymentModalPage"]
    }
];
let SelectPaymentModalPageRoutingModule = class SelectPaymentModalPageRoutingModule {
};
SelectPaymentModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectPaymentModalPageRoutingModule);



/***/ }),

/***/ "zZI3":
/*!****************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-payment-modal/select-payment-modal.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: SelectPaymentModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectPaymentModalPageModule", function() { return SelectPaymentModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_payment_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-payment-modal-routing.module */ "fCMU");
/* harmony import */ var _select_payment_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-payment-modal.page */ "Mc/L");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");








let SelectPaymentModalPageModule = class SelectPaymentModalPageModule {
};
SelectPaymentModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_payment_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectPaymentModalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_select_payment_modal_page__WEBPACK_IMPORTED_MODULE_6__["SelectPaymentModalPage"]]
    })
], SelectPaymentModalPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-select-payment-modal-select-payment-modal-module.js.map