(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-start-travel-start-travel-module"],{

/***/ "/VTD":
/*!********************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/start-travel/start-travel-routing.module.ts ***!
  \********************************************************************************************/
/*! exports provided: StartTravelPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartTravelPageRoutingModule", function() { return StartTravelPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _start_travel_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./start-travel.page */ "Cic1");




const routes = [
    {
        path: '',
        component: _start_travel_page__WEBPACK_IMPORTED_MODULE_3__["StartTravelPage"]
    }
];
let StartTravelPageRoutingModule = class StartTravelPageRoutingModule {
};
StartTravelPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], StartTravelPageRoutingModule);



/***/ }),

/***/ "geE7":
/*!************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/start-travel/start-travel.module.ts ***!
  \************************************************************************************/
/*! exports provided: StartTravelPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartTravelPageModule", function() { return StartTravelPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _start_travel_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./start-travel-routing.module */ "/VTD");
/* harmony import */ var _start_travel_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./start-travel.page */ "Cic1");







let StartTravelPageModule = class StartTravelPageModule {
};
StartTravelPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _start_travel_routing_module__WEBPACK_IMPORTED_MODULE_5__["StartTravelPageRoutingModule"]
        ],
        declarations: [_start_travel_page__WEBPACK_IMPORTED_MODULE_6__["StartTravelPage"]]
    })
], StartTravelPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-start-travel-start-travel-module.js.map