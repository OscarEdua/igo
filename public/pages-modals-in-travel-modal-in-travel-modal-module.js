(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-in-travel-modal-in-travel-modal-module"],{

/***/ "9zrF":
/*!******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/in-travel-modal/in-travel-modal.module.ts ***!
  \******************************************************************************************/
/*! exports provided: InTravelModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InTravelModalPageModule", function() { return InTravelModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _in_travel_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./in-travel-modal-routing.module */ "Ak9D");
/* harmony import */ var _in_travel_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./in-travel-modal.page */ "kCj7");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");








let InTravelModalPageModule = class InTravelModalPageModule {
};
InTravelModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _in_travel_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["InTravelModalPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_in_travel_modal_page__WEBPACK_IMPORTED_MODULE_6__["InTravelModalPage"]]
    })
], InTravelModalPageModule);



/***/ }),

/***/ "Ak9D":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/in-travel-modal/in-travel-modal-routing.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: InTravelModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InTravelModalPageRoutingModule", function() { return InTravelModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _in_travel_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./in-travel-modal.page */ "kCj7");




const routes = [
    {
        path: '',
        component: _in_travel_modal_page__WEBPACK_IMPORTED_MODULE_3__["InTravelModalPage"]
    }
];
let InTravelModalPageRoutingModule = class InTravelModalPageRoutingModule {
};
InTravelModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], InTravelModalPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-modals-in-travel-modal-in-travel-modal-module.js.map