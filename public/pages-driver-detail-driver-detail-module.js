(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-driver-detail-driver-detail-module"],{

/***/ "2Dnn":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/driver-detail/driver-detail.page.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n/* Estilos del header */\nion-toolbar ion-row {\n  align-items: center;\n  display: flex;\n  justify-content: center;\n  width: 85%;\n}\nion-toolbar ion-row ion-title {\n  font-size: 20px;\n  font-weight: bold;\n  --color: #3E4958;\n  text-align: center;\n}\n/* Estilo para colocar un margen izquierdo a cualquier elemento */\n.margin-left {\n  margin-left: 5%;\n}\n/* Estilos de las tarjetas */\nion-card {\n  border-radius: 15px;\n  box-shadow: none;\n  margin-top: 10%;\n  width: 90%;\n  --background: rgba(0, 0, 0, 0);\n  border: none;\n}\nion-card ion-card-header ion-card-title {\n  color: #333333;\n  font-weight: bold;\n}\nion-card ion-card-header ion-card-subtitle {\n  color: #3E4958;\n  font-size: 13.5px;\n  font-weight: 500;\n  margin: 8px 0px;\n}\nion-card ion-card-header .container-starts ion-icon {\n  font-size: 20px;\n  margin-right: 9px;\n}\n/* Contenedor de todas las tarjetas de navegación */\n.container {\n  padding: 0px 25px;\n}\n.container ion-row .title {\n  font-weight: bold;\n  font-size: 19px;\n  color: #333333;\n}\n.container ion-row ion-col .subtitle {\n  color: #3E4958;\n  font-weight: bold;\n  font-size: 14px;\n}\n.container ion-row ion-col .img-license {\n  width: 100%;\n  height: 200px;\n  border-radius: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n}\n.btn {\n  width: 100%;\n  height: 60px;\n  --border-radius: 15px;\n  --box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  font-size: 17px;\n}\n/* Estilo del boton verde  */\n.green-btn {\n  --background: #008D36;\n  --background-activated: #008D36;\n  --color: white;\n  font-weight: bold;\n}\n/* Estilo del boton blanco */\n.white {\n  --background: white;\n  --background-activated:white;\n  --color:#4B545A;\n  font-weight: bold;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n}\n/* Estilo del boton blanco */\n.red {\n  --background: rgb(255, 0, 0);\n  --background-activated:rgb(255, 0, 0);\n  --color:#ffffff;\n  font-weight: bold;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n}\n/* Contenedor de imagen del usuario */\n.container-img {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.container-img .img {\n  width: 82px;\n  height: 80px;\n  border-radius: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n/* Color verde para algunos elementos */\n.green {\n  color: #008D36;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2RyaXZlci1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQUFoQix1QkFBQTtBQUVJO0VBQ0UsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxVQUFBO0FBQ047QUFDTTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFDUjtBQU1FLGlFQUFBO0FBQ0E7RUFDRSxlQUFBO0FBSEo7QUFNRSw0QkFBQTtBQUNBO0VBQ0UsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsOEJBQUE7RUFDQSxZQUFBO0FBSEo7QUFNTTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtBQUpSO0FBT007RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUFMUjtBQVNRO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBUFY7QUFhRSxtREFBQTtBQUNBO0VBQ0UsaUJBQUE7QUFWSjtBQWFNO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQVhSO0FBZVE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBYlY7QUFnQlE7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0FBZFY7QUFzQkU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0Esb0RBQUE7RUFDQSxlQUFBO0FBbkJKO0FBc0JFLDRCQUFBO0FBQ0E7RUFDRSxxQkFBQTtFQUNBLCtCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FBbkJKO0FBdUJFLDRCQUFBO0FBQ0E7RUFDSSxtQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsaURBQUE7QUFwQk47QUF1QkksNEJBQUE7QUFDRjtFQUNFLDRCQUFBO0VBQ0EscUNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxpREFBQTtBQXBCSjtBQXlCRSxxQ0FBQTtBQUNBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUF0Qko7QUF3Qkk7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQXRCTjtBQTBCRSx1Q0FBQTtBQUNBO0VBQ0UsY0FBQTtBQXZCSiIsImZpbGUiOiJkcml2ZXItZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIEVzdGlsb3MgZGVsIGhlYWRlciAqL1xuaW9uLXRvb2xiYXIge1xuICAgIGlvbi1yb3cge1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIHdpZHRoOiA4NSU7XG4gIFxuICAgICAgaW9uLXRpdGxlIHtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgLS1jb2xvcjogIzNFNDk1ODtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBcbiAgICAgIH1cbiAgXG4gICAgfVxuICB9XG4gIFxuICAvKiBFc3RpbG8gcGFyYSBjb2xvY2FyIHVuIG1hcmdlbiBpenF1aWVyZG8gYSBjdWFscXVpZXIgZWxlbWVudG8gKi9cbiAgLm1hcmdpbi1sZWZ0IHtcbiAgICBtYXJnaW4tbGVmdDogNSU7XG4gIH1cbiAgXG4gIC8qIEVzdGlsb3MgZGUgbGFzIHRhcmpldGFzICovXG4gIGlvbi1jYXJkIHtcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgLS1iYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDApO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgXG4gICAgaW9uLWNhcmQtaGVhZGVyIHtcbiAgICAgIGlvbi1jYXJkLXRpdGxlIHtcbiAgICAgICAgY29sb3I6ICMzMzMzMzM7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgfVxuICBcbiAgICAgIGlvbi1jYXJkLXN1YnRpdGxlIHtcbiAgICAgICAgY29sb3I6ICMzRTQ5NTg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTMuNXB4O1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICBtYXJnaW46IDhweCAwcHg7XG4gICAgICB9XG4gIFxuICAgICAgLmNvbnRhaW5lci1zdGFydHMge1xuICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgIG1hcmdpbi1yaWdodDogOXB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG4gIFxuICAvKiBDb250ZW5lZG9yIGRlIHRvZGFzIGxhcyB0YXJqZXRhcyBkZSBuYXZlZ2FjacOzbiAqL1xuICAuY29udGFpbmVyIHtcbiAgICBwYWRkaW5nOiAwcHggMjVweDtcbiAgXG4gICAgaW9uLXJvdyB7XG4gICAgICAudGl0bGUge1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgZm9udC1zaXplOiAxOXB4O1xuICAgICAgICBjb2xvcjogIzMzMzMzMztcbiAgICAgIH1cbiAgXG4gICAgICBpb24tY29sIHtcbiAgICAgICAgLnN1YnRpdGxlIHtcbiAgICAgICAgICBjb2xvcjogIzNFNDk1ODtcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIH1cbiAgXG4gICAgICAgIC5pbWctbGljZW5zZSB7XG4gICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIFxuICB9XG4gIFxuICAvL0VzdGlsb3MgZGUgbG9zIGJvdG9uZXMgYm90b24gXG4gIC5idG4ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNjBweDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgLS1ib3gtc2hhZG93OiAwcHggNHB4IDIwcHggcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjI1KTtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gIH1cbiAgXG4gIC8qIEVzdGlsbyBkZWwgYm90b24gdmVyZGUgICovXG4gIC5ncmVlbi1idG4ge1xuICAgIC0tYmFja2dyb3VuZDogIzAwOEQzNjtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjMDA4RDM2O1xuICAgIC0tY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB9XG4gIFxuICBcbiAgLyogRXN0aWxvIGRlbCBib3RvbiBibGFuY28gKi9cbiAgLndoaXRle1xuICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6d2hpdGU7XG4gICAgICAtLWNvbG9yOiM0QjU0NUE7XG4gICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgIC0tYm94LXNoYWRvdzogMHB4IDJweCA4cHggcmdiYSgxNiwgMTA1LCAyMjcsIDAuMyk7XG4gICAgfVxuICBcbiAgICAvKiBFc3RpbG8gZGVsIGJvdG9uIGJsYW5jbyAqL1xuICAucmVke1xuICAgIC0tYmFja2dyb3VuZDogcmdiKDI1NSwgMCwgMCk7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpyZ2IoMjU1LCAwLCAwKTtcbiAgICAtLWNvbG9yOiNmZmZmZmY7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgLS1ib3gtc2hhZG93OiAwcHggMnB4IDhweCByZ2JhKDE2LCAxMDUsIDIyNywgMC4zKTtcbiAgfVxuICBcbiAgXG4gIFxuICAvKiBDb250ZW5lZG9yIGRlIGltYWdlbiBkZWwgdXN1YXJpbyAqL1xuICAuY29udGFpbmVyLWltZyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBcbiAgICAuaW1nIHtcbiAgICAgIHdpZHRoOiA4MnB4O1xuICAgICAgaGVpZ2h0OiA4MHB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIH1cbiAgfVxuICBcbiAgLyogQ29sb3IgdmVyZGUgcGFyYSBhbGd1bm9zIGVsZW1lbnRvcyAqL1xuICAuZ3JlZW4ge1xuICAgIGNvbG9yOiAjMDA4RDM2O1xuICB9XG4gIFxuICBcbiJdfQ== */");

/***/ }),

/***/ "7qgo":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/driver-detail/driver-detail-routing.module.ts ***!
  \***************************************************************************************/
/*! exports provided: DriverDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DriverDetailPageRoutingModule", function() { return DriverDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _driver_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./driver-detail.page */ "ZM7V");




const routes = [
    {
        path: '',
        component: _driver_detail_page__WEBPACK_IMPORTED_MODULE_3__["DriverDetailPage"]
    }
];
let DriverDetailPageRoutingModule = class DriverDetailPageRoutingModule {
};
DriverDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DriverDetailPageRoutingModule);



/***/ }),

/***/ "ZM7V":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/driver-detail/driver-detail.page.ts ***!
  \*****************************************************************************/
/*! exports provided: DriverDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DriverDetailPage", function() { return DriverDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_driver_detail_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./driver-detail.page.html */ "zQGe");
/* harmony import */ var _driver_detail_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./driver-detail.page.scss */ "2Dnn");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/modules/home/services/users.service */ "6JOU");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");







let DriverDetailPage = class DriverDetailPage {
    constructor(usersService, activatedRoute, alertService) {
        this.usersService = usersService;
        this.activatedRoute = activatedRoute;
        this.alertService = alertService;
        this.driverId = this.activatedRoute.snapshot.paramMap.get('driverId');
    }
    ngOnInit() {
        this.user = this.usersService.getUserById(this.driverId);
        this.user.subscribe(data => {
            this.uUser = data;
        });
    }
    updateTypeVehicleByUid() {
        this.uUser.vehicle.type = this.typeDriver;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Actualización en el conductor Satisfactoria');
    }
    unsuscribeUser() {
        this.uUser.isActive = false;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Usuario Desactivado');
    }
    suscribeUser() {
        this.uUser.isActive = true;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Usuario Activado');
    }
    checkTypeVehicle(event) {
        this.typeDriver = event.detail.value;
    }
};
DriverDetailPage.ctorParameters = () => [
    { type: src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_4__["UsersService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] }
];
DriverDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-driver-detail',
        template: _raw_loader_driver_detail_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_driver_detail_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], DriverDetailPage);



/***/ }),

/***/ "yPFg":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/driver-detail/driver-detail.module.ts ***!
  \*******************************************************************************/
/*! exports provided: DriverDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DriverDetailPageModule", function() { return DriverDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _driver_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./driver-detail-routing.module */ "7qgo");
/* harmony import */ var _driver_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./driver-detail.page */ "ZM7V");







let DriverDetailPageModule = class DriverDetailPageModule {
};
DriverDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _driver_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["DriverDetailPageRoutingModule"]
        ],
        declarations: [_driver_detail_page__WEBPACK_IMPORTED_MODULE_6__["DriverDetailPage"]]
    })
], DriverDetailPageModule);



/***/ }),

/***/ "zQGe":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/driver-detail/driver-detail.page.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons class=\"margin-left\" slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-row>\n      <ion-title>Vehículo</ion-title>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n \n  <div class=\"container\">\n\n    <ion-card>\n      <ion-row>\n        <ion-col size=\"4\" class=\"container-img\">\n          <div *ngIf=\"(user | async)?.imagen == ''\" style=\"background-image: url('../../../../../assets/img/user.png')\" class=\"img\">\n          </div>\n          <div *ngIf=\"(user | async)?.imagen != ''\" class=\"img\">\n            <img src=\"{{ ((user | async)?.imagen)}}\">\n          </div>\n        </ion-col>\n        <ion-col size=\"8\">\n          <ion-card-header>\n            <ion-card-title>{{ (user | async)?.name }}</ion-card-title>\n            <ion-card-subtitle>{{ (user | async)?.email }}</ion-card-subtitle>\n            \n            <div class=\"container-starts\">\n              <ion-icon class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon  class=\"green\" name=\"star\"></ion-icon>\n              <ion-icon name=\"star-outline\"></ion-icon>\n            </div>\n          </ion-card-header>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  \n    <ion-row>\n      <p class=\"title\">Descripción del vehículo</p>\n    </ion-row>  \n    \n  \n    <ion-row>\n      <ion-col size=\"6\">\n        <p class=\"subtitle\">PLACA</p>\n        <div class=\"img-license\" >\n          <img src=\"{{ ((user | async)?.vehicle.plateImagen)}}\">\n        </div>\n      </ion-col>\n      <ion-col size=\"6\">\n        <p class=\"subtitle\">LICENCIA</p>\n        <div class=\"img-license\" >\n          <img src=\"{{ ((user | async)?.vehicle.licenseImagen)}}\">          \n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <p class=\"subtitle\">MODELO DEL VEHÍCULO: {{ ((user | async)?.vehicle.brand) | uppercase }}</p>\n        <div class=\"img-license\">\n          <img src=\"{{ ((user | async)?.vehicle.imagen)}}\">  \n        </div>\n      </ion-col>\n      <ion-col size=\"12\">\n        <p class=\"subtitle\">COLOR: {{ (user | async)?.vehicle.color | uppercase }}</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button class=\"btn green-btn\" [routerLink]=\"['/stadriveradmin/user.uid']\" >\n         Historial\n        </ion-button>\n      </ion-col>\n  \n      <ion-col size=\"6\">\n        <ion-button class=\"btn white\" [routerLink]=\"['../../reviews']\">\n          Reseñas\n         </ion-button>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-note *ngIf=\"(user | async)?.vehicle.type === 'vip'\" color=\"success\">Vehiculo VIP</ion-note>\n        <ion-note *ngIf=\"(user | async)?.vehicle.type === 'standard'\" color=\"warning\">Vehiculo Standard</ion-note>\n        <ion-select placeholder=\"Tipo de Vehículo\" (ionChange)=\"checkTypeVehicle($event)\" okText=\"Aceptar\"\n                cancelText=\"Cancelar\">\n                <ion-select-option  value=\"vip\" name=\"typeDriver\">VIP</ion-select-option>\n                <ion-select-option  value=\"standard\" name=\"typeDriver\">Standard</ion-select-option>\n              </ion-select>\n      </ion-col>\n  \n    </ion-row>\n    <ion-row >\n      <ion-col size=\"6\">\n        <ion-button class=\"btn green-btn\" (click)=\"updateTypeVehicleByUid()\">\n         Guardar Cambios\n        </ion-button>\n      </ion-col>\n  \n      <ion-col size=\"6\" *ngIf=\"(user | async)?.isActive != false\">\n        <ion-button class=\"btn red\" (click)=\"unsuscribeUser()\">\n          Desactivar Usuario\n         </ion-button>\n      </ion-col>\n      <ion-col size=\"6\" *ngIf=\"(user | async)?.isActive != true\">\n        <ion-button class=\"btn white\" (click)=\"suscribeUser()\">\n          Activar Usuario\n         </ion-button>\n      </ion-col>\n  \n    </ion-row>\n\n  </div>\n\n  \n\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=pages-driver-detail-driver-detail-module.js.map