(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-dashboard-dashboard-module~pages-modals-start-travel-start-travel-module"],{

/***/ "CGdX":
/*!************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/start-travel/start-travel.page.scss ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".btn {\n  width: 100%;\n  height: 60px;\n  --border-radius:15px;\n  --box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  font-size: 17px;\n}\n\n/* Estilo del boton verde del modal */\n\n.green {\n  --background: #008D36;\n  --background-activated:#008D36;\n  color: white;\n  font-weight: bold;\n}\n\n/* Estilo del boton blanco del modal */\n\n.white {\n  --background: white;\n  --background-activated:white;\n  color: #4B545A;\n  font-weight: bold;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n}\n\n/* Se coloca paddings para ubicar los elementos de mejor manera */\n\nion-content {\n  --padding-start:5%;\n  --padding-end:5%;\n}\n\n/* Estilos de los parrafos del componenete */\n\np {\n  color: #3E4958;\n  font-size: 18px;\n  font-weight: bold;\n}\n\n/* Estilos de la linea que cierra el modal */\n\n.close-modal-line {\n  width: 30px;\n  height: 5px;\n  background-color: #D5DDE0;\n  border-radius: 50px;\n}\n\n/* Estilos del contenedor de la linea que cierra el modal */\n\n.container-close-modal {\n  padding: 5%;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3N0YXJ0LXRyYXZlbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0VBQ0Esb0RBQUE7RUFDQSxlQUFBO0FBQUY7O0FBR0EscUNBQUE7O0FBQ0E7RUFDRSxxQkFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBQUY7O0FBR0Esc0NBQUE7O0FBQ0E7RUFDRSxtQkFBQTtFQUNBLDRCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsaURBQUE7QUFBRjs7QUFHQSxpRUFBQTs7QUFDQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUFBRjs7QUFHQSw0Q0FBQTs7QUFDQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFBRjs7QUFHQSw0Q0FBQTs7QUFDQTtFQUNFLFdBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtBQUFGOztBQUdBLDJEQUFBOztBQUNBO0VBQ0UsV0FBQTtFQUNBLHVCQUFBO0FBQUYiLCJmaWxlIjoic3RhcnQtdHJhdmVsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vRXN0aWxvcyBkZSBsb3MgYm90b25lcyBib3RvbiBcbi5idG4ge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA2MHB4O1xuICAtLWJvcmRlci1yYWRpdXM6MTVweDtcbiAgLS1ib3gtc2hhZG93OiAwcHggNHB4IDIwcHggcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjI1KTtcbiAgZm9udC1zaXplOiAxN3B4O1xufVxuXG4vKiBFc3RpbG8gZGVsIGJvdG9uIHZlcmRlIGRlbCBtb2RhbCAqL1xuLmdyZWVuIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMDA4RDM2O1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiMwMDhEMzY7XG4gIGNvbG9yOndoaXRlO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLyogRXN0aWxvIGRlbCBib3RvbiBibGFuY28gZGVsIG1vZGFsICovXG4ud2hpdGUge1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOndoaXRlO1xuICBjb2xvcjojNEI1NDVBO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgLS1ib3gtc2hhZG93OiAwcHggMnB4IDhweCByZ2JhKDE2LCAxMDUsIDIyNywgMC4zKTtcbn1cblxuLyogU2UgY29sb2NhIHBhZGRpbmdzIHBhcmEgdWJpY2FyIGxvcyBlbGVtZW50b3MgZGUgbWVqb3IgbWFuZXJhICovXG5pb24tY29udGVudCB7XG4gIC0tcGFkZGluZy1zdGFydDo1JTtcbiAgLS1wYWRkaW5nLWVuZDo1JTtcbn1cblxuLyogRXN0aWxvcyBkZSBsb3MgcGFycmFmb3MgZGVsIGNvbXBvbmVuZXRlICovXG5wIHtcbiAgY29sb3I6ICMzRTQ5NTg7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi8qIEVzdGlsb3MgZGUgbGEgbGluZWEgcXVlIGNpZXJyYSBlbCBtb2RhbCAqL1xuLmNsb3NlLW1vZGFsLWxpbmUge1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNENURERTAgO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xufVxuXG4vKiBFc3RpbG9zIGRlbCBjb250ZW5lZG9yIGRlIGxhIGxpbmVhIHF1ZSBjaWVycmEgZWwgbW9kYWwgKi9cbi5jb250YWluZXItY2xvc2UtbW9kYWwge1xuICBwYWRkaW5nOiA1JTtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4iXX0= */");

/***/ }),

/***/ "Cic1":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/start-travel/start-travel.page.ts ***!
  \**********************************************************************************/
/*! exports provided: StartTravelPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartTravelPage", function() { return StartTravelPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_start_travel_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./start-travel.page.html */ "QDiV");
/* harmony import */ var _start_travel_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./start-travel.page.scss */ "CGdX");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _select_vehicle_type_select_vehicle_type_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../select-vehicle-type/select-vehicle-type.page */ "cHxJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");







let StartTravelPage = class StartTravelPage {
    constructor(alertsService, router) {
        this.alertsService = alertsService;
        this.router = router;
    }
    ngOnInit() {
    }
    selectVehicleType() {
        this.alertsService.presentModalWithoutTravelData(_select_vehicle_type_select_vehicle_type_page__WEBPACK_IMPORTED_MODULE_5__["SelectVehicleTypePage"], 'select-type-vehicle-modal');
    }
    redirectTo(pageName) {
        this.alertsService.closeModal();
        this.router.navigate(['dashboard/' + pageName]);
    }
};
StartTravelPage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] }
];
StartTravelPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-start-travel',
        template: _raw_loader_start_travel_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_start_travel_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], StartTravelPage);



/***/ }),

/***/ "QDiV":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/start-travel/start-travel.page.html ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content scrollY=\"false\">\n  <ion-row class=\"container-close-modal\">\n    <div class=\"close-modal-line\" (click)=\"alertsService.closeModal()\"> \n    </div>\n  </ion-row>\n  <ion-row>\n    <ion-col size=\"12\">\n      <p>Comenzar Viaje</p>\n    </ion-col>\n    <ion-col size=\"12\">\n      <ion-button class=\"btn green ion-text-capitalize\" (click)=\"redirectTo('select-vehicle/false')\">\n        Agenda tu viaje\n      </ion-button>\n    </ion-col>\n    <ion-col size=\"12\">\n      <ion-button class=\"btn white ion-text-capitalize\" (click)=\"redirectTo('select-vehicle/true')\">\n        Viaja Ahora\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</ion-content>\n\n");

/***/ }),

/***/ "VaCg":
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/select-vehicle-type/select-vehicle-type.page.html ***!
  \****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<app-close-modal-button (click)=\"alertsService.closeModal()\"></app-close-modal-button>\n<ion-content class=\"ion-padding-start ion-padding-end\">\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <p class=\"title\">Selecciona un tipo de vehículo</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <app-vehicle-type-button title=\"Standard\" (redirectTo)=\"redirectTo('select-stop', 'Standard')\"></app-vehicle-type-button>\n      </ion-col>\n      <ion-col size=\"6\">\n        <app-vehicle-type-button title=\"VIP\" (redirectTo)=\"redirectTo('select-stop', 'VIP')\"></app-vehicle-type-button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"8\" class=\"label-container\">\n        <ion-label>Número de Ocupantes</ion-label>\n      </ion-col>\n      <ion-col size=\"4\">\n        <ion-input type=\"number\" [(ngModel)]=\"travel.numberPassengers\"></ion-input>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "b5hr":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-vehicle-type/select-vehicle-type.page.scss ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\n  margin-top: 0px;\n}\n\nion-input {\n  background: #F2F2F2;\n  box-shadow: inset 0px 4px 15px rgba(0, 0, 0, 0.14);\n  border-radius: 8px;\n  width: 72px;\n  height: 40px;\n  text-align: center;\n}\n\n.label-container {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\nion-content {\n  --padding-top: 0px;\n}\n\n.number-passengers-row {\n  border: 1px solid row;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3NlbGVjdC12ZWhpY2xlLXR5cGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsZUFBQTtBQUNEOztBQUVBO0VBQ0MsbUJBQUE7RUFDQSxrREFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQUNEOztBQUVBO0VBQ0MsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFDRDs7QUFFQTtFQUNDLGtCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxxQkFBQTtBQUNEIiwiZmlsZSI6InNlbGVjdC12ZWhpY2xlLXR5cGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsicCB7XG5cdG1hcmdpbi10b3A6IDBweDtcbn1cblxuaW9uLWlucHV0IHtcblx0YmFja2dyb3VuZDogI0YyRjJGMjtcblx0Ym94LXNoYWRvdzogaW5zZXQgMHB4IDRweCAxNXB4IHJnYmEoMCwgMCwgMCwgMC4xNCk7XG5cdGJvcmRlci1yYWRpdXM6IDhweDtcblx0d2lkdGg6IDcycHg7XG5cdGhlaWdodDogNDBweDtcblx0dGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubGFiZWwtY29udGFpbmVyIHtcblx0ZGlzcGxheTogZmxleDtcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbmlvbi1jb250ZW50IHtcblx0LS1wYWRkaW5nLXRvcDogMHB4O1xufVxuXG4ubnVtYmVyLXBhc3NlbmdlcnMtcm93IHtcblx0Ym9yZGVyOiAxcHggc29saWQgcm93O1xufVxuIl19 */");

/***/ }),

/***/ "cHxJ":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-vehicle-type/select-vehicle-type.page.ts ***!
  \************************************************************************************************/
/*! exports provided: SelectVehicleTypePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectVehicleTypePage", function() { return SelectVehicleTypePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_vehicle_type_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-vehicle-type.page.html */ "VaCg");
/* harmony import */ var _select_vehicle_type_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-vehicle-type.page.scss */ "b5hr");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/storage.service */ "fbMX");
/* harmony import */ var src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modules/home/services/users.service */ "6JOU");








let SelectVehicleTypePage = class SelectVehicleTypePage {
    constructor(alertsService, router, storageService, usersService) {
        this.alertsService = alertsService;
        this.router = router;
        this.storageService = storageService;
        this.usersService = usersService;
        this.travel = {
            numberTravel: 1,
            numberPassengers: null
        };
    }
    ngOnInit() {
        this.usersService.readDrivers().subscribe(drivers => this.drivers = drivers);
    }
    redirectTo(pageName, vehicleType) {
        this.travel.vehicleType = vehicleType;
        const sTravel = JSON.stringify(this.travel);
        this.storageService.set('travel', sTravel);
        if (this.isNumberPassengersEntered()) {
            if (this.isNumberPassengersEnteredNegative())
                this.alertsService.presentAlertWithHeader('Tipo de vehiculo', 'Por favor ingresa una cantidad de pasajeros valida.');
            else {
                const filterDrivers = this.drivers.filter(driver => driver.vehicle.availableSeats >= this.travel.numberPassengers);
                if (filterDrivers.length > 0) {
                    this.alertsService.closeModal();
                    if (!this.isTravelNow) {
                        this.router.navigate(['dashboard/schedule-trip']);
                    }
                    else {
                        this.router.navigate(['dashboard/' + pageName]);
                    }
                }
                else
                    this.alertsService.presentAlertWithHeader('Sin vehiculos', 'Lo sentimos, no tenemos vehiculos con ' + this.travel.numberPassengers + ' asientos disponibles, por favor intente con otra cantidad.');
            }
        }
        else {
            this.alertsService.presentAlertWithHeader('Tipo de vehiculo', 'Por favor ingresa la cantidad de pasajeros.');
        }
    }
    isNumberPassengersEntered() {
        if (this.travel.numberPassengers == 0)
            return false;
        else
            return true;
    }
    isNumberPassengersEnteredNegative() {
        if (this.travel.numberPassengers > 0)
            return false;
        else
            return true;
    }
};
SelectVehicleTypePage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
    { type: src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_7__["UsersService"] }
];
SelectVehicleTypePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-vehicle-type',
        template: _raw_loader_select_vehicle_type_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_vehicle_type_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectVehicleTypePage);



/***/ })

}]);
//# sourceMappingURL=default~pages-dashboard-dashboard-module~pages-modals-start-travel-start-travel-module.js.map