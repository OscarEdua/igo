(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-select-destination-select-destination-module"],{

/***/ "5IMW":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/select-destination/select-destination.page.html ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content fullscreen>\n  <app-header slot=\"fixed\"></app-header>\n  <div id=\"map\"></div>\n  <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>\n</ion-content>\n");

/***/ }),

/***/ "7GqQ":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-destination/select-destination.page.scss ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#map {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NlbGVjdC1kZXN0aW5hdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxZQUFBO0FBQ0QiLCJmaWxlIjoic2VsZWN0LWRlc3RpbmF0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNtYXAge1xuXHRoZWlnaHQ6IDEwMCU7XG59XG4iXX0= */");

/***/ }),

/***/ "FC4g":
/*!*************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-destination/select-destination-routing.module.ts ***!
  \*************************************************************************************************/
/*! exports provided: SelectDestinationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectDestinationPageRoutingModule", function() { return SelectDestinationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_destination_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-destination.page */ "PVMD");




const routes = [
    {
        path: '',
        component: _select_destination_page__WEBPACK_IMPORTED_MODULE_3__["SelectDestinationPage"]
    }
];
let SelectDestinationPageRoutingModule = class SelectDestinationPageRoutingModule {
};
SelectDestinationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectDestinationPageRoutingModule);



/***/ }),

/***/ "Hu7B":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-destination/select-destination.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: SelectDestinationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectDestinationPageModule", function() { return SelectDestinationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_destination_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-destination-routing.module */ "FC4g");
/* harmony import */ var _select_destination_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-destination.page */ "PVMD");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let SelectDestinationPageModule = class SelectDestinationPageModule {
};
SelectDestinationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_destination_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectDestinationPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_select_destination_page__WEBPACK_IMPORTED_MODULE_6__["SelectDestinationPage"]]
    })
], SelectDestinationPageModule);



/***/ }),

/***/ "PVMD":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-destination/select-destination.page.ts ***!
  \***************************************************************************************/
/*! exports provided: SelectDestinationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectDestinationPage", function() { return SelectDestinationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_destination_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-destination.page.html */ "5IMW");
/* harmony import */ var _select_destination_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-destination.page.scss */ "7GqQ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _modals_select_destination_modal_select_destination_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modals/select-destination-modal/select-destination-modal.page */ "mXHT");
/* harmony import */ var src_app_shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/maps.service */ "Y0mc");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @capacitor/core */ "gcOT");
/* harmony import */ var _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/admin/stops.service */ "/+Y6");









const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_7__["Plugins"];
let SelectDestinationPage = class SelectDestinationPage {
    constructor(alertsService, mapsService, stopsService) {
        this.alertsService = alertsService;
        this.mapsService = mapsService;
        this.stopsService = stopsService;
        this.markers = [];
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.markerStops = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
    }
    ngOnInit() {
    }
    openModal() {
        this.alertsService.presentModalWithoutTravelData(_modals_select_destination_modal_select_destination_modal_page__WEBPACK_IMPORTED_MODULE_5__["SelectDestinationModalPage"], 'select-stop-modal');
    }
    ionViewWillEnter() {
        this.loadDataPageDashboard();
        this.stops = this.stopsService.getStop();
        this.stops.subscribe((res) => {
            this.stopsMark = res;
            this.generateMarks();
        });
    }
    generateMarks() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.deleteMarkers();
            this.stopsMark.map((res) => {
                this.markerStops.position.lat = res.lat;
                this.markerStops.position.lng = res.lng;
                this.markerStops.title = res.name;
                this.addMarker(this.markerStops, this.markerStops.title, 'stops');
            });
        });
    }
    loadDataPageDashboard() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.mapsService.loadmap('map');
            this.openModal();
            this.generateMarksMyLocation();
        });
    }
    generateMarksMyLocation() {
        Geolocation.getCurrentPosition().then((res) => {
            this.marker.position.lat = res.coords.latitude;
            this.marker.position.lng = res.coords.longitude;
            this.marker.title = 'Mi Ubicación';
            this.addMarker(this.marker, this.marker.title, 'myLocation');
        });
    }
    addMarker(marker, name, typeMarket) {
        let marke;
        let icon;
        if (typeMarket == 'myLocation') {
            icon = {
                url: '../../assets/img/ic_loc.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(60, 60),
            };
            /*     const image = '../../assets/img/Pineapplemenu.png'; */
        }
        else if (typeMarket == 'stops') {
            icon = {
                url: '../../assets/img/flag.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(25, 25),
            };
        }
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            //  draggable: true,
            // animation: google.maps.Animation.DROP,
            map: this.mapsService.map,
            icon: icon,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                    console.log('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
        const infoWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
                        if (infoWindow !== null) {
                        }
                        infoWindow.open(this.mapsService.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
    }
    deleteMarkers() {
        this.setMapOnAll(null);
        this.markers = [];
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
    ionViewWillLeave() {
        this.deleteMarkers();
    }
};
SelectDestinationPage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: src_app_shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__["MapsService"] },
    { type: _services_admin_stops_service__WEBPACK_IMPORTED_MODULE_8__["StopsService"] }
];
SelectDestinationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-destination',
        template: _raw_loader_select_destination_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_destination_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectDestinationPage);



/***/ })

}]);
//# sourceMappingURL=pages-select-destination-select-destination-module.js.map