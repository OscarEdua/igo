(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-profile-profile-module"],{

/***/ "/2MA":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/profile/profile.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"toolbar-header\">\n    <ion-buttons class=\"margin-left\" slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons>\n    <ion-row>\n      <ion-title>Perfil</ion-title>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content *ngIf=\"user\">\n  <ion-row class=\"container-img container-title\">\n    <p>Actualiza tu perfil</p>\n    <ion-item lines=\"none\">\n      <ion-input\n        slot=\"end\"\n        type=\"file\"\n        accept=\"image/*\"\n        id=\"file-input\"\n        (change)=\"previewImage($event)\"\n      ></ion-input>\n      <div class=\"img\" style=\"background-image: url({{ user.imagen }});\">\n        <ion-icon name=\"pencil-outline\"></ion-icon>\n      </div>\n    </ion-item>\n  </ion-row>\n  <ion-row class=\"container-all-inputs center-inputs\">\n    <div class=\"container-inputs\">\n      <ion-label class=\"label\">NOMBRE* </ion-label>\n      <ion-item class=\"input-ionItem\">\n        <ion-input name=\"name\" type=\"text\" [(ngModel)]=\"user.name\"></ion-input>\n      </ion-item>\n    </div>\n    <div class=\"container-inputs\">\n      <ion-label class=\"label\">EMAIL*</ion-label>\n      <ion-item class=\"input-ionItem\">\n        <ion-input\n          name=\"email\"\n          type=\"email\"\n          disabled=\"true\"\n          [(ngModel)]=\"user.email\"\n        ></ion-input>\n      </ion-item>\n    </div>\n\n    <div class=\"container-inputs\">\n      <ion-label class=\"label\">DIRECCIÓN*</ion-label>\n      <ion-item class=\"input-ionItem\">\n        <ion-input\n          type=\"text\"\n          [(ngModel)]=\"user.address\"\n          placeholder=\"Se obtiene desde el GPS\"\n          disabled=\"true\"\n          name=\"address\"\n          maxlenght=\"10\"\n        ></ion-input>\n      </ion-item>\n    </div>\n\n    <div class=\"container-select\">\n      <ion-label class=\"label gender\">GENERO</ion-label>\n      <ion-item class=\"select-ionItem\" style=\"width: 100%\">\n        <ion-select\n          [(ngModel)]=\"user.gender\"\n          cancelText=\"Cancelar\"\n          name=\"role\"\n          value=\"client\"\n          okText=\"Aceptar\"\n        >\n          <ion-select-option value=\"Femenino\"\n            ><span>Femenino</span></ion-select-option\n          >\n          <ion-select-option value=\"Masculino\"\n            ><span>Masculino</span></ion-select-option\n          >\n          <ion-select-option value=\"Otros\"\n            ><span>Otros</span></ion-select-option\n          >\n        </ion-select>\n      </ion-item>\n    </div>\n    <div class=\"container-inputs\">\n      <ion-label class=\"label\">NÚMERO DE CELULAR</ion-label>\n      <ion-item class=\"input-ionItem\">\n        <ion-input\n          [(ngModel)]=\"user.telephoneNumber\"\n          type=\"number\"\n          maxlength=\"10\"\n          name=\"number\"\n        ></ion-input>\n      </ion-item>\n    </div>\n  </ion-row>\n\n  <ion-row class=\"container-btn-update\">\n    <ion-button class=\"btn btn-green\" (click)=\"update()\">\n      Actualizar\n    </ion-button>\n  </ion-row>\n</ion-content>\n");

/***/ }),

/***/ "IkQH":
/*!***************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/profile/profile-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: ProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageRoutingModule", function() { return ProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile.page */ "r26J");




const routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_3__["ProfilePage"]
    }
];
let ProfilePageRoutingModule = class ProfilePageRoutingModule {
};
ProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProfilePageRoutingModule);



/***/ }),

/***/ "OwGE":
/*!*******************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/profile/profile.module.ts ***!
  \*******************************************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile-routing.module */ "IkQH");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile.page */ "r26J");







let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProfilePageRoutingModule"]
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
    })
], ProfilePageModule);



/***/ }),

/***/ "gx82":
/*!***************************************************!*\
  !*** ./src/app/shared/services/images.service.ts ***!
  \***************************************************/
/*! exports provided: ImagesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImagesService", function() { return ImagesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "Jgta");



let ImagesService = class ImagesService {
    constructor() { }
    uploadImage(storageFolder, image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const uploadTask = yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].storage().ref().child(`${storageFolder}/${image.name}`).put(image);
                return yield uploadTask.ref.getDownloadURL();
            }
            catch (error) {
                console.log(error);
                return false;
            }
        });
    }
    deleteImage(storageFolder, image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].storage().ref().child(`${storageFolder} / ${image.name}`).delete();
        });
    }
};
ImagesService.ctorParameters = () => [];
ImagesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ImagesService);



/***/ }),

/***/ "r26J":
/*!*****************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/profile/profile.page.ts ***!
  \*****************************************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_profile_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./profile.page.html */ "/2MA");
/* harmony import */ var _profile_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile.page.scss */ "s4UW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/authentication/authentication.service */ "6CRC");
/* harmony import */ var src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/modules/home/services/users.service */ "6JOU");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var src_app_shared_services_images_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/services/images.service */ "gx82");








let ProfilePage = class ProfilePage {
    constructor(authenticationService, usersService, imagesService, alertsService) {
        this.authenticationService = authenticationService;
        this.usersService = usersService;
        this.imagesService = imagesService;
        this.alertsService = alertsService;
        this.userId = authenticationService.getCurrentUserId();
        this.usersService.getUserById(this.userId).subscribe(user => {
            this.user = user;
        });
    }
    ngOnInit() {
    }
    update() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.alertsService.presentLoading('Actualizando...');
            if (!this.thereAreEmptyFields()) {
                this.usersService.update(this.user)
                    .then((result) => {
                    this.alertsService.loading.dismiss();
                    this.alertsService.presentAlertWithHeader('Perfil', 'Datos actualizados correctamente!');
                })
                    .catch((error) => {
                    this.alertsService.loading.dismiss();
                    console.log(error);
                    this.alertsService.presentAlertWithHeader('Perfil', 'Lo sentimos, por favor vuelva a intentarlo!');
                });
            }
            else {
                this.alertsService.loading.dismiss();
                this.alertsService.presentAlertWithHeader('Perfil', 'Por favor complete los campos requeridos!');
            }
        });
    }
    previewImage(event) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (event) => {
                this.user.imagen = event.target.result;
                console.log(this.user.imagen);
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    }
    thereAreEmptyFields() {
        if (this.user.name == '')
            return true;
        else
            return false;
    }
};
ProfilePage.ctorParameters = () => [
    { type: src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
    { type: src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UsersService"] },
    { type: src_app_shared_services_images_service__WEBPACK_IMPORTED_MODULE_7__["ImagesService"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] }
];
ProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-profile',
        template: _raw_loader_profile_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_profile_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ProfilePage);



/***/ }),

/***/ "s4UW":
/*!*******************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/profile/profile.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Contenedor de imagen del usuario */\n.container-img {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  margin-top: 10%;\n  margin-right: 5%;\n  margin-left: 8%;\n  /* Estilo de input de subir imagen  */\n}\n.container-img .img {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 80px;\n  height: 80px;\n  border-radius: 100%;\n  border: 2px solid rgba(0, 0, 0, 0.199);\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n.container-img .img ion-icon {\n  font-size: 25px;\n  color: rgba(0, 0, 0, 0.5);\n}\n.container-img #file-input {\n  opacity: 0;\n  position: absolute;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  z-index: 999;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHFDQUFBO0FBRUE7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFvQkEscUNBQUE7QUFuQko7QUFDSTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHNDQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FBQ047QUFDTTtFQUNFLGVBQUE7RUFDQSx5QkFBQTtBQUNSO0FBSUk7RUFDRSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtBQUZOIiwiZmlsZSI6InByb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogQ29udGVuZWRvciBkZSBpbWFnZW4gZGVsIHVzdWFyaW8gKi9cblxuLmNvbnRhaW5lci1pbWcge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xuICAgIG1hcmdpbi1yaWdodDogNSU7XG4gICAgbWFyZ2luLWxlZnQ6IDglO1xuICBcbiAgICAuaW1nIHtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICB3aWR0aDogODBweDtcbiAgICAgIGhlaWdodDogODBweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gICAgICBib3JkZXI6IDJweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTk5KTtcbiAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBcbiAgICAgIGlvbi1pY29uIHtcbiAgICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjUpO1xuICAgICAgfVxuICAgIH1cbiAgXG4gICAgLyogRXN0aWxvIGRlIGlucHV0IGRlIHN1YmlyIGltYWdlbiAgKi9cbiAgICAjZmlsZS1pbnB1dCB7XG4gICAgICBvcGFjaXR5OiAwO1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgdG9wOiAwO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICBsZWZ0OiAwO1xuICAgICAgei1pbmRleDogOTk5O1xuICAgIH1cbiAgfVxuICAiXX0= */");

/***/ })

}]);
//# sourceMappingURL=pages-profile-profile-module.js.map