(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-select-vehicle-type-select-vehicle-type-module"],{

/***/ "QAbw":
/*!**********************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-vehicle-type/select-vehicle-type-routing.module.ts ***!
  \**********************************************************************************************************/
/*! exports provided: SelectVehicleTypePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectVehicleTypePageRoutingModule", function() { return SelectVehicleTypePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_vehicle_type_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-vehicle-type.page */ "cHxJ");




const routes = [
    {
        path: '',
        component: _select_vehicle_type_page__WEBPACK_IMPORTED_MODULE_3__["SelectVehicleTypePage"]
    }
];
let SelectVehicleTypePageRoutingModule = class SelectVehicleTypePageRoutingModule {
};
SelectVehicleTypePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectVehicleTypePageRoutingModule);



/***/ }),

/***/ "SKZj":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-vehicle-type/select-vehicle-type.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: SelectVehicleTypePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectVehicleTypePageModule", function() { return SelectVehicleTypePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_vehicle_type_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-vehicle-type-routing.module */ "QAbw");
/* harmony import */ var _select_vehicle_type_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-vehicle-type.page */ "cHxJ");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "KQTD");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @capacitor/core */ "gcOT");









const { Storage } = _capacitor_core__WEBPACK_IMPORTED_MODULE_8__["Plugins"];
let SelectVehicleTypePageModule = class SelectVehicleTypePageModule {
};
SelectVehicleTypePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_vehicle_type_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectVehicleTypePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_select_vehicle_type_page__WEBPACK_IMPORTED_MODULE_6__["SelectVehicleTypePage"]]
    })
], SelectVehicleTypePageModule);



/***/ }),

/***/ "VaCg":
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/select-vehicle-type/select-vehicle-type.page.html ***!
  \****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<app-close-modal-button (click)=\"alertsService.closeModal()\"></app-close-modal-button>\n<ion-content class=\"ion-padding-start ion-padding-end\">\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <p class=\"title\">Selecciona un tipo de vehículo</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <app-vehicle-type-button title=\"Standard\" (redirectTo)=\"redirectTo('select-stop', 'Standard')\"></app-vehicle-type-button>\n      </ion-col>\n      <ion-col size=\"6\">\n        <app-vehicle-type-button title=\"VIP\" (redirectTo)=\"redirectTo('select-stop', 'VIP')\"></app-vehicle-type-button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"8\" class=\"label-container\">\n        <ion-label>Número de Ocupantes</ion-label>\n      </ion-col>\n      <ion-col size=\"4\">\n        <ion-input type=\"number\" [(ngModel)]=\"travel.numberPassengers\"></ion-input>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "b5hr":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-vehicle-type/select-vehicle-type.page.scss ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\n  margin-top: 0px;\n}\n\nion-input {\n  background: #F2F2F2;\n  box-shadow: inset 0px 4px 15px rgba(0, 0, 0, 0.14);\n  border-radius: 8px;\n  width: 72px;\n  height: 40px;\n  text-align: center;\n}\n\n.label-container {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\nion-content {\n  --padding-top: 0px;\n}\n\n.number-passengers-row {\n  border: 1px solid row;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3NlbGVjdC12ZWhpY2xlLXR5cGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsZUFBQTtBQUNEOztBQUVBO0VBQ0MsbUJBQUE7RUFDQSxrREFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQUNEOztBQUVBO0VBQ0MsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFDRDs7QUFFQTtFQUNDLGtCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxxQkFBQTtBQUNEIiwiZmlsZSI6InNlbGVjdC12ZWhpY2xlLXR5cGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsicCB7XG5cdG1hcmdpbi10b3A6IDBweDtcbn1cblxuaW9uLWlucHV0IHtcblx0YmFja2dyb3VuZDogI0YyRjJGMjtcblx0Ym94LXNoYWRvdzogaW5zZXQgMHB4IDRweCAxNXB4IHJnYmEoMCwgMCwgMCwgMC4xNCk7XG5cdGJvcmRlci1yYWRpdXM6IDhweDtcblx0d2lkdGg6IDcycHg7XG5cdGhlaWdodDogNDBweDtcblx0dGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubGFiZWwtY29udGFpbmVyIHtcblx0ZGlzcGxheTogZmxleDtcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbmlvbi1jb250ZW50IHtcblx0LS1wYWRkaW5nLXRvcDogMHB4O1xufVxuXG4ubnVtYmVyLXBhc3NlbmdlcnMtcm93IHtcblx0Ym9yZGVyOiAxcHggc29saWQgcm93O1xufVxuIl19 */");

/***/ }),

/***/ "cHxJ":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/select-vehicle-type/select-vehicle-type.page.ts ***!
  \************************************************************************************************/
/*! exports provided: SelectVehicleTypePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectVehicleTypePage", function() { return SelectVehicleTypePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_vehicle_type_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-vehicle-type.page.html */ "VaCg");
/* harmony import */ var _select_vehicle_type_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-vehicle-type.page.scss */ "b5hr");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/storage.service */ "fbMX");
/* harmony import */ var src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modules/home/services/users.service */ "6JOU");








let SelectVehicleTypePage = class SelectVehicleTypePage {
    constructor(alertsService, router, storageService, usersService) {
        this.alertsService = alertsService;
        this.router = router;
        this.storageService = storageService;
        this.usersService = usersService;
        this.travel = {
            numberTravel: 1,
            numberPassengers: null
        };
    }
    ngOnInit() {
        this.usersService.readDrivers().subscribe(drivers => this.drivers = drivers);
    }
    redirectTo(pageName, vehicleType) {
        this.travel.vehicleType = vehicleType;
        const sTravel = JSON.stringify(this.travel);
        this.storageService.set('travel', sTravel);
        if (this.isNumberPassengersEntered()) {
            if (this.isNumberPassengersEnteredNegative())
                this.alertsService.presentAlertWithHeader('Tipo de vehiculo', 'Por favor ingresa una cantidad de pasajeros valida.');
            else {
                const filterDrivers = this.drivers.filter(driver => driver.vehicle.availableSeats >= this.travel.numberPassengers);
                if (filterDrivers.length > 0) {
                    this.alertsService.closeModal();
                    if (!this.isTravelNow) {
                        this.router.navigate(['dashboard/schedule-trip']);
                    }
                    else {
                        this.router.navigate(['dashboard/' + pageName]);
                    }
                }
                else
                    this.alertsService.presentAlertWithHeader('Sin vehiculos', 'Lo sentimos, no tenemos vehiculos con ' + this.travel.numberPassengers + ' asientos disponibles, por favor intente con otra cantidad.');
            }
        }
        else {
            this.alertsService.presentAlertWithHeader('Tipo de vehiculo', 'Por favor ingresa la cantidad de pasajeros.');
        }
    }
    isNumberPassengersEntered() {
        if (this.travel.numberPassengers == 0)
            return false;
        else
            return true;
    }
    isNumberPassengersEnteredNegative() {
        if (this.travel.numberPassengers > 0)
            return false;
        else
            return true;
    }
};
SelectVehicleTypePage.ctorParameters = () => [
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_app_shared_services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
    { type: src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_7__["UsersService"] }
];
SelectVehicleTypePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-vehicle-type',
        template: _raw_loader_select_vehicle_type_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_vehicle_type_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectVehicleTypePage);



/***/ })

}]);
//# sourceMappingURL=pages-modals-select-vehicle-type-select-vehicle-type-module.js.map