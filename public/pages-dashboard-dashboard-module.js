(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dashboard-dashboard-module"],{

/***/ "C3py":
/*!*********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/dashboard/dashboard.page.ts ***!
  \*********************************************************************/
/*! exports provided: DashboardPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPage", function() { return DashboardPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_dashboard_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./dashboard.page.html */ "z0PU");
/* harmony import */ var _dashboard_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard.page.scss */ "ZiDz");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/authentication/authentication.service */ "6CRC");
/* harmony import */ var src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/modules/home/services/users.service */ "6JOU");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _modals_start_travel_start_travel_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../modals/start-travel/start-travel.page */ "Cic1");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../shared/services/maps.service */ "Y0mc");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @capacitor/core */ "gcOT");











const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_11__["Plugins"];

let DashboardPage = class DashboardPage {
    constructor(menuController, authenticationService, usersService, alertsService, router, mapsService) {
        this.menuController = menuController;
        this.authenticationService = authenticationService;
        this.usersService = usersService;
        this.alertsService = alertsService;
        this.router = router;
        this.mapsService = mapsService;
        this.observableList = [];
        this.markers = [];
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadDataPageDashboard();
    }
    loadDataPageDashboard() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.getDataUser();
            yield this.mapsService.loadmap('dashboardmap');
            this.generateMarksMyLocation();
            this.openModal();
        });
    }
    generateMarksMyLocation() {
        Geolocation.getCurrentPosition().then((res) => {
            this.marker.position.lat = res.coords.latitude;
            this.marker.position.lng = res.coords.longitude;
            this.marker.title = 'Mi Ubicación';
            this.addMarker(this.marker, this.marker.title, 'myLocation');
        });
    }
    addMarker(marker, name, typeMarket) {
        let marke;
        let icon;
        if (typeMarket == 'myLocation') {
            icon = {
                url: '../../assets/img/ic_loc.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(60, 60),
            };
            /*     const image = '../../assets/img/Pineapplemenu.png'; */
        }
        else if (typeMarket == 'stops') {
            icon = {
                url: '../../assets/img/flag.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(25, 25),
            };
        }
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            //  draggable: true,
            // animation: google.maps.Animation.DROP,
            map: this.mapsService.map,
            icon: icon,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                    console.log('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
        const infoWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
                        if (infoWindow !== null) {
                        }
                        infoWindow.open(this.mapsService.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
    }
    getDataUser() {
        return new Promise((resolve) => {
            const observable = this.usersService
                .getUserById(this.authenticationService.getCurrentUserId())
                .subscribe((user) => {
                this.user = user;
                resolve(this.user);
            });
            this.observableList.push(observable);
        });
    }
    openMenu() {
        this.menuController.open('principal');
    }
    openModal() {
        this.alertsService.presentModalWithoutTravelData(_modals_start_travel_start_travel_page__WEBPACK_IMPORTED_MODULE_8__["StartTravelPage"], 'my-custom-class');
    }
    redirectTo(path) {
        this.router.navigate([this.router.url + '/' + path]);
    }
    //desde
    deleteMarkers() {
        this.setMapOnAll(null);
        this.markers = [];
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
    ionViewWillLeave() {
        this.deleteMarkers();
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
DashboardPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"] },
    { type: src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UsersService"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_7__["AlertsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"] },
    { type: _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_10__["MapsService"] }
];
DashboardPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-dashboard',
        template: _raw_loader_dashboard_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_dashboard_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], DashboardPage);



/***/ }),

/***/ "HoTW":
/*!***********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/dashboard/dashboard.module.ts ***!
  \***********************************************************************/
/*! exports provided: DashboardPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function() { return DashboardPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard-routing.module */ "d69g");
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard.page */ "C3py");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let DashboardPageModule = class DashboardPageModule {
};
DashboardPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__["DashboardPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_dashboard_page__WEBPACK_IMPORTED_MODULE_6__["DashboardPage"]]
    })
], DashboardPageModule);



/***/ }),

/***/ "ZiDz":
/*!***********************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/dashboard/dashboard.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n/* Estilos cuando el usuario es administrador */\n/* Estilos del header */\n/* Estilo para colocar un margen izquierdo a cualquier elemento */\n.margin-left {\n  margin-left: 5%;\n}\n/* Contenedor de todas las tarjetas de navegación */\n.container {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n  padding: 15px;\n  height: 100%;\n}\n.container ion-card {\n  width: 90%;\n  height: 130px;\n  box-shadow: 0px 2px 15px rgba(0, 0, 0, 0.15);\n  border-radius: 15px;\n}\n/* Estilos del mensaje de bienvenido */\n.container-welcome {\n  display: flex;\n  justify-content: flex-start;\n  width: 85%;\n}\n.container-welcome p {\n  color: #3E4958;\n  font-size: 20px;\n  font-weight: bold;\n}\n/* Estilos de las tarjetas de cada una de las opciones */\n.card {\n  display: flex;\n  justify-content: space-between;\n  margin-top: 3%;\n}\n.card .container-icon {\n  width: 50%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.card .container-icon ion-icon {\n  font-size: 50px;\n}\n.card .container-title {\n  width: 50%;\n  display: flex;\n  justify-content: flex-start;\n  align-items: center;\n}\n.card .container-title h1 {\n  font-weight: bold;\n}\n/* Color verde para las tarjetas de ese color */\n.green {\n  --background: #008D36;\n  color: white;\n}\n/* Color azul para las tarjetas de ese color */\n.blue {\n  --background: #214E9D;\n  color: white;\n}\n/* Color azul transparente para las tarjetas de ese color */\n.blueTrans {\n  --background: #7590C2;\n  color: white;\n}\n/* Colores de los iconos de las tarjetas */\n.green-icon {\n  color: rgba(0, 141, 54, 0.7);\n}\n.blue-icon {\n  color: rgba(33, 78, 157, 0.7);\n}\n.white-icon {\n  color: rgba(255, 255, 255, 0.5);\n}\n/* Sombras de las tarjetas */\n.box-shadow-green {\n  box-shadow: 0px 2px 15px rgba(78, 255, 34, 0.15);\n}\n.box-shadow-blue {\n  box-shadow: 0px 2px 15px rgba(78, 255, 34, 0.15);\n}\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n#container a {\n  text-decoration: none;\n}\n/* Estilo del contenedor de la linea que cierra el modal */\n.open-modal {\n  box-shadow: 0px 0px 60px rgba(0, 0, 0, 0.2);\n  border-radius: 14px 14px 0px 0px;\n}\n.open-modal ion-row {\n  display: flex;\n  justify-content: center;\n}\n/* Estilo de la linea del modal */\n.open-modal-line {\n  width: 30px;\n  height: 5px;\n  background-color: #D5DDE0;\n  border-radius: 50px;\n}\nion-content {\n  position: relative;\n}\nion-content #maprofile {\n  width: 100%;\n  height: 70%;\n  opacity: 0;\n  transition: opacity 150ms ease-in;\n  display: block;\n}\nion-content #maprofile.show-map {\n  opacity: 1;\n}\nion-content .card-acepted {\n  position: relative;\n  bottom: 30px;\n  background-color: white;\n  height: 25%;\n  border-radius: 14px;\n}\nion-content .card-acepted .circle {\n  width: 116px;\n  height: 116px;\n  border-radius: 100%;\n  position: relative;\n  bottom: 40px;\n  background-color: white;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\nion-content .card-acepted .circle .img {\n  width: 70px;\n  height: 70px;\n  border-radius: 100%;\n}\nion-content .card-acepted .card-name {\n  justify-content: space-between;\n  align-items: center;\n  margin: 0px 13px;\n  position: relative;\n  bottom: 65px;\n}\nion-content .card-acepted .card-name p {\n  color: #3E4958;\n  font-weight: bold;\n  margin: 3px 0;\n  font-size: 18px;\n}\nion-content .card-acepted .card-name .license-plate {\n  background-color: #d5dde09a;\n  padding: 5px;\n  border-radius: 100px;\n}\nion-content .card-acepted .card-name-circle {\n  justify-content: flex-end;\n  margin: 0px 13px;\n  position: relative;\n  bottom: 80px;\n}\nion-content .card-acepted .container-acepted-btns {\n  position: relative;\n  bottom: 80px;\n  margin: 0px 13px;\n}\nion-content .card-acepted .container-acepted-btns .btn {\n  height: 60px;\n  --border-radius:15px;\n  width: 95%;\n}\nion-content .card-acepted .container-acepted-btns .btn ion-icon {\n  font-size: 30px;\n}\nion-fab ion-fab-button {\n  background-color: white;\n  border-radius: 100%;\n}\nion-icon {\n  color: black;\n}\n.title {\n  text-align: center;\n}\n.principal-container {\n  height: 100%;\n  position: relative;\n}\n.map-container {\n  height: 100%;\n}\nion-grid {\n  height: 45%;\n  border-radius: 12px 12px 0px 0px;\n  position: absolute;\n  bottom: 0px;\n  width: 100%;\n  z-index: 1;\n  background: #FFFFFF;\n  box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);\n}\nh2 {\n  height: 27px;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 28px;\n  color: #3E4958;\n}\n.price-button {\n  height: 29px;\n  width: 70px;\n  --background: #1069E3;\n  --border: 1px solid rgba(151, 173, 182, 0.40);\n  --box-shadow: 0px 4px 16px rba(62, 73, 88, 0.2);\n  --border-radius: 25px;\n  color: #FFFFFF;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 16px;\n}\nion-button.schedule {\n  width: 100%;\n  height: 60px;\n  --background: #FFFFFF;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n  --border-radius: 15px;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 24px;\n  color: #4B545A;\n}\n.price-container {\n  display: flex;\n  justify-content: flex-end;\n  align-items: center;\n}\n.button {\n  width: 100%;\n}\n.price-button {\n  width: 70px;\n  margin-right: 0px;\n}\n.initial-modal-container {\n  height: 5%;\n  position: absolute;\n  bottom: 0px;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  border-radius: 12px 12px 0px 0px;\n  background: #FFFFFF;\n  box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);\n  padding-top: 1%;\n}\n/* Estilos de la linea que cierra el modal */\n.close-modal-line {\n  width: 30px;\n  height: 5px;\n  background-color: #D5DDE0;\n  border-radius: 50px;\n}\n#dashboardmap {\n  width: 100%;\n  height: 100%;\n  opacity: 0;\n  border-radius: 5px;\n  border: 2px solid rgba(0, 0, 0, 0.288);\n  transition: opacity 150ms ease-in;\n  display: block;\n}\n#dashboardmap.show-map {\n  opacity: 1;\n}\nion-toolbar {\n  --background: transparent;\n}\napp-principal-header {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2Rhc2hib2FyZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FBQWhCLCtDQUFBO0FBQ0EsdUJBQUE7QUFDQSxpRUFBQTtBQUNBO0VBQ0UsZUFBQTtBQUVGO0FBQ0EsbURBQUE7QUFDQTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQUVGO0FBREU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLDRDQUFBO0VBQ0EsbUJBQUE7QUFHSjtBQUNBLHNDQUFBO0FBQ0E7RUFDRSxhQUFBO0VBQ0EsMkJBQUE7RUFDQSxVQUFBO0FBRUY7QUFERTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFHSjtBQUNBLHdEQUFBO0FBQ0E7RUFDRSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxjQUFBO0FBRUY7QUFERTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUdKO0FBRkk7RUFDRSxlQUFBO0FBSU47QUFERTtFQUNFLFVBQUE7RUFJQSxhQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtBQUFKO0FBTEk7RUFDRSxpQkFBQTtBQU9OO0FBQ0EsK0NBQUE7QUFDQTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtBQUVGO0FBQ0EsOENBQUE7QUFDQTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtBQUVGO0FBQ0EsMkRBQUE7QUFDQTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtBQUVGO0FBQ0EsMENBQUE7QUFDQTtFQUNFLDRCQUFBO0FBRUY7QUFFQTtFQUNFLDZCQUFBO0FBQ0Y7QUFFQTtFQUNFLCtCQUFBO0FBQ0Y7QUFFQSw0QkFBQTtBQUNBO0VBQ0UsZ0RBQUE7QUFDRjtBQUVBO0VBQ0UsZ0RBQUE7QUFDRjtBQUVBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLDJCQUFBO0FBQ0Y7QUFFQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQUNGO0FBRUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFFQSxjQUFBO0VBRUEsU0FBQTtBQURGO0FBSUE7RUFDRSxxQkFBQTtBQURGO0FBSUEsMERBQUE7QUFDQTtFQUNFLDJDQUFBO0VBQ0EsZ0NBQUE7QUFERjtBQUVFO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0FBQUo7QUFJQSxpQ0FBQTtBQUNBO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FBREY7QUFJQTtFQUNFLGtCQUFBO0FBREY7QUFFRztFQUNFLFdBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLGlDQUFBO0VBQ0EsY0FBQTtBQUFMO0FBQ0s7RUFDRSxVQUFBO0FBQ1A7QUFFRztFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FBQUw7QUFDSztFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBQ1A7QUFBTztFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFFVDtBQUNLO0VBQ0UsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FBQ1A7QUFBTztFQUNDLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0FBRVI7QUFBTztFQUNFLDJCQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0FBRVQ7QUFDSztFQUNFLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFDUDtBQUNLO0VBQ0Msa0JBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFDTjtBQUFNO0VBQ0UsWUFBQTtFQUNBLG9CQUFBO0VBQ0EsVUFBQTtBQUVSO0FBRFE7RUFDRSxlQUFBO0FBR1Y7QUFJQTtFQUNFLHVCQUFBO0VBQ0EsbUJBQUE7QUFERjtBQUlBO0VBQ0UsWUFBQTtBQURGO0FBSUE7RUFDRSxrQkFBQTtBQURGO0FBSUE7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7QUFERjtBQUlBO0VBQ0UsWUFBQTtBQURGO0FBSUE7RUFDRSxXQUFBO0VBQ0EsZ0NBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkNBQUE7QUFERjtBQUlBO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBREY7QUFJQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSw2Q0FBQTtFQUNBLCtDQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUFERjtBQUlBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLGlEQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFERjtBQUlBO0VBQ0UsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUFERjtBQUlBO0VBQ0UsV0FBQTtBQURGO0FBSUE7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7QUFERjtBQUtBO0VBQ0UsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsMkNBQUE7RUFDQSxlQUFBO0FBRkY7QUFLQSw0Q0FBQTtBQUNBO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FBRkY7QUFNQztFQUNDLFdBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0Esc0NBQUE7RUFDQSxpQ0FBQTtFQUNBLGNBQUE7QUFIRjtBQUlFO0VBQ0UsVUFBQTtBQUZKO0FBTUE7RUFDRSx5QkFBQTtBQUhGO0FBTUE7RUFDRSxXQUFBO0FBSEYiLCJmaWxlIjoiZGFzaGJvYXJkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIEVzdGlsb3MgY3VhbmRvIGVsIHVzdWFyaW8gZXMgYWRtaW5pc3RyYWRvciAqL1xuLyogRXN0aWxvcyBkZWwgaGVhZGVyICovXG4vKiBFc3RpbG8gcGFyYSBjb2xvY2FyIHVuIG1hcmdlbiBpenF1aWVyZG8gYSBjdWFscXVpZXIgZWxlbWVudG8gKi9cbi5tYXJnaW4tbGVmdHtcbiAgbWFyZ2luLWxlZnQ6IDUlO1xufVxuXG4vKiBDb250ZW5lZG9yIGRlIHRvZGFzIGxhcyB0YXJqZXRhcyBkZSBuYXZlZ2FjacOzbiAqL1xuLmNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nOiAxNXB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIGlvbi1jYXJkIHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGhlaWdodDogMTMwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDJweCAxNXB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgfVxufVxuXG4vKiBFc3RpbG9zIGRlbCBtZW5zYWplIGRlIGJpZW52ZW5pZG8gKi9cbi5jb250YWluZXItd2VsY29tZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgd2lkdGg6IDg1JTtcbiAgcCB7XG4gICAgY29sb3I6ICMzRTQ5NTg7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB9XG59XG5cbi8qIEVzdGlsb3MgZGUgbGFzIHRhcmpldGFzIGRlIGNhZGEgdW5hIGRlIGxhcyBvcGNpb25lcyAqL1xuLmNhcmQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIG1hcmdpbi10b3A6IDMlO1xuICAuY29udGFpbmVyLWljb24ge1xuICAgIHdpZHRoOiA1MCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGlvbi1pY29uIHtcbiAgICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICB9XG4gIH1cbiAgLmNvbnRhaW5lci10aXRsZSB7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBoMSB7XG4gICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICB9XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxufVxuXG4vKiBDb2xvciB2ZXJkZSBwYXJhIGxhcyB0YXJqZXRhcyBkZSBlc2UgY29sb3IgKi9cbi5ncmVlbiB7XG4gIC0tYmFja2dyb3VuZDogIzAwOEQzNjtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4vKiBDb2xvciBhenVsIHBhcmEgbGFzIHRhcmpldGFzIGRlIGVzZSBjb2xvciAqL1xuLmJsdWUge1xuICAtLWJhY2tncm91bmQ6ICMyMTRFOUQ7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLyogQ29sb3IgYXp1bCB0cmFuc3BhcmVudGUgcGFyYSBsYXMgdGFyamV0YXMgZGUgZXNlIGNvbG9yICovXG4uYmx1ZVRyYW5zIHtcbiAgLS1iYWNrZ3JvdW5kOiAjNzU5MEMyO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi8qIENvbG9yZXMgZGUgbG9zIGljb25vcyBkZSBsYXMgdGFyamV0YXMgKi9cbi5ncmVlbi1pY29uIHtcbiAgY29sb3I6IHJnYmEoMCwgMTQxLCA1NCwgMC43KTtcbiAgO1xufVxuXG4uYmx1ZS1pY29uIHtcbiAgY29sb3I6IHJnYmEoMzMsIDc4LCAxNTcsIDAuNyk7XG59XG5cbi53aGl0ZS1pY29uIHtcbiAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41KTtcbn1cblxuLyogU29tYnJhcyBkZSBsYXMgdGFyamV0YXMgKi9cbi5ib3gtc2hhZG93LWdyZWVuIHtcbiAgYm94LXNoYWRvdzogMHB4IDJweCAxNXB4IHJnYmEoNzgsIDI1NSwgMzQsIDAuMTUpO1xufVxuXG4uYm94LXNoYWRvdy1ibHVlIHtcbiAgYm94LXNoYWRvdzogMHB4IDJweCAxNXB4IHJnYmEoNzgsIDI1NSwgMzQsIDAuMTUpO1xufVxuXG4jY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG4jY29udGFpbmVyIHN0cm9uZyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbGluZS1oZWlnaHQ6IDI2cHg7XG59XG5cbiNjb250YWluZXIgcCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDIycHg7XG5cbiAgY29sb3I6ICM4YzhjOGM7XG5cbiAgbWFyZ2luOiAwO1xufVxuXG4jY29udGFpbmVyIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi8qIEVzdGlsbyBkZWwgY29udGVuZWRvciBkZSBsYSBsaW5lYSBxdWUgY2llcnJhIGVsIG1vZGFsICovXG4ub3Blbi1tb2RhbCB7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggNjBweCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gIGJvcmRlci1yYWRpdXM6IDE0cHggMTRweCAwcHggMHB4O1xuICBpb24tcm93IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB9XG59XG5cbi8qIEVzdGlsbyBkZSBsYSBsaW5lYSBkZWwgbW9kYWwgKi9cbi5vcGVuLW1vZGFsLWxpbmUge1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNENURERTA7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG59XG5cbmlvbi1jb250ZW50e1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAjbWFwcm9maWxlIHtcbiAgICAgd2lkdGg6IDEwMCU7XG4gICAgIGhlaWdodDogNzAlO1xuICAgICBvcGFjaXR5OiAwO1xuICAgICB0cmFuc2l0aW9uOiBvcGFjaXR5IDE1MG1zIGVhc2UtaW47XG4gICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAmLnNob3ctbWFwIHtcbiAgICAgICBvcGFjaXR5OiAxO1xuICAgICB9XG4gICB9XG4gICAuY2FyZC1hY2VwdGVkIHtcbiAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICBib3R0b206MzBweDtcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgIGhlaWdodDogMjUlO1xuICAgICBib3JkZXItcmFkaXVzOiAxNHB4O1xuICAgICAuY2lyY2xlIHtcbiAgICAgICB3aWR0aDogMTE2cHg7XG4gICAgICAgaGVpZ2h0OiAxMTZweDtcbiAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICBib3R0b206IDQwcHg7XG4gICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgIC5pbWcge1xuICAgICAgICAgd2lkdGg6IDcwcHg7XG4gICAgICAgICBoZWlnaHQ6IDcwcHg7XG4gICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgIH1cbiAgICAgfVxuICAgICAuY2FyZC1uYW1lIHtcbiAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICBtYXJnaW46IDBweCAxM3B4O1xuICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICBib3R0b206IDY1cHg7XG4gICAgICAgcCB7XG4gICAgICAgIGNvbG9yOiAjM0U0OTU4O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgbWFyZ2luOiAzcHggMDtcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgfVxuICAgICAgIC5saWNlbnNlLXBsYXRlIHtcbiAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkNWRkZTA5YTtcbiAgICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICAgICAgIH1cbiAgICAgfVxuICAgICAuY2FyZC1uYW1lLWNpcmNsZSB7XG4gICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICAgICBtYXJnaW46IDBweCAxM3B4O1xuICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICBib3R0b206IDgwcHg7XG4gICAgIH1cbiAgICAgLmNvbnRhaW5lci1hY2VwdGVkLWJ0bnMge1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgYm90dG9tOiA4MHB4O1xuICAgICAgbWFyZ2luOiAwcHggMTNweDtcbiAgICAgIC5idG4ge1xuICAgICAgICBoZWlnaHQ6NjBweDtcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOjE1cHg7XG4gICAgICAgIHdpZHRoOiA5NSU7XG4gICAgICAgIGlvbi1pY29ue1xuICAgICAgICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICB9XG4gICB9XG4gfVxuXG5pb24tZmFiIGlvbi1mYWItYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG59XG5cbmlvbi1pY29uIHtcbiAgY29sb3I6IHJnYigwLCAwLCAwKTtcbn1cblxuLnRpdGxlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ucHJpbmNpcGFsLWNvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ubWFwLWNvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuaW9uLWdyaWQge1xuICBoZWlnaHQ6IDQ1JTtcbiAgYm9yZGVyLXJhZGl1czogMTJweCAxMnB4IDBweCAwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAxO1xuICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICBib3gtc2hhZG93OiAwcHggMHB4IDE1cHggcmdiYSgwLCAwLCAwLCAwLjEpO1xufVxuXG5oMiB7XG4gIGhlaWdodDogMjdweDtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogMjhweDtcbiAgY29sb3I6ICMzRTQ5NTg7XG59XG5cbi5wcmljZS1idXR0b24ge1xuICBoZWlnaHQ6IDI5cHg7XG4gIHdpZHRoOiA3MHB4O1xuICAtLWJhY2tncm91bmQ6ICMxMDY5RTM7XG4gIC0tYm9yZGVyOiAxcHggc29saWQgcmdiYSgxNTEsIDE3MywgMTgyLCAwLjQwKTtcbiAgLS1ib3gtc2hhZG93OiAwcHggNHB4IDE2cHggcmJhKDYyLCA3MywgODgsIDAuMik7XG4gIC0tYm9yZGVyLXJhZGl1czogMjVweDtcbiAgY29sb3I6ICNGRkZGRkY7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cblxuaW9uLWJ1dHRvbi5zY2hlZHVsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDYwcHg7XG4gIC0tYmFja2dyb3VuZDogI0ZGRkZGRjtcbiAgLS1ib3gtc2hhZG93OiAwcHggMnB4IDhweCByZ2JhKDE2LCAxMDUsIDIyNywgMC4zKTtcbiAgLS1ib3JkZXItcmFkaXVzOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogMjRweDtcbiAgY29sb3I6ICM0QjU0NUE7XG59XG5cbi5wcmljZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCVcbn1cblxuLnByaWNlLWJ1dHRvbiB7XG4gIHdpZHRoOiA3MHB4O1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbn1cblxuXG4uaW5pdGlhbC1tb2RhbC1jb250YWluZXIge1xuICBoZWlnaHQ6IDUlOyAgICBcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiAxMnB4IDEycHggMHB4IDBweDtcbiAgYmFja2dyb3VuZDogI0ZGRkZGRjtcbiAgYm94LXNoYWRvdzogMHB4IDBweCAxNXB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgcGFkZGluZy10b3A6IDElO1xufVxuXG4vKiBFc3RpbG9zIGRlIGxhIGxpbmVhIHF1ZSBjaWVycmEgZWwgbW9kYWwgKi9cbi5jbG9zZS1tb2RhbC1saW5lIHtcbiAgd2lkdGg6IDMwcHg7XG4gIGhlaWdodDogNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRDVEREUwIDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbn1cblxuIC8vRXN0aWxvIGRlIG1hcGFcbiAjZGFzaGJvYXJkbWFwIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgb3BhY2l0eTogMDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBib3JkZXI6IDJweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMjg4KTtcbiAgdHJhbnNpdGlvbjogb3BhY2l0eSAxNTBtcyBlYXNlLWluO1xuICBkaXNwbGF5OiBibG9jaztcbiAgJi5zaG93LW1hcHtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuYXBwLXByaW5jaXBhbC1oZWFkZXIge1xuICB3aWR0aDogMTAwJTtcbn1cbiJdfQ== */");

/***/ }),

/***/ "d69g":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/dashboard/dashboard-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: DashboardPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageRoutingModule", function() { return DashboardPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _users_users_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../users/users.page */ "R9cC");
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard.page */ "C3py");
/* harmony import */ var _request_menu_request_menu_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../request-menu/request-menu.page */ "BLIM");






const routes = [
    {
        path: '',
        component: _dashboard_page__WEBPACK_IMPORTED_MODULE_4__["DashboardPage"]
    },
    {
        path: 'users/:buttonPressed',
        component: _users_users_page__WEBPACK_IMPORTED_MODULE_3__["UsersPage"]
    },
    {
        path: 'resquest-menu',
        component: _request_menu_request_menu_page__WEBPACK_IMPORTED_MODULE_5__["RequestMenuPage"]
    }
];
let DashboardPageRoutingModule = class DashboardPageRoutingModule {
};
DashboardPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DashboardPageRoutingModule);



/***/ }),

/***/ "z0PU":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/dashboard/dashboard.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-principal-header *ngIf=\"user?.role != 'client'\" title=\"Bienvenido a iGO\" (openMenu)=\"openMenu()\"></app-principal-header>\n<ion-content fullscreen>\n  <app-principal-header *ngIf=\"user?.role == 'client'\" title=\"Bienvenido a iGO\" (openMenu)=\"openMenu()\" slot=\"fixed\"></app-principal-header>\n  <div class=\"container\" *ngIf=\"user?.role == 'admin'\">\n    <div class=\"container-welcome\">\n      <p>Bienvenido, Administrador</p>\n    </div>\n    <ion-card class=\"card blueTrans\">\n      <div class=\"container-title\" [routerLink]=\"['review-transfer']\">\n        <h1>Transferencias</h1>\n      </div>\n      <div class=\"container-icon\">\n        <ion-icon class=\"white-icon\" name=\"cash-outline\" [routerLink]=\"['useradmin']\"></ion-icon>\n      </div>\n    </ion-card>\n    <ion-card class=\"card green\">\n      <div class=\"container-title\" [routerLink]=\"['users/users']\">\n        <h1>Usuarios</h1>\n      </div>\n      <div class=\"container-icon\">\n        <ion-icon class=\"white-icon\" name=\"person\" [routerLink]=\"['users/users']\"></ion-icon>\n      </div>\n    </ion-card>\n    <ion-card class=\"card blue\">\n      <div class=\"container-title\" [routerLink]=\"['users/drivers']\">\n        <h1>Conductores</h1>\n      </div>\n      <div class=\"container-icon\">\n        <ion-icon class=\"white-icon\" name=\"car-sport\" [routerLink]=\"['users/drivers']\"></ion-icon>\n      </div>\n    </ion-card>\n    <ion-card class=\"card\">\n      <div class=\"container-title \">\n        <h1>Estadistica</h1>\n      </div>\n      <div class=\"container-icon\">\n        <ion-icon class=\"green-icon\" name=\"stats-chart\" [routerLink]=\"['/stadisticadmin']\"></ion-icon>\n      </div>\n    </ion-card>\n    <ion-card class=\"card\">\n      <div class=\"container-title\" [routerLink]=\"['stops']\">\n        <h1>Paradas</h1>\n      </div>\n      <div class=\"container-icon \" [routerLink]=\"['stops']\">\n        <ion-icon name=\"flag\" class=\"blue-icon\"></ion-icon>\n      </div>\n    </ion-card>\n    <ion-card class=\"card\">\n      <div class=\"container-title\" [routerLink]=\"['shipping-parameters']\">\n        <h1>Parámetros</h1>\n      </div>\n      <div class=\"container-icon \" [routerLink]=\"['shipping-parameters']\">\n        <ion-icon name=\"cog\" class=\"blue-icon\"></ion-icon>\n      </div>\n    </ion-card>\n    <ion-card class=\"card\">\n      <div class=\"container-title\" [routerLink]=\"['bank-data']\">\n        <h1>Datos Bancarios</h1>\n      </div>\n      <div class=\"container-icon \" [routerLink]=\"['bank-data']\">\n        <ion-icon name=\"card-outline\" class=\"blue-icon\"></ion-icon>\n      </div>\n    </ion-card>\n  </div>\n  <div class=\"container\" *ngIf=\"user?.role == 'driver'\">\n    <div class=\"container-welcome\">\n      <p>Bienvenido, Conductor</p>\n    </div>\n    <ion-card class=\"card green\">\n      <div class=\"container-title\" routerLink=\"requests\">\n        <h1>Solicitudes</h1>\n      </div>\n      <div class=\"container-icon\">\n        <ion-icon name=\"person\" class=\"white-icon\"></ion-icon>\n      </div>\n    </ion-card>\n    <ion-card class=\"card blue\" (click)=\"redirectTo('history')\">\n      <div class=\"container-title\">\n        <h1>Historial</h1>\n      </div>\n      <div class=\"container-icon\">\n        <ion-icon class=\"green-icon\" name=\"stats-chart\"></ion-icon>\n      </div>\n    </ion-card>\n    <ion-card class=\"card\">\n      <div class=\"container-title \">\n        <h1>Datos Vehiculares</h1>\n      </div>\n      <div class=\"container-icon\" (click)=\"redirectTo('edit-vehicle-data')\">\n        <ion-icon name=\"car-sport\" class=\"blue-icon\"></ion-icon>\n      </div>\n    </ion-card>\n  </div>\n  <div *ngIf=\"user?.role == 'client'\" class=\"principal-container\">\n    <div class=\"map-container\">\n      <div #dashboardmap\n      id=\"dashboardmap\"></div>\n    </div>\n    <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>\n  </div>\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=pages-dashboard-dashboard-module.js.map