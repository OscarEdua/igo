(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-search-complete-search-complete-module"],{

/***/ "5kai":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/search-complete/search-complete.page.scss ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content .search {\n  height: 60%;\n  width: 100%;\n}\nion-content .search ion-col {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\nion-content .search ion-col .circle {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background: rgba(255, 255, 255, 0.4);\n  width: 220px;\n  height: 220px;\n  border-radius: 100%;\n}\nion-content .search ion-col .circle .circle-small {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background: rgba(255, 255, 255, 0.4);\n  width: 120px;\n  height: 120px;\n  border-radius: 100%;\n}\nion-content .search ion-col .circle .circle-very-small {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background: rgba(255, 255, 255, 0.4);\n  width: 55px;\n  height: 55px;\n  border-radius: 100%;\n}\n.search-text {\n  color: white;\n  font-weight: bold;\n  display: flex;\n  justify-content: center;\n  height: 10%;\n  font-size: 25px;\n}\n.img-car {\n  position: absolute;\n  width: 100px;\n}\n.close-next {\n  justify-content: center;\n}\n.close-next .circle-whatsapp {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 50px;\n  height: 50px;\n  background-color: #4cd60c;\n  border-radius: 100%;\n}\n.close-next .circle-whatsapp ion-icon {\n  color: #ffffff;\n  font-size: 40px;\n  font-weight: bold;\n}\n.close-check {\n  justify-content: center;\n}\n.close-check .circle-close {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 50px;\n  height: 50px;\n  background-color: #fff;\n  border-radius: 100%;\n}\n.close-check .circle-close ion-icon {\n  color: #3E4958;\n  font-size: 40px;\n  font-weight: bold;\n}\n.close {\n  height: 10%;\n  padding-top: 5%;\n  padding-left: 3%;\n}\n.close .circle-close {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 50px;\n  height: 50px;\n  background-color: #fff;\n  border-radius: 100%;\n}\n.close .circle-close ion-icon {\n  color: #3E4958;\n  font-size: 40px;\n  font-weight: bold;\n}\n.buttons {\n  display: flex;\n  justify-content: flex-start;\n  align-items: center;\n}\n.close-message {\n  height: 10%;\n  margin-left: auto;\n  padding-right: 5%;\n}\n.close-message .circle-messege {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 50px;\n  height: 50px;\n  background-color: #fff;\n  border-radius: 100%;\n}\n.close-message .circle-messege ion-icon {\n  color: #3E4958;\n  font-size: 40px;\n  font-weight: bold;\n}\nion-content.blue {\n  --background: rgba(33, 78, 157, 0.8);\n}\nion-content.green {\n  --background: rgba(9, 116, 39, 0.8);\n}\nion-content.red {\n  --background: red;\n}\n#reject {\n  text-align: center;\n  width: 80%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NlYXJjaC1jb21wbGV0ZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDRSxXQUFBO0VBQ0EsV0FBQTtBQUFOO0FBRU07RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUFSO0FBRVE7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQUFWO0FBRVU7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQUFkO0FBR1U7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQURkO0FBU0U7RUFDRSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQU5KO0FBUUU7RUFDSSxrQkFBQTtFQUNBLFlBQUE7QUFMTjtBQVNFO0VBQ0UsdUJBQUE7QUFOSjtBQU9JO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUFMTjtBQU1NO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUpSO0FBU0U7RUFDRSx1QkFBQTtBQU5KO0FBUUk7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtBQU5OO0FBT007RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBTFI7QUFVRTtFQUNFLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFQSjtBQVFJO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUFOTjtBQU9NO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUxSO0FBU0U7RUFDRSxhQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtBQU5KO0FBUUU7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQUxKO0FBTUk7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtBQUpOO0FBS007RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBSFI7QUFRQTtFQUNFLG9DQUFBO0FBTEY7QUFRQTtFQUNFLG1DQUFBO0FBTEY7QUFRQTtFQUNFLGlCQUFBO0FBTEY7QUFRQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtBQUxGIiwiZmlsZSI6InNlYXJjaC1jb21wbGV0ZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLnNlYXJjaCB7XG4gICAgICBoZWlnaHQ6IDYwJTtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICBcbiAgICAgIGlvbi1jb2wge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgXG4gICAgICAgIC5jaXJjbGUge1xuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNCk7XG4gICAgICAgICAgd2lkdGg6IDIyMHB4O1xuICAgICAgICAgIGhlaWdodDogMjIwcHg7XG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgXG4gICAgICAgICAgLmNpcmNsZS1zbWFsbCB7XG4gICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNCk7XG4gICAgICAgICAgICAgIHdpZHRoOiAxMjBweDtcbiAgICAgICAgICAgICAgaGVpZ2h0OiAxMjBweDtcbiAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICAgICAgICB9XG4gIFxuICAgICAgICAgIC5jaXJjbGUtdmVyeS1zbWFsbHtcbiAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC40KTtcbiAgICAgICAgICAgICAgd2lkdGg6IDU1cHg7XG4gICAgICAgICAgICAgIGhlaWdodDogNTVweDtcbiAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICAgICAgICB9XG4gIFxuICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG4gIC5zZWFyY2gtdGV4dHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDEwJTtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gIH1cbiAgLmltZy1jYXJ7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB3aWR0aDogMTAwcHg7XG4gICAgICBcbiAgfVxuXG4gIC5jbG9zZS1uZXh0e1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIC5jaXJjbGUtd2hhdHNhcHB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOnJnYig3NiwgMjE0LCAxMik7XG4gICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgaW9uLWljb257XG4gICAgICAgIGNvbG9yOiNmZmZmZmY7XG4gICAgICAgIGZvbnQtc2l6ZTogNDBweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLmNsb3NlLWNoZWNre1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgLmNpcmNsZS1jbG9zZXtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICB3aWR0aDogNTBweDtcbiAgICAgIGhlaWdodDogNTBweDtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6I2ZmZjtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gICAgICBpb24taWNvbntcbiAgICAgICAgY29sb3I6IzNFNDk1ODtcbiAgICAgICAgZm9udC1zaXplOiA0MHB4O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgXG4gIC5jbG9zZXtcbiAgICBoZWlnaHQ6IDEwJTtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgcGFkZGluZy1sZWZ0OiAzJTtcbiAgICAuY2lyY2xlLWNsb3Nle1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIHdpZHRoOiA1MHB4O1xuICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjojZmZmO1xuICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjojM0U0OTU4O1xuICAgICAgICBmb250LXNpemU6IDQwcHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgfVxuICAgIH1cbiAgfVxuICAuYnV0dG9ucyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAuY2xvc2UtbWVzc2FnZXtcbiAgICBoZWlnaHQ6IDEwJTtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgICAuY2lyY2xlLW1lc3NlZ2V7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7XG4gICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgaW9uLWljb257XG4gICAgICAgIGNvbG9yOiMzRTQ5NTg7XG4gICAgICAgIGZvbnQtc2l6ZTogNDBweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbmlvbi1jb250ZW50LmJsdWUge1xuICAtLWJhY2tncm91bmQ6IHJnYmEoMzMsIDc4LCAxNTcsIDAuOCk7XG59XG5cbmlvbi1jb250ZW50LmdyZWVuIHtcbiAgLS1iYWNrZ3JvdW5kOiByZ2JhKDksIDExNiwgMzksIDAuOCk7XG59XG5cbmlvbi1jb250ZW50LnJlZCB7XG4gIC0tYmFja2dyb3VuZDogcmVkO1xufVxuXG4jcmVqZWN0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogODAlO1xufVxuIl19 */");

/***/ }),

/***/ "DZ2l":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/search-complete/search-complete.page.ts ***!
  \*********************************************************************************/
/*! exports provided: SearchCompletePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchCompletePage", function() { return SearchCompletePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_search_complete_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./search-complete.page.html */ "begf");
/* harmony import */ var _search_complete_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./search-complete.page.scss */ "5kai");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/admin/travels.service */ "KlPZ");
/* harmony import */ var _shared_services_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../shared/services/shipping-parameters.service */ "jNID");








let SearchCompletePage = class SearchCompletePage {
    constructor(router, activatedRoute, travelsService, shippingParametersService, animationCtrl) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.travelsService = travelsService;
        this.shippingParametersService = shippingParametersService;
        this.animationCtrl = animationCtrl;
        this.numbersEmergency = [];
        this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
    }
    ngOnInit() {
        this.travel = this.travelsService.getTravelId(this.travelId);
        console.log(this.travelId);
        this.travel.subscribe(travel => console.log(travel));
        this.getParameters();
    }
    getParameters() {
        this.shippingParametersService.getCollection().subscribe((res) => {
            this.numbersEmergency = res;
        });
    }
    onNavigate() {
        window.open("https://api.whatsapp.com/send/?phone=593" + this.numbersEmergency[0].numCall + "&text=" + this.numbersEmergency[0].messageCall + "&app_absent=0", "_blank");
    }
    ionViewDidEnter() {
        this.animationAllCircles();
    }
    animationAllCircles() {
        this.animationIncreaseCircle(this.circleVerySmall.nativeElement).play();
        this.animationIncreaseCircle(this.circleSmall.nativeElement).play();
        this.animationIncreaseCircle(this.circle.nativeElement).play();
    }
    animationIncreaseCircle(element) {
        const animation = this.animationCtrl.create();
        animation
            .addElement(element)
            .duration(2500)
            .iterations(Infinity)
            .fromTo('transform', 'scale(1)', 'scale(1.3)')
            .fromTo('opacity', '1', '0.7');
        return animation;
    }
    goToMethodPayment() {
        this.router.navigate(['dashboard/transfer/', this.travelId]);
    }
    goToTravel() {
        this.router.navigate(['dashboard/travel', this.travelId, 'client']);
    }
};
SearchCompletePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_6__["TravelsService"] },
    { type: _shared_services_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_7__["ShippingParametersService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AnimationController"] }
];
SearchCompletePage.propDecorators = {
    circle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['circle',] }],
    circleSmall: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['circleSmall',] }],
    circleVerySmall: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['circleVerySmall',] }]
};
SearchCompletePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-search-complete',
        template: _raw_loader_search_complete_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_search_complete_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SearchCompletePage);



/***/ }),

/***/ "Q11+":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/search-complete/search-complete.module.ts ***!
  \***********************************************************************************/
/*! exports provided: SearchCompletePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchCompletePageModule", function() { return SearchCompletePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _search_complete_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./search-complete-routing.module */ "gemh");
/* harmony import */ var _search_complete_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./search-complete.page */ "DZ2l");







let SearchCompletePageModule = class SearchCompletePageModule {
};
SearchCompletePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _search_complete_routing_module__WEBPACK_IMPORTED_MODULE_5__["SearchCompletePageRoutingModule"]
        ],
        declarations: [_search_complete_page__WEBPACK_IMPORTED_MODULE_6__["SearchCompletePage"]]
    })
], SearchCompletePageModule);



/***/ }),

/***/ "begf":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/search-complete/search-complete.page.html ***!
  \*************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content [ngClass]=\"{ blue: ((travel | async)?.status === 'waiting_payment'), green: ((travel | async)?.status === 'paid'), red: ((travel | async)?.status === 'reject')  }\">\n  <ion-row *ngIf=\"(travel | async)?.status === 'waiting_payment' || (travel | async)?.status === 'reject'\" class=\"close\" (click)=\"goToMethodPayment()\"> \n    <div class=\"circle-close\">\n      <ion-icon  name=\"close-outline\"></ion-icon>\n    </div>\n  </ion-row>\n\n  <ion-row class=\"search\"> \n    <ion-col size=\"12\">\n      <div class=\"circle\" #circle>\n        <div class=\"circle-small\" #circleSmall>\n          <div class=\"circle-very-small\" #circleVerySmall>\n          </div>\n        </div>\n      </div>\n      <img class=\"img-car\" src=\"../../../../../assets/img/search-car.png\" alt=\"\">\n    </ion-col>\n  </ion-row>\n\n  <ion-row *ngIf=\"(travel | async)?.status === 'reject'\" class=\"close-next\"  (click)=\"onNavigate()\">\n    <div class=\"circle-whatsapp\">\n      <ion-icon name=\"logo-whatsapp\"></ion-icon>\n    </div>\n  </ion-row>\n  \n  <ion-row *ngIf=\"(travel | async)?.status === 'paid'\" class=\"close-check\" (click)=\"goToTravel()\"> \n    <div class=\"circle-close\">\n      <ion-icon name=\"checkmark-outline\"></ion-icon>\n    </div>\n  </ion-row>\n\n  <ion-row class=\"search-text\">\n    <p *ngIf=\"(travel | async)?.status === 'waiting_payment'\">Validando boucher...</p>\n    <p *ngIf=\"(travel | async)?.status === 'paid'\">¡ Vaucher validado !</p>\n    <p *ngIf=\"(travel | async)?.status === 'reject'\" id=\"reject\">Vaucher rechazado, comuníquese con administración</p>\n  </ion-row>\n</ion-content>\n");

/***/ }),

/***/ "gemh":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/search-complete/search-complete-routing.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: SearchCompletePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchCompletePageRoutingModule", function() { return SearchCompletePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _search_complete_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./search-complete.page */ "DZ2l");




const routes = [
    {
        path: '',
        component: _search_complete_page__WEBPACK_IMPORTED_MODULE_3__["SearchCompletePage"]
    }
];
let SearchCompletePageRoutingModule = class SearchCompletePageRoutingModule {
};
SearchCompletePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SearchCompletePageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-search-complete-search-complete-module.js.map