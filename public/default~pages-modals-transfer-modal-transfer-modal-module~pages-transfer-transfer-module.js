(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-modals-transfer-modal-transfer-modal-module~pages-transfer-transfer-module"],{

/***/ "HmT7":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/transfer-modal/transfer-modal.page.html ***!
  \******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<app-close-modal-button (click)=\"alertsService.closeModal()\"></app-close-modal-button>\n<ion-content class=\"ion-padding-horizontal\">\n  <ion-row>\n    <ion-col size=\"12\">\n      <p class=\"title-modal\"> Total a Transferir: $ {{(travelPrice | async)?.price.dollars}}.{{(travelPrice | async)?.price.cents}}</p>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col size=\"12\">\n      <div class=\"container-inputs-vehicle\">\n        <ion-item  class=\"input-ionItem\" style=\"width: 75%\">\n          <ion-input type=\"text\" [(ngModel)]=\"voucherImgText\"  disabled=\"true\"></ion-input>\n        </ion-item>\n        <div class=\"container-icon\">\n          <ion-item lines=\"none\">\n            <ion-input type=\"file\" accept=\"image/*\" id=\"file-input-license-imagen\" class=\"file-input\"\n              (change)=\"changeFileImagen($event)\" ></ion-input>\n            <button class=\"btn-subir\">\n              <ion-icon name=\"arrow-up-outline\"></ion-icon>\n            </button>\n          </ion-item>\n        </div>\n      </div>\n    </ion-col>\n  </ion-row>\n  <ion-row class=\"container-img\">\n    <img src=\"{{voucherImg}}\" alt=\"\">\n  </ion-row>\n  <ion-row>\n    <ion-col size=\"12\">\n      <app-button title=\"Confirmar forma de pago\" (pressButton)=\"goToTravelDetails('travel-details')\"></app-button>\n    </ion-col>\n  </ion-row>\n <app-bank-account-card \n  [accountNumber]=\"getbankParameters.accountNumber\"\n  [email]=\"getbankParameters.email\"\n  [nameBank]=\"getbankParameters.nameBank\"\n  [nameOwner]=\"getbankParameters.nameOwner\"\n  [typeAccount]=\"getbankParameters.typeAccount\"\n  ></app-bank-account-card> \n</ion-content>\n");

/***/ }),

/***/ "Y70h":
/*!**************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/transfer-modal/transfer-modal.page.ts ***!
  \**************************************************************************************/
/*! exports provided: TransferModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferModalPage", function() { return TransferModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_transfer_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./transfer-modal.page.html */ "HmT7");
/* harmony import */ var _transfer_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./transfer-modal.page.scss */ "loCm");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../shared/services/maps.service */ "Y0mc");
/* harmony import */ var _shared_services_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../shared/services/storage.service */ "fbMX");
/* harmony import */ var _shared_services_images_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../shared/services/images.service */ "gx82");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/admin/travels.service */ "KlPZ");
/* harmony import */ var _shared_services_bank_data_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../shared/services/bank-data.service */ "nrTL");











let TransferModalPage = class TransferModalPage {
    constructor(bankParametersService, alertsService, storageService, mapsService, imagesService, travelsService, router) {
        this.bankParametersService = bankParametersService;
        this.alertsService = alertsService;
        this.storageService = storageService;
        this.mapsService = mapsService;
        this.imagesService = imagesService;
        this.travelsService = travelsService;
        this.router = router;
        // inicio de la ruta
        this.origin = { lat: 0, lng: 0 };
        // Fin d e  la ruta
        this.destination = { lat: 0, lng: 0 };
        this.isConfirmedPayment = false;
    }
    ngOnInit() {
        this.getParametrers();
        this.travelPrice = this.travelsService.getTravelAndUserById(this.travelId, "driver");
    }
    ionViewWillEnter() {
        this.loadData();
    }
    getParametrers() {
        this.bankParametersService.getparametes().subscribe((res) => {
            this.getbankParameters = res;
        });
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.getTravel();
            this.origin.lat = this.travel.startLocation.lat;
            this.origin.lng = this.travel.startLocation.lng;
            this.destination.lat = this.travel.destinyLocation.lat;
            this.destination.lng = this.travel.destinyLocation.lng;
        });
    }
    getTravel() {
        return new Promise((resolve) => {
            this.storageService.get('travel').then((travel) => {
                this.travel = JSON.parse(travel);
                resolve(this.travel);
            });
        });
    }
    changeFileImagen($event) {
        if ($event.target.files && $event.target.files[0]) {
            let reader = new FileReader();
            reader.readAsDataURL($event.target.files[0]);
            reader.onload = (event) => {
                this.voucherImagenFile = $event.target.files[0];
                console.log(this.voucherImagenFile);
                this.voucherImg = event.target.result;
            };
        }
    }
    goToTravelDetails(route) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.alertsService.presentLoading('Confirmando pago...');
            if (this.voucherImg == undefined) {
                this.alertsService.loading.dismiss();
                this.alertsService.presentAlertOk('Transferencia', 'Por favor suba el comprobante de pago');
            }
            else {
                yield this.imagesService.uploadImage('bouchers', this.voucherImagenFile)
                    .then(urlImagen => {
                    this.travelsService.updateInvoiceImg(this.travelId, urlImagen)
                        .then((result) => {
                        console.log(result);
                        this.alertsService.loading.dismiss();
                        this.isConfirmedPayment = true;
                        this.alertsService.presentAlertOk('Pago', 'Pago confirmado exitosamente, por favor espere mientras el administrador valida su comprobante.')
                            .then(result => {
                            if (result === true) {
                                this.alertsService.closeModal();
                                this.router.navigate(['dashboard/search-complete', this.travelId]);
                            }
                        })
                            .catch(error => {
                            console.log(error);
                            this.alertsService.loading.dismiss();
                        });
                    })
                        .catch(error => {
                        console.log(error);
                        this.alertsService.loading.dismiss();
                    });
                })
                    .catch(error => {
                    console.log(error);
                    this.alertsService.loading.dismiss();
                });
            }
        });
    }
};
TransferModalPage.ctorParameters = () => [
    { type: _shared_services_bank_data_service__WEBPACK_IMPORTED_MODULE_10__["BankParametersService"] },
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: _shared_services_storage_service__WEBPACK_IMPORTED_MODULE_7__["StorageService"] },
    { type: _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__["MapsService"] },
    { type: _shared_services_images_service__WEBPACK_IMPORTED_MODULE_8__["ImagesService"] },
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_9__["TravelsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
TransferModalPage.propDecorators = {
    travelId: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
TransferModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-transfer-modal',
        template: _raw_loader_transfer_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_transfer_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TransferModalPage);



/***/ }),

/***/ "gx82":
/*!***************************************************!*\
  !*** ./src/app/shared/services/images.service.ts ***!
  \***************************************************/
/*! exports provided: ImagesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImagesService", function() { return ImagesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "Jgta");



let ImagesService = class ImagesService {
    constructor() { }
    uploadImage(storageFolder, image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const uploadTask = yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].storage().ref().child(`${storageFolder}/${image.name}`).put(image);
                return yield uploadTask.ref.getDownloadURL();
            }
            catch (error) {
                console.log(error);
                return false;
            }
        });
    }
    deleteImage(storageFolder, image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].storage().ref().child(`${storageFolder} / ${image.name}`).delete();
        });
    }
};
ImagesService.ctorParameters = () => [];
ImagesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ImagesService);



/***/ }),

/***/ "loCm":
/*!****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/transfer-modal/transfer-modal.page.scss ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container-inputs-vehicle {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n}\n.container-inputs-vehicle .input-ionItem {\n  --background: #F7F8F9;\n  --highlight-color-focused: #008D36;\n  --highlight-color-valid: #008D36;\n  border: 0.5px solid #D5DDE0;\n  border-radius: 15px;\n  margin-top: 3%;\n}\n.container-inputs-vehicle .file-input {\n  opacity: 0;\n  position: absolute;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  z-index: 999;\n}\n.btn-subir {\n  background-color: #008D36;\n  color: white;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 45px;\n  height: 45px;\n  border-radius: 100%;\n  padding: 5%;\n  font-size: 25px;\n}\n.container-img {\n  display: flex;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3RyYW5zZmVyLW1vZGFsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FBQ0Y7QUFDRTtFQUNFLHFCQUFBO0VBQ0Esa0NBQUE7RUFDQSxnQ0FBQTtFQUNBLDJCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FBQ0o7QUFFRTtFQUNFLFVBQUE7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0FBQUo7QUFNQTtFQUNFLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBSEY7QUFNQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBQUhKIiwiZmlsZSI6InRyYW5zZmVyLW1vZGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXItaW5wdXRzLXZlaGljbGUge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cbiAgLmlucHV0LWlvbkl0ZW0ge1xuICAgIC0tYmFja2dyb3VuZDogI0Y3RjhGOTtcbiAgICAtLWhpZ2hsaWdodC1jb2xvci1mb2N1c2VkOiAjMDA4RDM2O1xuICAgIC0taGlnaGxpZ2h0LWNvbG9yLXZhbGlkOiAjMDA4RDM2O1xuICAgIGJvcmRlcjogMC41cHggc29saWQgI0Q1RERFMDtcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIG1hcmdpbi10b3A6IDMlO1xuICB9XG5cbiAgLmZpbGUtaW5wdXQge1xuICAgIG9wYWNpdHk6IDA7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgbGVmdDogMDtcbiAgICB6LWluZGV4OiA5OTk7XG4gIH1cblxufVxuXG4vL0VzdGlsb3MgZGUgYm90b24gc3ViaXIgaW1nXG4uYnRuLXN1YmlyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwOEQzNjtcbiAgY29sb3I6IHdoaXRlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgd2lkdGg6IDQ1cHg7XG4gIGhlaWdodDogNDVweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgcGFkZGluZzogNSU7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLmNvbnRhaW5lci1pbWd7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn0iXX0= */");

/***/ })

}]);
//# sourceMappingURL=default~pages-modals-transfer-modal-transfer-modal-module~pages-transfer-transfer-module.js.map