(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-edit-vehicle-data-edit-vehicle-data-module"],{

/***/ "7pvP":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/edit-vehicle-data/edit-vehicle-data.page.scss ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  display: flex;\n  justify-content: center;\n}\n.container .container-inputs {\n  margin-bottom: 3%;\n  padding: 10px;\n  width: 90%;\n  text-align: left;\n}\n.container .container-inputs ion-select {\n  width: 100%;\n  justify-content: center;\n}\n.container .container-inputs .label {\n  color: #3E4958;\n  opacity: 0.8;\n  font-weight: bold;\n  padding-left: 10px;\n}\n.container .container-inputs .input-ionItem {\n  --background: #F7F8F9;\n  --highlight-color-focused: #008D36;\n  --highlight-color-valid: #008D36;\n  border: 0.5px solid #D5DDE0;\n  border-radius: 15px;\n  margin-top: 3%;\n}\n.container .container-select {\n  text-align: left;\n  padding: 10px;\n  width: 90%;\n  margin: 0px;\n}\n.container-inputs-vehicle {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n}\n.btn-see-img {\n  --background: #F7F8F9;\n  --background-activated: #F7F8F9;\n  --color:#3E4958;\n  font-size: 13px;\n  font-weight: bold;\n  --border-color:rgba(255,255,255,0);\n}\n.btn-subir {\n  background-color: #008D36;\n  color: white;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 45px;\n  height: 45px;\n  border-radius: 100%;\n  padding: 5%;\n  font-size: 25px;\n}\nion-checkbox {\n  --background: #F2F2F2;\n  --border-radius: 8px;\n  --size: 30px;\n  --border-width: 1px;\n}\n.checkbox-text {\n  font-style: normal;\n  font-weight: normal;\n  color: #3E4958;\n}\n.btn {\n  width: 75%;\n  height: 60px;\n  --background: #008D36;\n  --border-radius:15px;\n  --box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  color: white;\n  font-size: 17px;\n}\n.file-input {\n  opacity: 0;\n  position: absolute;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  z-index: 999;\n}\nh5 {\n  font-size: 18px;\n  color: #3E4958;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2VkaXQtdmVoaWNsZS1kYXRhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtBQUNGO0FBQUU7RUFDRSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUFFSjtBQURJO0VBQ0UsV0FBQTtFQUNBLHVCQUFBO0FBR047QUFERTtFQUNFLGNBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUdKO0FBREk7RUFDRSxxQkFBQTtFQUNBLGtDQUFBO0VBQ0EsZ0NBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQUdOO0FBQUU7RUFDRSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQUVKO0FBRUE7RUFDRSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtBQUNGO0FBRUE7RUFDRSxxQkFBQTtFQUNBLCtCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtDQUFBO0FBQ0Y7QUFFQTtFQUNFLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBQ0Y7QUFFQTtFQUNFLHFCQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFDRjtBQUVBO0VBQ0Usa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFDRjtBQUVBO0VBQ0UsVUFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLG9CQUFBO0VBQ0Esb0RBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQUNGO0FBRUE7RUFDRSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtBQUNGO0FBRUE7RUFDRSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FBQ0YiLCJmaWxlIjoiZWRpdC12ZWhpY2xlLWRhdGEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAuY29udGFpbmVyLWlucHV0cyB7XG4gICAgbWFyZ2luLWJvdHRvbTogMyU7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICB3aWR0aDogOTAlO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgaW9uLXNlbGVjdCB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIH1cbiAgLmxhYmVsIHtcbiAgICBjb2xvcjogIzNFNDk1ODtcbiAgICBvcGFjaXR5OiAwLjg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICB9XG4gICAgLmlucHV0LWlvbkl0ZW0ge1xuICAgICAgLS1iYWNrZ3JvdW5kOiAjRjdGOEY5O1xuICAgICAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDogIzAwOEQzNjtcbiAgICAgIC0taGlnaGxpZ2h0LWNvbG9yLXZhbGlkOiAjMDA4RDM2O1xuICAgICAgYm9yZGVyOiAwLjVweCBzb2xpZCAjRDVEREUwO1xuICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgIG1hcmdpbi10b3A6IDMlO1xuICAgIH1cbiAgfVxuICAuY29udGFpbmVyLXNlbGVjdCB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIHdpZHRoOiA5MCU7XG4gICAgbWFyZ2luOiAwcHg7XG4gIH1cbn1cblxuLmNvbnRhaW5lci1pbnB1dHMtdmVoaWNsZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLmJ0bi1zZWUtaW1nIHtcbiAgLS1iYWNrZ3JvdW5kOiAjRjdGOEY5O1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjRjdGOEY5O1xuICAtLWNvbG9yOiMzRTQ5NTg7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIC0tYm9yZGVyLWNvbG9yOnJnYmEoMjU1LDI1NSwyNTUsMCk7XG59XG5cbi5idG4tc3ViaXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA4RDM2O1xuICBjb2xvcjp3aGl0ZTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiA0NXB4O1xuICBoZWlnaHQ6IDQ1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gIHBhZGRpbmc6IDUlO1xuICBmb250LXNpemU6IDI1cHg7XG59XG5cbmlvbi1jaGVja2JveCB7XG4gIC0tYmFja2dyb3VuZDogI0YyRjJGMjtcbiAgLS1ib3JkZXItcmFkaXVzOiA4cHg7XG4gIC0tc2l6ZTogMzBweDtcbiAgLS1ib3JkZXItd2lkdGg6IDFweDtcbn1cblxuLmNoZWNrYm94LXRleHQge1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGNvbG9yOiAjM0U0OTU4O1xufVxuXG4uYnRuIHtcbiAgd2lkdGg6IDc1JTtcbiAgaGVpZ2h0OiA2MHB4O1xuICAtLWJhY2tncm91bmQ6ICMwMDhEMzY7XG4gIC0tYm9yZGVyLXJhZGl1czoxNXB4O1xuICAtLWJveC1zaGFkb3c6IDBweCA0cHggMjBweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMjUpO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTdweDtcbn1cblxuLmZpbGUtaW5wdXQge1xuICBvcGFjaXR5OiAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgbGVmdDogMDtcbiAgei1pbmRleDogOTk5O1xufVxuXG5oNSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgY29sb3I6ICMzRTQ5NTg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuIl19 */");

/***/ }),

/***/ "AM1N":
/*!***********************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/edit-vehicle-data/edit-vehicle-data-routing.module.ts ***!
  \***********************************************************************************************/
/*! exports provided: EditVehicleDataPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditVehicleDataPageRoutingModule", function() { return EditVehicleDataPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _edit_vehicle_data_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit-vehicle-data.page */ "JaI/");




const routes = [
    {
        path: '',
        component: _edit_vehicle_data_page__WEBPACK_IMPORTED_MODULE_3__["EditVehicleDataPage"]
    }
];
let EditVehicleDataPageRoutingModule = class EditVehicleDataPageRoutingModule {
};
EditVehicleDataPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditVehicleDataPageRoutingModule);



/***/ }),

/***/ "JaI/":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/edit-vehicle-data/edit-vehicle-data.page.ts ***!
  \*************************************************************************************/
/*! exports provided: EditVehicleDataPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditVehicleDataPage", function() { return EditVehicleDataPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_edit_vehicle_data_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./edit-vehicle-data.page.html */ "NuVh");
/* harmony import */ var _edit_vehicle_data_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./edit-vehicle-data.page.scss */ "7pvP");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/authentication/authentication.service */ "6CRC");
/* harmony import */ var _home_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../home/services/users.service */ "6JOU");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var src_app_shared_services_images_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/services/images.service */ "gx82");








let EditVehicleDataPage = class EditVehicleDataPage {
    constructor(autenticationService, usersService, alertsService, imagesService) {
        this.autenticationService = autenticationService;
        this.usersService = usersService;
        this.alertsService = alertsService;
        this.imagesService = imagesService;
        this.userId = this.autenticationService.getCurrentUserId();
        this.oUser = this.usersService.getUserById(this.userId);
        this.oUser.subscribe(user => this.uUser = user);
        this.plateImagenFile = null;
        this.vehicleImagenFile = null;
        this.licenseImagenFile = null;
    }
    ngOnInit() {
    }
    update(user) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.alertsService.presentLoading('Editando...');
            if (this.plateImagenFile != null) {
                user.vehicle.plateImagen = yield this.imagesService.uploadImage('vehicles', this.plateImagenFile);
            }
            if (this.plateImagenFile != null) {
                user.vehicle.plateImagen = yield this.imagesService.uploadImage('vehicles', this.plateImagenFile);
            }
            if (this.plateImagenFile != null) {
                user.vehicle.imagen = yield this.imagesService.uploadImage('vehicles', this.vehicleImagenFile);
            }
            if (this.plateImagenFile != null) {
                user.vehicle.licenseImagen = yield this.imagesService.uploadImage('vehicles', this.licenseImagenFile);
            }
            this.usersService.update(user)
                .then(() => {
                this.alertsService.loading.dismiss();
                this.alertsService.presentAlertWithHeader('Editar!', 'Datos editados correctamente!');
            })
                .catch(() => {
                this.alertsService.loading.dismiss();
                this.alertsService.presentAlertWithHeader('Lo sentimos!', 'Por favor vuelva a intentarlo!');
            });
        });
    }
    changeFileImagen($event, inputFile) {
        if ($event.target.files && $event.target.files[0]) {
            const reader = new FileReader();
            reader.readAsDataURL($event.target.files[0]);
            reader.onload = (event) => {
                switch (inputFile) {
                    case 'plateImagen':
                        this.plateImagenFile = $event.target.files[0];
                        this.uUser.vehicle.plateImagen = event.target.result;
                        break;
                    case 'vehicleImagen':
                        this.vehicleImagenFile = $event.target.files[0];
                        this.uUser.vehicle.imagen = event.target.result;
                        break;
                    case 'licenseImagen':
                        this.licenseImagenFile = $event.target.files[0];
                        this.uUser.vehicle.licenseImagen = event.target.result;
                        break;
                }
            };
        }
    }
};
EditVehicleDataPage.ctorParameters = () => [
    { type: _core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
    { type: _home_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UsersService"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_6__["AlertsService"] },
    { type: src_app_shared_services_images_service__WEBPACK_IMPORTED_MODULE_7__["ImagesService"] }
];
EditVehicleDataPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-edit-vehicle-data',
        template: _raw_loader_edit_vehicle_data_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_edit_vehicle_data_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], EditVehicleDataPage);



/***/ }),

/***/ "NuVh":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/edit-vehicle-data/edit-vehicle-data.page.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header title=\"Editar\"></app-header>\n<ion-content>\n  <ion-grid>\n    <ion-row class=\"container-title\">\n      <ion-col size=\"8\">\n        <h5>Describe tu vehículo</h5>\n      </ion-col>\n      <ion-col size=\"4\">\n        <ion-avatar>\n          <img *ngIf=\"uUser?.imagen == ''\" src=\"../../../../../assets/img/user.png\">\n          <img *ngIf=\"uUser?.imagen != ''\" src=\"{{ uUser?.imagen }}\">\n        </ion-avatar>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"container\">\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">PLACA*</ion-label>\n        <div class=\"container-inputs-vehicle\">\n          <ion-item *ngIf=\"uUser?.vehicle.plateImagen == ''\" class=\"input-ionItem\" style=\"width: 75%\">\n            <ion-input type=\"text\" value=\"Imagen no subida\" disabled=\"true\"></ion-input>\n          </ion-item>\n          <ion-item *ngIf=\"uUser?.vehicle.plateImagen != ''\" class=\"input-ionItem\" style=\"width: 75%\"\n            >\n            <img src=\"{{ uUser?.vehicle.plateImagen }}\">\n          </ion-item>\n          <div class=\"container-icon\">\n            <ion-item lines=\"none\">\n              <ion-input type=\"file\" accept=\"image/*\" id=\"file-input-license-imagen\" class=\"file-input\"\n                (change)=\"changeFileImagen($event, 'plateImagen')\"></ion-input>\n              <button class=\"btn-subir\">\n                <ion-icon name=\"arrow-up-outline\"></ion-icon>\n              </button>\n            </ion-item>\n          </div>\n        </div>\n      </div>\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">MODELO DEL VEHÍCULO*</ion-label>\n        <div class=\"container-inputs-vehicle\">\n          <ion-item class=\"input-ionItem\" style=\"width: 75%\">\n            <ion-input type=\"text\" [(ngModel)]=\"uUser?.vehicle.brand\"></ion-input>\n          </ion-item>\n        </div>\n      </div>\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">FOTO DEL VEHICULO*</ion-label>\n        <div class=\"container-inputs-vehicle\">\n          <ion-item class=\"input-ionItem\" *ngIf=\"uUser?.vehicle.imagen == ''\" style=\"width: 75%\">\n            <ion-input type=\"text\" value=\"Imagen no subida\" disabled=\"true\"></ion-input>\n          </ion-item>\n          <ion-item *ngIf=\"uUser?.vehicle.imagen != ''\" class=\"input-ionItem\" style=\"width: 75%\"\n            >\n            <img src=\"{{ uUser?.vehicle.imagen }}\">\n          </ion-item>\n          <ion-item lines=\"none\">\n            <ion-input type=\"file\" accept=\"image/*\" id=\"flicenseImagenile-input\" #fileInp class=\"file-input\"\n              (change)=\"changeFileImagen($event, 'vehicleImagen')\"></ion-input>\n            <button class=\"btn-subir\">\n              <ion-icon name=\"arrow-up-outline\"></ion-icon>\n            </button>\n          </ion-item>\n        </div>\n      </div>\n\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">LICENCIA*</ion-label>\n        <div class=\"container-inputs-vehicle\">\n          <ion-item class=\"input-ionItem\" *ngIf=\"uUser?.vehicle.licenseImagen == ''\" style=\"width: 75%\">\n            <ion-input type=\"text\" value=\"Imagen no subida\" disabled=\"true\"></ion-input>\n          </ion-item>\n          <ion-item *ngIf=\"uUser?.vehicle.licenseImagen != ''\" class=\"input-ionItem\" style=\"width: 75%\"\n            >\n            <img src=\"{{ uUser?.vehicle.licenseImagen }}\">\n          </ion-item>\n          <ion-item lines=\"none\">\n            <ion-input type=\"file\" accept=\"image/*\" class=\"file-input\" #fileInp\n              (change)=\"changeFileImagen($event, 'licenseImagen')\"></ion-input>\n            <button class=\"btn-subir\">\n              <ion-icon name=\"arrow-up-outline\"></ion-icon>\n            </button>\n          </ion-item>\n        </div>\n      </div>\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">COLOR*</ion-label>\n        <ion-item class=\"input-ionItem\" style=\"width: 75%\">\n          <ion-input type=\"text\" [(ngModel)]=\"uUser?.vehicle.color\"></ion-input>\n        </ion-item>\n      </div>\n      <div class=\"container-inputs\">\n        <ion-label class=\"label\">NUMERO DE ASIENTOS DISPONIBLES*</ion-label>\n        <ion-item class=\"input-ionItem\" style=\"width: 75%\">\n          <ion-input type=\"number\" [(ngModel)]=\"uUser?.vehicle.availableSeats\"></ion-input>\n        </ion-item>\n      </div>\n      <div class=\"container-inputs\">\n        <ion-list lines=\"none\">\n          <ion-item>\n            <ion-label class=\"checkbox-text\">ACEPTA MASCOTAS</ion-label>\n            <ion-checkbox slot=\"start\" [(ngModel)]=\"uUser?.vehicle.accepPets\"></ion-checkbox>\n          </ion-item>\n          <ion-item>\n            <ion-label class=\"ion-text-wrap checkbox-text\">ESPACIO PARA EQUIPAJE</ion-label>\n            <ion-checkbox slot=\"start\" [(ngModel)]=\"uUser?.vehicle.luggageSpace\"></ion-checkbox>\n          </ion-item>\n          <ion-item>\n            <ion-label class=\"checkbox-text\">PERMITE FUMADORES</ion-label>\n            <ion-checkbox slot=\"start\" [(ngModel)]=\"uUser?.vehicle.acceptSmok\"></ion-checkbox>\n          </ion-item>\n        </ion-list>\n      </div>\n      <ion-button class=\"btn\" (click)=\"update(uUser)\">Actualizar</ion-button> \n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "gx82":
/*!***************************************************!*\
  !*** ./src/app/shared/services/images.service.ts ***!
  \***************************************************/
/*! exports provided: ImagesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImagesService", function() { return ImagesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "Jgta");



let ImagesService = class ImagesService {
    constructor() { }
    uploadImage(storageFolder, image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const uploadTask = yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].storage().ref().child(`${storageFolder}/${image.name}`).put(image);
                return yield uploadTask.ref.getDownloadURL();
            }
            catch (error) {
                console.log(error);
                return false;
            }
        });
    }
    deleteImage(storageFolder, image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].storage().ref().child(`${storageFolder} / ${image.name}`).delete();
        });
    }
};
ImagesService.ctorParameters = () => [];
ImagesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ImagesService);



/***/ }),

/***/ "hLyZ":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/edit-vehicle-data/edit-vehicle-data.module.ts ***!
  \***************************************************************************************/
/*! exports provided: EditVehicleDataPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditVehicleDataPageModule", function() { return EditVehicleDataPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _edit_vehicle_data_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-vehicle-data-routing.module */ "AM1N");
/* harmony import */ var _edit_vehicle_data_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-vehicle-data.page */ "JaI/");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let EditVehicleDataPageModule = class EditVehicleDataPageModule {
};
EditVehicleDataPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _edit_vehicle_data_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditVehicleDataPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_edit_vehicle_data_page__WEBPACK_IMPORTED_MODULE_6__["EditVehicleDataPage"]]
    })
], EditVehicleDataPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-edit-vehicle-data-edit-vehicle-data-module.js.map