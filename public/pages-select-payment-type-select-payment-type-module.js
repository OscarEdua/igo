(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-select-payment-type-select-payment-type-module"],{

/***/ "0mBq":
/*!***************************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-payment-type/select-payment-type-routing.module.ts ***!
  \***************************************************************************************************/
/*! exports provided: SelectPaymentTypePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectPaymentTypePageRoutingModule", function() { return SelectPaymentTypePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_payment_type_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-payment-type.page */ "aLy6");




const routes = [
    {
        path: '',
        component: _select_payment_type_page__WEBPACK_IMPORTED_MODULE_3__["SelectPaymentTypePage"]
    }
];
let SelectPaymentTypePageRoutingModule = class SelectPaymentTypePageRoutingModule {
};
SelectPaymentTypePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectPaymentTypePageRoutingModule);



/***/ }),

/***/ "Hi2B":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-payment-type/select-payment-type.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: SelectPaymentTypePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectPaymentTypePageModule", function() { return SelectPaymentTypePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_payment_type_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-payment-type-routing.module */ "0mBq");
/* harmony import */ var _select_payment_type_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-payment-type.page */ "aLy6");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "KQTD");








let SelectPaymentTypePageModule = class SelectPaymentTypePageModule {
};
SelectPaymentTypePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_payment_type_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectPaymentTypePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_select_payment_type_page__WEBPACK_IMPORTED_MODULE_6__["SelectPaymentTypePage"]]
    })
], SelectPaymentTypePageModule);



/***/ }),

/***/ "aLy6":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-payment-type/select-payment-type.page.ts ***!
  \*****************************************************************************************/
/*! exports provided: SelectPaymentTypePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectPaymentTypePage", function() { return SelectPaymentTypePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_payment_type_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-payment-type.page.html */ "dXLo");
/* harmony import */ var _select_payment_type_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-payment-type.page.scss */ "tf5W");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _modals_select_payment_modal_select_payment_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modals/select-payment-modal/select-payment-modal.page */ "Mc/L");
/* harmony import */ var _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../shared/services/maps.service */ "Y0mc");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/admin/travels.service */ "KlPZ");









let SelectPaymentTypePage = class SelectPaymentTypePage {
    constructor(alertsService, mapsService, activatedRoute, travelsService) {
        this.alertsService = alertsService;
        this.mapsService = mapsService;
        this.activatedRoute = activatedRoute;
        this.travelsService = travelsService;
        this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
    }
    ngOnInit() {
        console.log("test");
    }
    openModal() {
        this.alertsService.presentModalWithData(_modals_select_payment_modal_select_payment_modal_page__WEBPACK_IMPORTED_MODULE_5__["SelectPaymentModalPage"], { travelId: this.travelId }, 'select-stop-modal');
    }
    ionViewWillEnter() {
        this.openModal();
        this.mapsService.loadmap('select-payment-type-map');
    }
};
SelectPaymentTypePage.ctorParameters = () => [
    { type: _shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: _shared_services_maps_service__WEBPACK_IMPORTED_MODULE_6__["MapsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_8__["TravelsService"] }
];
SelectPaymentTypePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-payment-type',
        template: _raw_loader_select_payment_type_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_payment_type_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectPaymentTypePage);



/***/ }),

/***/ "dXLo":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/select-payment-type/select-payment-type.page.html ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content fullscreen>\n  <app-header title=\"Opciones de Pago\" slot=\"fixed\"></app-header>\n  <div id=\"select-payment-type-map\"></div>\n  <app-initial-modal-button (openModal)=\"openModal()\"></app-initial-modal-button>\n</ion-content>");

/***/ }),

/***/ "tf5W":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/select-payment-type/select-payment-type.page.scss ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#select-payment-type-map {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NlbGVjdC1wYXltZW50LXR5cGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsWUFBQTtBQUNEIiwiZmlsZSI6InNlbGVjdC1wYXltZW50LXR5cGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI3NlbGVjdC1wYXltZW50LXR5cGUtbWFwIHtcblx0aGVpZ2h0OiAxMDAlO1xufVxuIl19 */");

/***/ })

}]);
//# sourceMappingURL=pages-select-payment-type-select-payment-type-module.js.map