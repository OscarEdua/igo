(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-modals-in-travel-modal-in-travel-modal-module~pages-travel-travel-module"],{

/***/ "jZ2g":
/*!******************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/in-travel-modal/in-travel-modal.page.scss ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".btn {\n  width: 78%;\n  height: 60px;\n}\n\n.btn-close {\n  font-size: 30px;\n  color: #3E4958;\n  width: 18%;\n  height: 60px;\n  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.14);\n  border-radius: 15px;\n}\n\n.buttons {\n  display: flex;\n  justify-content: flex-start;\n  align-items: center;\n}\n\n.blue {\n  --background:#233ccb;\n  --background-activated:#012cb9;\n  color: white;\n  font-weight: bold;\n}\n\n.red {\n  --background:#CB2323;\n  --background-activated:#CB2323;\n  color: white;\n  font-weight: bold;\n}\n\n.second-row {\n  padding-top: 10px;\n}\n\n.details {\n  left: -20px;\n  text-align: right;\n}\n\n.details p {\n  color: #3E4958;\n  font-weight: normal;\n}\n\n.price {\n  display: flex;\n  justify-content: flex-end;\n  position: relative;\n  font-size: 13px;\n}\n\n.cents {\n  font-size: 13px;\n  line-height: 16px;\n}\n\np.dollars, p.cents {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  color: #3E4958;\n  font-weight: bold;\n}\n\np.vehicle {\n  margin-top: -2%;\n  height: 30px;\n  left: -14.46%;\n  right: 0%;\n  top: calc(50% - 24px/2);\n  background: #D5DDE0;\n  opacity: 0.8;\n  border-radius: 100px;\n  text-align: center;\n}\n\np.typevehicle {\n  text-align: center;\n}\n\n.dollars {\n  font-size: 26px;\n  line-height: 31px;\n  position: relative;\n  top: -5px;\n  font-size: 26px;\n}\n\n.text {\n  font-weight: bold;\n  color: #3E4958;\n}\n\nion-button {\n  --background: #FFFFFF;\n  --box-shadow: 0px 2px 8px rgb(16, 105, 227, 0.3);\n  --border-radius: 15px;\n  width: 264px;\n  height: 60px;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 28px;\n}\n\n/* Estilo del boton rojo del modal */\n\n.green {\n  --background: #008D36;\n  --background-activated:#008D36;\n  color: white;\n  font-weight: bold;\n}\n\n.red {\n  --background:#CB2323;\n  --background-activated:#CB2323;\n  color: white;\n  font-weight: bold;\n}\n\n/* Estilo del boton blanco del modal */\n\n.white {\n  --background: white;\n  --background-activated:white;\n  color: #4B545A;\n  font-weight: bold;\n  --box-shadow: 0px 2px 8px rgba(16, 105, 227, 0.3);\n}\n\n/* Se coloca paddings para ubicar los elementos de mejor manera */\n\nion-content {\n  --padding-start:5%;\n  --padding-end:5%;\n}\n\n/* Estilos de los parrafos del componenete */\n\np {\n  color: #3E4958;\n  font-size: 18px;\n  font-weight: bold;\n}\n\n/* Estilos de la linea que cierra el modal */\n\n.close-modal-line {\n  width: 30px;\n  height: 5px;\n  background-color: #D5DDE0;\n  border-radius: 50px;\n}\n\n/* Estilos del contenedor de la linea que cierra el modal */\n\n.container-close-modal {\n  padding: 3%;\n  justify-content: center;\n}\n\nion-content {\n  --background:rgba(255,255,255,0);\n}\n\ndiv.relative {\n  position: relative;\n  width: 112%;\n  height: 100%;\n  top: 15%;\n  background: white;\n  right: 6%;\n  box-shadow: 0px 0px 60px rgba(0, 0, 0, 0.2);\n  border-radius: 14px 14px 0px 0px;\n}\n\n.img-container {\n  margin-left: auto;\n  margin-right: auto;\n  display: block;\n}\n\ndiv.absolute {\n  border: solid #ffff 20px;\n  position: absolute;\n  top: -35px;\n  margin: -4px;\n  border-radius: 160px;\n  background-color: #ffffff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL2luLXRyYXZlbC1tb2RhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxVQUFBO0VBQ0EsWUFBQTtBQUFKOztBQUVFO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLDRDQUFBO0VBQ0EsbUJBQUE7QUFDSjs7QUFDQTtFQUNDLGFBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FBRUQ7O0FBQ0E7RUFDRSxvQkFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBRUY7O0FBQ0E7RUFDRSxvQkFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBRUY7O0FBQUE7RUFDQyxpQkFBQTtBQUdEOztBQURBO0VBQ0UsV0FBQTtFQUNELGlCQUFBO0FBSUQ7O0FBSEU7RUFDRSxjQUFBO0VBQ0EsbUJBQUE7QUFLSjs7QUFEQTtFQUNDLGFBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQUlEOztBQUZBO0VBQ0MsZUFBQTtFQUNBLGlCQUFBO0FBS0Q7O0FBSEE7RUFDQyxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUFNRDs7QUFKQTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFNBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7QUFPRjs7QUFMQTtFQUNFLGtCQUFBO0FBUUY7O0FBTkE7RUFDQyxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0FBU0Q7O0FBTkE7RUFDQyxpQkFBQTtFQUNBLGNBQUE7QUFTRDs7QUFOQTtFQUNDLHFCQUFBO0VBQ0EsZ0RBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFTRDs7QUFQRSxvQ0FBQTs7QUFDQTtFQUNFLHFCQUFBO0VBQ0EsOEJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUFVSjs7QUFSRTtFQUNFLG9CQUFBO0VBQ0EsOEJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUFXSjs7QUFSRSxzQ0FBQTs7QUFDQTtFQUNFLG1CQUFBO0VBQ0EsNEJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxpREFBQTtBQVdKOztBQVJFLGlFQUFBOztBQUNBO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBQVdKOztBQVJFLDRDQUFBOztBQUNBO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQVdKOztBQVJFLDRDQUFBOztBQUNBO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FBV0o7O0FBUkUsMkRBQUE7O0FBQ0E7RUFDRSxXQUFBO0VBQ0EsdUJBQUE7QUFXSjs7QUFQRTtFQUNFLGdDQUFBO0FBVUo7O0FBTkU7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsUUFBQTtFQUNBLGlCQUFBO0VBQ0EsU0FBQTtFQUNBLDJDQUFBO0VBQ0EsZ0NBQUE7QUFTSjs7QUFORTtFQUNFLGlCQUFBO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0FBU047O0FBTkU7RUFDRSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtFQUNBLHlCQUFBO0FBU0oiLCJmaWxlIjoiaW4tdHJhdmVsLW1vZGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vRXN0aWxvcyBkZSBsb3MgYm90b25lcyBib3RvbiBcbi5idG4ge1xuICAgIHdpZHRoOiA3OCU7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICB9XG4gIC5idG4tY2xvc2Uge1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICBjb2xvcjogIzNFNDk1ODtcbiAgICB3aWR0aDogMTglO1xuICAgIGhlaWdodDogNjBweDtcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDE1cHggcmdiYSgwLCAwLCAwLCAwLjE0KTtcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xufVxuLmJ1dHRvbnMge1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5ibHVlIHtcbiAgLS1iYWNrZ3JvdW5kOiMyMzNjY2I7XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IzAxMmNiOTtcbiAgY29sb3I6d2hpdGU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ucmVkIHtcbiAgLS1iYWNrZ3JvdW5kOiNDQjIzMjM7XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6I0NCMjMyMztcbiAgY29sb3I6d2hpdGU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLnNlY29uZC1yb3cge1xuXHRwYWRkaW5nLXRvcDogMTBweDtcbn1cbi5kZXRhaWxzIHtcbiAgbGVmdDogLTIwcHg7XG5cdHRleHQtYWxpZ246IHJpZ2h0O1xuICBwe1xuICAgIGNvbG9yOiAjM0U0OTU4O1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIH1cbiAgXG59XG4ucHJpY2Uge1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuXHRwb3NpdGlvbjogcmVsYXRpdmU7XG5cdGZvbnQtc2l6ZTogMTNweDtcbn1cbi5jZW50cyB7XG5cdGZvbnQtc2l6ZTogMTNweDtcblx0bGluZS1oZWlnaHQ6IDE2cHg7XG59XG5wLmRvbGxhcnMsIHAuY2VudHMge1xuXHRtYXJnaW4tdG9wOiAwcHg7XG5cdG1hcmdpbi1ib3R0b206IDBweDtcblx0Y29sb3I6ICMzRTQ5NTg7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xufVxucC52ZWhpY2xle1xuICBtYXJnaW4tdG9wOiAtMiU7XG4gIGhlaWdodDogMzBweDtcbiAgbGVmdDogLTE0LjQ2JTtcbiAgcmlnaHQ6IDAlO1xuICB0b3A6IGNhbGMoNTAlIC0gMjRweC8yKTtcbiAgYmFja2dyb3VuZDogI0Q1RERFMDtcbiAgb3BhY2l0eTogMC44O1xuICBib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxucC50eXBldmVoaWNsZXtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmRvbGxhcnMge1xuXHRmb250LXNpemU6IDI2cHg7XG5cdGxpbmUtaGVpZ2h0OiAzMXB4O1xuXHRwb3NpdGlvbjogcmVsYXRpdmU7XG5cdHRvcDogLTVweDtcblx0Zm9udC1zaXplOiAyNnB4O1xufVxuXG4udGV4dCB7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xuXHRjb2xvcjogIzNFNDk1ODtcbn1cblxuaW9uLWJ1dHRvbiB7XG5cdC0tYmFja2dyb3VuZDogI0ZGRkZGRjtcblx0LS1ib3gtc2hhZG93OiAwcHggMnB4IDhweCByZ2IoMTYsIDEwNSwgMjI3LCAwLjMpO1xuXHQtLWJvcmRlci1yYWRpdXM6IDE1cHg7XG5cdHdpZHRoOiAyNjRweDtcblx0aGVpZ2h0OiA2MHB4O1xuXHRmb250LXdlaWdodDogYm9sZDtcblx0Zm9udC1zaXplOiAxOHB4O1xuXHRsaW5lLWhlaWdodDogMjhweDtcbn1cbiAgLyogRXN0aWxvIGRlbCBib3RvbiByb2pvIGRlbCBtb2RhbCAqL1xuICAuZ3JlZW4ge1xuICAgIC0tYmFja2dyb3VuZDogIzAwOEQzNjtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiMwMDhEMzY7XG4gICAgY29sb3I6d2hpdGU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIH1cbiAgLnJlZCB7XG4gICAgLS1iYWNrZ3JvdW5kOiNDQjIzMjM7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDojQ0IyMzIzO1xuICAgIGNvbG9yOndoaXRlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB9XG5cbiAgLyogRXN0aWxvIGRlbCBib3RvbiBibGFuY28gZGVsIG1vZGFsICovXG4gIC53aGl0ZSB7XG4gICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOndoaXRlO1xuICAgIGNvbG9yOiM0QjU0NUE7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgLS1ib3gtc2hhZG93OiAwcHggMnB4IDhweCByZ2JhKDE2LCAxMDUsIDIyNywgMC4zKTtcbiAgfVxuICBcbiAgLyogU2UgY29sb2NhIHBhZGRpbmdzIHBhcmEgdWJpY2FyIGxvcyBlbGVtZW50b3MgZGUgbWVqb3IgbWFuZXJhICovXG4gIGlvbi1jb250ZW50IHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6NSU7XG4gICAgLS1wYWRkaW5nLWVuZDo1JTtcbiAgfVxuICBcbiAgLyogRXN0aWxvcyBkZSBsb3MgcGFycmFmb3MgZGVsIGNvbXBvbmVuZXRlICovXG4gIHAge1xuICAgIGNvbG9yOiAjM0U0OTU4O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgfVxuICBcbiAgLyogRXN0aWxvcyBkZSBsYSBsaW5lYSBxdWUgY2llcnJhIGVsIG1vZGFsICovXG4gIC5jbG9zZS1tb2RhbC1saW5lIHtcbiAgICB3aWR0aDogMzBweDtcbiAgICBoZWlnaHQ6IDVweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRDVEREUwIDtcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICB9XG4gIFxuICAvKiBFc3RpbG9zIGRlbCBjb250ZW5lZG9yIGRlIGxhIGxpbmVhIHF1ZSBjaWVycmEgZWwgbW9kYWwgKi9cbiAgLmNvbnRhaW5lci1jbG9zZS1tb2RhbCB7XG4gICAgcGFkZGluZzogMyU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cbiAgXG5cbiAgaW9uLWNvbnRlbnR7XG4gICAgLS1iYWNrZ3JvdW5kOnJnYmEoMjU1LDI1NSwyNTUsMCk7XG4gIH1cblxuXG4gIGRpdi5yZWxhdGl2ZSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMTIlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB0b3A6IDE1JTtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKCAyNTUsIDI1NSwgMjU1LCAxLjAwICk7XG4gICAgcmlnaHQ6IDYlO1xuICAgIGJveC1zaGFkb3c6IDBweCAwcHggNjBweCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gICAgYm9yZGVyLXJhZGl1czogMTRweCAxNHB4IDBweCAwcHg7XG4gIH0gXG4gIFxuICAuaW1nLWNvbnRhaW5lciB7XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIGRpdi5hYnNvbHV0ZSB7XG4gICAgYm9yZGVyOiBzb2xpZCAjZmZmZiAyMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IC0zNXB4O1xuICAgIG1hcmdpbjogLTRweDtcbiAgICBib3JkZXItcmFkaXVzOiAxNjBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICB9XG4gICJdfQ== */");

/***/ }),

/***/ "kCj7":
/*!****************************************************************************************!*\
  !*** ./src/app/modules/dashboard/pages/modals/in-travel-modal/in-travel-modal.page.ts ***!
  \****************************************************************************************/
/*! exports provided: InTravelModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InTravelModalPage", function() { return InTravelModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_in_travel_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./in-travel-modal.page.html */ "mVOU");
/* harmony import */ var _in_travel_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./in-travel-modal.page.scss */ "jZ2g");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/alerts.service */ "X0HJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/modules/home/services/users.service */ "6JOU");
/* harmony import */ var src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/core/authentication/authentication.service */ "6CRC");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "Wwn5");
/* harmony import */ var _shared_services_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../shared/services/shipping-parameters.service */ "jNID");
/* harmony import */ var _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/admin/travels.service */ "KlPZ");
/* harmony import */ var src_app_shared_services_maps_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/shared/services/maps.service */ "Y0mc");












let InTravelModalPage = class InTravelModalPage {
    constructor(shippingParametersService, alertsService, usersService, authenticationService, callNumber, router, mapsService, travelsService) {
        this.shippingParametersService = shippingParametersService;
        this.alertsService = alertsService;
        this.usersService = usersService;
        this.authenticationService = authenticationService;
        this.callNumber = callNumber;
        this.router = router;
        this.mapsService = mapsService;
        this.travelsService = travelsService;
        this.observableList = [];
        this.numbersEmergency = [];
        this.marker = {
            position: {
                lat: 0,
                lng: 0
            },
            title: '',
        };
        this.marker2 = {
            position: {
                lat: 0,
                lng: 0
            },
            title: '',
        };
    }
    ngOnInit() {
        this.travelWithUserData = this.travelsService.getTravelAndUserById(this.travelId, this.roleCurrentUser);
        this.suscription = this.travelWithUserData.subscribe(travel => this.travel = travel);
    }
    getTravel() {
        return new Promise((resolve) => {
            this.oTravel = this.travelsService.getTravelAndUserById(this.travelId, this.roleCurrentUser);
            this.oTravel.subscribe(travel => {
                this.travel = travel;
                console.log(this.travel);
                resolve(this.travel);
            });
        });
    }
    ionViewWillEnter() {
        this.loadDataPageDashboard();
        this.suscription.unsubscribe();
        this.getTravel();
    }
    loadDataPageDashboard() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.getDataUser();
        });
    }
    getDataUser() {
        return new Promise((resolve) => {
            const observable = this.usersService
                .getUserById(this.authenticationService.getCurrentUserId())
                .subscribe((user) => {
                this.user = user;
                resolve(this.user);
            });
            this.observableList.push(observable);
        });
    }
    irA() {
        this.marker.position.lat = this.travel['startLocation']['lat'];
        this.marker.position.lng = this.travel['startLocation']['lng'];
        this.marker.title = this.travel['startLocation']['address'];
        this.mapsService.navegate(this.marker);
    }
    irB() {
        this.marker2.position.lat = this.travel['destinyLocation']['lat'];
        this.marker2.position.lng = this.travel['destinyLocation']['lng'];
        this.marker2.title = this.travel['destinyLocation']['address'];
        this.mapsService.navegate(this.marker2);
    }
    redirectTo(pageName) {
        this.alertsService.closeModal();
        this.router.navigate(['dashboard/' + pageName]);
    }
    onNavigate() {
        window.open("https://api.whatsapp.com/send/?phone=593" + this.numbersEmergency[0].numWhatsApp + "&text=" + this.numbersEmergency[0].messageWhatsApp + "&app_absent=0", "_blank");
    }
    clickCallNumber(number) {
        this.callNumber.callNumber(number, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
    goToChat() {
        this.alertsService.closeModal();
        console.log(this.travel.user.email);
        this.router.navigate(['dashboard/chat', this.travel.user.email]);
    }
    startTravel() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.alertsService.presentLoading('Empezando viaje...');
            this.travelsService.startTravel(this.travelId)
                .then(() => {
                this.alertsService.loading.dismiss();
                this.alertsService.presentAlertConfirmAccept('Viaje', 'Viaje comenzado');
            })
                .catch((error) => console.log(error));
        });
    }
    endTravel(currentUser) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.alertsService.presentAlertConfirm('Viaje', 'Esta seguro de finalizar el viaje?')
                .then(result => {
                if (result === true) {
                    this.travelsService.endTravel(this.travelId)
                        .then(() => {
                        this.alertsService.presentAlertConfirmAccept('Viaje', 'Viaje finalizado exitosamente')
                            .then(res => {
                            if (res === true) {
                                this.alertsService.closeModal();
                                if (currentUser == 'client') {
                                    this.router.navigate(['dashboard/travel-complete/', this.travelId]);
                                }
                                else {
                                    this.router.navigate(['dashboard/successful-arrival']);
                                    console.log(';qjk');
                                }
                            }
                        });
                    })
                        .catch(error => console.log(error));
                }
            });
        });
    }
};
InTravelModalPage.ctorParameters = () => [
    { type: _shared_services_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_9__["ShippingParametersService"] },
    { type: src_app_shared_services_alerts_service__WEBPACK_IMPORTED_MODULE_4__["AlertsService"] },
    { type: src_app_modules_home_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UsersService"] },
    { type: src_app_core_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_7__["AuthenticationService"] },
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_8__["CallNumber"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_app_shared_services_maps_service__WEBPACK_IMPORTED_MODULE_11__["MapsService"] },
    { type: _services_admin_travels_service__WEBPACK_IMPORTED_MODULE_10__["TravelsService"] }
];
InTravelModalPage.propDecorators = {
    nombre: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    travelId: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    roleCurrentUser: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
InTravelModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-in-travel-modal',
        template: _raw_loader_in_travel_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_in_travel_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], InTravelModalPage);



/***/ }),

/***/ "mVOU":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/dashboard/pages/modals/in-travel-modal/in-travel-modal.page.html ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content fullscreen scroll-y=\"true\">\n  <div class=\"relative\">\n  <ion-row class=\"container-close-modal\">\n    <div class=\"close-modal-line\" (click)=\"alertsService.closeModal()\"> \n    </div>\n  </ion-row>\n  <div id=\"map\"></div>\n  <ion-grid>\n    <div class=\"absolute\">\n      <ion-row>\n            <ion-col class=\"img-container\">\n              <ion-avatar>\n                <img [src]=\"(travelWithUserData | async)?.user.imagen || '../../../../../../assets/img/user.png'\">\n              </ion-avatar>\n            </ion-col>\n        </ion-row>\n  </div>\n    <ion-row class=\"second-row\">\n        <ion-col size=\"7\" class=\"text\" *ngIf=\"roleCurrentUser === 'driver'\">\n          Clt. {{ (travelWithUserData | async)?.user.name }}\n        </ion-col>\n        <ion-col size=\"7\" class=\"text\" *ngIf=\"roleCurrentUser === 'client'\">\n          Cdtr. {{ (travelWithUserData | async)?.user.name }}\n        </ion-col>\n      <ion-col class=\"details\" size=\"5\" *ngIf=\"roleCurrentUser === 'driver'\">\n        <div class=\"price\">\n          <p class=\"dollars\">{{ (travelWithUserData | async)?.price.dollars }}<p>\n          <p class=\"cents\">{{ (travelWithUserData | async)?.price.cents }}<p>\n        </div>\n        <p>{{ (travelWithUserData | async)?.destinyLocation.address }}</p>\n      </ion-col>\n      <ion-col size=\"5\" *ngIf=\"roleCurrentUser === 'client'\">\n        <p class=\"ion-text-capitalize vehicle\">{{ (travelWithUserData | async)?.user.vehicle.type }}</p>\n        <p class=\"ion-text-capitalize typevehicle\">{{ (travelWithUserData | async)?.user.vehicle.brand }}</p>\n      </ion-col>\n    </ion-row>\n    <div *ngIf=\"user?.role == 'driver'\">\n      <ion-row>\n        <ion-col class=\"buttons\">\n          <ion-button (click)=\"goToChat()\" class=\"btn green ion-text-capitalize\">\n            Contactar Cliente\n          </ion-button>\n          <ion-button (click)=\"endTravel('driver')\" class=\"btn-close\">&times;</ion-button> \n        </ion-col>\n        <ion-col class=\"buttons\" *ngIf=\"(travelWithUserData | async)?.status === 'paid'\" (click)=\"startTravel()\">\n          <ion-button class=\"btn green ion-text-capitalize\">\n            Empezar viaje\n          </ion-button>\n        </ion-col>\n        <ion-col class=\"buttons_class\" *ngIf=\"(travelWithUserData | async)?.status === 'traveling'\">\n          <ion-button class=\"red btn\" (click)=\"onNavigate()\"> MSG emergencia </ion-button>\n          <ion-button class=\"btn-close\" (click)='clickCallNumber(\"+593\"+numbersEmergency[0].numEmergency)'> 📞</ion-button> \n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"buttons\">\n          <ion-button (click)=\"irA()\" class=\"btn green ion-text-capitalize\">\n           Navegar Punto A\n          </ion-button>\n        </ion-col>\n        <ion-col class=\"buttons\" (click)=\"irB()\">\n          <ion-button class=\"btn green ion-text-capitalize\">\n            Navegar Punto B\n          </ion-button>\n        </ion-col>\n        <ion-col class=\"buttons_class\" *ngIf=\"(travelWithUserData | async)?.status === 'traveling'\">\n          <ion-button class=\"red btn\" (click)=\"onNavigate()\"> MSG emergencia </ion-button>\n          <ion-button class=\"btn-close\" (click)='clickCallNumber(\"+593\"+numbersEmergency[0].numEmergency)'> 📞</ion-button> \n        </ion-col>\n      </ion-row>\n    </div>\n    <div *ngIf=\"user?.role == 'client'\">\n      <ion-row>\n        <ion-col class=\"buttons\">\n          <ion-button (click)=\"goToChat()\" class=\"btn green ion-text-capitalize\">\n            Contactar Conductor\n          </ion-button>\n         <ion-button (click)=\"endTravel('client')\" class=\"btn-close\">&times;</ion-button> \n        </ion-col>\n        <ion-col class=\"buttons_class\" *ngIf=\"(travelWithUserData | async)?.status === 'traveling'\">\n          <ion-button class=\"red btn\" (click)=\"onNavigate()\"> MSG emergencia </ion-button>\n          <ion-button class=\"btn-close\" (click)='clickCallNumber(\"+593\"+numbersEmergency[0].numEmergency)'> 📞</ion-button> \n        </ion-col>\n      </ion-row>\n    </div>\n</ion-grid>\n</div>\n</ion-content>\n\n");

/***/ })

}]);
//# sourceMappingURL=default~pages-modals-in-travel-modal-in-travel-modal-module~pages-travel-travel-module.js.map