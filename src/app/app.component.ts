import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthenticationService } from './core/authentication/authentication.service';
import { User } from './modules/home/interfaces/user.interface';
import firebase from 'firebase/app';
import { UsersService } from './modules/home/services/users.service';
import { Router } from '@angular/router';
import { ShippingParametersService } from './shared/services/shipping-parameters.service';
import { ShippingParameters } from './modules/dashboard/interfaces/shipping-parameters.interface';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  user: Observable<User>;
  numbersEmergency: ShippingParameters[] = [];

constructor(
  private callNumber: CallNumber,
  private shippingParametersService: ShippingParametersService,
  private menuController: MenuController, 
  public authenticationService: AuthenticationService, 
  private usersService: UsersService, 
  private router: Router) {
  }

  ngOnInit() {
    this.getAuthenticatedUser();
    this. getParameters();
  }

  getParameters(){
    this.shippingParametersService.getCollection<ShippingParameters>().subscribe((res) => {
     this.numbersEmergency= res;
   });
 }
  showHiddenMenu() {
    this.menuController.toggle(); 
  }

  getAuthenticatedUser() {
    firebase.auth().onAuthStateChanged(async (u: firebase.User) => {
      if (u) {
        this.user = this.usersService.getUserById(u.uid);
      }
    })
  }

  openLink(link: string): void {
    window.open(link);
  }

  goToHome(): void {
    this.menuController.toggle(); 
    this.router.navigate(['dashboard']);
  }
  
  goToHistory(): void {
    this.menuController.toggle(); 
    this.router.navigate(['dashboard/history']);
  }

  onNavigate(){
    window.open("https://api.whatsapp.com/send/?phone=593"+this.numbersEmergency[0].numCall+"&text="+this.numbersEmergency[0].messageCall+"&app_absent=0", "_blank");
}
  clickCallNumber(number:string){
    console.log(typeof number);
    this.callNumber.callNumber(number, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
    console.log(number)
  }
}
