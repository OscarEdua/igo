import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../app/core/guard/auth.guard';
import { canActivate, redirectUnauthorizedTo, redirectLoggedInTo } from '@angular/fire/auth-guard';
// Enviar usuarios no autorizados para iniciar sesión
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['/home/login']);
// Iniciar sesión automáticamente en usuarios
const redirectLoggedInToHome = () => redirectLoggedInTo(['/dashboard']);

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then( m => m.HomeModule),
    ...canActivate(redirectLoggedInToHome),
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./modules/dashboard/dashboard.module').then( m => m.DashboardModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'logout',
    loadChildren: () => import('./modules/home/pages/logout/logout.module').then( m => m.LogoutPageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'test',
    loadChildren: () => import('./test/test.module').then( m => m.TestPageModule)
  },
  {
    path: 'new-stop',
    loadChildren: () => import('./modules/dashboard/pages/new-stop/new-stop.module').then( m => m.NewStopPageModule)
  },
  {
    path: 'test',
    loadChildren: () => import('./test/test.module').then( m => m.TestPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
