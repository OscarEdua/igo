import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.page.html',
  styleUrls: ['./test.page.scss'],
})
export class TestPage implements OnInit {
  map: google.maps.Map;

  constructor() { }

  ngOnInit() {
  }

  ionViewDidEnter(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(currentPosition => {
        this.loadMap(currentPosition);
        this.loadAutocomplete();
      });
    } else {
      console.log('No compatible');
    }
  }

  loadMap(position: any): void {
    const settingsMap = {
      zoom: 20,
      center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
    };
    this.map = new google.maps.Map(document.getElementById('map'), settingsMap); 
    const marker = new google.maps.Marker({
      position: this.map.getCenter(),
      animation: google.maps.Animation.BOUNCE,
      icon: {
        url: '../../assets/img/stops.png',
        scaledSize: new google.maps.Size(50, 50)
      }
    });
    marker.setMap(this.map);
  }

  loadAutocomplete(): void {
    const autocomplete = new google.maps.places.Autocomplete(document.querySelector('#autocomplete-input'));
    google.maps.event.addListener(autocomplete, 'place_changed', event => {
      const place = autocomplete.getPlace();
      this.map.setCenter(place.geometry.location);
      const marker = new google.maps.Marker({
        position: place.geometry.location,
        animation: google.maps.Animation.DROP,
        icon: {
          url: '../../assets/img/stops.png',
          scaledSize: new google.maps.Size(50, 50)
        }
      });
      marker.setMap(this.map);
    });
  }
}
