"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RatingService = void 0;
var core_1 = require("@angular/core");
var app_1 = require("firebase/app");
var RatingService = /** @class */ (function () {
    function RatingService(afs) {
        this.afs = afs;
    }
    RatingService.prototype.addRating = function (rating) {
        rating.createAt = app_1["default"].firestore.Timestamp.now().toDate();
        rating.updateAt = app_1["default"].firestore.Timestamp.now().toDate();
        return this.afs.collection('rating').add(rating);
    };
    RatingService.prototype.getRatingByOrder = function (uidOrder) {
        return this.afs
            .collection('rating', function (ref) { return ref.where('travelId', '==', uidOrder); })
            .valueChanges({ idField: 'uid' });
    };
    RatingService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], RatingService);
    return RatingService;
}());
exports.RatingService = RatingService;
