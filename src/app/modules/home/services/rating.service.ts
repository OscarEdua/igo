import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Rating } from '../../dashboard/interfaces/rating.interface';
import app from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class RatingService {

  constructor(
    private afs: AngularFirestore,
  ) { }
  
  addRating(rating: Rating){
    rating.createAt = app.firestore.Timestamp.now().toDate();
    rating.updateAt = app.firestore.Timestamp.now().toDate();
    return this.afs.collection('rating').add(rating);
  }

  getRatingByOrder(uidOrder: string) {
    return this.afs
      .collection('rating', (ref) => ref.where('travelId', '==', uidOrder))
      .valueChanges({ idField: 'uid' });
  }
}
