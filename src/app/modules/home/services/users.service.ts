import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore'
import { Observable } from 'rxjs';
import { User } from '../interfaces/user.interface';


@Injectable({
  providedIn: 'root'
})

export class UsersService {
  usersCollection: AngularFirestoreCollection<User>;
  users: Observable<User[]>;
  private userDoc: AngularFirestoreDocument<User>;
  user: Observable<User>;

  constructor(private afs: AngularFirestore) {
    this.usersCollection = afs.collection<User>('users');
  }

  add(user: User): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.usersCollection.add(user);
      resolve();
    });
  }

  getUserById(userId: string): Observable<User> {
    this.userDoc = this.afs.doc<User>('users/' + userId);
    this.user = this.userDoc.valueChanges({ idField: 'uid' });
    return this.user;
  }


  getUserByIdSimple(uid: string): Observable<any> {
    return this.afs.collection('users').doc(uid).valueChanges({ idField: 'uid' });
  }

  read(): Observable<User[]> {
    this.usersCollection = this.afs.collection<User>('users');
    this.users = this.usersCollection.valueChanges({ idField: 'userId' });
    return this.users;
  }

  readDrivers(): Observable<User[]> {
    this.usersCollection = this.afs.collection<User>('users', ref => ref.where('role', '==', 'driver'));
    this.users = this.usersCollection.valueChanges({ idField: 'userId' });
    return this.users;
  }
  //service powered Mario
  getUsersByRole(role:string) { 
    return this.afs.collection('users', (ref) => ref.where('role', '==', role))
    .valueChanges({ idField: 'uid' });
  }

  update(user: User) {
    this.userDoc = this.afs.doc<User>('users/' + user.uid);
    return this.userDoc.update(user);
  }

}
