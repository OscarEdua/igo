import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { Vehicle } from 'src/app/modules/home/interfaces/vehicle.interface';
import { AlertsService } from '../../../../shared/services/alerts.service';
import { ImagesService } from 'src/app/shared/services/images.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { Router } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { MapsAPILoader } from '@agm/core';
import { PushService } from 'src/app/shared/services/push.service';
import { UsersService } from '../../services/users.service';
import { Subscription } from 'rxjs';
const { Geolocation } = Plugins;

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})

export class RegisterPage implements OnInit {
  user: User;
  iconName: string;
  inputType: string;
  vehicle: Vehicle;
  plateImagenFile: File;
  vehicleImagenFile: File;
  licenseImagenFile: File;
  latitude: number;
  longitude: number;
  zoom: number;
  geoCoder: any;
  users: User[];
  observableList: Subscription[] = [];

  constructor(
    private alertsService: AlertsService,
    private imagesService: ImagesService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private pushService: PushService,
    private userService: UsersService,
    private mapsAPILoader: MapsAPILoader) {
    this.user = {
      name: '',
      email: '',
      password: '',
      address: '',
      telephoneNumber: '',
      role: 'client',
      imagen: ''
    };
    this.inputType = 'password';
    this.iconName = 'eye';
    this.vehicle = {
      licenseImagen: '',
      brand: '',
      plateImagen: '',
      imagen: '',
      color: '',
      accepPets: false,
      luggageSpace: false,
      acceptSmok: false
    };
    this.plateImagenFile = null;
    this.vehicleImagenFile = null;
    this.licenseImagenFile = null;
  }

  ngOnInit() {
    this.mapsAPILoader.load()
      .then(() => {
        this.setAddressWithCurrentPosition();
        this.geoCoder = new google.maps.Geocoder;
      });
  }
  ionViewWillEnter() {
    this.getUserAdmin();
  }

  changeFileImagen($event, inputFile: string): void {
    if ($event.target.files && $event.target.files[0]) {
      let reader = new FileReader();
      reader.readAsDataURL($event.target.files[0]);
      reader.onload = (event: any) => {
        switch (inputFile) {
          case 'plateImagen':
            this.plateImagenFile = $event.target.files[0];
            console.log(this.plateImagenFile);
            this.vehicle.plateImagen = event.target.result;
            console.log(this.vehicleImagenFile);
            break;
          case 'vehicleImagen':
            this.vehicleImagenFile = $event.target.files[0];
            this.vehicle.imagen = event.target.result;
            break;
          case 'licenseImagen':
            this.licenseImagenFile = $event.target.files[0];
            this.vehicle.licenseImagen = event.target.result;
            break;
        }
      }
    }
  }

  showHiddenPassword(): void {
    this.iconName = this.iconName === 'eye' ? 'eye-off' : 'eye';
    this.inputType = this.inputType === 'text' ? 'password' : 'text';
  }

  async signIn() {
    await this.alertsService.presentLoading('Registrando...');
    if (this.user.role == 'client') {
      if (!this.thereAreEmptyFieldsClient()) {
        this.authenticationService.signUp(this.user)
          .then(() => {
            this.alertsService.loading.dismiss();
            this.cleanClientFields();
            this.alertsService.presentAlertConfirmAccept('Ya casi!', 'Por favor revise su correo electronico para activar su cuenta.')
              .then(result => {
                this.router.navigate(['home/login']);
              });
          })
          .catch((error) => {
            this.alertsService.loading.dismiss();
            if (error.message = 'The email address is already in use by another account.')
              this.alertsService.presentAlert('La direccion de correo ya esta siendo usado por otra cuenta.');
          })
      } else {
        this.alertsService.loading.dismiss();
        this.alertsService.presentAlertWithHeader('Registro!', 'Por favor complete los campos requeridos!');
      }
    } else if (this.user.role == 'driver') {
      if (!this.thereAreEmptyFieldsDriver()) {
        if (!this.thereAreEmptyImagensFields()) {
          this.vehicle.plateImagen = await this.imagesService.uploadImage('vehicles', this.plateImagenFile);
          this.vehicle.imagen = await this.imagesService.uploadImage('vehicles', this.vehicleImagenFile);
          this.vehicle.licenseImagen = await this.imagesService.uploadImage('vehicles', this.licenseImagenFile);
          this.user.vehicle = this.vehicle;
          this.user.vehicle.type = '';
          this.authenticationService.signUp(this.user)
            .then(() => {
              this.alertsService.loading.dismiss();
              this.alertsService.presentAlertConfirmAccept('Ya casi!', 'Por favor revise su correo electronico para activar su cuenta.')
                .then(() => {
                  // notificación a admins
                  this.users.map((res) => {
                    this.pushService.sendByUid('IGO', 'Se ha registrado un conductor en el sistema, revisa su información para aprobarlo', 'Se ha registrado un conductor en el sistema, revisa su información para aprobarlo', 'dashboard/users/drivers', res.uid)

                  })
                  this.router.navigate(['home/login']);
                })
            })
            .catch((error) => {
              this.alertsService.loading.dismiss();
              if (error.message = 'The email address is already in use by another account.')
                this.alertsService.presentAlert('La direccion de correo ya esta siendo usado por otra cuenta.')
            })
        } else {
          this.alertsService.loading.dismiss();
          this.alertsService.presentAlert('Por favor seleccione las imagenes requeridas!');
        }
      } else {
        this.alertsService.loading.dismiss();
        this.alertsService.presentAlertWithHeader('Register!', 'Por favor complete los campos requeridos!');
      }
    }
  }

  thereAreEmptyFieldsClient(): boolean {
    if (this.user.name == '' || this.user.email == '' || this.user.password == '' || this.user.gender == '')
      return true;
    else
      return false;
  }

  thereAreEmptyFieldsDriver(): boolean {
    if (this.user.name == '' || this.user.email == '' || this.user.password == '' || this.user.telephoneNumber == '' || this.user.gender == '' || this.vehicle.model == '' || this.vehicle.color == '')
      return true;
    else
      return false;
  }

  thereAreEmptyImagensFields(): boolean {
    if (this.vehicle.plateImagen == '' || this.vehicle.imagen == '' || this.vehicle.licenseImagen == '')
      return true;
    else
      return false;
  }

  cleanClientFields(): void {
    this.user = {
      name: '',
      email: '',
      password: '',
      address: '',
      telephoneNumber: '',
      role: 'client',
      imagen: ''
    }
  }

  setAddressWithCurrentPosition(): void {
    const options = {
      enableHighAccuracy: false,
      timeout: Infinity,
      maximumAge: 0
    };
    const wait = Geolocation.watchPosition(options, (position, error) => {
      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
      this.zoom = 8;
      this.setAddres(this.latitude, this.longitude);
    });
  }

  getUserAdmin() {
    const observable = this.userService
      .getUsersByRole('admin')
      .subscribe((res) => {
        this.users = res as User[];
        console.log(this.users)
      });
    this.observableList.push(observable);
  }
  setAddres(latitude: number, longitude: number): void {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.user.address = results[0].formatted_address;
        } else {
          console.log('No results found...');
        }
      } else {
        console.log('Geocode failed due to: ' + status);
      }
    });
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
