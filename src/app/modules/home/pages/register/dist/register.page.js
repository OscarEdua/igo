"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.RegisterPage = void 0;
var core_1 = require("@angular/core");
var core_2 = require("@capacitor/core");
var Geolocation = core_2.Plugins.Geolocation;
var RegisterPage = /** @class */ (function () {
    function RegisterPage(alertsService, imagesService, authenticationService, router, mapsAPILoader) {
        this.alertsService = alertsService;
        this.imagesService = imagesService;
        this.authenticationService = authenticationService;
        this.router = router;
        this.mapsAPILoader = mapsAPILoader;
        this.user = {
            name: '',
            email: '',
            password: '',
            address: '',
            telephoneNumber: '',
            role: 'client',
            imagen: ''
        };
        this.inputType = 'password';
        this.iconName = 'eye';
        this.vehicle = {
            licenseImagen: '',
            brand: '',
            plateImagen: '',
            imagen: '',
            color: '',
            accepPets: false,
            luggageSpace: false,
            acceptSmok: false
        };
        this.plateImagenFile = null;
        this.vehicleImagenFile = null;
        this.licenseImagenFile = null;
    }
    RegisterPage.prototype.ngOnInit = function () {
        var _this = this;
        this.mapsAPILoader.load()
            .then(function () {
            _this.setAddressWithCurrentPosition();
            _this.geoCoder = new google.maps.Geocoder;
        });
    };
    RegisterPage.prototype.changeFileImagen = function ($event, inputFile) {
        var _this = this;
        if ($event.target.files && $event.target.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL($event.target.files[0]);
            reader.onload = function (event) {
                switch (inputFile) {
                    case 'plateImagen':
                        _this.plateImagenFile = $event.target.files[0];
                        _this.vehicle.plateImagen = event.target.result;
                        break;
                    case 'vehicleImagen':
                        _this.vehicleImagenFile = $event.target.files[0];
                        _this.vehicle.imagen = event.target.result;
                        break;
                    case 'licenseImagen':
                        _this.licenseImagenFile = $event.target.files[0];
                        _this.vehicle.licenseImagen = event.target.result;
                        break;
                }
            };
        }
    };
    RegisterPage.prototype.showHiddenPassword = function () {
        this.iconName = this.iconName === 'eye' ? 'eye-off' : 'eye';
        this.inputType = this.inputType === 'text' ? 'password' : 'text';
    };
    RegisterPage.prototype.signIn = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c;
            var _this = this;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0: return [4 /*yield*/, this.alertsService.presentLoading('Registrando...')];
                    case 1:
                        _d.sent();
                        if (!(this.user.role == 'client')) return [3 /*break*/, 2];
                        if (!this.thereAreEmptyFieldsClient()) {
                            this.authenticationService.signUp(this.user)
                                .then(function () {
                                _this.alertsService.loading.dismiss();
                                _this.cleanClientFields();
                                _this.alertsService.presentAlertConfirmAccept('Ya casi!', 'Por favor revise su correo electronico para activar su cuenta.')
                                    .then(function (result) {
                                    _this.router.navigate(['home/login']);
                                });
                            })["catch"](function (error) {
                                _this.alertsService.loading.dismiss();
                                if (error.message = 'The email address is already in use by another account.')
                                    _this.alertsService.presentAlert('La direccion de correo ya esta siendo usado por otra cuenta.');
                            });
                        }
                        else {
                            this.alertsService.loading.dismiss();
                            this.alertsService.presentAlertWithHeader('Registro!', 'Por favor complete los campos requeridos!');
                        }
                        return [3 /*break*/, 9];
                    case 2:
                        if (!(this.user.role == 'driver')) return [3 /*break*/, 9];
                        if (!!this.thereAreEmptyFieldsDriver()) return [3 /*break*/, 8];
                        if (!!this.thereAreEmptyImagensFields()) return [3 /*break*/, 6];
                        _a = this.vehicle;
                        return [4 /*yield*/, this.imagesService.uploadImage('vehicles', this.plateImagenFile)];
                    case 3:
                        _a.plateImagen = _d.sent();
                        _b = this.vehicle;
                        return [4 /*yield*/, this.imagesService.uploadImage('vehicles', this.vehicleImagenFile)];
                    case 4:
                        _b.imagen = _d.sent();
                        _c = this.vehicle;
                        return [4 /*yield*/, this.imagesService.uploadImage('vehicles', this.licenseImagenFile)];
                    case 5:
                        _c.licenseImagen = _d.sent();
                        this.user.vehicle = this.vehicle;
                        this.user.vehicle.type = '';
                        this.authenticationService.signUp(this.user)
                            .then(function () {
                            _this.alertsService.loading.dismiss();
                            _this.alertsService.presentAlertConfirmAccept('Ya casi!', 'Por favor revise su correo electronico para activar su cuenta.')
                                .then(function () {
                                _this.router.navigate(['home/login']);
                            });
                        })["catch"](function (error) {
                            _this.alertsService.loading.dismiss();
                            if (error.message = 'The email address is already in use by another account.')
                                _this.alertsService.presentAlert('La direccion de correo ya esta siendo usado por otra cuenta.');
                        });
                        return [3 /*break*/, 7];
                    case 6:
                        this.alertsService.loading.dismiss();
                        this.alertsService.presentAlert('Por favor seleccione las imagenes requeridas!');
                        _d.label = 7;
                    case 7: return [3 /*break*/, 9];
                    case 8:
                        this.alertsService.loading.dismiss();
                        this.alertsService.presentAlertWithHeader('Register!', 'Por favor complete los campos requeridos!');
                        _d.label = 9;
                    case 9: return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage.prototype.thereAreEmptyFieldsClient = function () {
        if (this.user.name == '' || this.user.email == '' || this.user.password == '' || this.user.gender == '')
            return true;
        else
            return false;
    };
    RegisterPage.prototype.thereAreEmptyFieldsDriver = function () {
        if (this.user.name == '' || this.user.email == '' || this.user.password == '' || this.user.telephoneNumber == '' || this.user.gender == '' || this.vehicle.model == '' || this.vehicle.color == '')
            return true;
        else
            return false;
    };
    RegisterPage.prototype.thereAreEmptyImagensFields = function () {
        if (this.vehicle.plateImagen == '' || this.vehicle.imagen == '' || this.vehicle.licenseImagen == '')
            return true;
        else
            return false;
    };
    RegisterPage.prototype.cleanClientFields = function () {
        this.user = {
            name: '',
            email: '',
            password: '',
            address: '',
            telephoneNumber: '',
            role: 'client',
            imagen: ''
        };
    };
    RegisterPage.prototype.setAddressWithCurrentPosition = function () {
        var _this = this;
        var options = {
            enableHighAccuracy: false,
            timeout: Infinity,
            maximumAge: 0
        };
        var wait = Geolocation.watchPosition(options, function (position, error) {
            _this.latitude = position.coords.latitude;
            _this.longitude = position.coords.longitude;
            _this.zoom = 8;
            _this.setAddres(_this.latitude, _this.longitude);
        });
    };
    RegisterPage.prototype.setAddres = function (latitude, longitude) {
        var _this = this;
        this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    _this.zoom = 12;
                    _this.user.address = results[0].formatted_address;
                }
                else {
                    console.log('No results found...');
                }
            }
            else {
                console.log('Geocode failed due to: ' + status);
            }
        });
    };
    RegisterPage = __decorate([
        core_1.Component({
            selector: 'app-register',
            templateUrl: './register.page.html',
            styleUrls: ['./register.page.scss']
        })
    ], RegisterPage);
    return RegisterPage;
}());
exports.RegisterPage = RegisterPage;
