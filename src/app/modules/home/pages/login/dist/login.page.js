"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.LoginPage = void 0;
var core_1 = require("@angular/core");
var core_2 = require("@capacitor/core");
var Geolocation = core_2.Plugins.Geolocation;
var LoginPage = /** @class */ (function () {
    function LoginPage(alertsService, router, authenticationService, firestore) {
        this.alertsService = alertsService;
        this.router = router;
        this.authenticationService = authenticationService;
        this.firestore = firestore;
        this.inputType = 'password';
        this.iconName = 'eye';
        this.user = {
            email: '',
            password: ''
        };
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.showHiddenPassword = function () {
        this.iconName = this.iconName === 'eye' ? 'eye-off' : 'eye';
        this.inputType = this.inputType === 'text' ? 'password' : 'text';
    };
    LoginPage.prototype.signIn = function (email, password) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertsService.presentLoading('Cargando...')];
                    case 1:
                        _a.sent();
                        if (this.isEmptyFields()) {
                            this.alertsService.loading.dismiss();
                            this.alertsService.presentAlertWithHeader('Lo sentimos!', 'Por favor ingrese todos los campos!');
                        }
                        else {
                            this.authenticationService.signIn(email, password)
                                .then(function (userFirebase) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    if (userFirebase && !userFirebase.user.emailVerified) {
                                        this.alertsService.presentAlert('Su cuenta no ha sido verificada. Por favor revise su correo.');
                                        this.alertsService.loading.dismiss();
                                        return [2 /*return*/];
                                    }
                                    else {
                                        this.firestore.collection('/users').doc(userFirebase.user.uid).get().subscribe(function (result) { return __awaiter(_this, void 0, void 0, function () {
                                            var userData;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        userData = result.data();
                                                        if (!!userData.isActive) return [3 /*break*/, 2];
                                                        return [4 /*yield*/, this.authenticationService.logoutUser()];
                                                    case 1:
                                                        _a.sent();
                                                        this.alertsService.loading.dismiss();
                                                        this.alertsService.presentAlert('Su cuenta está desactivada, contáctese con iGo por favor!');
                                                        return [3 /*break*/, 3];
                                                    case 2:
                                                        this.cleanFields();
                                                        this.alertsService.loading.dismiss();
                                                        this.router.navigate(['dashboard']);
                                                        _a.label = 3;
                                                    case 3: return [2 /*return*/];
                                                }
                                            });
                                        }); });
                                    }
                                    return [2 /*return*/];
                                });
                            }); })["catch"](function (error) {
                                _this.alertsService.loading.dismiss();
                                _this.alertsService.presentAlert('Usuario y/o contraseña invalidos.');
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.cleanFields = function () {
        this.user = {
            email: '',
            password: ''
        };
    };
    LoginPage.prototype.isEmptyFields = function () {
        if (this.user.email == '' || this.user.password == '')
            return true;
        else
            return false;
    };
    LoginPage.prototype.assingAddres = function () {
        var _this = this;
        var geocoder = new google.maps.Geocoder();
        Geolocation.watchPosition({}, function (position, error) {
            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            geocoder.geocode({ location: pos }, function (results, status) {
                if (status == "OK") {
                    if (results[0]) {
                        _this.user.address = results[0].formatted_address;
                        console.log(_this.user.address);
                    }
                }
            });
        });
    };
    LoginPage.prototype.goToRegister = function () {
        this.cleanFields();
        this.router.navigate(['home/login/register']);
    };
    LoginPage = __decorate([
        core_1.Component({
            selector: 'app-login',
            templateUrl: './login.page.html',
            styleUrls: ['./login.page.scss']
        })
    ], LoginPage);
    return LoginPage;
}());
exports.LoginPage = LoginPage;
