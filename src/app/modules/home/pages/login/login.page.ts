import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { User } from '../../interfaces/user.interface';
import { Plugins } from '@capacitor/core';
import { AngularFirestore } from '@angular/fire/firestore';
const { Geolocation } = Plugins;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
  inputType: string;
  iconName: string;
  user: User;

  constructor(
    private alertsService: AlertsService,
    private router: Router,
    public authenticationService: AuthenticationService,
    private firestore: AngularFirestore,) {
    this.inputType = 'password';
    this.iconName = 'eye';
    this.user = {
      email: '',
      password: ''
    }
  }

  ngOnInit() {
  }

  showHiddenPassword(): void {
    this.iconName = this.iconName === 'eye' ? 'eye-off' : 'eye';
    this.inputType = this.inputType === 'text' ? 'password' : 'text';
  }

  async signIn(email: string, password: string) {
    await this.alertsService.presentLoading('Cargando...');
    if (this.isEmptyFields()) {
      this.alertsService.loading.dismiss();
      this.alertsService.presentAlertWithHeader('Lo sentimos!', 'Por favor ingrese todos los campos!');
    } else {
      this.authenticationService.signIn(email, password)
        .then(
          async (userFirebase) => {
            if (userFirebase && !userFirebase.user.emailVerified) {
              this.alertsService.presentAlert('Su cuenta no ha sido verificada. Por favor revise su correo.');
              this.alertsService.loading.dismiss();
              return;
            }
            else {
              this.firestore.collection('/users').doc(userFirebase.user.uid).get().subscribe(async (result) => {
                const userData = result.data() as User;
                if (!userData.isActive) {
                  await this.authenticationService.logoutUser();
                  this.alertsService.loading.dismiss();
                  this.alertsService.presentAlert('Su cuenta está desactivada, contáctese con iGo por favor!');
                } else {
                  this.cleanFields();
                  this.alertsService.loading.dismiss();
                  this.router.navigate(['dashboard']);
                }
              })
            }
          })
        .catch(error => {
          this.alertsService.loading.dismiss();
          this.alertsService.presentAlert('Usuario y/o contraseña invalidos.');
        })
    }
  }

  cleanFields(): void {
    this.user = {
      email: '',
      password: ''
    }
  }

  isEmptyFields(): boolean {
    if (this.user.email == '' || this.user.password == '')
      return true;
    else
      return false;
  }

  assingAddres() {
    const geocoder = new google.maps.Geocoder();
    Geolocation.watchPosition({}, (position, error) => {
      let pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      geocoder.geocode({ location: pos }, (results, status) => {
        if (status == "OK") {
          if (results[0]) {
            this.user.address = results[0].formatted_address;
            console.log(this.user.address)
          }
        }
      })
    })
  }

  goToRegister(): void {
    this.cleanFields();
    this.router.navigate(['home/login/register']);
  }
}
