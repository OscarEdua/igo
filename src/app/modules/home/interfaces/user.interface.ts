import { Ubication } from "./ubication.interface";
import { Vehicle } from "./vehicle.interface";

export interface User {
    uid?: string;
    name?: string;
    gender?: string;
    role?: string;
    address?: string;
    birthdayDate?: string;
    telephoneNumber?: string;
    email: string;
    imagen?: string;
    ubication?: Ubication;
    vehicle?: Vehicle
    password?: string;
    createAt?: Date;
    updateAt?: Date;
    isActive?: boolean;
}