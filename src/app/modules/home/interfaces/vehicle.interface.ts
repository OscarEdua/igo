import { Category } from './category.interface';

export interface Vehicle {
  luggageCapacity?: string;
  availableSeats?: number;
  status?: string;
  type?: string;
  model?: string;
  color: string;
  licenseImagen: string;
  imagen: string;
  plateImagen: string;
  numberSeats?: number;
  luggageDescription?: string;
  category?: Category;
  maximumNumberPeople?: number;
  brand?: string;
  accepPets?: boolean;
  luggageSpace?: boolean;
  acceptSmok?: boolean;
  createAt?: Date;
  updateAt?: Date;
  isActive?:  boolean;
}
