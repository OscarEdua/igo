import { NgModule } from '@angular/core';
import { HomeRoutingModule } from 'src/app/modules/home/home-routing.module';

@NgModule({
  declarations: [],
  imports: [
    HomeRoutingModule
  ]
})
export class HomeModule { }
