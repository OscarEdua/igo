import { Component, OnInit } from '@angular/core';
import { AlertsService } from '../../../../shared/services/alerts.service';
import { TransferModalPage } from '../modals/transfer-modal/transfer-modal.page';
import { MapsService } from '../../../../shared/services/maps.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.page.html',
  styleUrls: ['./transfer.page.scss'],
})
export class TransferPage implements OnInit {
  travelId: string;

  constructor(private alertsService: AlertsService, private mapsService: MapsService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.travelId = this.activatedRoute.snapshot.paramMap.get('travelId');
  }

  openModal(): void {
    //this.alertsService.presentModalWithoutTravelData(TransferModalPage, 'high-modal');
    this.alertsService.presentModalWithData(TransferModalPage, { travelId: this.travelId }, 'high-modal');
  }

  ionViewWillEnter() {
    this.openModal();
    this.mapsService.loadmap('trasfer-map');
  }
}
