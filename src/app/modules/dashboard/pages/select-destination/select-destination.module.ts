import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SelectDestinationPageRoutingModule } from './select-destination-routing.module';
import { SelectDestinationPage } from './select-destination.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectDestinationPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelectDestinationPage]
})
export class SelectDestinationPageModule {}
