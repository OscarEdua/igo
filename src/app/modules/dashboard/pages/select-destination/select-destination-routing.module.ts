import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectDestinationPage } from './select-destination.page';

const routes: Routes = [
  {
    path: '',
    component: SelectDestinationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectDestinationPageRoutingModule {}
