import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { SelectDestinationModalPage } from '../modals/select-destination-modal/select-destination-modal.page';
import { MapsService } from 'src/app/shared/services/maps.service';
import { Plugins } from '@capacitor/core';
import { Marker } from '../../interfaces/marker';
import { Stop } from '../../interfaces/stop.interface';
import { Observable } from 'rxjs';
import { StopsService } from '../../services/admin/stops.service';
const { Geolocation } = Plugins;

@Component({
  selector: 'app-select-destination',
  templateUrl: './select-destination.page.html',
  styleUrls: ['./select-destination.page.scss'],
})

export class SelectDestinationPage implements OnInit {
  markers = [];
  marker: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  stopsMark: Stop[];
  stops: Observable<Stop[]>;
  markerStops: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  constructor(private alertsService: AlertsService, private mapsService: MapsService,    private stopsService: StopsService) { }

  ngOnInit() {
  }

  openModal(): void {
    this.alertsService.presentModalWithoutTravelData(SelectDestinationModalPage, 'select-stop-modal');
  }

  ionViewWillEnter() {
    this.loadDataPageDashboard();
    this.stops = this.stopsService.getStop();
    this.stops.subscribe((res) => {
      this.stopsMark = res as Stop[];
      this.generateMarks();
    })
  }
  async generateMarks() {
    await this.deleteMarkers();
    this.stopsMark.map((res) => {
      this.markerStops.position.lat = res.lat;
      this.markerStops.position.lng = res.lng;
      this.markerStops.title = res.name;
      this.addMarker(this.markerStops, this.markerStops.title, 'stops');
    });
  }
  
  async loadDataPageDashboard() {
    await this.mapsService.loadmap('map');
    this.openModal();
    this.generateMarksMyLocation();
  }
  
  generateMarksMyLocation() {
    Geolocation.getCurrentPosition().then((res) => {
      this.marker.position.lat = res.coords.latitude;
      this.marker.position.lng = res.coords.longitude;
      this.marker.title = 'Mi Ubicación';
      this.addMarker(this.marker, this.marker.title, 'myLocation');
    });
  }

  addMarker(marker: Marker, name: String, typeMarket: string) {
    let marke;
    let icon;
    if (typeMarket == 'myLocation') {
      icon = {
        url: '../../assets/img/ic_loc.png',
        //size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(60, 60),
      };
      /*     const image = '../../assets/img/Pineapplemenu.png'; */
    } else if (typeMarket == 'stops') {
      icon = {
        url: '../../assets/img/flag.png',
        //size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(25, 25),
      };
    }
    const geocoder = new google.maps.Geocoder();
    marke = new google.maps.Marker({
      position: marker.position,
      //  draggable: true,
      // animation: google.maps.Animation.DROP,
      map: this.mapsService.map,
      icon: icon,
      title: marker.title,
    });
    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          marke.getPosition().lat();
          marke.getPosition().lng();
          results[0].formatted_address;
        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
    const infoWindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marke, 'click', () => {
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            infoWindow.setContent(
              '<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address
            );
            if (infoWindow !== null) {
            }
            infoWindow.open(this.mapsService.map, marke);
          } else {
          }
        } else {
        }
      });
    });
    this.markers.push(marke);
  }

  deleteMarkers() {
    this.setMapOnAll(null);
    this.markers = [];
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  ionViewWillLeave() {
    this.deleteMarkers();
  }
}
