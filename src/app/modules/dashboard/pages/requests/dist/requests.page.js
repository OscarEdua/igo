"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RequestsPage = void 0;
var core_1 = require("@angular/core");
var RequestsPage = /** @class */ (function () {
    function RequestsPage(travelsService, alertsService, router) {
        this.travelsService = travelsService;
        this.alertsService = alertsService;
        this.router = router;
    }
    RequestsPage.prototype.ngOnInit = function () {
        this.travels = this.travelsService.getTravels();
        //this.travels.subscribe(travel => console.log(travel));
    };
    RequestsPage.prototype.accept = function (travelId) {
        var _this = this;
        this.alertsService.presentLoading('Aceptando...');
        this.travelsService.acceptTravel(travelId)
            .then(function () {
            _this.alertsService.loading.dismiss();
            _this.alertsService.presentAlertConfirmAccept('Solicitud', 'Por favor espere mientras en cliente realiza el pago.');
        })["catch"](function () {
            _this.alertsService.loading.dismiss();
            _this.alertsService.presentAlertWithHeader('Solicitudes', 'Lo sentimos, por favor intente nuevamente');
        });
    };
    RequestsPage.prototype.getWholePart = function (cost) {
        return Math.trunc(cost);
    };
    RequestsPage.prototype.getDecimalPart = function (cost) {
        return (cost.toFixed(2)).split('.')[1];
    };
    RequestsPage = __decorate([
        core_1.Component({
            selector: 'app-requests',
            templateUrl: './requests.page.html',
            styleUrls: ['./requests.page.scss']
        })
    ], RequestsPage);
    return RequestsPage;
}());
exports.RequestsPage = RequestsPage;
