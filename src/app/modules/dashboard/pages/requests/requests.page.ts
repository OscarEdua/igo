import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TravelsService } from '../../services/admin/travels.service';
import { AlertsService } from '../../../../shared/services/alerts.service';
import { Router } from '@angular/router';
import { PushService } from 'src/app/shared/services/push.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.page.html',
  styleUrls: ['./requests.page.scss'],
})
export class RequestsPage implements OnInit {
  oTravels: Observable<any>;
  travels: any[];
  suscription: any;
  searchText: string;
  
  constructor(
    private travelsService: TravelsService, 
    private alertsService: AlertsService, 
    private pushService: PushService,
    private router: Router
    ) { }

  ngOnInit() {
    this.oTravels = this.travelsService.getTravels();
    this.suscription = this.oTravels.subscribe(travels => this.travels = travels);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value; 
  }

  ionViewWillLeave() {
    this.suscription.unsuscribe();
  }

  accept(travelId: string) {
    this.alertsService.presentLoading('Aceptando...');
    this.travelsService.acceptTravel(travelId)
    .then(() => {
      this.alertsService.loading.dismiss();
      this.alertsService.presentAlertConfirmAccept('Solicitud aceptada', 'Por favor espere mientras el cliente realiza el pago.');
      //this.pushService.sendByUid('IGO','','','',this.user.uid)
    })
    .catch(() => {
      this.alertsService.loading.dismiss();
      this.alertsService.presentAlertWithHeader('Solicitudes', 'Lo sentimos, por favor intente nuevamente');
    
    });
  }

  async toTravel(travelId: string) {
    await this.alertsService.presentLoading('Esperando...');
    const travels = this.travels.filter(travel => travel.travelId === travelId);
    if (travels[0].status === 'paid') {
      this.alertsService.loading.dismiss();
      this.router.navigate(['dashboard/travel', travelId, 'driver']);
    } else {
      this.alertsService.loading.dismiss();
      this.alertsService.presentAlertConfirmAccept('Esperando pago...', 'Por favor espere mientras el cliente realiza el pago.');
    }
  }
  
  getWholePart(cost: number): number {
    return Math.trunc(cost);
  }
  
  getDecimalPart(cost: number): string {
    return (cost.toFixed(2)).split('.')[1];
  }
}
