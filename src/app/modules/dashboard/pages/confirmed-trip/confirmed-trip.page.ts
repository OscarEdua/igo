import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AnimationController } from '@ionic/angular';

@Component({
  selector: 'app-confirmed-trip',
  templateUrl: './confirmed-trip.page.html',
  styleUrls: ['./confirmed-trip.page.scss'],
})
export class ConfirmedTripPage implements OnInit {
  @ViewChild('circle') circle: ElementRef;
  @ViewChild('circleSmall') circleSmall: ElementRef;
  @ViewChild('circleVerySmall') circleVerySmall: ElementRef;
  constructor(
    private animationCtrl: AnimationController,
    public element: ElementRef
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.animationAllCircles();
  }

  animationAllCircles() {
    this.animationIncreaseCircle(this.circleVerySmall.nativeElement).play();
    this.animationIncreaseCircle(this.circleSmall.nativeElement).play();
    this.animationIncreaseCircle(this.circle.nativeElement).play();
  }

  animationIncreaseCircle(element: any) {
    const animation = this.animationCtrl.create();
    animation
      .addElement(element)
      .duration(2500)
      .iterations(Infinity)
      .fromTo('transform', 'scale(1)', 'scale(1.3)')
      .fromTo('opacity', '1', '0.7');
    return animation;
  }
}
