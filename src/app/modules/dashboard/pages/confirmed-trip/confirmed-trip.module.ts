import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmedTripPageRoutingModule } from './confirmed-trip-routing.module';

import { ConfirmedTripPage } from './confirmed-trip.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmedTripPageRoutingModule
  ],
  declarations: [ConfirmedTripPage]
})
export class ConfirmedTripPageModule {}
