"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TravelCompletePage = void 0;
var core_1 = require("@angular/core");
var TravelCompletePage = /** @class */ (function () {
    function TravelCompletePage(menuController, alertsService, activatedRoute, travelsService, router) {
        this.menuController = menuController;
        this.alertsService = alertsService;
        this.activatedRoute = activatedRoute;
        this.travelsService = travelsService;
        this.router = router;
        this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
    }
    TravelCompletePage.prototype.ngOnInit = function () {
        this.loadTravelData();
    };
    TravelCompletePage.prototype.loadTravelData = function () {
        var _this = this;
        this.travelsService.getTravelId(this.travelId).subscribe(function (data) {
            _this.travelData = data;
            console.log(_this.travelData);
        });
    };
    TravelCompletePage.prototype.redirectTo = function (pageName) {
        this.alertsService.closeModal();
        this.router.navigate(['dashboard/' + pageName]);
    };
    TravelCompletePage.prototype.openMenu = function () {
        this.menuController.open('principal');
    };
    TravelCompletePage = __decorate([
        core_1.Component({
            selector: 'app-travel-complete',
            templateUrl: './travel-complete.page.html',
            styleUrls: ['./travel-complete.page.scss']
        })
    ], TravelCompletePage);
    return TravelCompletePage;
}());
exports.TravelCompletePage = TravelCompletePage;
