import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TravelCompletePage } from './travel-complete.page';

const routes: Routes = [
  {
    path: '',
    component: TravelCompletePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TravelCompletePageRoutingModule {}
