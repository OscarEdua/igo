import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { Travel } from '../../interfaces/travel.interface';
import { TravelsService } from '../../services/admin/travels.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-travel-complete',
  templateUrl: './travel-complete.page.html',
  styleUrls: ['./travel-complete.page.scss'],
})
export class TravelCompletePage implements OnInit {
  travelId: string;
  travelData: Travel ;
  travelPrice: Observable<any>;


  constructor(
    private menuController: MenuController,
    private alertsService: AlertsService,
    private activatedRoute: ActivatedRoute,
    private travelsService: TravelsService,
    private router: Router,

  ) {
    this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
   }

  ngOnInit() {
    this.loadTravelData();
    this.travelPrice = this.travelsService.getTravelAndUserById(this.travelId, "driver");
  }

  loadTravelData(){
    this.travelsService.getTravelId(this.travelId).subscribe((data)=>{
      this.travelData = data as Travel;
    });
  }

  redirectTo(pageName: string): void {
    this.alertsService.closeModal();
    this.router.navigate(['dashboard/' + pageName]);
  }

  openMenu() {    
    this.menuController.open('principal');
  }

}
