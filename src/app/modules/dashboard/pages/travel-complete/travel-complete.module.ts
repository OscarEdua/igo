import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TravelCompletePageRoutingModule } from './travel-complete-routing.module';

import { TravelCompletePage } from './travel-complete.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TravelCompletePageRoutingModule,
    ComponentsModule

  ],
  declarations: [TravelCompletePage]
})
export class TravelCompletePageModule {}
