import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { InTravelModalPage } from '../modals/in-travel-modal/in-travel-modal.page';
import { MenuController } from '@ionic/angular';
import { MapsService } from '../../../../shared/services/maps.service';
import { Marker } from '../../interfaces/marker';
import { ActivatedRoute } from '@angular/router';
const { Geolocation } = Plugins;
import { Plugins } from '@capacitor/core';
import { TravelsService } from '../../services/admin/travels.service';
import { Location } from '../../interfaces/location.interface';
import { Observable } from 'rxjs';
let marke;

@Component({
  selector: 'app-travel',
  templateUrl: './travel.page.html',
  styleUrls: ['./travel.page.scss'],
})

export class TravelPage implements OnInit {
  markers = [];
  marker: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  current: Location = {
    address: '',
    lat: 0,
    lng: 0
  }
  isTracking = true;
  travelId: string;
  watch: string;
  oTravel: Observable<any>;
  travel: any;
  origin = { lat: 0, lng: 0 };
  destination = { lat: 0, lng: 0 };
  roleCurrentUser: string;

  constructor(
    private alertsService: AlertsService,
    private menuController: MenuController,
    private mapsService: MapsService,
    private activatedRoute: ActivatedRoute,
    private travelsService: TravelsService

  ) {
    this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
    this.roleCurrentUser = activatedRoute.snapshot.paramMap.get('roleCurrentUser');
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loadDataPageDashboard();
    this.loadData();
  }
  
  async loadData() {
    await this.getTravel();
    this.origin.lat = this.travel.startLocation.lat;
    this.origin.lng = this.travel.startLocation.lng;
    this.destination.lat = this.travel.destinyLocation.lat;
    this.destination.lng = this.travel.destinyLocation.lng;
    this.calculateRoute();
  }
  
  getTravel() {
    return new Promise((resolve) => {
      this.oTravel = this.travelsService.getTravelAndUserById(this.travelId, 'client');
      this.oTravel.subscribe(travel => {
        this.travel = travel;
        resolve(this.travel);
      });
    })
  }

  private calculateRoute() {
    this.mapsService.directionsService.route(
      {
        // paramentros de la ruta  inicial y final
        origin: this.origin,
        destination: this.destination,
        // Que  vehiculo se usa  para realizar el viaje
        travelMode: google.maps.TravelMode.DRIVING,
      },
      (response, status) => {
        const sDistance = response.routes[0].legs[0].distance.text;

        if (status === google.maps.DirectionsStatus.OK) {
          this.mapsService.directionsDisplay.setDirections(response);
        } else {
          //  alert('Could not display directions due to: ' + status);
        }
      }
    );
  }

  async loadDataPageDashboard() {
    await this.mapsService.loadmap('travelmap');
    this.generateMarksMyLocation();
    this.openModal();
  }

  generateMarksMyLocation() {
    Geolocation.getCurrentPosition().then(async (res) => {
      this.marker.position.lat = res.coords.latitude;
      this.marker.position.lng = res.coords.longitude;
      this.marker.title = 'Mi Ubicación';
      await this.addMarker(this.marker, this.marker.title, 'myLocation');
      this.traking();
    });
  }

  traking() {
    //let market;
    if (this.markers[0] != undefined) {
      this.watch = Geolocation.watchPosition({}, (position, err) => {
        let result = [position.coords.latitude, position.coords.longitude];
        var latlng = new google.maps.LatLng(result[0], result[1]);
        marke.setPosition(latlng); // this.transition(result);
        /* this.mapsService.map.setCenter(latlng); */
        this.current.lat = position.coords.latitude;
        this.current.lng = position.coords.longitude;
       // this.travelsService.updateCurrentLocation(this.travelId, this.current)
      });
    }
  }

  // Unsubscribe from the geolocation watch using the initial ID
  stopTracking() {
    Geolocation.clearWatch({ id: this.watch }).then(() => {
      this.isTracking = false;
    });
  }

  addMarker(marker: Marker, name: String, typeMarket: string) {
    return new Promise((resolve) => {
      let icon;
      if (typeMarket == 'myLocation') {
        icon = {
          url: '../../assets/img/auto.png',
          //ic_loc size: new google.maps.Size(100, 100),
          origin: new google.maps.Point(0, 0),
          //    anchor: new google.maps.Point(34, 60),
          scaledSize: new google.maps.Size(40, 65),
        };
        /*     const image = '../../assets/img/Pineapplemenu.png'; */
      } else if (typeMarket == 'stops') {
        icon = {
          url: '../../assets/img/flag.png',
          //size: new google.maps.Size(100, 100),
          origin: new google.maps.Point(0, 0),
          //    anchor: new google.maps.Point(34, 60),
          scaledSize: new google.maps.Size(25, 25),
        };
      }
      const geocoder = new google.maps.Geocoder();
      marke = new google.maps.Marker({
        position: marker.position,
        //  draggable: true,
        // animation: google.maps.Animation.DROP,
        map: this.mapsService.map,
        icon: icon,
        title: marker.title,
      });
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            marke.getPosition().lat();
            marke.getPosition().lng();
            results[0].formatted_address;
          } else {
            console.log('No results found');
          }
        } else {
          console.log('Geocoder failed due to: ' + status);
        }
      });
      const infoWindow = new google.maps.InfoWindow();
      google.maps.event.addListener(marke, 'click', () => {
        geocoder.geocode({ location: marke.position }, (results, status) => {
          if (status === 'OK') {
            if (results[0]) {
              infoWindow.setContent(
                '<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address
              );
              if (infoWindow !== null) {
              }
              infoWindow.open(this.mapsService.map, marke);
            } else {
            }
          } else {
          }
        });
      });
      this.markers.push(marke);
      resolve(marke)
    })
    this.traking();
  }

  openModal(): void {
    //this.alertsService.presentModalWithoutTravelData(
    //InTravelModalPage,
    //'my-custom-class-complete'
    //);
    this.alertsService.presentModalWithData(
      InTravelModalPage,
      {
        travelId: this.travelId,
        roleCurrentUser: this.roleCurrentUser
      },
      'my-custom-class-complete'
    );
  }

  ionViewDidLeave() {
    this.stopTracking();
  }

  openMenu() {
    this.menuController.open('principal');
  }
}
