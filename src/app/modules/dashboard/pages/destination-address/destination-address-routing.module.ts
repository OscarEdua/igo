import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DestinationAddressPage } from './destination-address.page';

const routes: Routes = [
  {
    path: '',
    component: DestinationAddressPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DestinationAddressPageRoutingModule {}
