import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DestinationAddressPageRoutingModule } from './destination-address-routing.module';
import { DestinationAddressPage } from './destination-address.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DestinationAddressPageRoutingModule,
    ComponentsModule
  ],
  declarations: [DestinationAddressPage]
})

export class DestinationAddressPageModule {}
