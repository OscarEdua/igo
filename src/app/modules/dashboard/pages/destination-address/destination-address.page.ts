import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { DestinationAddressModalPage } from '../modals/destination-address-modal/destination-address-modal.page';

@Component({
  selector: 'app-destination-address',
  templateUrl: './destination-address.page.html',
  styleUrls: ['./destination-address.page.scss'],
})

export class DestinationAddressPage implements OnInit {

  constructor(private alertsService: AlertsService) { }

  ngOnInit() {
  }

  openModal(): void {
    this.alertsService.presentModalWithoutTravelData(DestinationAddressModalPage, 'destination-address-modal');
  }
}
