import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { RecommendationsModalPage } from '../modals/recommendations-modal/recommendations-modal.page';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { MapsService } from '../../../../shared/services/maps.service';
import { Marker } from '../../interfaces/marker';
const { Geolocation } = Plugins;

import { Plugins } from '@capacitor/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { UsersService } from 'src/app/modules/home/services/users.service';
import { TravelsService } from '../../services/admin/travels.service';
import { Travel } from '../../interfaces/travel.interface';

@Component({
  selector: 'app-recommendations',
  templateUrl: './recommendations.page.html',
  styleUrls: ['./recommendations.page.scss'],
})
export class RecommendationsPage implements OnInit {
  travel: Travel;
  travelId: string;
  driver: User;
  driverId: string;
  markers = [];
  marker: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };

  constructor(
    private menuController: MenuController,
    private alertsService: AlertsService,
    private mapsService: MapsService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
   }

  ngOnInit() {   
  }

  ionViewDidEnter() {
    this.loadDataPageDashboard();    
  }

  async loadDataPageDashboard() {
    try {
        await this.mapsService.loadmap('recommendations-map');
        this.generateMarksMyLocation();
        this.openModal();
    } catch (error) {
      console.log(error)
    }
  
  }

  generateMarksMyLocation() {
    Geolocation.getCurrentPosition().then((res) => {
      this.marker.position.lat = res.coords.latitude;
      this.marker.position.lng = res.coords.longitude;
      this.marker.title = 'Mi Ubicación';
      this.addMarker(this.marker, this.marker.title, 'myLocation');
    });
  }

  addMarker(marker: Marker, name: String, typeMarket: string) {
    let marke;
    let icon;
    if (typeMarket == 'myLocation') {
      icon = {
        url: '../../assets/img/ic_loc.png',
        //ic_loc size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(60, 60),
      };
      /*     const image = '../../assets/img/Pineapplemenu.png'; */
    } else if (typeMarket == 'stops') {
      icon = {
        url: '../../assets/img/flag.png',
        //size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(25, 25),
      };
    }

    const geocoder = new google.maps.Geocoder();
    marke = new google.maps.Marker({
      position: marker.position,
      //  draggable: true,
      // animation: google.maps.Animation.DROP,
      map: this.mapsService.map,
      icon: icon,
      title: marker.title,
    });


    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          marke.getPosition().lat();
          marke.getPosition().lng();
          results[0].formatted_address;
        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
    const infoWindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marke, 'click', () => {
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            infoWindow.setContent(
              '<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address
            );
            if (infoWindow !== null) {
            }
            infoWindow.open(this.mapsService.map, marke);
          } else {
          }
        } else {
        }
      });
    });
    this.markers.push(marke);
  }
  
  openModal(): void {
    this.alertsService.presentModalWithData(
      RecommendationsModalPage,
      {travelId: this.travelId},
      'my-custom-class-recommendations'
    );
  }
  openMenu() {
    
    this.menuController.open('principal');
  }

}
