"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.RecommendationsPage = void 0;
var core_1 = require("@angular/core");
var recommendations_modal_page_1 = require("../modals/recommendations-modal/recommendations-modal.page");
var Geolocation = core_2.Plugins.Geolocation;
var core_2 = require("@capacitor/core");
var RecommendationsPage = /** @class */ (function () {
    function RecommendationsPage(menuController, alertsService, mapsService, activatedRoute) {
        this.menuController = menuController;
        this.alertsService = alertsService;
        this.mapsService = mapsService;
        this.activatedRoute = activatedRoute;
        this.markers = [];
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: ''
        };
        this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
    }
    RecommendationsPage.prototype.ngOnInit = function () {
    };
    RecommendationsPage.prototype.ionViewDidEnter = function () {
        this.loadDataPageDashboard();
    };
    RecommendationsPage.prototype.loadDataPageDashboard = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.mapsService.loadmap('recommendations-map')];
                    case 1:
                        _a.sent();
                        this.generateMarksMyLocation();
                        this.openModal();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    RecommendationsPage.prototype.generateMarksMyLocation = function () {
        var _this = this;
        Geolocation.getCurrentPosition().then(function (res) {
            _this.marker.position.lat = res.coords.latitude;
            _this.marker.position.lng = res.coords.longitude;
            _this.marker.title = 'Mi Ubicación';
            _this.addMarker(_this.marker, _this.marker.title, 'myLocation');
        });
    };
    RecommendationsPage.prototype.addMarker = function (marker, name, typeMarket) {
        var _this = this;
        var marke;
        var icon;
        if (typeMarket == 'myLocation') {
            icon = {
                url: '../../assets/img/ic_loc.png',
                //ic_loc size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(60, 60)
            };
            /*     const image = '../../assets/img/Pineapplemenu.png'; */
        }
        else if (typeMarket == 'stops') {
            icon = {
                url: '../../assets/img/flag.png',
                //size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                //    anchor: new google.maps.Point(34, 60),
                scaledSize: new google.maps.Size(25, 25)
            };
        }
        var geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            //  draggable: true,
            // animation: google.maps.Animation.DROP,
            map: this.mapsService.map,
            icon: icon,
            title: marker.title
        });
        geocoder.geocode({ location: marke.position }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                    console.log('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
        var infoWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marke, 'click', function () {
            geocoder.geocode({ location: marke.position }, function (results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
                        if (infoWindow !== null) {
                        }
                        infoWindow.open(_this.mapsService.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
    };
    RecommendationsPage.prototype.openModal = function () {
        this.alertsService.presentModalWithData(recommendations_modal_page_1.RecommendationsModalPage, { travelId: this.travelId }, 'my-custom-class-recommendations');
    };
    RecommendationsPage.prototype.openMenu = function () {
        this.menuController.open('principal');
    };
    RecommendationsPage = __decorate([
        core_1.Component({
            selector: 'app-recommendations',
            templateUrl: './recommendations.page.html',
            styleUrls: ['./recommendations.page.scss']
        })
    ], RecommendationsPage);
    return RecommendationsPage;
}());
exports.RecommendationsPage = RecommendationsPage;
