import { Component, OnInit, Input } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { Router } from '@angular/router';
import { Travel } from '../../../interfaces/travel.interface';
import { StorageService } from 'src/app/shared/services/storage.service';
import { Observable } from 'rxjs';
import { User } from '../../../../home/interfaces/user.interface';
import { UsersService } from 'src/app/modules/home/services/users.service';
import { filter, map, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-select-vehicle-type',
  templateUrl: './select-vehicle-type.page.html',
  styleUrls: ['./select-vehicle-type.page.scss'],
})
export class SelectVehicleTypePage implements OnInit {
  travel: Travel;
  drivers: User[];
  isTravelNow: boolean;

  constructor(public alertsService: AlertsService, private router: Router, private storageService: StorageService, private usersService: UsersService) {
    this.travel = {
      numberTravel: 1,
      numberPassengers: null
    };
  }

  ngOnInit() {
    this.usersService.readDrivers().subscribe(drivers => this.drivers = drivers);
  }

  redirectTo(pageName: string, vehicleType: string): void {
    this.travel.vehicleType = vehicleType;
    const sTravel = JSON.stringify(this.travel);
    this.storageService.set('travel', sTravel);
    if (this.isNumberPassengersEntered()) {
      if (this.isNumberPassengersEnteredNegative()) 
        this.alertsService.presentAlertWithHeader('Tipo de vehiculo', 'Por favor ingresa una cantidad de pasajeros valida.');
      else {
        const filterDrivers = this.drivers.filter(driver => driver.vehicle.availableSeats >= this.travel.numberPassengers);
        if (filterDrivers.length > 0) { 
          this.alertsService.closeModal();
          if (!this.isTravelNow) {
            this.router.navigate(['dashboard/schedule-trip']);
          } else {
            this.router.navigate(['dashboard/' + pageName]);
          }
        } else 
          this.alertsService.presentAlertWithHeader('Sin vehiculos', 'Lo sentimos, no tenemos vehiculos con ' + this.travel.numberPassengers + ' asientos disponibles, por favor intente con otra cantidad.');
      }
    } else {
      this.alertsService.presentAlertWithHeader('Tipo de vehiculo', 'Por favor ingresa la cantidad de pasajeros.');
    }
  }

  isNumberPassengersEntered(): boolean {
    if (this.travel.numberPassengers == 0)  
      return false;
    else 
      return true;
  }

  isNumberPassengersEnteredNegative(): boolean {
    if (this.travel.numberPassengers > 0) 
      return false;
    else 
      return true;
  }
}
