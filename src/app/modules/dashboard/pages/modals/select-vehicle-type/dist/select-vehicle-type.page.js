"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SelectVehicleTypePage = void 0;
var core_1 = require("@angular/core");
var SelectVehicleTypePage = /** @class */ (function () {
    function SelectVehicleTypePage(alertsService, router, storageService, usersService) {
        this.alertsService = alertsService;
        this.router = router;
        this.storageService = storageService;
        this.usersService = usersService;
        this.travel = {
            numberTravel: 1,
            numberPassengers: null
        };
    }
    SelectVehicleTypePage.prototype.ngOnInit = function () {
        var _this = this;
        this.usersService.readDrivers().subscribe(function (drivers) { return _this.drivers = drivers; });
    };
    SelectVehicleTypePage.prototype.redirectTo = function (pageName, vehicleType) {
        var _this = this;
        this.travel.vehicleType = vehicleType;
        var sTravel = JSON.stringify(this.travel);
        this.storageService.set('travel', sTravel);
        if (this.isNumberPassengersEntered()) {
            if (this.isNumberPassengersEnteredNegative())
                this.alertsService.presentAlertWithHeader('Tipo de vehiculo', 'Por favor ingresa una cantidad de pasajeros valida.');
            else {
                var filterDrivers = this.drivers.filter(function (driver) { return driver.vehicle.availableSeats >= _this.travel.numberPassengers; });
                if (filterDrivers.length > 0) {
                    this.alertsService.closeModal();
                    this.router.navigate(['dashboard/' + pageName]);
                }
                else
                    this.alertsService.presentAlertWithHeader('Sin vehiculos', 'Lo sentimos, no tenemos vehiculos con ' + this.travel.numberPassengers + ' asientos disponibles, por favor intente con otra cantidad.');
            }
        }
        else {
            this.alertsService.presentAlertWithHeader('Tipo de vehiculo', 'Por favor ingresa la cantidad de pasajeros.');
        }
    };
    SelectVehicleTypePage.prototype.isNumberPassengersEntered = function () {
        if (this.travel.numberPassengers == 0)
            return false;
        else
            return true;
    };
    SelectVehicleTypePage.prototype.isNumberPassengersEnteredNegative = function () {
        if (this.travel.numberPassengers > 0)
            return false;
        else
            return true;
    };
    SelectVehicleTypePage = __decorate([
        core_1.Component({
            selector: 'app-select-vehicle-type',
            templateUrl: './select-vehicle-type.page.html',
            styleUrls: ['./select-vehicle-type.page.scss']
        })
    ], SelectVehicleTypePage);
    return SelectVehicleTypePage;
}());
exports.SelectVehicleTypePage = SelectVehicleTypePage;
