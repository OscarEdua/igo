import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SelectVehicleTypePageRoutingModule } from './select-vehicle-type-routing.module';
import { SelectVehicleTypePage } from './select-vehicle-type.page';
import { ComponentsModule } from '../../../components/components.module';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectVehicleTypePageRoutingModule,
    ComponentsModule,
  ],
  declarations: [SelectVehicleTypePage]
})

export class SelectVehicleTypePageModule {}
