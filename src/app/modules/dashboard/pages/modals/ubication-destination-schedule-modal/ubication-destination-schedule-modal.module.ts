import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { UbicationDestinationScheduleModalPageRoutingModule } from './ubication-destination-schedule-modal-routing.module';
import { UbicationDestinationScheduleModalPage } from './ubication-destination-schedule-modal.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UbicationDestinationScheduleModalPageRoutingModule,
    ComponentsModule
  ],
  declarations: [UbicationDestinationScheduleModalPage]
})

export class UbicationDestinationScheduleModalPageModule {}
