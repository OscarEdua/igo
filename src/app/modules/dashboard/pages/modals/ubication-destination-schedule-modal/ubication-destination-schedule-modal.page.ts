import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ubication-destination-schedule-modal',
  templateUrl: './ubication-destination-schedule-modal.page.html',
  styleUrls: ['./ubication-destination-schedule-modal.page.scss'],
})

export class UbicationDestinationScheduleModalPage implements OnInit {

  constructor(public alertsService: AlertsService, private router: Router) { }

  ngOnInit() {
  }

  redirectTo(pageName: string): void {
    this.alertsService.closeModal();
    this.router.navigate(['dashboard/' + pageName]);
  }
}
