import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UbicationDestinationScheduleModalPage } from './ubication-destination-schedule-modal.page';

const routes: Routes = [
  {
    path: '',
    component: UbicationDestinationScheduleModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UbicationDestinationScheduleModalPageRoutingModule {}
