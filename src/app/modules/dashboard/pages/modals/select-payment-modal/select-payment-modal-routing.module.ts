import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectPaymentModalPage } from './select-payment-modal.page';

const routes: Routes = [
  {
    path: '',
    component: SelectPaymentModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectPaymentModalPageRoutingModule {}
