"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.SelectPaymentModalPage = void 0;
var core_1 = require("@angular/core");
var SelectPaymentModalPage = /** @class */ (function () {
    function SelectPaymentModalPage(renderer, alertsService, storageService, mapsService, router, travelsService, activatedRoute) {
        this.renderer = renderer;
        this.alertsService = alertsService;
        this.storageService = storageService;
        this.mapsService = mapsService;
        this.router = router;
        this.travelsService = travelsService;
        this.activatedRoute = activatedRoute;
        // inicio de la ruta
        this.origin = { lat: 0, lng: 0 };
        // Fin d e  la ruta
        this.destination = { lat: 0, lng: 0 };
    }
    SelectPaymentModalPage.prototype.ngOnInit = function () { };
    SelectPaymentModalPage.prototype.ionViewWillEnter = function () {
        this.loadData();
    };
    SelectPaymentModalPage.prototype.loadData = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getTravel()];
                    case 1:
                        _a.sent();
                        this.origin.lat = this.travel.startLocation.lat;
                        this.origin.lng = this.travel.startLocation.lng;
                        this.destination.lat = this.travel.destinyLocation.lat;
                        this.destination.lng = this.travel.destinyLocation.lng;
                        this.calculateRoute();
                        return [2 /*return*/];
                }
            });
        });
    };
    SelectPaymentModalPage.prototype.calculateRoute = function () {
        var _this = this;
        this.mapsService.directionsService.route({
            // paramentros de la ruta  inicial y final
            origin: this.origin,
            destination: this.destination,
            // Que  vehiculo se usa  para realizar el viaje
            travelMode: google.maps.TravelMode.DRIVING
        }, function (response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                _this.mapsService.directionsDisplay.setDirections(response);
            }
            else {
                //  alert('Could not display directions due to: ' + status);
            }
        });
    };
    SelectPaymentModalPage.prototype.confirmPaymentMethod = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertsService.presentLoading('Confirmando...')];
                    case 1:
                        _a.sent();
                        if (this.numberCard === undefined) {
                            this.alertsService.loading.dismiss();
                            this.alertsService.presentAlertWithHeader("Método de pago", "Por favor seleccion un método de pago");
                        }
                        else {
                            if (this.numberCard === 1) {
                                this.travelsService.updatePayMethod(this.travelId, 'card')
                                    .then(function () {
                                    _this.alertsService.loading.dismiss();
                                })["catch"](function (error) {
                                    _this.alertsService.loading.dismiss();
                                    console.log(error);
                                });
                            }
                            else if (this.numberCard === 2) {
                                this.travelsService.updatePayMethod(this.travelId, 'transfer')
                                    .then(function () {
                                    _this.alertsService.loading.dismiss();
                                    _this.alertsService.closeModal();
                                    _this.router.navigate(['dashboard/transfer/' + _this.travelId]);
                                })["catch"](function (error) {
                                    _this.alertsService.loading.dismiss();
                                    console.log(error);
                                });
                            }
                            else if (this.numberCard === 3) {
                                this.travelsService.updatePayMethod(this.travelId, 'cash')
                                    .then(function () {
                                    _this.alertsService.loading.dismiss();
                                    _this.alertsService.closeModal();
                                    _this.router.navigate(['dashboard/travel', _this.travelId, 'client'])
                                        .then(function (nav) { return console.log(nav); })["catch"](function (error) { return console.log(error); });
                                })["catch"](function (error) {
                                    _this.alertsService.loading.dismiss();
                                    console.log(error);
                                });
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    SelectPaymentModalPage.prototype.getTravel = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.storageService.get('travel').then(function (travel) {
                _this.travel = JSON.parse(travel);
                resolve(_this.travel);
            });
        });
    };
    SelectPaymentModalPage.prototype.select = function (cardNumber) {
        this.numberCard = cardNumber;
        var card = this["card" + cardNumber].nativeElement.getElementsByTagName('ion-card')[0];
        this.renderer.addClass(card, 'select');
        for (var indexCard = 1; indexCard <= 3; indexCard++) {
            if (indexCard !== cardNumber) {
                this.renderer.removeClass(this["card" + indexCard].nativeElement.getElementsByTagName('ion-card')[0], 'select');
            }
        }
    };
    __decorate([
        core_1.ViewChild('card1', { read: core_1.ElementRef })
    ], SelectPaymentModalPage.prototype, "card1");
    __decorate([
        core_1.ViewChild('card2', { read: core_1.ElementRef })
    ], SelectPaymentModalPage.prototype, "card2");
    __decorate([
        core_1.ViewChild('card3', { read: core_1.ElementRef })
    ], SelectPaymentModalPage.prototype, "card3");
    __decorate([
        core_1.Input()
    ], SelectPaymentModalPage.prototype, "travelId");
    SelectPaymentModalPage = __decorate([
        core_1.Component({
            selector: 'app-select-payment-modal',
            templateUrl: './select-payment-modal.page.html',
            styleUrls: ['./select-payment-modal.page.scss']
        })
    ], SelectPaymentModalPage);
    return SelectPaymentModalPage;
}());
exports.SelectPaymentModalPage = SelectPaymentModalPage;
