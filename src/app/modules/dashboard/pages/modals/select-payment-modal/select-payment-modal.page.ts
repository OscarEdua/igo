import { AlertsService } from '../../../../../shared/services/alerts.service';
import { Router } from '@angular/router';
import { StorageService } from '../../../../../shared/services/storage.service';
import { Travel } from '../../../interfaces/travel.interface';
import { MapsService } from '../../../../../shared/services/maps.service';
import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  Renderer2,
  Input
} from '@angular/core';
import { TravelsService } from '../../../services/admin/travels.service';
import { ActivatedRoute } from '@angular/router';
import {classicNameResolver} from 'typescript';

@Component({
  selector: 'app-select-payment-modal',
  templateUrl: './select-payment-modal.page.html',
  styleUrls: ['./select-payment-modal.page.scss'],
})

export class SelectPaymentModalPage implements OnInit {
  @ViewChild('card1', { read: ElementRef }) card1: ElementRef;
  @ViewChild('card2', { read: ElementRef }) card2: ElementRef;
  @ViewChild('card3', { read: ElementRef }) card3: ElementRef;
  @Input() travelId: string;
  travel: Travel;
  // inicio de la ruta
  origin = { lat: 0, lng: 0 };
  // Fin d e  la ruta
  destination = { lat: 0, lng: 0 };
  private numberCard;

  constructor(
    private renderer: Renderer2,
    private alertsService: AlertsService,
    private storageService: StorageService,
    private mapsService: MapsService,
    private router: Router,
    private travelsService: TravelsService,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {}
  ionViewWillEnter() {
    this.loadData();    
  }

  async loadData() {
    await this.getTravel();
    this.origin.lat = this.travel.startLocation.lat;
    this.origin.lng = this.travel.startLocation.lng;
    this.destination.lat = this.travel.destinyLocation.lat;
    this.destination.lng = this.travel.destinyLocation.lng;
    this.calculateRoute();
  }

  private calculateRoute() {
    this.mapsService.directionsService.route(
      {
        // paramentros de la ruta  inicial y final
        origin: this.origin,
        destination: this.destination,
        // Que  vehiculo se usa  para realizar el viaje
        travelMode: google.maps.TravelMode.DRIVING,
      },
      (response, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.mapsService.directionsDisplay.setDirections(response);
        } else {
          //  alert('Could not display directions due to: ' + status);
        }
      }
    );
  }

  async confirmPaymentMethod() {
    await this.alertsService.presentLoading('Confirmando...');
    if (this.numberCard === undefined) {
      this.alertsService.loading.dismiss();
      this.alertsService.presentAlertWithHeader("Método de pago","Por favor seleccion un método de pago")
    } else {
      if (this.numberCard === 1) {
        this.travelsService.updatePayMethod(this.travelId, 'card')
        .then(() => {
          this.alertsService.loading.dismiss();
        })
        .catch((error) => {
          this.alertsService.loading.dismiss();
          console.log(error);
        });
      }
      else if (this.numberCard === 2) {
        this.travelsService.updatePayMethod(this.travelId, 'transfer')
        .then(() => {
          this.alertsService.loading.dismiss();
          this.alertsService.closeModal();
          this.router.navigate(['dashboard/transfer/' + this.travelId]);
        })
        .catch((error) => {
          this.alertsService.loading.dismiss();
          console.log(error);
        });
      } else if (this.numberCard === 3) {
        this.travelsService.updatePayMethod(this.travelId, 'cash')
        .then(() => {
          this.alertsService.loading.dismiss();
          this.alertsService.closeModal();
            this.router.navigate(['dashboard/travel', this.travelId, 'client'])
            .then(nav => console.log(nav))
            .catch(error => console.log(error))
        })
        .catch((error) => {
          this.alertsService.loading.dismiss();
          console.log(error);
        });
      }
    }
  }

  getTravel() {
    return new Promise((resolve) => {
      this.storageService.get('travel').then((travel) => {
        this.travel = JSON.parse(travel);
        resolve(this.travel);
      });
    });
  }

  select(cardNumber: number) {
    this.numberCard = cardNumber;
    const card =
      this[`card${cardNumber}`].nativeElement.getElementsByTagName(
        'ion-card'
      )[0];
    this.renderer.addClass(card, 'select');
    for (let indexCard = 1; indexCard <= 3; indexCard++) {
      if (indexCard !== cardNumber) {
        this.renderer.removeClass(
          this[`card${indexCard}`].nativeElement.getElementsByTagName(
            'ion-card'
          )[0],
          'select'
        );
      }
    }
  }
}
