import { Component, Input, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { Router } from '@angular/router';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { UsersService } from 'src/app/modules/home/services/users.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ShippingParametersService } from '../../../../../shared/services/shipping-parameters.service';
import { ShippingParameters } from '../../../interfaces/shipping-parameters.interface';
import { Observable } from 'rxjs';
import { TravelsService } from '../../../services/admin/travels.service';
import { MapsService } from 'src/app/shared/services/maps.service';
import { Marker } from '../../../interfaces/marker';

@Component({
  selector: 'app-in-travel-modal',
  templateUrl: './in-travel-modal.page.html',
  styleUrls: ['./in-travel-modal.page.scss'],
})
export class InTravelModalPage implements OnInit {
  @Input() nombre: string;
  @Input() travelId: string;
  @Input() roleCurrentUser: string;
  user: User;
  observableList: any[] = [];
  travelWithUserData: Observable<any>;
  oTravel: Observable<any>;
  numbersEmergency: ShippingParameters[] = [];
  suscription: any;
  travel: any;
  marker: Marker = {
    position: {
      lat: 0,
      lng: 0
    },
    title: '',
  }
  marker2: Marker = {
    position: {
      lat: 0,
      lng: 0
    },
    title: '',
  }
  constructor(
    private shippingParametersService: ShippingParametersService,
    public alertsService: AlertsService,
    private usersService: UsersService,
    private authenticationService: AuthenticationService,
    private callNumber: CallNumber,
    private router: Router,
    private mapsService: MapsService,
    private travelsService: TravelsService
  ) { }

  ngOnInit() {
    this.travelWithUserData = this.travelsService.getTravelAndUserById(this.travelId, this.roleCurrentUser);
    this.suscription = this.travelWithUserData.subscribe(travel => this.travel = travel);

  }
  getTravel() {
    return new Promise((resolve) => {
      this.oTravel = this.travelsService.getTravelAndUserById(this.travelId, this.roleCurrentUser);
      this.oTravel.subscribe(travel => {
        this.travel = travel;
        console.log(this.travel)
        resolve(this.travel);
      });
    })
  }

  ionViewWillEnter() {
    this.loadDataPageDashboard();
    this.suscription.unsubscribe();
    this.getTravel();
  }

  async loadDataPageDashboard() {
    await this.getDataUser();
  }

  getDataUser() {
    return new Promise((resolve) => {
      const observable = this.usersService
        .getUserById(this.authenticationService.getCurrentUserId())
        .subscribe((user) => {
          this.user = user;
          resolve(this.user);
        });
      this.observableList.push(observable);
    });
  }
  irA() {
    this.marker.position.lat = this.travel['startLocation']['lat'];
    this.marker.position.lng = this.travel['startLocation']['lng'];
    this.marker.title =   this.travel['startLocation']['address']

    this.mapsService.navegate(this.marker);
  }
  irB() {
    
    this.marker2.position.lat = this.travel['destinyLocation']['lat'];
    this.marker2.position.lng = this.travel['destinyLocation']['lng'];
    this.marker2.title =   this.travel['destinyLocation']['address']
    

   this.mapsService.navegate(this.marker2);
  }
  redirectTo(pageName: string): void {
    this.alertsService.closeModal();
    this.router.navigate(['dashboard/' + pageName]);
  }

  onNavigate() {
    window.open("https://api.whatsapp.com/send/?phone=593" + this.numbersEmergency[0].numWhatsApp + "&text=" + this.numbersEmergency[0].messageWhatsApp + "&app_absent=0", "_blank");
  }

  clickCallNumber(number: string) {
    this.callNumber.callNumber(number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  goToChat(): void {
    this.alertsService.closeModal();
    console.log(this.travel.user.email)
    this.router.navigate(['dashboard/chat', this.travel.user.email]);
  }

  async startTravel() {
    await this.alertsService.presentLoading('Empezando viaje...');
    this.travelsService.startTravel(this.travelId)
      .then(() => {
        this.alertsService.loading.dismiss();
        this.alertsService.presentAlertConfirmAccept('Viaje', 'Viaje comenzado');
      })
      .catch((error) => console.log(error));
  }

  async endTravel(currentUser: string) {
    this.alertsService.presentAlertConfirm('Viaje', 'Esta seguro de finalizar el viaje?')
      .then(result => {
        if (result === true) {
          this.travelsService.endTravel(this.travelId)
            .then(() => {
              this.alertsService.presentAlertConfirmAccept('Viaje', 'Viaje finalizado exitosamente')
                .then(res => {
                  if (res === true) {
                    this.alertsService.closeModal();
                    if (currentUser == 'client') {
                      this.router.navigate(['dashboard/travel-complete/', this.travelId]);
                    } else {
                      this.router.navigate(['dashboard/successful-arrival']);
                      console.log(';qjk')
                    }
                  }
                });
            })
            .catch(error => console.log(error));
        }
      });
  }
}
