import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InTravelModalPage } from './in-travel-modal.page';

const routes: Routes = [
  {
    path: '',
    component: InTravelModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InTravelModalPageRoutingModule {}
