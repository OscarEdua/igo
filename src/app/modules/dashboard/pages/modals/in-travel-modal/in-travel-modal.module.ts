import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InTravelModalPageRoutingModule } from './in-travel-modal-routing.module';

import { InTravelModalPage } from './in-travel-modal.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InTravelModalPageRoutingModule,
    ComponentsModule
  ],
  declarations: [InTravelModalPage]
})
export class InTravelModalPageModule {}
