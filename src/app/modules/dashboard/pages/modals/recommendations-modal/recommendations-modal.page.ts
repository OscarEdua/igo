import { Component, OnInit, Input, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { RatingService } from 'src/app/modules/home/services/rating.service';
import { UsersService } from 'src/app/modules/home/services/users.service';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { Rating } from '../../../interfaces/rating.interface';
import { Travel } from '../../../interfaces/travel.interface';
import { TravelsService } from '../../../services/admin/travels.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-recommendations-modal',
  templateUrl: './recommendations-modal.page.html',
  styleUrls: ['./recommendations-modal.page.scss'],
})
export class RecommendationsModalPage implements OnInit {

  form: FormGroup;
  observables: Subscription[] = [];
  @Input() travelId: string;
  @ViewChild('star1') star1: ElementRef;
  @ViewChild('star2') star2: ElementRef;
  @ViewChild('star3') star3: ElementRef;
  @ViewChild('star4') star4: ElementRef;
  @ViewChild('star5') star5: ElementRef;
  driver: User;
  travel: Travel;
  driverId: string;
  rating: Rating = {
    comment: '',
    rate: 0,
  };

  ratingQuery: Rating;


  constructor(
    public alertsService: AlertsService,
    private userService: UsersService,
    private travelService: TravelsService,
    private ratingService: RatingService,
    private renderer: Renderer2,
    private alertService: AlertsService,
  ) { }

  ngOnInit() { }
  ionViewWillEnter() {
    this.loadData();
    this.getRatingByOrder()
  }

  initializeFormFields(): void {
    this.form = new FormGroup({
      commentary: new FormControl('', [Validators.required]),
    });
  }

  getRatingByOrder() {
    const observable = this.ratingService
      .getRatingByOrder(this.travel.travelId)
      .subscribe((res) => {
        this.ratingQuery = res[0] as Rating;
        if (this.ratingQuery !== undefined) {
          this.addStar(this.ratingQuery.rate);
          this.form.controls.commentary.setValue(this.ratingQuery.comment);
        }
      });

    this.observables.push(observable);
  }

  loadData() {
    this.travelService.getTravelId(this.travelId).subscribe((traveldData) => {
      this.travel = traveldData as Travel;
      this.driverId = this.travel.driverId;
      console.log(this.driverId);
      this.userService.getUserById(this.driverId).subscribe((userData) => {
        this.driver = userData as User;
        console.log(this.driver, this.travel);
      });
    });
  }

  rateTravel() {
    if (this.rating.rate === 0 || this.rating.comment === '') {
      this.alertService.presentAlert(
        'Por favor ingrese una calificación y un comentario!'
      );
    } else {
      this.rating.travelId = this.travelId;
      this.rating.isQualified = true;
      if (this.rating.rate > 0 || this.rating.rate <= 2) {
        this.rating.status = 'bad';
      }
      if (this.rating.rate > 2 || this.rating.rate <= 4) {
        this.rating.status = 'good';
      }
      if (this.rating.rate > 4) {
        this.rating.status = 'excelent';
      }
      this.rating.isQualified = true;
      this.rating.clientId = this.travel.clientId;
      this.rating.travelId = this.travel.travelId;
      this.rating.driverId = this.travel.driverId;

      this.ratingService.addRating(this.rating).then((res) => {
        this.travelService.updateTravel(this.travel, this.travelId).then((res) => {
          console.log('yeah');

          this.alertService.presentAlert('Proceso de calificación Exitoso 😁');
        });
      });
    }
  }

  addStar(numberStar: number) {
    this.switchToWhite();
    this.switchToBlue(numberStar);
  }

  switchToBlue(numberStar: number) {
    this.rating.rate = numberStar;
    switch (numberStar) {
      case 1:
        this.renderer.removeClass(this.star1.nativeElement, 'white-star');
        this.renderer.addClass(this.star1.nativeElement, 'blue-star');
        break;
      case 2:
        this.fillWithBlue(numberStar);
        break;
      case 3:
        this.fillWithBlue(numberStar);
        break;
      case 4:
        this.fillWithBlue(numberStar);
        break;
      case 5:
        this.fillWithBlue(numberStar);
        break;

      default:
        break;
    }
  }

  switchToWhite() {
    for (let star = 1; star < 6; star = star + 1) {
      this.renderer.removeClass(this[`star${star}`].nativeElement, 'blue-star');
      this.renderer.addClass(this[`star${star}`].nativeElement, 'white-star');
    }
  }

  fillWithBlue(numberStar: number) {
    for (let star = 1; star <= numberStar; star = star + 1) {
      this.renderer.removeClass(this[`star${star}`].nativeElement, 'white-star');
      this.renderer.addClass(this[`star${star}`].nativeElement, 'blue-star');
    }
  }




}
