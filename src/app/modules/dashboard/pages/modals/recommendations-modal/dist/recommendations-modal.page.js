"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RecommendationsModalPage = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var RecommendationsModalPage = /** @class */ (function () {
    function RecommendationsModalPage(alertsService, userService, travelService, ratingService, renderer, alertService) {
        this.alertsService = alertsService;
        this.userService = userService;
        this.travelService = travelService;
        this.ratingService = ratingService;
        this.renderer = renderer;
        this.alertService = alertService;
        this.observables = [];
        this.rating = {
            comment: '',
            rate: 0
        };
    }
    RecommendationsModalPage.prototype.ngOnInit = function () { };
    RecommendationsModalPage.prototype.ionViewWillEnter = function () {
        this.loadData();
        this.getRatingByOrder();
    };
    RecommendationsModalPage.prototype.initializeFormFields = function () {
        this.form = new forms_1.FormGroup({
            commentary: new forms_1.FormControl('', [forms_1.Validators.required])
        });
    };
    RecommendationsModalPage.prototype.getRatingByOrder = function () {
        var _this = this;
        var observable = this.ratingService
            .getRatingByOrder(this.travel.travelId)
            .subscribe(function (res) {
            _this.ratingQuery = res[0];
            if (_this.ratingQuery !== undefined) {
                _this.addStar(_this.ratingQuery.rate);
                _this.form.controls.commentary.setValue(_this.ratingQuery.comment);
            }
        });
        this.observables.push(observable);
    };
    RecommendationsModalPage.prototype.loadData = function () {
        var _this = this;
        this.travelService.getTravelId(this.travelId).subscribe(function (traveldData) {
            _this.travel = traveldData;
            _this.driverId = _this.travel.driverId;
            console.log(_this.driverId);
            _this.userService.getUserById(_this.driverId).subscribe(function (userData) {
                _this.driver = userData;
                console.log(_this.driver, _this.travel);
            });
        });
    };
    RecommendationsModalPage.prototype.rateTravel = function () {
        var _this = this;
        if (this.rating.rate === 0 || this.rating.comment === '') {
            this.alertService.presentAlert('Por favor ingrese una calificación y un comentario!');
        }
        else {
            this.rating.travelId = this.travelId;
            this.rating.isQualified = true;
            if (this.rating.rate > 0 || this.rating.rate <= 2) {
                this.rating.status = 'bad';
            }
            if (this.rating.rate > 2 || this.rating.rate <= 4) {
                this.rating.status = 'good';
            }
            if (this.rating.rate > 4) {
                this.rating.status = 'excelent';
            }
            this.rating.isQualified = true;
            this.rating.clientId = this.travel.clientId;
            this.rating.travelId = this.travel.travelId;
            this.rating.driverId = this.travel.driverId;
            this.ratingService.addRating(this.rating).then(function (res) {
                _this.travelService.updateTravel(_this.travel, _this.travelId).then(function (res) {
                    console.log('yeah');
                    _this.alertService.presentAlert('Proceso de calificación Exitoso 😁');
                });
            });
        }
    };
    RecommendationsModalPage.prototype.addStar = function (numberStar) {
        this.switchToWhite();
        this.switchToBlue(numberStar);
    };
    RecommendationsModalPage.prototype.switchToBlue = function (numberStar) {
        this.rating.rate = numberStar;
        switch (numberStar) {
            case 1:
                this.renderer.removeClass(this.star1.nativeElement, 'white-star');
                this.renderer.addClass(this.star1.nativeElement, 'blue-star');
                break;
            case 2:
                this.fillWithBlue(numberStar);
                break;
            case 3:
                this.fillWithBlue(numberStar);
                break;
            case 4:
                this.fillWithBlue(numberStar);
                break;
            case 5:
                this.fillWithBlue(numberStar);
                break;
            default:
                break;
        }
    };
    RecommendationsModalPage.prototype.switchToWhite = function () {
        for (var star = 1; star < 6; star = star + 1) {
            this.renderer.removeClass(this["star" + star].nativeElement, 'blue-star');
            this.renderer.addClass(this["star" + star].nativeElement, 'white-star');
        }
    };
    RecommendationsModalPage.prototype.fillWithBlue = function (numberStar) {
        for (var star = 1; star <= numberStar; star = star + 1) {
            this.renderer.removeClass(this["star" + star].nativeElement, 'white-star');
            this.renderer.addClass(this["star" + star].nativeElement, 'blue-star');
        }
    };
    __decorate([
        core_1.Input()
    ], RecommendationsModalPage.prototype, "travelId");
    __decorate([
        core_1.ViewChild('star1')
    ], RecommendationsModalPage.prototype, "star1");
    __decorate([
        core_1.ViewChild('star2')
    ], RecommendationsModalPage.prototype, "star2");
    __decorate([
        core_1.ViewChild('star3')
    ], RecommendationsModalPage.prototype, "star3");
    __decorate([
        core_1.ViewChild('star4')
    ], RecommendationsModalPage.prototype, "star4");
    __decorate([
        core_1.ViewChild('star5')
    ], RecommendationsModalPage.prototype, "star5");
    RecommendationsModalPage = __decorate([
        core_1.Component({
            selector: 'app-recommendations-modal',
            templateUrl: './recommendations-modal.page.html',
            styleUrls: ['./recommendations-modal.page.scss']
        })
    ], RecommendationsModalPage);
    return RecommendationsModalPage;
}());
exports.RecommendationsModalPage = RecommendationsModalPage;
