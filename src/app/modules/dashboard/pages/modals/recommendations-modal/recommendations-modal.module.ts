import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecommendationsModalPageRoutingModule } from './recommendations-modal-routing.module';

import { RecommendationsModalPage } from './recommendations-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecommendationsModalPageRoutingModule
  ],
  declarations: [RecommendationsModalPage]
})
export class RecommendationsModalPageModule {}
