import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecommendationsModalPage } from './recommendations-modal.page';

const routes: Routes = [
  {
    path: '',
    component: RecommendationsModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecommendationsModalPageRoutingModule {}
