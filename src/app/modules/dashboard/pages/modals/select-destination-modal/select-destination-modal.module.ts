import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SelectDestinationModalPageRoutingModule } from './select-destination-modal-routing.module';
import { SelectDestinationModalPage } from './select-destination-modal.page';
import { ComponentsModule } from '../../../components/components.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectDestinationModalPageRoutingModule,
    ComponentsModule,
    PipesModule
  ],
  declarations: [SelectDestinationModalPage]
})

export class SelectDestinationModalPageModule {}
