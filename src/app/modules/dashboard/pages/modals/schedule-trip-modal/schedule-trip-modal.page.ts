import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { Router } from '@angular/router';
import { Travel } from '../../../interfaces/travel.interface';
import { StorageService } from '../../../../../shared/services/storage.service';

@Component({
  selector: 'app-schedule-trip-modal',
  templateUrl: './schedule-trip-modal.page.html',
  styleUrls: ['./schedule-trip-modal.page.scss'],
})

export class ScheduleTripModalPage implements OnInit {
  scheduleHour: string;
  scheduleDate: string;
  travel: Travel;

  constructor(public alertsService: AlertsService, private router: Router, private storageService: StorageService) { }

  ngOnInit() {
    this.storageService.get('travel')
    .then(travel => {
      this.travel = JSON.parse(travel);
    })
    .catch(error => console.log(error));
  }

  redirectTo(pageName: string): void { 
    if (this.scheduleDate && this.scheduleHour) {
      const date = this.scheduleDate.split('T')[0];
      let hour = this.scheduleHour.split('T')[1];
      hour = hour.substr(0, 5);
      const dateTime = new Date(date + 'T' + hour + ':00Z');
      const currentDate = new Date();
      if (dateTime > currentDate) {
        this.travel.schedulingDate = dateTime;
        const sTravel = JSON.stringify(this.travel);
        this.storageService.set('travel', sTravel);
        this.alertsService.closeModal();
        this.router.navigate(['dashboard/select-stop']);
      } else 
        this.alertsService.presentAlertOk('Agenda tu viaje', 'Por favor seleccione una fecha posterior a la actual');
    } else { 
      this.alertsService.presentAlertOk('Agenda tu viaje', 'Por favor complete los campos de fecha');
    }
  }
}
