import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ScheduleTripModalPageRoutingModule } from './schedule-trip-modal-routing.module';
import { ScheduleTripModalPage } from './schedule-trip-modal.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScheduleTripModalPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ScheduleTripModalPage]
})
export class ScheduleTripModalPageModule {}
