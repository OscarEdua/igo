"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ScheduleTripModalPage = void 0;
var core_1 = require("@angular/core");
var ScheduleTripModalPage = /** @class */ (function () {
    function ScheduleTripModalPage(alertsService, router) {
        this.alertsService = alertsService;
        this.router = router;
        this.scheduleHour = null;
        this.scheduleDate = null;
    }
    ScheduleTripModalPage.prototype.ngOnInit = function () {
    };
    ScheduleTripModalPage.prototype.redirectTo = function (pageName) {
        this.alertsService.closeModal();
        this.router.navigate(['dashboard/' + pageName]);
    };
    ScheduleTripModalPage = __decorate([
        core_1.Component({
            selector: 'app-schedule-trip-modal',
            templateUrl: './schedule-trip-modal.page.html',
            styleUrls: ['./schedule-trip-modal.page.scss']
        })
    ], ScheduleTripModalPage);
    return ScheduleTripModalPage;
}());
exports.ScheduleTripModalPage = ScheduleTripModalPage;
