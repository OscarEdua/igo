import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';

@Component({
  selector: 'app-destination-address-modal',
  templateUrl: './destination-address-modal.page.html',
  styleUrls: ['./destination-address-modal.page.scss'],
})

export class DestinationAddressModalPage implements OnInit {

  constructor(public alertsService: AlertsService) { }

  ngOnInit() {
  }

  redirectTo(pageName: string): void {

  }
}
