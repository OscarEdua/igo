import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DestinationAddressModalPageRoutingModule } from './destination-address-modal-routing.module';
import { DestinationAddressModalPage } from './destination-address-modal.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DestinationAddressModalPageRoutingModule,
    ComponentsModule
  ],
  declarations: [DestinationAddressModalPage]
})

export class DestinationAddressModalPageModule {}
