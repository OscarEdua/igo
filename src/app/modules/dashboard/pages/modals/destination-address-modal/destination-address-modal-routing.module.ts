import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DestinationAddressModalPage } from './destination-address-modal.page';

const routes: Routes = [
  {
    path: '',
    component: DestinationAddressModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DestinationAddressModalPageRoutingModule {}
