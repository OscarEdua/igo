/// <reference path="../../../../../../../node_modules/@types/googlemaps/index.d.ts" />
import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/shared/services/storage.service';
import { Travel } from '../../../interfaces/travel.interface';
import { StopsService } from '../../../services/admin/stops.service';
import { Stop } from '../../../interfaces/stop.interface';
import { Observable } from 'rxjs';
import { MapsService } from 'src/app/shared/services/maps.service';
import { Location } from '../../../interfaces/location.interface';
import { Marker } from '../../../interfaces/marker';

@Component({
  selector: 'app-select-stop-modal',
  templateUrl: './select-stop-modal.page.html',
  styleUrls: ['./select-stop-modal.page.scss'],
})

export class SelectStopModalPage implements OnInit {
  travel: Travel;
  stops: Observable<Stop[]>;
  addressStop: string;
  inputDisabled: boolean;
  inputPlaceholder: string;
  startLocation: Location;
  searchText: string;
  marker: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  markers = [];
  
  constructor(
    public alertsService: AlertsService,
    private router: Router,
    private storageService: StorageService,
    private stopsService: StopsService,
    private mapsService: MapsService
  ) {
    this.addressStop = '';
    this.inputDisabled = true;
    this.inputPlaceholder = 'Seleccione una parada';
    this.startLocation = {};
  }

  ngOnInit() {
    this.stops = this.stopsService.getStop();
    this.storageService.get('travel')
    .then(travel => {
      this.travel = JSON.parse(travel);
      console.log(this.travel);
      if (this.travel.vehicleType == 'Standard') {
      } else {
        this.inputDisabled = false;
        this.inputPlaceholder = 'Seleccione o ingrese una parada';
      }
    })
    .catch(error => console.log(error));
  }

  ionViewDidEnter(): void {
    this.loadAutocomplete();
  }

 async chooseStop(stop: Stop) {
    await  this.deleteMarkers();
    this.addressStop = stop.address;
    this.startLocation.lat = stop.lat;
    this.startLocation.lng = stop.lng;
    this.startLocation.address = stop.address;
    this.marker.position.lat = stop.lat;
    this.marker.position.lng = stop.lng;
    this.marker.title = stop.address;
    this.addMarker(this.marker, this.marker.title, 'stops');
  }

  addMarker(marker: Marker, name: String, typeMarket: string) {
    let marke;
    let icon;
    if (typeMarket == 'myLocation') {
      icon = {
        url: '../../assets/img/ic_loc.png',
        //size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(90, 90),
      };
      /*     const image = '../../assets/img/Pineapplemenu.png'; */
    } else if (typeMarket == 'stops') {
      icon = {
        url: '../../assets/img/ic_dropoff.png',
        //size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(50, 50),
      };
    }
    const geocoder = new google.maps.Geocoder();
    marke = new google.maps.Marker({
      position: marker.position,
      //  draggable: true,
      // animation: google.maps.Animation.DROP,
      map: this.mapsService.map,
      icon: icon,
      title: marker.title,
    });
    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          marke.getPosition().lat();
          marke.getPosition().lng();
          results[0].formatted_address;
        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
      this.mapsService.map.setCenter(this.marker.position);
    });
    const infoWindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marke, 'click', () => {
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            infoWindow.setContent(
              '<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address
            );
            if (infoWindow !== null) {
            }
            infoWindow.open(this.mapsService.map, marke);
          } else {
          }
        } else {
        }
      });
    });
    this.markers.push(marke);
  }

  selectUbication(pageName: string): void {
    this.storageService.get('travel').then((travel) => {
      this.travel = JSON.parse(travel);
      this.travel.startLocation = this.startLocation;
      const sTravel = JSON.stringify(this.travel);
      this.storageService.set('travel', sTravel);
    });
    if (this.addressStop == '') {
      this.alertsService.presentAlertWithHeader(
        'Elige tu parada.',
        'Por favor escoja o ingrese una parada.'
      );
    } else {
      this.alertsService.closeModal();
      this.router.navigate(['dashboard/' + pageName]);
      this.deleteMarkers();
    }
  }
  
  deleteMarkers() {
    this.markers = [];
    this.setMapOnAll(null);
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  loadAutocomplete(): void {
    const autocomplete = new google.maps.places.Autocomplete(document.querySelector(
      '#autocomplete-input').getElementsByTagName('input')[0]
    );
    google.maps.event.addListener(autocomplete, 'place_changed', event => {
      const place =  autocomplete.getPlace();
      this.startLocation.lat = place.geometry.location.lat();
      this.startLocation.lng = place.geometry.location.lng();
      this.startLocation.address = place.name + " " + place.formatted_address;
      this.startLocation.address = this.startLocation.address.replace('\\', '');
      this.mapsService.map.setCenter(place.geometry.location);
      const market = new google.maps.Marker({
        position: place.geometry.location,
        //animation: google.maps.Animation.DROP,
        icon: {
          url: '../../../../../../assets/img/ic_loc.png',
          scaledSize: new google.maps.Size(60, 60)
        } 
      });
      this.mapsService.deleteMarkers();
      market.setMap(this.mapsService.map);
    }); 
  }
}
