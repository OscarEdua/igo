import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SelectStopModalPageRoutingModule } from './select-stop-modal-routing.module';
import { SelectStopModalPage } from './select-stop-modal.page';
import { ComponentsModule } from '../../../components/components.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectStopModalPageRoutingModule,
    ComponentsModule,
    PipesModule
  ],
  declarations: [SelectStopModalPage]
})
export class SelectStopModalPageModule {}
