import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-select-stop-schedule-modal',
  templateUrl: './select-stop-schedule-modal.page.html',
  styleUrls: ['./select-stop-schedule-modal.page.scss'],
})
export class SelectStopScheduleModalPage implements OnInit {

  constructor(public alertsService: AlertsService, private router: Router) { }

  ngOnInit() {
  }

  redirectTo(pageName: string): void {
    this.alertsService.closeModal();
    this.router.navigate(['dashboard/' + pageName]);
  }
}
