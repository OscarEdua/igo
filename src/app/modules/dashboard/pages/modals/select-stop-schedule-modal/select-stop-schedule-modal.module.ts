import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SelectStopScheduleModalPageRoutingModule } from './select-stop-schedule-modal-routing.module';
import { SelectStopScheduleModalPage } from './select-stop-schedule-modal.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectStopScheduleModalPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelectStopScheduleModalPage]
})

export class SelectStopScheduleModalPageModule {}
