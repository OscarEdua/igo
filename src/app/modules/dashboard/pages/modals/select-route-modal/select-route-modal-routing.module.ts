import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectRouteModalPage } from './select-route-modal.page';

const routes: Routes = [
  {
    path: '',
    component: SelectRouteModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectRouteModalPageRoutingModule {}
