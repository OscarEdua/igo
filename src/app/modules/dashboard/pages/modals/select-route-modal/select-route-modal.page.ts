import { Component, OnInit } from '@angular/core';
import { AlertsService } from '../../../../../shared/services/alerts.service';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/shared/services/storage.service';
import { Travel } from '../../../interfaces/travel.interface';
import { MapsService } from '../../../../../shared/services/maps.service';
import { resolve } from '@angular/compiler-cli/src/ngtsc/file_system';

@Component({
  selector: 'app-select-route-modal',
  templateUrl: './select-route-modal.page.html',
  styleUrls: ['./select-route-modal.page.scss'],
})
export class SelectRouteModalPage implements OnInit {
  travel: Travel;
  // inicio de la ruta
  origin = { lat: 0, lng: 0 };
  // Fin d e  la ruta
  destination = { lat: 0, lng: 0 };

  constructor(
    public  alertsService: AlertsService,
    private router: Router,
    private storageService: StorageService,
    private mapsService: MapsService
  ) {}

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loadData();
  }

  async loadData() {
    await this.getTravel();
    this.origin.lat = this.travel.startLocation.lat;
    this.origin.lng = this.travel.startLocation.lng;
    this.destination.lat = this.travel.destinyLocation.lat;
    this.destination.lng = this.travel.destinyLocation.lng;
    this.calculateRoute();
  }

  getTravel() {
    return new Promise((resolve) => {
      this.storageService.get('travel').then((travel) => {
        this.travel = JSON.parse(travel);
        resolve(this.travel);
      });
    });
  }

  confirmRoute(pageName: string): void {
    this.alertsService.closeModal();
    this.router.navigate(['dashboard/' + pageName]);
}

  private calculateRoute() {
    this.mapsService.directionsService.route(
      {
        // paramentros de la ruta  inicial y final
        origin: this.origin,
        destination: this.destination,
        // Que  vehiculo se usa  para realizar el viaje
        travelMode: google.maps.TravelMode.DRIVING,
      },
      (response, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.mapsService.directionsDisplay.setDirections(response);
        } else {
          //  alert('Could not display directions due to: ' + status);
        }
      }
    );
  }

  ionViewWillLeave() {}
}
