import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectRouteModalPageRoutingModule } from './select-route-modal-routing.module';

import { SelectRouteModalPage } from './select-route-modal.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectRouteModalPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelectRouteModalPage]
})
export class SelectRouteModalPageModule {}
