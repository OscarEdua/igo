import { Component, OnInit, Input } from '@angular/core';
import { AlertsService } from '../../../../../shared/services/alerts.service';
import { Router } from '@angular/router';
import { Travel } from '../../../interfaces/travel.interface';
import { MapsService } from '../../../../../shared/services/maps.service';
import { StorageService } from '../../../../../shared/services/storage.service';
import { ImagesService } from '../../../../../shared/services/images.service';
import { TravelsService } from '../../../services/admin/travels.service';
import { Observable } from 'rxjs';
import { BankParameters } from '../../../interfaces/bank-parameters.interface';
import { BankParametersService } from '../../../../../shared/services/bank-data.service';

@Component({
  selector: 'app-transfer-modal',
  templateUrl: './transfer-modal.page.html',
  styleUrls: ['./transfer-modal.page.scss'],
})

export class TransferModalPage implements OnInit {
  @Input() travelId: string;
  voucherImagenFile: File;
  voucherImg: string;
  travel: Travel;
  // inicio de la ruta
  origin = { lat: 0, lng: 0 };
  // Fin d e  la ruta
  destination = { lat: 0, lng: 0 };
  isConfirmedPayment: boolean;
  oTravel: Observable<Travel>;
  getbankParameters: BankParameters;
  travelPrice: Observable<any>;
  wholePartPrice: number;
  decimalPartPrice: string;

  constructor(
    private bankParametersService: BankParametersService,
    private alertsService: AlertsService,
    private storageService: StorageService,
    private mapsService: MapsService,
    private imagesService: ImagesService,
    private travelsService: TravelsService,
    private router: Router) {
    this.isConfirmedPayment = false;
  }

  ngOnInit() {
    this.getParametrers();
    this.travelPrice = this.travelsService.getTravelAndUserById(this.travelId, "driver");
  }

  ionViewWillEnter() {
    this.loadData();
  }
    
  getParametrers() {
    this.bankParametersService.getparametes().subscribe((res: BankParameters) => {
      this.getbankParameters = res;  
    });  
  }

  async loadData() {
    await this.getTravel();
    this.origin.lat = this.travel.startLocation.lat;
    this.origin.lng = this.travel.startLocation.lng;
    this.destination.lat = this.travel.destinyLocation.lat;
    this.destination.lng = this.travel.destinyLocation.lng;
  }
   
  getTravel() {
    return new Promise((resolve) => {
      this.storageService.get('travel').then((travel) => {
        this.travel = JSON.parse(travel);
        resolve(this.travel);
      });
    });
  }

  changeFileImagen($event): void {
    if ($event.target.files && $event.target.files[0]) {
      let reader = new FileReader();
      reader.readAsDataURL($event.target.files[0]);
      reader.onload = (event: any) => {
        this.voucherImagenFile = $event.target.files[0];
        console.log(this.voucherImagenFile);
        this.voucherImg = event.target.result;
      };
    }
  }

  async goToTravelDetails(route: string) {
    await this.alertsService.presentLoading('Confirmando pago...');
    if (this.voucherImg == undefined) {
      this.alertsService.loading.dismiss();
      this.alertsService.presentAlertOk('Transferencia', 'Por favor suba el comprobante de pago');
    } else {
      await this.imagesService.uploadImage('bouchers', this.voucherImagenFile)
      .then(urlImagen => {
         this.travelsService.updateInvoiceImg(this.travelId, urlImagen)
         .then((result) => {
            console.log(result);
            this.alertsService.loading.dismiss();
            this.isConfirmedPayment = true;
            this.alertsService.presentAlertOk('Pago', 'Pago confirmado exitosamente, por favor espere mientras el administrador valida su comprobante.')
            .then(result => {
              if (result === true) {
                this.alertsService.closeModal();
                this.router.navigate(['dashboard/search-complete', this.travelId]);
              }
            })
            .catch(error => {
              console.log(error);
              this.alertsService.loading.dismiss();
            });
         })
         .catch(error => {
           console.log(error);
           this.alertsService.loading.dismiss();
         });
      })
      .catch(error => {
        console.log(error);
        this.alertsService.loading.dismiss();
      });
    }
  }
}
