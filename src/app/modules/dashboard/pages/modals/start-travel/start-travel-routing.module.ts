import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartTravelPage } from './start-travel.page';

const routes: Routes = [
  {
    path: '',
    component: StartTravelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartTravelPageRoutingModule {}
