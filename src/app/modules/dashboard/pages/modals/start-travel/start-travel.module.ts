import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StartTravelPageRoutingModule } from './start-travel-routing.module';

import { StartTravelPage } from './start-travel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StartTravelPageRoutingModule
  ],
  declarations: [StartTravelPage]
})
export class StartTravelPageModule {}
