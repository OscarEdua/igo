import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { SelectVehicleTypePage } from '../select-vehicle-type/select-vehicle-type.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-start-travel',
  templateUrl: './start-travel.page.html',
  styleUrls: ['./start-travel.page.scss'],
})
export class StartTravelPage implements OnInit {

  constructor(public alertsService: AlertsService, private router: Router) { }

  ngOnInit() {
  }

  selectVehicleType(): void {
    this.alertsService.presentModalWithoutTravelData(SelectVehicleTypePage, 'select-type-vehicle-modal');
  }

  redirectTo(pageName: string): void {
    this.alertsService.closeModal();
    this.router.navigate(['dashboard/' + pageName]);
  }
}
