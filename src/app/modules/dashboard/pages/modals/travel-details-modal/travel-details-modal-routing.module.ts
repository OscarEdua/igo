import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TravelDetailsModalPage } from './travel-details-modal.page';

const routes: Routes = [
  {
    path: '',
    component: TravelDetailsModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TravelDetailsModalPageRoutingModule {}
