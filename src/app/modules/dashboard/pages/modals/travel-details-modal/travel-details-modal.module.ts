import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TravelDetailsModalPageRoutingModule } from './travel-details-modal-routing.module';

import { TravelDetailsModalPage } from './travel-details-modal.page';
import { ComponentsModule } from '../../../components/components.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TravelDetailsModalPageRoutingModule,
    ComponentsModule
  ],
  declarations: [TravelDetailsModalPage]
})
export class TravelDetailsModalPageModule {}
