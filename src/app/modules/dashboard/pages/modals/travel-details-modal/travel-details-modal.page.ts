import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertsService } from '../../../../../shared/services/alerts.service';
import { Travel } from '../../../interfaces/travel.interface';
import { MapsService } from '../../../../../shared/services/maps.service';
import { StorageService } from '../../../../../shared/services/storage.service';
import { TravelsService } from '../../../services/admin/travels.service';
import { AuthenticationService } from '../../../../../core/authentication/authentication.service';
import { ShippingParametersService } from '../../../../../shared/services/shipping-parameters.service';
import { ShippingParameters } from '../../../interfaces/shipping-parameters.interface';
import { UsersService } from 'src/app/modules/home/services/users.service';
import { PushService } from 'src/app/shared/services/push.service';
import { Subscription } from 'rxjs';
import { User } from 'src/app/modules/home/interfaces/user.interface';

@Component({
  selector: 'app-travel-details-modal',
  templateUrl: './travel-details-modal.page.html',
  styleUrls: ['./travel-details-modal.page.scss'],
})

export class TravelDetailsModalPage implements OnInit {
  travel: Travel;
  origin = { lat: 0, lng: 0 };
  destination = { lat: 0, lng: 0 };
  distance: number;
  duration: string;
  price: number;
  wholePartPrice: number;
  decimalPartPrice: string;
  percentageVip: number;
  numbersEmergency: ShippingParameters []= [];
  users: User[];
  observableList: Subscription[] = [];


  constructor( 
    public ShippingParametersService: ShippingParametersService,
    public alertsService: AlertsService, 
    private router:Router,
    private mapsService: MapsService,
    private travelsService: TravelsService,
    private authenticationService: AuthenticationService,
    private pushService: PushService,
    private userService: UsersService,
    private storageService: StorageService) { }

  ngOnInit() {
    this.getParameters();
  }

  ionViewWillEnter() {
    this.loadData();
  }

  getParameters(){
    this.ShippingParametersService.getCollection<ShippingParameters>().subscribe((res) => {
     this.numbersEmergency = res as ShippingParameters[];   
   });
 }

  async loadData() {
    await this.getTravel();
    this.origin.lat = this.travel.startLocation.lat;
    this.origin.lng = this.travel.startLocation.lng;
    this.destination.lat = this.travel.destinyLocation.lat;
    this.destination.lng = this.travel.destinyLocation.lng;
    this.calculateRoute();
    this.travel.allowPets = false;
    this.travel.allowLuggage = false;
    this.travel.allowSmok = false;
    this.getUserDriver();
  }

  private calculateRoute() {
    this.mapsService.directionsService.route(
      {
        // paramentros de la ruta  inicial y final
        origin: this.origin,
        destination: this.destination,
        // Que  vehiculo se usa  para realizar el viaje
        travelMode: google.maps.TravelMode.DRIVING,
      },
      (response, status) => {
        const sDistance = response.routes[0].legs[0].distance.text;
        this.duration = response.routes[0].legs[0].duration.text;
        
        this.price = 
        this.numbersEmergency[0].baseCost+ 
        this.getExtraPrice(parseFloat(sDistance),+
        this.numbersEmergency[0].baseKilometers,+
        this.numbersEmergency[0].extraKilometerCost);
        this.percentageVip = this.numbersEmergency[0].extraVip * this.price / 100;
        this.travel.cost = (this.travel.vehicleType === 'VIP') ? (this.price + this.percentageVip) : this.price;
        this.wholePartPrice = Math.trunc(this.travel.cost );
        this.decimalPartPrice = (this.travel.cost .toFixed(2)).split('.')[1];
        this.travel.clientId = this.authenticationService.getCurrentUserId();
        this.travel.status = 'pending';
        if (status === google.maps.DirectionsStatus.OK) {
          this.mapsService.directionsDisplay.setDirections(response);
        } else {
          //  alert('Could not display directions due to: ' + status);
        }
      }
      );
    }

    getUserDriver() { 
      const observable = this.userService
        .getUsersByRole('driver')
        .subscribe((res) => {
          this.users = res as User[];
          console.log(this.users)
        });
      this.observableList.push(observable);
    }

    getExtraPrice(kilometers: number, baseKilometers: number, price: number): number {
      return (kilometers - baseKilometers) * price;
    }
    
    getTravel() {
      return new Promise((resolve) => {
      this.storageService.get('travel').then((travel) => {
        this.travel = JSON.parse(travel);
        resolve(this.travel);
      });
    });
  }

  goToSearchDriver(page: string){
    this.alertsService.presentLoading('Confirmando viaje...');
    this.travelsService.add(this.travel)
    .then((result) => {
      this.alertsService.dismissLoading();
      this.router.navigate([`/dashboard/${page}/${result.id}`]);
      this.users.map((res)=>{
        this.pushService.sendByUid('IGO','Se ha generado una solicitud por parte de un cliente.','Se ha generado una solicitud por parte de un cliente.','dashboard/requests',res.uid)
      })
      this.alertsService.closeModal();
    })
    .catch((error) => {
      this.alertsService.dismissLoading();
      this.alertsService.presentAlertWithHeader('Detalles del viaje', 'Lo sentimos vuelva a intentarlo');
    });
  }
  
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
 }
