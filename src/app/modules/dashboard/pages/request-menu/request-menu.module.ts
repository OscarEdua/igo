import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RequestMenuPageRoutingModule } from './request-menu-routing.module';
import { RequestMenuPage } from './request-menu.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RequestMenuPageRoutingModule,
    ComponentsModule
  ],
  declarations: [RequestMenuPage]
})
export class RequestMenuPageModule {}
