import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-request-menu',
  templateUrl: './request-menu.page.html',
  styleUrls: ['./request-menu.page.scss'],
})
export class RequestMenuPage implements OnInit {

  constructor(private menuController: MenuController) {
  }

  ngOnInit() {
  }

  openMenu() {
    this.menuController.open('principal');
  }
}
