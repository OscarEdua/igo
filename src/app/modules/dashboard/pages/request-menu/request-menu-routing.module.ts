import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestMenuPage } from './request-menu.page';
import { RequestsPage } from '../requests/requests.page';

const routes: Routes = [
  {
    path: '',
    component: RequestMenuPage
  }, 
  {
    path: 'requests',
    component: RequestsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestMenuPageRoutingModule {}
