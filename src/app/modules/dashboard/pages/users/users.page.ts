import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { UsersService } from 'src/app/modules/home/services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  users: Observable<User[]>;
  searchText: string;
  buttonPressed: string;
  titlePage: string;

  constructor(private usersService: UsersService, private route: ActivatedRoute) { 
    this.buttonPressed = this.route.snapshot.paramMap.get('buttonPressed');
    this.titlePage = (this.buttonPressed == 'users') ? 'Usuarios' : 'Conductores';
  }

  ngOnInit() {
    if (this.buttonPressed == 'users') {
      this.users = this.usersService.read();
    } else {
      this.users = this.usersService.readDrivers();
    }
  }

  onSearchChange(event) {
    this.searchText = event.detail.value; 
  }

  greeting(): void {
    console.log('Hola...');
  }
}
