import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmTransfersPage } from './confirm-transfers.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmTransfersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmTransfersPageRoutingModule {}
