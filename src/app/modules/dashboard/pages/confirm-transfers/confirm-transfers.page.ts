import { Component, OnInit,Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TravelsService } from "src/app/modules/dashboard/services/admin/travels.service";
import { Observable } from 'rxjs';
import { AlertsService } from '../../../../shared/services/alerts.service';


@Component({
  selector: 'app-confirm-transfers',
  templateUrl: './confirm-transfers.page.html',
  styleUrls: ['./confirm-transfers.page.scss'],
})
export class ConfirmTransfersPage implements OnInit {
  travelId: string;
  travel: Observable<any>;
  private numberCard;

  constructor(
    private activatedRoute: ActivatedRoute,
    private travelsService: TravelsService,
    private alertsService: AlertsService,
    private router: Router,
    private renderer: Renderer2

  ) { 
    this.travelId = activatedRoute.snapshot.paramMap.get("travelId");
  }
  
  ngOnInit() {
    this.travel =  this.travelsService.getTravelAndUserById(this.travelId, "driver");
  }

   confirmPaymentMethod() {
     this.alertsService.presentLoading('Confirmando...');
        this.travelsService.updatePayMethodTransfer(this.travelId, 'paid')
        .then(() => {
          this.alertsService.loading.dismiss();
          this.router.navigate(['dashboard/review-transfer'])
          .then(nav => console.log(nav))
          .catch(error => console.log(error))
        })
        .catch((error) => {
          this.alertsService.loading.dismiss();
          console.log(error);
        });
    }

    confirmPaymentMethodReject() {
      this.alertsService.presentLoading('Rechazando...');
         this.travelsService.updatePayMethodTransfer(this.travelId, 'reject')
         .then(() => {
           this.alertsService.loading.dismiss();
           this.router.navigate(['dashboard/review-transfer'])
         })
         .catch((error) => {
           this.alertsService.loading.dismiss();
           console.log(error);
         });
     }

  }







