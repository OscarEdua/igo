import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmTransfersPageRoutingModule } from './confirm-transfers-routing.module';

import { ConfirmTransfersPage } from './confirm-transfers.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmTransfersPageRoutingModule,
    ComponentsModule

  ],
  declarations: [ConfirmTransfersPage]
})
export class ConfirmTransfersPageModule {}
