import { Component, OnInit } from '@angular/core';
import { MapsService } from '../../../../shared/services/maps.service';
import { AlertsService } from '../../../../shared/services/alerts.service';
import { TravelDetailsModalPage } from '../modals/travel-details-modal/travel-details-modal.page';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-travel-details',
  templateUrl: './travel-details.page.html',
  styleUrls: ['./travel-details.page.scss'],
})

export class TravelDetailsPage implements OnInit {

  constructor(private alertsService: AlertsService, 
              private mapsService: MapsService,
              private router:Router) { }

  ngOnInit() {
  }
  
  ionViewWillEnter() {
    this.openModal();
    this.mapsService.loadmap('map-travel-detail');
  }

  openModal(): void {
    this.alertsService.presentModalWithoutTravelData(TravelDetailsModalPage, 'select-type-vehicle-modal');
  }
}
