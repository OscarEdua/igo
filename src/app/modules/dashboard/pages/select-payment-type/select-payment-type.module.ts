import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectPaymentTypePageRoutingModule } from './select-payment-type-routing.module';

import { SelectPaymentTypePage } from './select-payment-type.page';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectPaymentTypePageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelectPaymentTypePage]
})
export class SelectPaymentTypePageModule {}
