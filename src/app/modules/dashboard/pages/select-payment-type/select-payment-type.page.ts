import { Component, OnInit } from '@angular/core';
import { AlertsService } from '../../../../shared/services/alerts.service';
import { SelectPaymentModalPage } from '../modals/select-payment-modal/select-payment-modal.page';
import { MapsService } from '../../../../shared/services/maps.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TravelsService } from '../../services/admin/travels.service';

@Component({
  selector: 'app-select-payment-type',
  templateUrl: './select-payment-type.page.html',
  styleUrls: ['./select-payment-type.page.scss'],
})
export class SelectPaymentTypePage implements OnInit {
  travelId: string;
  travel: Observable<any>;

  constructor(
    private alertsService: AlertsService,
    private mapsService: MapsService,
    private activatedRoute: ActivatedRoute,
    private travelsService: TravelsService
  ) {
    this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
  }

  ngOnInit() {
    console.log ("test")

    
  }

  openModal(): void {
    this.alertsService.presentModalWithData(
      SelectPaymentModalPage,
      {travelId: this.travelId},
      'select-stop-modal'
    );
  }

  ionViewWillEnter() {
    this.openModal();
    this.mapsService.loadmap('select-payment-type-map');
  }
}
