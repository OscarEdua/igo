"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SelectPaymentTypePage = void 0;
var core_1 = require("@angular/core");
var select_payment_modal_page_1 = require("../modals/select-payment-modal/select-payment-modal.page");
var SelectPaymentTypePage = /** @class */ (function () {
    function SelectPaymentTypePage(alertsService, mapsService, activatedRoute, travelsService) {
        this.alertsService = alertsService;
        this.mapsService = mapsService;
        this.activatedRoute = activatedRoute;
        this.travelsService = travelsService;
        this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
    }
    SelectPaymentTypePage.prototype.ngOnInit = function () {
        this.travelsService.getTravelAndUserById(this.travelId);
    };
    SelectPaymentTypePage.prototype.openModal = function () {
        //this.alertsService.presentModalWithoutTravelData(
        //SelectPaymentModalPage,
        //'select-stop-modal'
        //);
        this.alertsService.presentModalWithData(select_payment_modal_page_1.SelectPaymentModalPage, { travelId: this.travelId }, 'select-stop-modal');
    };
    SelectPaymentTypePage.prototype.ionViewWillEnter = function () {
        this.openModal();
        this.mapsService.loadmap('select-payment-type-map');
    };
    SelectPaymentTypePage = __decorate([
        core_1.Component({
            selector: 'app-select-payment-type',
            templateUrl: './select-payment-type.page.html',
            styleUrls: ['./select-payment-type.page.scss']
        })
    ], SelectPaymentTypePage);
    return SelectPaymentTypePage;
}());
exports.SelectPaymentTypePage = SelectPaymentTypePage;
