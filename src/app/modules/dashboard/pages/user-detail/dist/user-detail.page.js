"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.UserDetailPage = void 0;
var core_1 = require("@angular/core");
var UserDetailPage = /** @class */ (function () {
    function UserDetailPage(usersService, activatedRoute, alertService) {
        this.usersService = usersService;
        this.activatedRoute = activatedRoute;
        this.alertService = alertService;
        this.userId = activatedRoute.snapshot.paramMap.get('userId');
    }
    UserDetailPage.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.usersService.getUserById(this.userId);
        this.user.subscribe(function (data) {
            _this.uUser = data;
        });
    };
    UserDetailPage.prototype.updateRoleByUid = function () {
        this.uUser.role = this.role;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Cambio de Rol satisfactorio');
    };
    UserDetailPage.prototype.unsuscribeUser = function () {
        this.uUser.isActive = false;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Usuario Desactivado');
    };
    UserDetailPage.prototype.suscribeUser = function () {
        this.uUser.isActive = true;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Usuario Activado');
    };
    UserDetailPage.prototype.checkRole = function (event) {
        this.role = event.detail.value;
    };
    UserDetailPage = __decorate([
        core_1.Component({
            selector: 'app-user-detail',
            templateUrl: './user-detail.page.html',
            styleUrls: ['./user-detail.page.scss']
        })
    ], UserDetailPage);
    return UserDetailPage;
}());
exports.UserDetailPage = UserDetailPage;
