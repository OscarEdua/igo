import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { UsersService } from 'src/app/modules/home/services/users.service';
import { AlertsService } from 'src/app/shared/services/alerts.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.page.html',
  styleUrls: ['./user-detail.page.scss'],
})
export class UserDetailPage implements OnInit {
  userId: string;
  user: Observable<User>;
  uUser: User;
  role: string;

  constructor(
    private usersService: UsersService, 
    private activatedRoute: ActivatedRoute,
    private alertService: AlertsService,) { 
    this.userId = activatedRoute.snapshot.paramMap.get('userId');
  }

  ngOnInit() {
    this.user = this.usersService.getUserById(this.userId);
    this.user.subscribe((data)=>{
      this.uUser = data as User;
    });
  }
  updateRoleByUid(){
    this.uUser.role = this.role;
    this.usersService.update(this.uUser);
    this.alertService.presentAlert('Cambio de Rol satisfactorio');
  }

  unsuscribeUser(){
    this.uUser.isActive = false;
    this.usersService.update(this.uUser);
    this.alertService.presentAlert('Usuario Desactivado');
  }

  suscribeUser(){
    this.uUser.isActive = true;
    this.usersService.update(this.uUser);
    this.alertService.presentAlert('Usuario Activado');
  }

  checkRole(event) {
    this.role = event.detail.value;
  }
}
