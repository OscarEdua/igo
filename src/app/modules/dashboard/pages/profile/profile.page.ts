import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { UsersService } from 'src/app/modules/home/services/users.service';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { ImagesService } from 'src/app/shared/services/images.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  userId: string;
  user: User;

  constructor(private authenticationService: AuthenticationService, private usersService: UsersService, private imagesService: ImagesService, private alertsService: AlertsService) { 
    this.userId = authenticationService.getCurrentUserId();
    this.usersService.getUserById(this.userId).subscribe(user => {
      this.user = user;
    });
  }

  ngOnInit() {
  }

  async update() {
    await this.alertsService.presentLoading('Actualizando...');
    if (!this.thereAreEmptyFields()) {
      this.usersService.update(this.user)
      .then((result) => {
        this.alertsService.loading.dismiss();
        this.alertsService.presentAlertWithHeader('Perfil', 'Datos actualizados correctamente!');
      })
      .catch((error) => {
        this.alertsService.loading.dismiss();
        console.log(error)
        this.alertsService.presentAlertWithHeader('Perfil', 'Lo sentimos, por favor vuelva a intentarlo!');
      })
    } else {
      this.alertsService.loading.dismiss();
      this.alertsService.presentAlertWithHeader('Perfil', 'Por favor complete los campos requeridos!');
    }
  }

  previewImage(event: any) {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (event: any) => {
        this.user.imagen = event.target.result;
        console.log(this.user.imagen)
      }
      reader.readAsDataURL(event.target.files[0]);  
    }
  }

  thereAreEmptyFields(): boolean {
    if (this.user.name == '')
      return true;
    else 
      return false; 
  }
}
