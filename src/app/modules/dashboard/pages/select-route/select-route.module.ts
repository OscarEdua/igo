import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectRoutePageRoutingModule } from './select-route-routing.module';

import { SelectRoutePage } from './select-route.page';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectRoutePageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelectRoutePage]
})
export class SelectRoutePageModule {}
