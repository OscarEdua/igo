import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectRoutePage } from './select-route.page';

const routes: Routes = [
  {
    path: '',
    component: SelectRoutePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectRoutePageRoutingModule {}
