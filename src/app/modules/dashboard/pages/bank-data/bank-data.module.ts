import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BankDataPageRoutingModule } from './bank-data-routing.module';

import { BankDataPage } from './bank-data.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BankDataPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule
  ],
  declarations: [BankDataPage]
})
export class BankDataPageModule {}
