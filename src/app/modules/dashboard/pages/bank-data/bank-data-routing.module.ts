import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BankDataPage } from './bank-data.page';

const routes: Routes = [
  {
    path: '',
    component: BankDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BankDataPageRoutingModule {}
