import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { BankParametersService } from '../../../../shared/services/bank-data.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { BankParameters } from '../../interfaces/bank-parameters.interface';
import { AlertsService } from '../../../../shared/services/alerts.service';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-bank-data',
  templateUrl: './bank-data.page.html',
  styleUrls: ['./bank-data.page.scss'],
})
export class BankDataPage implements OnInit {

  form: FormGroup;
  observableList: Subscription[] = [];
  bankParameters: BankParameters;
  getbankParameters: BankParameters;
  obsBankParameters: Observable<BankParameters>;

  constructor(
    private bankParametersService: BankParametersService,
    private alertService: AlertsService,
    private menuController: MenuController

  ) {
    this.form = new FormGroup({
      accountNumber: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      nameBank: new FormControl('', [Validators.required]),
      nameOwner: new FormControl('', [Validators.required]),
      typeAccount: new FormControl('', [Validators.required]),
          
    });
  }

  ngOnInit() {
    this.getParametrers();
  }

  getParametrers() {
    const observable = this.bankParametersService.getparametes().subscribe((res: BankParameters) => {
      this.getbankParameters = res;
      this.form = new FormGroup({
        accountNumber: new FormControl(this.getbankParameters['accountNumber'], [Validators.required,]),
        email: new FormControl(this.getbankParameters['email'], [Validators.required]),
        nameBank: new FormControl(this.getbankParameters['nameBank'], [Validators.required,]),
        nameOwner: new FormControl(this.getbankParameters['nameOwner'], [Validators.required]),
        typeAccount: new FormControl(this.getbankParameters['typeAccount'], [Validators.required]),
      });
    });
  }

  update(): void {
    this.bankParameters = {
      accountNumber: this.form.controls.accountNumber.value,
      email: this.form.controls.email.value,
      nameBank: this.form.controls.nameBank.value,
      nameOwner: this.form.controls.nameOwner.value,
      typeAccount: this.form.controls.typeAccount.value
    };
    this.bankParametersService
      .update(this.bankParameters)
      .then(() => this.alertService.presentAlert('Parametros Actualizados ✔'))
      .catch((error) => console.log(error));
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
   
  openMenu() {
    this.menuController.open('principal');
  }
}
