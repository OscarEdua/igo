import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { SelectStopScheduleModalPage } from '../modals/select-stop-schedule-modal/select-stop-schedule-modal.page';

@Component({
  selector: 'app-select-stop-schedule',
  templateUrl: './select-stop-schedule.page.html',
  styleUrls: ['./select-stop-schedule.page.scss'],
})
export class SelectStopSchedulePage implements OnInit {

  constructor(private alertsService: AlertsService) { }

  ngOnInit() {
  }

  openModal(): void {
    this.alertsService.presentModalWithoutTravelData(SelectStopScheduleModalPage, 'select-stop-schedule-modal');
  }
}
