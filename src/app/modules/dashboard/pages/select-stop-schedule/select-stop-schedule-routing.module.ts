import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectStopSchedulePage } from './select-stop-schedule.page';

const routes: Routes = [
  {
    path: '',
    component: SelectStopSchedulePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectStopSchedulePageRoutingModule {}
