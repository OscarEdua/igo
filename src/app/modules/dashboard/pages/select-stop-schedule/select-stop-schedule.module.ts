import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SelectStopSchedulePageRoutingModule } from './select-stop-schedule-routing.module';
import { SelectStopSchedulePage } from './select-stop-schedule.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectStopSchedulePageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelectStopSchedulePage]
})

export class SelectStopSchedulePageModule {}
