import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UsersService } from '../../../home/services/users.service';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../home/interfaces/user.interface';
import { TravelsService } from '../../services/admin/travels.service';
@Component({
  selector: 'app-review-transfer',
  templateUrl: './review-transfer.page.html',
  styleUrls: ['./review-transfer.page.scss'],
})
export class ReviewTransferPage implements OnInit {
    users: Observable<User[]>;
    searchText: string;
    buttonPressed: string;
    titlePage: string;
    travels: Observable<any>;

  constructor(
    private usersService: UsersService, 
    private route: ActivatedRoute,
    private travelsService:TravelsService

    ) 
    { 
    this.buttonPressed = this.route.snapshot.paramMap.get('buttonPressed');
    this.titlePage = (this.buttonPressed == 'users') ? 'Usuarios' : 'Conductores';
  }

  ngOnInit() {
      this.users = this.usersService.read();
      this.travels = this.travelsService.getTravelsTransfer();
  }

  onSearchChange(event) {
    this.searchText = event.detail.value; 
  }

  greeting(): void {
    console.log('Hola...');
  }
}
