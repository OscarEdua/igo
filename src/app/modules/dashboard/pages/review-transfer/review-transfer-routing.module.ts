import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReviewTransferPage } from './review-transfer.page';

const routes: Routes = [
  {
    path: '',
    component: ReviewTransferPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReviewTransferPageRoutingModule {}
