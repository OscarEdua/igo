import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReviewTransferPageRoutingModule } from './review-transfer-routing.module';

import { ReviewTransferPage } from './review-transfer.page';
import { PipesModule } from '../../../../shared/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReviewTransferPageRoutingModule,
    PipesModule
  ],
  declarations: [ReviewTransferPage]
})
export class ReviewTransferPageModule {}
