import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchDriverPageRoutingModule } from './search-driver-routing.module';

import { SearchDriverPage } from './search-driver.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchDriverPageRoutingModule
  ],
  declarations: [SearchDriverPage]
})
export class SearchDriverPageModule {}
