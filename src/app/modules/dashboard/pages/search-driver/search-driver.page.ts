import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AnimationController } from '@ionic/angular';
import { ActivatedRoute } from "@angular/router";
import { TravelsService } from '../../services/admin/travels.service';
import { Observable } from 'rxjs';
import { Travel } from '../../interfaces/travel.interface';
import { StorageService } from '../../../../shared/services/storage.service';

@Component({
  selector: 'app-search-driver',
  templateUrl: './search-driver.page.html',
  styleUrls: ['./search-driver.page.scss'],
})

export class SearchDriverPage implements OnInit {
  @ViewChild('circle') circle: ElementRef;
  @ViewChild('circleSmall') circleSmall: ElementRef;
  @ViewChild('circleVerySmall') circleVerySmall: ElementRef;
  travelId: string;
  travel: Observable<Travel>;

  constructor(
    private travelsService: TravelsService,
    private activatedRoute: ActivatedRoute,
    private animationCtrl: AnimationController,
    public element: ElementRef,
    private router: Router,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    this.travelId = this.activatedRoute.snapshot.paramMap.get('travelId');
    this.travel = this.travelsService.getTravelId(this.travelId);
    this.storageService.set('travelId', this.travelId);
  }

  ionViewDidEnter() {
    this.animationAllCircles();
  }

  animationAllCircles() {
    this.animationIncreaseCircle(this.circleVerySmall.nativeElement).play();
    this.animationIncreaseCircle(this.circleSmall.nativeElement).play();
    this.animationIncreaseCircle(this.circle.nativeElement).play();
  }

  animationIncreaseCircle(element: any) {
    const animation = this.animationCtrl.create();
    animation
      .addElement(element)
      .duration(2500)
      .iterations(Infinity)
      .fromTo('transform', 'scale(1)', 'scale(1.3)')
      .fromTo('opacity', '1', '0.7');
    return animation;
  }

  goToDashBoard(){
    this.router.navigate(['dashboard/select-payment-type/' + this.travelId]);
  }
}
