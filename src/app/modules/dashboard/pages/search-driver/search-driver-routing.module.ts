import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchDriverPage } from './search-driver.page';

const routes: Routes = [
  {
    path: '',
    component: SearchDriverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchDriverPageRoutingModule {}
