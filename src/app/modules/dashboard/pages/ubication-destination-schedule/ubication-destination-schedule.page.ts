import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { UbicationDestinationScheduleModalPage } from '../modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal.page';

@Component({
  selector: 'app-ubication-destination-schedule',
  templateUrl: './ubication-destination-schedule.page.html',
  styleUrls: ['./ubication-destination-schedule.page.scss'],
})
export class UbicationDestinationSchedulePage implements OnInit {

  constructor(public alertsService: AlertsService) { }

  ngOnInit() {
  }

  openModal(): void {
    this.alertsService.presentModalWithoutTravelData(UbicationDestinationScheduleModalPage, 'ubication-destination-schedula-modal');
  }
}
