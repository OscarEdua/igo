import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UbicationDestinationSchedulePage } from './ubication-destination-schedule.page';

const routes: Routes = [
  {
    path: '',
    component: UbicationDestinationSchedulePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UbicationDestinationSchedulePageRoutingModule {}
