import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { UbicationDestinationSchedulePageRoutingModule } from './ubication-destination-schedule-routing.module';
import { UbicationDestinationSchedulePage } from './ubication-destination-schedule.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UbicationDestinationSchedulePageRoutingModule,
    ComponentsModule
  ],
  declarations: [UbicationDestinationSchedulePage]
})

export class UbicationDestinationSchedulePageModule {}
