import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { IonContent } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { ChatService } from '../../services/admin/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  messages: Observable<any[]>;
  newMsg = '';
  currentUser: User = null;
  currentEmail: string;
  transmitterEmail: string;
  receiberEmail: string;
  userDoc: AngularFirestoreDocument<User>;
  user: Observable<User>;

  constructor(private chatService: ChatService, private afAuth: AngularFireAuth, private afs: AngularFirestore, private activatedRoute: ActivatedRoute ) { }

  ngOnInit() {
    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
      this.userDoc = this.afs.doc<User>('users/' + user.uid);
      this.user = this.userDoc.valueChanges();
      this.user.subscribe((currentUser) => {
        switch (currentUser.role) {
          case 'client':
            this.transmitterEmail = currentUser.email;
            this.receiberEmail = this.activatedRoute.snapshot.paramMap.get('email');
            break;
          case 'driver':
            this.transmitterEmail = this.activatedRoute.snapshot.paramMap.get('email');
            this.receiberEmail = currentUser.email;
            break;
        }
        this.messages = this.chatService.getChatMessages(this.transmitterEmail, this.receiberEmail);
        this.currentEmail = currentUser.email;
      });
    });
  }

  sendMessage() {
    this.chatService.addChatMessage(this.newMsg, this.transmitterEmail, this.receiberEmail).then(() => {
      this.newMsg = '';
      this.content.scrollToBottom();
    });
  }
}
