import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SuccessfulArrivalPageRoutingModule } from './successful-arrival-routing.module';
import { SuccessfulArrivalPage } from './successful-arrival.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuccessfulArrivalPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SuccessfulArrivalPage]
})

export class SuccessfulArrivalPageModule {}
