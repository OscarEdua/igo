import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuccessfulArrivalPage } from './successful-arrival.page';

const routes: Routes = [
  {
    path: '',
    component: SuccessfulArrivalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuccessfulArrivalPageRoutingModule {}
