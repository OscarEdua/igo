import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TravelsService } from '../../services/admin/travels.service';
import { AuthenticationService } from '../../../../core/authentication/authentication.service';
import { UsersService } from '../../../home/services/users.service';
import { User } from '../../../home/interfaces/user.interface';
import { Travel } from '../../interfaces/travel.interface';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  travels: Observable<any>;
  searchText: string;
  userId: string;
  oUser: Observable<User>;
  uUser: User;
  wholePartPrice: number;
  decimalPartPrice: string;
 
  constructor(
    private travelsService:TravelsService,
    private usersService: UsersService, 
    private autenticationService: AuthenticationService, 

  ) {
    this.userId = this.autenticationService.getCurrentUserId();
    this.oUser = this.usersService.getUserById(this.userId);
    this.oUser.subscribe(user => this.uUser = user);
   }

  ngOnInit() {
    this.travels = this.travelsService.getTravelsHistorial();

  }

  onSearchChange(event) {
    this.searchText = event.detail.value; 
  }
}
