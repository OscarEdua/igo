import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersPage } from '../users/users.page';

import { DashboardPage } from './dashboard.page';
import { RequestMenuPage } from '../request-menu/request-menu.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardPage
  },
  {
    path: 'users/:buttonPressed',
    component: UsersPage
  },
  {
    path: 'resquest-menu',
    component: RequestMenuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardPageRoutingModule {}
