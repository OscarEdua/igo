import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { UsersService } from 'src/app/modules/home/services/users.service';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { StartTravelPage } from '../modals/start-travel/start-travel.page';
import { Router } from '@angular/router';
import { MapsService } from '../../../../shared/services/maps.service';
const { Geolocation } = Plugins;

import { Plugins } from '@capacitor/core';
import { resolve } from '@angular/compiler-cli/src/ngtsc/file_system';
import { Marker } from '../../interfaces/marker';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  user: User;
  observableList: any[] = [];
  markers = [];
  marker: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  constructor(
    private menuController: MenuController,
    private authenticationService: AuthenticationService,
    private usersService: UsersService,
    private alertsService: AlertsService,
    private router: Router,
    private mapsService: MapsService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.loadDataPageDashboard();
  }

  async loadDataPageDashboard() {
    await this.getDataUser();
    await this.mapsService.loadmap('dashboardmap');
    this.generateMarksMyLocation();
    this.openModal();
  }
  generateMarksMyLocation() {
    Geolocation.getCurrentPosition().then((res) => {
      this.marker.position.lat = res.coords.latitude;
      this.marker.position.lng = res.coords.longitude;
      this.marker.title = 'Mi Ubicación';
      this.addMarker(this.marker, this.marker.title, 'myLocation');
    });
  }

  addMarker(marker: Marker, name: String, typeMarket: string) {
    let marke;
    let icon;
    if (typeMarket == 'myLocation') {
      icon = {
        url: '../../assets/img/ic_loc.png',
        //size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(60, 60),
      };
      /*     const image = '../../assets/img/Pineapplemenu.png'; */
    } else if (typeMarket == 'stops') {
      icon = {
        url: '../../assets/img/flag.png',
        //size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(25, 25),
      };
    }

    const geocoder = new google.maps.Geocoder();
    marke = new google.maps.Marker({
      position: marker.position,
      //  draggable: true,
      // animation: google.maps.Animation.DROP,
      map: this.mapsService.map,
      icon: icon,
      title: marker.title,
    });
    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          marke.getPosition().lat();
          marke.getPosition().lng();
          results[0].formatted_address;
        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
    const infoWindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marke, 'click', () => {
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            infoWindow.setContent(
              '<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address
            );
            if (infoWindow !== null) {
            }
            infoWindow.open(this.mapsService.map, marke);
          } else {
          }
        } else {
        }
      });
    });
    this.markers.push(marke);
  }

  getDataUser() {
    return new Promise((resolve) => {
      const observable = this.usersService
        .getUserById(this.authenticationService.getCurrentUserId())
        .subscribe((user) => {
          this.user = user;
          resolve(this.user);
        });
      this.observableList.push(observable);
    });
  }

  openMenu() {
    this.menuController.open('principal');
  }

  openModal(): void {
    this.alertsService.presentModalWithoutTravelData(
      StartTravelPage,
      'my-custom-class'
    );
  }

  redirectTo(path: string): void {
    this.router.navigate([this.router.url + '/' + path]);
  }
  //desde
  deleteMarkers() {
    this.setMapOnAll(null);
    this.markers = [];
  }
  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }
  ionViewWillLeave() {
    this.deleteMarkers();
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
