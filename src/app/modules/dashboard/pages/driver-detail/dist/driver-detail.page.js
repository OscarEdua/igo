"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.DriverDetailPage = void 0;
var core_1 = require("@angular/core");
var DriverDetailPage = /** @class */ (function () {
    function DriverDetailPage(usersService, activatedRoute, alertService) {
        this.usersService = usersService;
        this.activatedRoute = activatedRoute;
        this.alertService = alertService;
        this.driverId = this.activatedRoute.snapshot.paramMap.get('driverId');
    }
    DriverDetailPage.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.usersService.getUserById(this.driverId);
        this.user.subscribe(function (data) {
            _this.uUser = data;
        });
    };
    DriverDetailPage.prototype.updateTypeVehicleByUid = function () {
        this.uUser.vehicle.type = this.typeDriver;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Actualización en el conductor Satisfactoria');
    };
    DriverDetailPage.prototype.unsuscribeUser = function () {
        this.uUser.isActive = false;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Usuario Desactivado');
    };
    DriverDetailPage.prototype.suscribeUser = function () {
        this.uUser.isActive = true;
        this.usersService.update(this.uUser);
        this.alertService.presentAlert('Usuario Activado');
    };
    DriverDetailPage.prototype.checkTypeVehicle = function (event) {
        this.typeDriver = event.detail.value;
    };
    DriverDetailPage = __decorate([
        core_1.Component({
            selector: 'app-driver-detail',
            templateUrl: './driver-detail.page.html',
            styleUrls: ['./driver-detail.page.scss']
        })
    ], DriverDetailPage);
    return DriverDetailPage;
}());
exports.DriverDetailPage = DriverDetailPage;
