import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { UsersService } from 'src/app/modules/home/services/users.service';
import { ActivatedRoute } from '@angular/router';
import { AlertsService } from 'src/app/shared/services/alerts.service';

@Component({
  selector: 'app-driver-detail',
  templateUrl: './driver-detail.page.html',
  styleUrls: ['./driver-detail.page.scss'],
})
export class DriverDetailPage implements OnInit {
  user: Observable<User>;
  driverId: string;
  uUser: User;
  typeDriver: string;

  constructor(
    private usersService: UsersService, 
    private activatedRoute: ActivatedRoute,
    private alertService: AlertsService,
    ) {
    this.driverId = this.activatedRoute.snapshot.paramMap.get('driverId');
  }

  ngOnInit() {
    this.user = this.usersService.getUserById(this.driverId);
    this.user.subscribe(data =>{
      this.uUser = data as User;
    })
  }
  updateTypeVehicleByUid(){
    this.uUser.vehicle.type = this.typeDriver;
    this.usersService.update(this.uUser);
    this.alertService.presentAlert('Actualización en el conductor Satisfactoria');
  }

  unsuscribeUser(){
    this.uUser.isActive = false;
    this.usersService.update(this.uUser);
    this.alertService.presentAlert('Usuario Desactivado');
  }

  suscribeUser(){
    this.uUser.isActive = true;
    this.usersService.update(this.uUser);
    this.alertService.presentAlert('Usuario Activado');
  }

  checkTypeVehicle(event) {
    this.typeDriver = event.detail.value;
  }

}
