import { Component, OnInit } from '@angular/core';
import { Vehicle } from '../../../home/interfaces/vehicle.interface';
import { AuthenticationService } from '../../../../core/authentication/authentication.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { UsersService } from '../../../home/services/users.service';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { ImagesService } from 'src/app/shared/services/images.service';

@Component({
  selector: 'app-edit-vehicle-data',
  templateUrl: './edit-vehicle-data.page.html',
  styleUrls: ['./edit-vehicle-data.page.scss'],
})

export class EditVehicleDataPage implements OnInit {
  userId: string;
  oUser: Observable<User>;
  uUser: User;
  plateImagenFile: File;
  vehicleImagenFile: File;
  licenseImagenFile: File;

  constructor(
    private autenticationService: AuthenticationService, 
    private usersService: UsersService, 
    private alertsService: AlertsService,
    private imagesService: ImagesService
  ) {
    this.userId = this.autenticationService.getCurrentUserId();
    this.oUser = this.usersService.getUserById(this.userId);
    this.oUser.subscribe(user => this.uUser = user);
    this.plateImagenFile = null;
    this.vehicleImagenFile = null;
    this.licenseImagenFile = null;
  }

  ngOnInit() {    
  }

  async update(user: User) {
    await this.alertsService.presentLoading('Editando...');
    if(this.plateImagenFile != null){
      user.vehicle.plateImagen = await this.imagesService.uploadImage('vehicles', this.plateImagenFile);
    }
    if(this.plateImagenFile != null){
      user.vehicle.plateImagen = await this.imagesService.uploadImage('vehicles', this.plateImagenFile);
    }
    if(this.plateImagenFile != null){
      user.vehicle.imagen = await this.imagesService.uploadImage('vehicles', this.vehicleImagenFile);
    }
    if(this.plateImagenFile != null){
      user.vehicle.licenseImagen = await this.imagesService.uploadImage('vehicles', this.licenseImagenFile);
    }    
    this.usersService.update(user)
    .then(() => {
      this.alertsService.loading.dismiss();
      this.alertsService.presentAlertWithHeader('Editar!', 'Datos editados correctamente!');
    })
    .catch(() => {
      this.alertsService.loading.dismiss();
      this.alertsService.presentAlertWithHeader('Lo sentimos!', 'Por favor vuelva a intentarlo!');
    });
  }

  changeFileImagen($event, inputFile: string): void {
    if ($event.target.files && $event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL($event.target.files[0]);
      reader.onload = (event: any) => {
        switch (inputFile) {
          case 'plateImagen':
            this.plateImagenFile = $event.target.files[0];
            this.uUser.vehicle.plateImagen = event.target.result;
            break;
          case 'vehicleImagen':
            this.vehicleImagenFile = $event.target.files[0];
            this.uUser.vehicle.imagen = event.target.result;
            break;
          case 'licenseImagen':
            this.licenseImagenFile = $event.target.files[0];
            this.uUser.vehicle.licenseImagen = event.target.result;
            break;
        }
      };
    }
  }
}
