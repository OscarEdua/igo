import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewStopPage } from './new-stop.page';

const routes: Routes = [
  {
    path: '',
    component: NewStopPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewStopPageRoutingModule {}
