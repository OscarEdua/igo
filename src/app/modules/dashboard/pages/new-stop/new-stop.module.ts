import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NewStopPageRoutingModule } from './new-stop-routing.module';
import { NewStopPage } from './new-stop.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewStopPageRoutingModule,
    ComponentsModule
  ],
  declarations: [NewStopPage]
})
export class NewStopPageModule {}
