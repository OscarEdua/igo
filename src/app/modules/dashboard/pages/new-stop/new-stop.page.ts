import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { StopsService } from '../../services/admin/stops.service';
import { AlertsService } from '../../../../shared/services/alerts.service';
import { Stop } from '../../interfaces/stop.interface';
import { Plugins } from '@capacitor/core';
const { Geolocation } = Plugins;
//Librerias para generar la marca
declare var google;
//Interfaz creada para crear los  marcadores
interface Marker {
  position: {
    lat: number;
    lng: number;
  };
  title: string;
}
@Component({
  selector: 'app-new-stop',
  templateUrl: './new-stop.page.html',
  styleUrls: ['./new-stop.page.scss'],
})
export class NewStopPage implements OnInit {
  //variables para generar el mapa servicio y rederere
  map: any;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  markers = [];
  stop: Stop = {
    name: '',
    description: '',
    city: '',
    provinces: '',
    address: '',
    lat: 0,
    lng: 0,
    state: '',
    isActive: true,
  };
  GoogleAutocomplete: any;
  sessionToken = new google.maps.places.AutocompleteSessionToken();
  constructor(
    private router: Router,
    public loadingController: LoadingController,
    private stopsService: StopsService,
    private alertsService: AlertsService,
    private menuController: MenuController
  ) {}
  ngOnInit() {}
  ionViewWillEnter() {
    this.loadmap();
    this.GoogleAutocomplete = new google.maps.places.Autocomplete(
      document.getElementById('stop_address').getElementsByTagName('input')[0],
      {
        types: ['address'],
        sessionToken: this.sessionToken,
        componentRestrictions: { country: 'ec' },
        fields: [
          'address_components',
          'geometry',
          'icon',
          'name',
          'formatted_address',
        ],
      }
    );
  }

  //subir datos
  async addstop() {
    if (
      this.stop.name != '' &&
      this.stop.description != '' &&
      this.stop.address != '' &&
      this.stop.lat != 0 &&
      this.stop.lng != 0 &&
      this.stop.state != ''
    ) {
      this.stopsService.addStop(this.stop);
      this.alertsService.presentAlert('Parada Agregada');
      this.router.navigate(['dashboard/stops']);
    } else {
      this.alertsService.presentAlert('Debe ingresar todos los datos');
    }
  }

  ////////////////////////////////API DE GOOGLE MAPS/////////////////////////////////////////////////////////////

  //Cargar el mapa
  async loadmap() {
    let latLng;
    let market;
    const coordinates = await Geolocation.getCurrentPosition();
    latLng = new google.maps.LatLng(
      coordinates.coords.latitude,
      coordinates.coords.longitude
    );
    market = {
      position: {
        lat: coordinates.coords.latitude,
        lng: coordinates.coords.longitude,
      },
      title: 'Mi Ubicación',
    };
    //CUANDO TENEMOS LAS COORDENADAS SIMPLEMENTE NECESITAMOS PASAR AL MAPA DE GOOGLE TODOS LOS PARAMETROS.
    const mapEle: HTMLElement = document.getElementById('mapstops');
    // this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    // Crear el mapa
    this.map = new google.maps.Map(mapEle, {
      center: latLng,
      zoom: 14,
      //estilo del mapa (color gris)
      styles: [
        {
          elementType: 'geometry',
          stylers: [
            {
              color: '#f5f5f5',
            },
          ],
        },
        {
          elementType: 'labels.icon',
          stylers: [
            {
              visibility: 'off',
            },
          ],
        },
        {
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#616161',
            },
          ],
        },
        {
          elementType: 'labels.text.stroke',
          stylers: [
            {
              color: '#f5f5f5',
            },
          ],
        },
        {
          featureType: 'administrative.land_parcel',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#bdbdbd',
            },
          ],
        },
        {
          featureType: 'poi',
          elementType: 'geometry',
          stylers: [
            {
              color: '#eeeeee',
            },
          ],
        },
        {
          featureType: 'poi',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#757575',
            },
          ],
        },
        {
          featureType: 'poi.park',
          elementType: 'geometry',
          stylers: [
            {
              color: '#e5e5e5',
            },
          ],
        },
        {
          featureType: 'poi.park',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#9e9e9e',
            },
          ],
        },
        {
          featureType: 'road',
          elementType: 'geometry',
          stylers: [
            {
              color: '#ffffff',
            },
          ],
        },
        {
          featureType: 'road.arterial',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#757575',
            },
          ],
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry',
          stylers: [
            {
              color: '#dadada',
            },
          ],
        },
        {
          featureType: 'road.highway',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#616161',
            },
          ],
        },
        {
          featureType: 'road.local',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#9e9e9e',
            },
          ],
        },
        {
          featureType: 'transit.line',
          elementType: 'geometry',
          stylers: [
            {
              color: '#e5e5e5',
            },
          ],
        },
        {
          featureType: 'transit.station',
          elementType: 'geometry',
          stylers: [
            {
              color: '#eeeeee',
            },
          ],
        },
        {
          featureType: 'water',
          elementType: 'geometry',
          stylers: [
            {
              color: '#c9c9c9',
            },
          ],
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#9e9e9e',
            },
          ],
        },
      ],
    });

    //  this.addMarker(market);
    //colocar el mapa con los parametros
    this.directionsDisplay.setMap(this.map);
    mapEle.classList.add('show-map');
  }
  //Auto completado
  //busqueda de datos por me dio de un input
  keyUpHandler() {
    if (this.stop.address.length > 0) {
      google.maps.event.addListener(
        this.GoogleAutocomplete,
        `place_changed`,
        () => {
          const place = this.GoogleAutocomplete.getPlace();
          let market = {
            position: {
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng(),
            },
            title: 'Mi Ubicación',
          };
          var centro = {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng(),
          };
          this.map.setCenter(centro);
          this.addMarker(market);
        }
      );
    }
  }
  //añadir marcador
  addMarker(marker: Marker) {
    this.deleteMarkers();
    let marke;
    const meta = '../../assets/img/stops.png';
    const icon2 = {
      url: meta,
      /*     size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
         anchor: new google.maps.Point(40, 60), */
      scaledSize: new google.maps.Size(100, 100),
    };
    marke = new google.maps.Marker({
      position: marker.position,
      draggable: true,
      animation: google.maps.Animation.DROP,
      map: this.map,
      icon: icon2,
      title: marker.title,
    });
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          //Info Windows de google
          var content = results[0].formatted_address;
          var infoWindow = new google.maps.InfoWindow({
            content: content,
          });
          //   infoWindow.close(this.map, marke);
          this.stop.lat = marke.getPosition().lat();
          this.stop.lng = marke.getPosition().lng();
          this.stop.address = results[0].formatted_address;
          this.stop.city = results[4].formatted_address;
          this.stop.provinces = results[7].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
    google.maps.event.addListener(marke, 'dragend', () => {
      var currentInfoWindow = null;
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            //Info Windows de google
            var content = results[0].formatted_address;
            var infoWindow = new google.maps.InfoWindow({
              content: content,
            });
            //   infoWindow.close(this.map, marke);
            this.stop.lat = marke.getPosition().lat();
            this.stop.lng = marke.getPosition().lng();
            this.stop.address = results[0].formatted_address;
            this.stop.city = results[4].formatted_address;
            this.stop.provinces = results[7].formatted_address;
          } else {
            window.alert('No results found');
          }
        } else {
          console.log('Geocoder failed due to: ' + status);
        }
      });
    });
    this.markers.push(marke);
  }
  //funciones para eliminar las marcas de mapa de google
  //desde
  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
  }
  clearMarkers() {
    this.setMapOnAll(null);
  }
  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }
  //hasta
  openMenu() {
    this.menuController.open('principal');
  }
}
