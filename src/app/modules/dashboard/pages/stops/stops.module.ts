import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StopsPageRoutingModule } from './stops-routing.module';

import { StopsPage } from './stops.page';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StopsPageRoutingModule,
    PipesModule
  ],
  declarations: [StopsPage]
})
export class StopsPageModule {}
