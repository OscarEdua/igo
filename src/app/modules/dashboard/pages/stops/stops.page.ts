/// <reference path="../../../../../../node_modules/@types/googlemaps/index.d.ts" />
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { StopUpdateComponent } from '../../components/stop-update/stop-update.component';
import { Stop } from '../../interfaces/stop.interface';
import { StopsService } from '../../services/admin/stops.service';

@Component({
  selector: 'app-stops',
  templateUrl: './stops.page.html',
  styleUrls: ['./stops.page.scss'],
})
export class StopsPage implements OnInit {
  map: google.maps.Map;
  /*   stops: Observable<Stop[]>; */
  searchText: string;
  stops: Stop[];
  observableList: any[] = [];
  constructor(
    private stopsService: StopsService,
    private alertService: AlertsService
  ) {
    this.stops = [];
  }

  ngOnInit() {
    // this.stops = this.stopsService.read();
  }
  ionViewWillEnter() {
    this.getData();
  }

  async getData() {
    /*     this.stops = (await this.getListStops()) as Stop[]; */

    await this.getListStops();
  }
  getListStops() {
    return new Promise((resolve) => {
      const observable = this.stopsService.getStop().subscribe((res) => {
        this.stops = res as Stop[];
        resolve(this.stops);
      });
      this.observableList.push(observable);
    });
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  delete(stopId: string) {
    this.alertService
      .presentAlertConfirm('Paradas!', 'Esta seguro de borrar la parada?')
      .then((result) => {
        if (result) {
          this.stopsService.deleteStop(stopId);
        }
      })
      .catch((result) => {
        this.alertService.presentAlert(
          'Lo sentimos no se pudo borrar la parada!'
        );
      });
  }

  openModal(stopId: string, stop: Stop) {
    this.alertService.presentModal(StopUpdateComponent, {
      stopId: stopId,
      address: stop.address,
      name: stop.name,
      description: stop.description,
      city: stop.city,
      provinces: stop.provinces,
      lat: stop.lat,
      lng: stop.lng,
      state: stop.state,
      isActive: stop.isActive,
    });
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
