import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { ScheduleTripModalPage } from '../modals/schedule-trip-modal/schedule-trip-modal.page';
import { MapsService } from 'src/app/shared/services/maps.service';
import { Plugins } from '@capacitor/core';
import { Marker } from '../../interfaces/marker';
import { MenuController } from '@ionic/angular';
const { Geolocation } = Plugins;

@Component({
  selector: 'app-schedule-trip',
  templateUrl: './schedule-trip.page.html',
  styleUrls: ['./schedule-trip.page.scss'],
})

export class ScheduleTripPage implements OnInit {
  observableList: any[] = [];
  markers = [];
  marker: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };

  constructor(
    private menuController: MenuController,
    private mapsService: MapsService,
    private alertsService: AlertsService) { }

  ngOnInit() {
  }
  
  ionViewWillEnter() {
    this.loadDataPageDashboard();
  }

  async loadDataPageDashboard() {
    await this.mapsService.loadmap('schedulemap');
    this.generateMarksMyLocation();
    this.openModal();
  }

  generateMarksMyLocation() {
    Geolocation.getCurrentPosition().then((res) => {
      this.marker.position.lat = res.coords.latitude;
      this.marker.position.lng = res.coords.longitude;
      this.marker.title = 'Mi Ubicación';
      this.addMarker(this.marker, this.marker.title, 'myLocation');
    });
  }
  
  addMarker(marker: Marker, name: String, typeMarket: string) {
    let marke;
    let icon;
    if (typeMarket == 'myLocation') {
      icon = {
        url: '../../assets/img/ic_loc.png',
        //size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(60, 60),
      };
      /*     const image = '../../assets/img/Pineapplemenu.png'; */
    } else if (typeMarket == 'stops') {
      icon = {
        url: '../../assets/img/flag.png',
        //size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(25, 25),
      };
    }
    const geocoder = new google.maps.Geocoder();
    marke = new google.maps.Marker({
      position: marker.position,
      //  draggable: true,
      // animation: google.maps.Animation.DROP,
      map: this.mapsService.map,
      icon: icon,
      title: marker.title,
    });
    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          marke.getPosition().lat();
          marke.getPosition().lng();
          results[0].formatted_address;
        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
    const infoWindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marke, 'click', () => {
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            infoWindow.setContent(
              '<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address
            );
            if (infoWindow !== null) {
            }
            infoWindow.open(this.mapsService.map, marke);
          } else {
          }
        } else {
        }
      });
    });
    this.markers.push(marke);
  }

  openMenu() {
    this.menuController.open('principal');
  }

  openModal(): void {
    this.alertsService.presentModalWithoutTravelData(ScheduleTripModalPage, 'medium-modal');
  }
} 
