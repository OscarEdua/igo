import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ScheduleTripPageRoutingModule } from './schedule-trip-routing.module';
import { ScheduleTripPage } from './schedule-trip.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScheduleTripPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ScheduleTripPage]
})
export class ScheduleTripPageModule {}
