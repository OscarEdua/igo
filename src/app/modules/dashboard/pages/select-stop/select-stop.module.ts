import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SelectStopPageRoutingModule } from './select-stop-routing.module';
import { SelectStopPage } from './select-stop.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectStopPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelectStopPage]
})

export class SelectStopPageModule {}
