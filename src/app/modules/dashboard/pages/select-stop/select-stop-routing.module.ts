import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectStopPage } from './select-stop.page';

const routes: Routes = [
  {
    path: '',
    component: SelectStopPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectStopPageRoutingModule {}
