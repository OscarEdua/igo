import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShippingParametersPage } from './shipping-parameters.page';

const routes: Routes = [
  {
    path: '',
    component: ShippingParametersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShippingParametersPageRoutingModule {}
