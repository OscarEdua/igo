import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { ShippingParameters } from '../../interfaces/shipping-parameters.interface';
import { ShippingParametersService } from '../../../../shared/services/shipping-parameters.service';
import { AlertsService } from '../../../../shared/services/alerts.service';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-shipping-parameters',
  templateUrl: './shipping-parameters.page.html',
  styleUrls: ['./shipping-parameters.page.scss'],
})
export class ShippingParametersPage implements OnInit {
  form: FormGroup;
  observableList: Subscription[] = [];

  setshippingParameters: ShippingParameters;
  getshippingParameters: ShippingParameters[];
  obsShippingParameters: Observable<ShippingParameters>;

  constructor(
    private shippingParametersService: ShippingParametersService,
    private alertService: AlertsService,
    private menuController: MenuController

  ) {
    this.form = new FormGroup({
      baseKilometers: new FormControl('', [Validators.required]),
      baseCost: new FormControl('', [Validators.required]),
      extraKilometerCost: new FormControl('', [Validators.required]),
      extraVip: new FormControl('', [Validators.required]),
      messageCall: new FormControl('', [Validators.required]),
      messageWhatsApp: new FormControl('', [Validators.required]),
      numCall: new FormControl('', [Validators.required]),
      numEmergency: new FormControl('', [Validators.required]),
      numWhatsApp: new FormControl('', [Validators.required]),
      
    });
  }

  ngOnInit() {
    this.getParametrers();
  }

  getParametrers() {
    const observable = this.shippingParametersService.getparametes().subscribe((res) => {
      this.getshippingParameters = res as ShippingParameters[];
      this.form = new FormGroup({
        baseKilometers: new FormControl(this.getshippingParameters['baseKilometers'], [Validators.required,]),
        baseCost: new FormControl(this.getshippingParameters['baseCost'], [Validators.required]),
        extraKilometerCost: new FormControl(this.getshippingParameters['extraKilometerCost'], [Validators.required,]),
        extraVip: new FormControl(this.getshippingParameters['extraVip'], [Validators.required]),
        messageCall: new FormControl(this.getshippingParameters['messageCall'], [Validators.required]),
        messageWhatsApp: new FormControl(this.getshippingParameters['messageWhatsApp'], [Validators.required]),
        numCall: new FormControl(this.getshippingParameters['numCall'], [Validators.required]),
        numEmergency: new FormControl(this.getshippingParameters['numEmergency'], [Validators.required]),
        numWhatsApp: new FormControl(this.getshippingParameters['numWhatsApp'], [Validators.required])
      });
    });
  }

  update(): void {
    this.setshippingParameters = {
      baseKilometers: this.form.controls.baseKilometers.value,
      baseCost: this.form.controls.baseCost.value,
      extraKilometerCost: this.form.controls.extraKilometerCost.value,
      extraVip: this.form.controls.extraVip.value,
      messageCall: this.form.controls.messageCall.value,
      messageWhatsApp: this.form.controls.messageWhatsApp.value,
      numCall: this.form.controls.numCall.value,
      numEmergency: this.form.controls.numEmergency.value,
      numWhatsApp: this.form.controls.numWhatsApp.value
    };
    this.shippingParametersService
      .update(this.setshippingParameters)
      .then(() => this.alertService.presentAlert('Parametros Actualizados ✔'))
      .catch((error) => console.log(error));
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
   
  openMenu() {
    this.menuController.open('principal');
  }
}
