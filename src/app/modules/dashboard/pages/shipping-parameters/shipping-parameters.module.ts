import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShippingParametersPageRoutingModule } from './shipping-parameters-routing.module';

import { ShippingParametersPage } from './shipping-parameters.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShippingParametersPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule
  ],
  declarations: [ShippingParametersPage]
})
export class ShippingParametersPageModule {}
