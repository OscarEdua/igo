import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AnimationController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { TravelsService } from '../../services/admin/travels.service';
import { Travel } from '../../interfaces/travel.interface';
import { ShippingParameters } from '../../interfaces/shipping-parameters.interface';
import { ShippingParametersService } from '../../../../shared/services/shipping-parameters.service';

@Component({
  selector: 'app-search-complete',
  templateUrl: './search-complete.page.html',
  styleUrls: ['./search-complete.page.scss'],
})

export class SearchCompletePage implements OnInit {
  @ViewChild('circle') circle: ElementRef;
  @ViewChild('circleSmall') circleSmall: ElementRef;
  @ViewChild('circleVerySmall') circleVerySmall: ElementRef;
  travelId: string;
  travel: Observable<Travel>;
  numbersEmergency: ShippingParameters[] = [];

  constructor(
    private router:Router,
    private activatedRoute: ActivatedRoute,
    private travelsService: TravelsService,
    private shippingParametersService: ShippingParametersService,
    private animationCtrl: AnimationController) {
    this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
  }

  ngOnInit() {
    this.travel = this.travelsService.getTravelId(this.travelId);
    console.log(this.travelId);
    this.travel.subscribe(travel => console.log(travel));
    this.getParameters()
  }

  getParameters(){
    this.shippingParametersService.getCollection<ShippingParameters>().subscribe((res) => {
     this.numbersEmergency= res;
   });
 }

  onNavigate(){
    window.open("https://api.whatsapp.com/send/?phone=593"+this.numbersEmergency[0].numCall+"&text="+this.numbersEmergency[0].messageCall+"&app_absent=0", "_blank");
  }

  ionViewDidEnter() {
    this.animationAllCircles();
  }
  
  animationAllCircles() {
    this.animationIncreaseCircle(this.circleVerySmall.nativeElement).play();
    this.animationIncreaseCircle(this.circleSmall.nativeElement).play();
    this.animationIncreaseCircle(this.circle.nativeElement).play();
  }

  animationIncreaseCircle(element: any) {
    const animation = this.animationCtrl.create();
    animation
      .addElement(element)
      .duration(2500)
      .iterations(Infinity)
      .fromTo('transform', 'scale(1)', 'scale(1.3)')
      .fromTo('opacity', '1', '0.7');
    return animation;
  }

  goToMethodPayment() {
    this.router.navigate(['dashboard/transfer/', this.travelId]);
  }

  goToTravel() { 
    this.router.navigate(['dashboard/travel', this.travelId, 'client']);
  }

  
  
}
