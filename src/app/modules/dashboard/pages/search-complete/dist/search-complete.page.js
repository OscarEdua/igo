"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SearchCompletePage = void 0;
var core_1 = require("@angular/core");
var SearchCompletePage = /** @class */ (function () {
    function SearchCompletePage(router, activatedRoute, travelsService, shippingParametersService, animationCtrl) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.travelsService = travelsService;
        this.shippingParametersService = shippingParametersService;
        this.animationCtrl = animationCtrl;
        this.numbersEmergency = [];
        this.travelId = activatedRoute.snapshot.paramMap.get('travelId');
    }
    SearchCompletePage.prototype.ngOnInit = function () {
        this.travel = this.travelsService.getTravelId(this.travelId);
        console.log(this.travelId);
        this.travel.subscribe(function (travel) { return console.log(travel); });
        this.getParameters();
    };
    SearchCompletePage.prototype.getParameters = function () {
        var _this = this;
        this.shippingParametersService.getCollection().subscribe(function (res) {
            _this.numbersEmergency = res;
        });
    };
    SearchCompletePage.prototype.onNavigate = function () {
        window.open("https://api.whatsapp.com/send/?phone=593" + this.numbersEmergency[0].numCall + "&text=" + this.numbersEmergency[0].messageCall + "&app_absent=0", "_blank");
    };
    SearchCompletePage.prototype.ionViewDidEnter = function () {
        this.animationAllCircles();
    };
    SearchCompletePage.prototype.animationAllCircles = function () {
        this.animationIncreaseCircle(this.circleVerySmall.nativeElement).play();
        this.animationIncreaseCircle(this.circleSmall.nativeElement).play();
        this.animationIncreaseCircle(this.circle.nativeElement).play();
    };
    SearchCompletePage.prototype.animationIncreaseCircle = function (element) {
        var animation = this.animationCtrl.create();
        animation
            .addElement(element)
            .duration(2500)
            .iterations(Infinity)
            .fromTo('transform', 'scale(1)', 'scale(1.3)')
            .fromTo('opacity', '1', '0.7');
        return animation;
    };
    SearchCompletePage.prototype.goToMethodPayment = function () {
        this.router.navigate(['dashboard/transfer/', this.travelId]);
    };
    SearchCompletePage.prototype.goToTravel = function () {
        this.router.navigate(['dashboard/travel', this.travelId, 'client']);
    };
    __decorate([
        core_1.ViewChild('circle')
    ], SearchCompletePage.prototype, "circle");
    __decorate([
        core_1.ViewChild('circleSmall')
    ], SearchCompletePage.prototype, "circleSmall");
    __decorate([
        core_1.ViewChild('circleVerySmall')
    ], SearchCompletePage.prototype, "circleVerySmall");
    SearchCompletePage = __decorate([
        core_1.Component({
            selector: 'app-search-complete',
            templateUrl: './search-complete.page.html',
            styleUrls: ['./search-complete.page.scss']
        })
    ], SearchCompletePage);
    return SearchCompletePage;
}());
exports.SearchCompletePage = SearchCompletePage;
