import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchCompletePage } from './search-complete.page';

const routes: Routes = [
  {
    path: '',
    component: SearchCompletePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchCompletePageRoutingModule {}
