import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchCompletePageRoutingModule } from './search-complete-routing.module';

import { SearchCompletePage } from './search-complete.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchCompletePageRoutingModule
  ],
  declarations: [SearchCompletePage]
})
export class SearchCompletePageModule {}
