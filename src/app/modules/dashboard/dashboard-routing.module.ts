import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'users',
    loadChildren: () => import('./pages/users/users.module').then( m => m.UsersPageModule)
  },
  {
    path: 'stops',
    loadChildren: () => import('./pages/stops/stops.module').then( m => m.StopsPageModule)
  },
  {
    path: 'user-detail/:userId',
    loadChildren: () => import('./pages/user-detail/user-detail.module').then( m => m.UserDetailPageModule)
  },
  {
    path: 'reviews',
    loadChildren: () => import('./pages/reviews/reviews.module').then( m => m.ReviewsPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'driver-detail/:driverId',
    loadChildren: () => import('./pages/driver-detail/driver-detail.module').then( m => m.DriverDetailPageModule)
  },
  {
    path: 'request-menu',
    loadChildren: () => import('./pages/request-menu/request-menu.module').then( m => m.RequestMenuPageModule)
  },
  {
    path: 'requests',
    loadChildren: () => import('./pages/requests/requests.module').then( m => m.RequestsPageModule)
  },
  {
    path: 'travel/:travelId/:roleCurrentUser',
    loadChildren: () => import('./pages/travel/travel.module').then( m => m.TravelPageModule)
  },
  {
    path: 'successful-arrival',
    loadChildren: () => import('./pages/successful-arrival/successful-arrival.module').then( m => m.SuccessfulArrivalPageModule)
  },
  {
    path: 'recommendations/:travelId',
    loadChildren: () => import('./pages/recommendations/recommendations.module').then( m => m.RecommendationsPageModule)
  },
  {
    path: 'chat/:email',
    loadChildren: () => import('./pages/chat/chat.module').then( m => m.ChatPageModule)
  },
  {
    path: 'start-travel',
    loadChildren: () => import('./pages/modals/start-travel/start-travel.module').then( m => m.StartTravelPageModule)
  },
  {
    path: 'select-vehicle-type',
    loadChildren: () => import('./pages/modals/select-vehicle-type/select-vehicle-type.module').then( m => m.SelectVehicleTypePageModule)
  },
  {
    path: 'select-vehicle/:isTravelNow',
    loadChildren: () => import('./pages/select-vehicle/select-vehicle.module').then( m => m.SelectVehiclePageModule)
  },
  {
    path: 'history',
    loadChildren: () => import('./pages/history/history.module').then( m => m.HistoryPageModule)
  },
  {
    path: 'select-stop',
    loadChildren: () => import('./pages/select-stop/select-stop.module').then( m => m.SelectStopPageModule)
  },
  {
    path: 'select-stop-modal',
    loadChildren: () => import('./pages/modals/select-stop-modal/select-stop-modal.module').then( m => m.SelectStopModalPageModule)
  },
  {
    path: 'select-destination',
    loadChildren: () => import('./pages/select-destination/select-destination.module').then( m => m.SelectDestinationPageModule)
  },
  {
    path: 'select-destination-modal',
    loadChildren: () => import('./pages/modals/select-destination-modal/select-destination-modal.module').then( m => m.SelectDestinationModalPageModule)
  },
  {
    path: 'schedule-trip',
    loadChildren: () => import('./pages/schedule-trip/schedule-trip.module').then( m => m.ScheduleTripPageModule)
  },
  {
    path: 'schedule-trip-modal',
    loadChildren: () => import('./pages/modals/schedule-trip-modal/schedule-trip-modal.module').then( m => m.ScheduleTripModalPageModule)
  },
  {
    path: 'select-stop-schedule',
    loadChildren: () => import('./pages/select-stop-schedule/select-stop-schedule.module').then( m => m.SelectStopSchedulePageModule)
  },
  {
    path: 'select-stop-schedule-modal',
    loadChildren: () => import('./pages/modals/select-stop-schedule-modal/select-stop-schedule-modal.module').then( m => m.SelectStopScheduleModalPageModule)
  },
  {
    path: 'ubication-destination-schedule',
    loadChildren: () => import('./pages/ubication-destination-schedule/ubication-destination-schedule.module').then( m => m.UbicationDestinationSchedulePageModule)
  },
  {
    path: 'ubication-destination-schedule-modal',
    loadChildren: () => import('./pages/modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal.module').then( m => m.UbicationDestinationScheduleModalPageModule)
  },
  {
    path: 'destination-address',
    loadChildren: () => import('./pages/destination-address/destination-address.module').then( m => m.DestinationAddressPageModule)
  },
  {
    path: 'destination-address-modal',
    loadChildren: () => import('./pages/modals/destination-address-modal/destination-address-modal.module').then( m => m.DestinationAddressModalPageModule)
  },
  {
    path: 'edit-vehicle-data',
    loadChildren: () => import('./pages/edit-vehicle-data/edit-vehicle-data.module').then( m => m.EditVehicleDataPageModule)
  },
  {
    path: 'select-route',
    loadChildren: () => import('./pages/select-route/select-route.module').then( m => m.SelectRoutePageModule)
  },
  {
    path: 'select-route-modal',
    loadChildren: () => import('./pages/modals/select-route-modal/select-route-modal.module').then( m => m.SelectRouteModalPageModule)
  },
  {
    path: 'select-payment-type/:travelId',
    loadChildren: () => import('./pages/select-payment-type/select-payment-type.module').then( m => m.SelectPaymentTypePageModule)
  },
  {
    path: 'select-payment-modal',
    loadChildren: () => import('./pages/modals/select-payment-modal/select-payment-modal.module').then( m => m.SelectPaymentModalPageModule)
  },
  {
    path: 'transfer',
    loadChildren: () => import('./pages/transfer/transfer.module').then( m => m.TransferPageModule)
  },
  {
    path: 'transfer-modal',
    loadChildren: () => import('./pages/modals/transfer-modal/transfer-modal.module').then( m => m.TransferModalPageModule)
  },
  {
    path: 'search-driver/:travelId',
    loadChildren: () => import('./pages/search-driver/search-driver.module').then( m => m.SearchDriverPageModule)
  },
  {
    path: 'confirmed-trip',
    loadChildren: () => import('./pages/confirmed-trip/confirmed-trip.module').then( m => m.ConfirmedTripPageModule)
  },
  {
    path: 'travel-details',
    loadChildren: () => import('./pages/travel-details/travel-details.module').then( m => m.TravelDetailsPageModule)
  },
  {
    path: 'travel-details-modal',
    loadChildren: () => import('./pages/modals/travel-details-modal/travel-details-modal.module').then( m => m.TravelDetailsModalPageModule)
  },
  {
    path: 'travel-complete/:travelId',
    loadChildren: () => import('./pages/travel-complete/travel-complete.module').then( m => m.TravelCompletePageModule)
  },
  {
    path: 'in-travel-modal',
    loadChildren: () => import('./pages/modals/in-travel-modal/in-travel-modal.module').then( m => m.InTravelModalPageModule)
  },
  {
    path: 'recommendations-modal',
    loadChildren: () => import('./pages/modals/recommendations-modal/recommendations-modal.module').then( m => m.RecommendationsModalPageModule)
  },
  {
    path: 'search-complete',
    loadChildren: () => import('./pages/search-complete/search-complete.module').then( m => m.SearchCompletePageModule)
  },
  {
    path: 'shipping-parameters',
    loadChildren: () => import('./pages/shipping-parameters/shipping-parameters.module').then( m => m.ShippingParametersPageModule)
  },
  {
    path: 'review-transfer',
    loadChildren: () => import('./pages/review-transfer/review-transfer.module').then( m => m.ReviewTransferPageModule)
  },
  {
    path: 'select-stop-schedule-modal',
    loadChildren: () => import('./pages/modals/select-stop-schedule-modal/select-stop-schedule-modal.module').then( m => m.SelectStopScheduleModalPageModule)
  },
  {
    path: 'ubication-destination-schedule',
    loadChildren: () => import('./pages/ubication-destination-schedule/ubication-destination-schedule.module').then( m => m.UbicationDestinationSchedulePageModule)
  },
  {
    path: 'ubication-destination-schedule-modal',
    loadChildren: () => import('./pages/modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal.module').then( m => m.UbicationDestinationScheduleModalPageModule)
  },
  {
    path: 'destination-address',
    loadChildren: () => import('./pages/destination-address/destination-address.module').then( m => m.DestinationAddressPageModule)
  },
  {
    path: 'destination-address-modal',
    loadChildren: () => import('./pages/modals/destination-address-modal/destination-address-modal.module').then( m => m.DestinationAddressModalPageModule)
  },
  {
    path: 'edit-vehicle-data',
    loadChildren: () => import('./pages/edit-vehicle-data/edit-vehicle-data.module').then( m => m.EditVehicleDataPageModule)
  },
  {
    path: 'select-route',
    loadChildren: () => import('./pages/select-route/select-route.module').then( m => m.SelectRoutePageModule)
  },
  {
    path: 'select-route-modal',
    loadChildren: () => import('./pages/modals/select-route-modal/select-route-modal.module').then( m => m.SelectRouteModalPageModule)
  },
  {
    path: 'select-payment-type/:travelId',
    loadChildren: () => import('./pages/select-payment-type/select-payment-type.module').then( m => m.SelectPaymentTypePageModule)
  },
  {
    path: 'select-payment-modal',
    loadChildren: () => import('./pages/modals/select-payment-modal/select-payment-modal.module').then( m => m.SelectPaymentModalPageModule)
  },
  {
    path: 'transfer/:travelId',
    loadChildren: () => import('./pages/transfer/transfer.module').then( m => m.TransferPageModule)
  },
  {
    path: 'transfer-modal',
    loadChildren: () => import('./pages/modals/transfer-modal/transfer-modal.module').then( m => m.TransferModalPageModule)
  },
  {
    path: 'search-driver/:travelId',
    loadChildren: () => import('./pages/search-driver/search-driver.module').then( m => m.SearchDriverPageModule)
  },
  {
    path: 'confirmed-trip',
    loadChildren: () => import('./pages/confirmed-trip/confirmed-trip.module').then( m => m.ConfirmedTripPageModule)
  },
  {
    path: 'travel-details',
    loadChildren: () => import('./pages/travel-details/travel-details.module').then( m => m.TravelDetailsPageModule)
  },
  {
    path: 'travel-details-modal',
    loadChildren: () => import('./pages/modals/travel-details-modal/travel-details-modal.module').then( m => m.TravelDetailsModalPageModule)
  },
  {
    path: 'travel-complete/:travelId',
    loadChildren: () => import('./pages/travel-complete/travel-complete.module').then( m => m.TravelCompletePageModule)
  },
  {
    path: 'in-travel-modal',
    loadChildren: () => import('./pages/modals/in-travel-modal/in-travel-modal.module').then( m => m.InTravelModalPageModule)
  },
  {
    path: 'recommendations-modal',
    loadChildren: () => import('./pages/modals/recommendations-modal/recommendations-modal.module').then( m => m.RecommendationsModalPageModule)
  },
  {
    path: 'search-complete/:travelId',
    loadChildren: () => import('./pages/search-complete/search-complete.module').then( m => m.SearchCompletePageModule)
  },
  {
    path: 'shipping-parameters',
    loadChildren: () => import('./pages/shipping-parameters/shipping-parameters.module').then( m => m.ShippingParametersPageModule)
  },
  {
    path: 'review-transfer',
    loadChildren: () => import('./pages/review-transfer/review-transfer.module').then( m => m.ReviewTransferPageModule)
  },
  {
    path: 'confirm-transfers/:travelId',
    loadChildren: () => import('./pages/confirm-transfers/confirm-transfers.module').then( m => m.ConfirmTransfersPageModule)
  },  {
    path: 'bank-data',
    loadChildren: () => import('./pages/bank-data/bank-data.module').then( m => m.BankDataPageModule)
  }


];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class DashboardRoutingModule { }

