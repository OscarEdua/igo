export interface ShippingParameters {
  baseKilometers: number;
  baseCost: number;
  extraKilometerCost: number;
  extraVip: number;
  messageCall: string;
  messageWhatsApp: string;
  numCall: string;
  numEmergency: string;
  numWhatsApp: string;
}
