export interface Rating {
    uid?: string;
    travelId?: string;
    driverId?: string;
    clientId?: string;
    rate?: number;
    comment?: string;
    status?: string;
    isQualified?: boolean;
    createAt?: Date;
    updateAt?: Date;
}
