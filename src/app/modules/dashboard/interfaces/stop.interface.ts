export interface Stop {
  uid?: string;
  address?: string;
  name?: string;
  description?: string;
  city?: string;
  provinces?: string;
  lat?: number;
  lng?: number;
  state?: string;
  isActive?: boolean;
  createAt?: Date;
  updateDate?: Date;
}
