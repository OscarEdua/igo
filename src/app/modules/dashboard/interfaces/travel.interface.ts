import { Location } from './location.interface';
import { Music } from './music.interface';

export interface Travel {
    travelId?: string;
    numberTravel?: number;
    destinyLocation?: Location;
    startLocation?: Location;
    numberPassengers?: number;
    detail?: string;
    cost?: number;
    kilometers?: string;
    payMethod?: string;  
    clientId?: string;
    driverId?: string;
    invoiceImg?: string;
    travelType?: boolean;
    allowPets?: boolean;
    musicType?: Music;
    luggageSize?: string;
    status?: string;
    createdAt?: Date;
    updateAt?: Date;
    vehicleType?: string;
    allowLuggage?: boolean;
    allowSmok?: boolean;
    schedulingDate?: Date;
    currentPosition?: Location;
}
