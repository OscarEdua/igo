export interface BankParameters {
  accountNumber: number;
  email: string;
  nameBank: string;
  nameOwner: string;
  typeAccount: string;
}
