"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.DashboardRoutingModule = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var routes = [
    {
        path: '',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/dashboard/dashboard.module'); }).then(function (m) { return m.DashboardPageModule; }); }
    },
    {
        path: 'users',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/users/users.module'); }).then(function (m) { return m.UsersPageModule; }); }
    },
    {
        path: 'stops',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/stops/stops.module'); }).then(function (m) { return m.StopsPageModule; }); }
    },
    {
        path: 'user-detail/:userId',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/user-detail/user-detail.module'); }).then(function (m) { return m.UserDetailPageModule; }); }
    },
    {
        path: 'reviews',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/reviews/reviews.module'); }).then(function (m) { return m.ReviewsPageModule; }); }
    },
    {
        path: 'profile',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/profile/profile.module'); }).then(function (m) { return m.ProfilePageModule; }); }
    },
    {
        path: 'driver-detail/:driverId',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/driver-detail/driver-detail.module'); }).then(function (m) { return m.DriverDetailPageModule; }); }
    },
    {
        path: 'request-menu',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/request-menu/request-menu.module'); }).then(function (m) { return m.RequestMenuPageModule; }); }
    },
    {
        path: 'requests',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/requests/requests.module'); }).then(function (m) { return m.RequestsPageModule; }); }
    },
    {
        path: 'travel/:travelId',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/travel/travel.module'); }).then(function (m) { return m.TravelPageModule; }); }
    },
    {
        path: 'successful-arrival',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/successful-arrival/successful-arrival.module'); }).then(function (m) { return m.SuccessfulArrivalPageModule; }); }
    },
    {
        path: 'recommendations/:travelId',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/recommendations/recommendations.module'); }).then(function (m) { return m.RecommendationsPageModule; }); }
    },
    {
        path: 'chat',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/chat/chat.module'); }).then(function (m) { return m.ChatPageModule; }); }
    },
    {
        path: 'start-travel',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/start-travel/start-travel.module'); }).then(function (m) { return m.StartTravelPageModule; }); }
    },
    {
        path: 'select-vehicle-type',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/select-vehicle-type/select-vehicle-type.module'); }).then(function (m) { return m.SelectVehicleTypePageModule; }); }
    },
    {
        path: 'select-vehicle',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/select-vehicle/select-vehicle.module'); }).then(function (m) { return m.SelectVehiclePageModule; }); }
    },
    {
        path: 'history',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/history/history.module'); }).then(function (m) { return m.HistoryPageModule; }); }
    },
    {
        path: 'select-stop',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/select-stop/select-stop.module'); }).then(function (m) { return m.SelectStopPageModule; }); }
    },
    {
        path: 'select-stop-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/select-stop-modal/select-stop-modal.module'); }).then(function (m) { return m.SelectStopModalPageModule; }); }
    },
    {
        path: 'select-destination',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/select-destination/select-destination.module'); }).then(function (m) { return m.SelectDestinationPageModule; }); }
    },
    {
        path: 'select-destination-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/select-destination-modal/select-destination-modal.module'); }).then(function (m) { return m.SelectDestinationModalPageModule; }); }
    },
    {
        path: 'schedule-trip',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/schedule-trip/schedule-trip.module'); }).then(function (m) { return m.ScheduleTripPageModule; }); }
    },
    {
        path: 'schedule-trip-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/schedule-trip-modal/schedule-trip-modal.module'); }).then(function (m) { return m.ScheduleTripModalPageModule; }); }
    },
    {
        path: 'select-stop-schedule',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/select-stop-schedule/select-stop-schedule.module'); }).then(function (m) { return m.SelectStopSchedulePageModule; }); }
    },
    {
        path: 'select-stop-schedule-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/select-stop-schedule-modal/select-stop-schedule-modal.module'); }).then(function (m) { return m.SelectStopScheduleModalPageModule; }); }
    },
    {
        path: 'ubication-destination-schedule',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/ubication-destination-schedule/ubication-destination-schedule.module'); }).then(function (m) { return m.UbicationDestinationSchedulePageModule; }); }
    },
    {
        path: 'ubication-destination-schedule-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/ubication-destination-schedule-modal/ubication-destination-schedule-modal.module'); }).then(function (m) { return m.UbicationDestinationScheduleModalPageModule; }); }
    },
    {
        path: 'destination-address',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/destination-address/destination-address.module'); }).then(function (m) { return m.DestinationAddressPageModule; }); }
    },
    {
        path: 'destination-address-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/destination-address-modal/destination-address-modal.module'); }).then(function (m) { return m.DestinationAddressModalPageModule; }); }
    },
    {
        path: 'edit-vehicle-data',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/edit-vehicle-data/edit-vehicle-data.module'); }).then(function (m) { return m.EditVehicleDataPageModule; }); }
    },
    {
        path: 'select-route',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/select-route/select-route.module'); }).then(function (m) { return m.SelectRoutePageModule; }); }
    },
    {
        path: 'select-route-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/select-route-modal/select-route-modal.module'); }).then(function (m) { return m.SelectRouteModalPageModule; }); }
    },
    {
        path: 'select-payment-type/:travelId',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/select-payment-type/select-payment-type.module'); }).then(function (m) { return m.SelectPaymentTypePageModule; }); }
    },
    {
        path: 'select-payment-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/select-payment-modal/select-payment-modal.module'); }).then(function (m) { return m.SelectPaymentModalPageModule; }); }
    },
    {
        path: 'transfer',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/transfer/transfer.module'); }).then(function (m) { return m.TransferPageModule; }); }
    },
    {
        path: 'transfer-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/transfer-modal/transfer-modal.module'); }).then(function (m) { return m.TransferModalPageModule; }); }
    },
    {
        path: 'search-driver/:travelId',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/search-driver/search-driver.module'); }).then(function (m) { return m.SearchDriverPageModule; }); }
    },
    {
        path: 'confirmed-trip',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/confirmed-trip/confirmed-trip.module'); }).then(function (m) { return m.ConfirmedTripPageModule; }); }
    },
    {
        path: 'travel-details',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/travel-details/travel-details.module'); }).then(function (m) { return m.TravelDetailsPageModule; }); }
    },
    {
        path: 'travel-details-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/travel-details-modal/travel-details-modal.module'); }).then(function (m) { return m.TravelDetailsModalPageModule; }); }
    },
    {
        path: 'travel-complete/:travelId',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/travel-complete/travel-complete.module'); }).then(function (m) { return m.TravelCompletePageModule; }); }
    },
    {
        path: 'in-travel-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/in-travel-modal/in-travel-modal.module'); }).then(function (m) { return m.InTravelModalPageModule; }); }
    },
    {
        path: 'recommendations-modal',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/modals/recommendations-modal/recommendations-modal.module'); }).then(function (m) { return m.RecommendationsModalPageModule; }); }
    },
    {
        path: 'search-complete',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/search-complete/search-complete.module'); }).then(function (m) { return m.SearchCompletePageModule; }); }
    },
    {
        path: 'shipping-parameters',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./pages/shipping-parameters/shipping-parameters.module'); }).then(function (m) { return m.ShippingParametersPageModule; }); }
    },
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        core_1.NgModule({
            declarations: [],
            imports: [
                router_1.RouterModule.forChild(routes)
            ]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());
exports.DashboardRoutingModule = DashboardRoutingModule;
