import { EventEmitter } from '@angular/core';
import {
  Component,
  OnInit,
  Input,
  Renderer2,
  ViewChild,
  ElementRef,
  Output,
} from '@angular/core';

@Component({
  selector: 'app-payment-type',
  templateUrl: './payment-type.component.html',
  styleUrls: ['./payment-type.component.scss'],
})
export class PaymentTypeComponent implements OnInit {
  @Input() text: string;
  @Input() icon: string;


  constructor() {}

  ngOnInit() {}

}
