import { Component, Input, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { BankDataPage } from '../../../pages/bank-data/bank-data.page';

@Component({
  selector: 'app-bank-account-card',
  templateUrl: './bank-account-card.component.html',
  styleUrls: ['./bank-account-card.component.scss'],
})
export class BankAccountCardComponent implements OnInit {
  @Input() accountNumber: number;
  @Input() email: string;
  @Input() nameBank: string;
  @Input() nameOwner: string;
  @Input() typeAccount: string;

  constructor(
    private alertController: AlertController,
    private alertService: AlertsService
  ) {}

  ngOnInit() {}

  async presentAlertConfirm(categoryId: string) {
    const alert = await this.alertController.create({
      header: '¡Confirme!',
      message: '¿Esta seguro de que desea <strong>eliminar</strong> esta cuenta bancaría?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Ok',
          handler: () => {
          },
        },
      ],
    });
    await alert.present();
  }

  presentModal(
    accountNumber: number,
    email: string,
    nameBank: string,
    nameOwner: string,
    typeAccount: string,
 
  ): void {
    this.alertService.presentModal(BankDataPage, {
      accountNumber: accountNumber,
      email: email,
      nameBank: nameBank,
      nameOwner: nameOwner,
      typeAccount: typeAccount,
    });
  }
}
