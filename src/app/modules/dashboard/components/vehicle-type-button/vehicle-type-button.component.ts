import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-vehicle-type-button',
  templateUrl: './vehicle-type-button.component.html',
  styleUrls: ['./vehicle-type-button.component.scss'],
})
export class VehicleTypeButtonComponent implements OnInit {
  @Input() title: string;
  @Output() redirectTo = new EventEmitter();

  constructor() { }

  ngOnInit() {}

}
