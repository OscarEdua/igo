import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-request-button',
  templateUrl: './request-button.component.html',
  styleUrls: ['./request-button.component.scss'],
})
export class RequestButtonComponent implements OnInit {
  @Input() title: string;
  @Input() iconName: string;
  @Input() backgroundColorCard: string;

  constructor() {}

  ngOnInit() {}
}
