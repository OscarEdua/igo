import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-item-map',
  templateUrl: './item-map.component.html',
  styleUrls: ['./item-map.component.scss'],
})
export class ItemMapComponent implements OnInit {
  @Input() classes: string;
  @Input() name: string;
  @Input() description: string;
  @Output() chooseStop = new EventEmitter();
  @Input() city: string;

  constructor() { }

  ngOnInit() {}
}
