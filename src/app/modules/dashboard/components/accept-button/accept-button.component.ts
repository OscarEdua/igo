import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-accept-button',
  templateUrl: './accept-button.component.html',
  styleUrls: ['./accept-button.component.scss'],
})

export class AcceptButtonComponent implements OnInit {
  @Input() text: string;

  constructor() { }

  ngOnInit() {}
}
