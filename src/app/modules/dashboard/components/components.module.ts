import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { RequestButtonComponent } from '../../dashboard/components/request-button/request-button.component';
import { RequestCardComponent } from './request-card/request-card.component';
import { ButtonComponent } from './button/button.component';
import { PrincipalHeaderComponent } from './principal-header/principal-header.component';
import { CloseModalButtonComponent } from './close-modal-button/close-modal-button.component';
import { VehicleTypeButtonComponent } from './vehicle-type-button/vehicle-type-button.component';
import { InitialModalButtonComponent } from './initial-modal-button/initial-modal-button.component';
import { AddressInputComponent } from './address-input/address-input.component';
import { ItemMapComponent } from './item-map/item-map.component';
import { AcceptButtonComponent } from './accept-button/accept-button.component';
import { TravelCardComponent } from './travel-card/travel-card.component';
import { PaymentTypeComponent } from './payment-type/payment-type.component'
import { TravelDetailCardComponent } from './travel-detail-card/travel-detail-card.component'
import { FormsModule } from '@angular/forms';
import { BankAccountCardComponent } from './cards/bank-account-card/bank-account-card.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ],
  declarations: [
    HeaderComponent,
    RequestButtonComponent,
    RequestCardComponent,
    ButtonComponent,
    PrincipalHeaderComponent,
    CloseModalButtonComponent,
    VehicleTypeButtonComponent,
    InitialModalButtonComponent,
    AddressInputComponent,
    ItemMapComponent,
    AcceptButtonComponent,
    TravelCardComponent,
    PaymentTypeComponent,
    TravelDetailCardComponent,
    BankAccountCardComponent    
  ],
  exports: [
    HeaderComponent,
    RequestButtonComponent,
    RequestCardComponent,
    ButtonComponent,
    PrincipalHeaderComponent,
    CloseModalButtonComponent,
    VehicleTypeButtonComponent,
    InitialModalButtonComponent,
    AddressInputComponent,
    ItemMapComponent,
    AcceptButtonComponent,
    TravelCardComponent,
    PaymentTypeComponent,
    TravelDetailCardComponent,
    BankAccountCardComponent
  ]
})
export class ComponentsModule { }
