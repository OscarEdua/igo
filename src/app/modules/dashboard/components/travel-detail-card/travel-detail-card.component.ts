import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-travel-detail-card',
  templateUrl: './travel-detail-card.component.html',
  styleUrls: ['./travel-detail-card.component.scss'],
})
export class TravelDetailCardComponent implements OnInit {
  @Input() vehicleType: string;
  @Input() wholePartPrice: number;
  @Input() decimalPartPrice: number;
  @Input() duration: string;

  constructor() { }

  ngOnInit() {}

}
