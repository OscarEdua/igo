import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-request-card',
  templateUrl: './request-card.component.html',
  styleUrls: ['./request-card.component.scss'],
})

export class RequestCardComponent implements OnInit {
  @Input() title: string;
  @Input() date: Date;
  @Input() dollars: number;
  @Input() cents: string;
  @Input() address: string;
  @Input() schedulingDate: Date;
  @Input() stop: string;
  @Input() imagen: string;
  @Input() status: string;
  @Output() accept = new EventEmitter<string>();
  @Output() toTravel = new EventEmitter<string>(); 

  constructor() { }

  ngOnInit() {}
}
