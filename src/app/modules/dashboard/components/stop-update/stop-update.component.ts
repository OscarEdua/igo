/// <reference path="../../../../../../node_modules/@types/googlemaps/index.d.ts" />
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { Stop } from '../../interfaces/stop.interface';
import { StopsService } from '../../services/admin/stops.service';
interface Marker {
  position: {
    lat: number;
    lng: number;
  };
  title: string;
}
@Component({
  selector: 'app-stop-update',
  templateUrl: './stop-update.component.html',
  styleUrls: ['./stop-update.component.scss'],
})
export class StopUpdateComponent implements OnInit {
  @Input() stopId: string;
  @Input() address: string;
  @Input() name: string;
  @Input() description: string;
  @Input() city: string;
  @Input() provinces: string;
  @Input() lat: number;
  @Input() lng: number;
  @Input() state: string;
  @Input() isActive: boolean;

  @ViewChild('divMap') divMap: ElementRef;
  @ViewChild('inputPlaces') inputPlaces: ElementRef;
  map: google.maps.Map;

  stop: Stop;
  markers = [];
  GoogleAutocomplete: any;
  constructor(
    private stopsService: StopsService,
    private alertsService: AlertsService,
    private modalController: ModalController
  ) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.GoogleAutocomplete = new google.maps.places.Autocomplete(
      document.getElementById('stop_address').getElementsByTagName('input')[0],
      {
        types: ['address'],

        componentRestrictions: { country: 'ec' },
        fields: [
          'address_components',
          'geometry',
          'icon',
          'name',
          'formatted_address',
        ],
      }
    );
    this.stop = {
      uid: this.stopId,
      address: this.address,
      name: this.name,
      description: this.description,
      city: this.city,
      provinces: this.provinces,
      lat: this.lat,
      lng: this.lng,
      state: this.state,
      isActive: this.isActive,
    };
    console.log(this.lat);
  }

  ngAfterViewInit(): void {
    this.loadMap();
  }

  loadMap() {
    let latLng = new google.maps.LatLng(this.lat, this.lng);
    this.map = new google.maps.Map(this.divMap.nativeElement, {
      center: latLng,
      zoom: 16,
      //estilo del mapa (color gris)
      styles: [
        {
          elementType: 'geometry',
          stylers: [
            {
              color: '#f5f5f5',
            },
          ],
        },
        {
          elementType: 'labels.icon',
          stylers: [
            {
              visibility: 'off',
            },
          ],
        },
        {
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#616161',
            },
          ],
        },
        {
          elementType: 'labels.text.stroke',
          stylers: [
            {
              color: '#f5f5f5',
            },
          ],
        },
        {
          featureType: 'administrative.land_parcel',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#bdbdbd',
            },
          ],
        },
        {
          featureType: 'poi',
          elementType: 'geometry',
          stylers: [
            {
              color: '#eeeeee',
            },
          ],
        },
        {
          featureType: 'poi',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#757575',
            },
          ],
        },
        {
          featureType: 'poi.park',
          elementType: 'geometry',
          stylers: [
            {
              color: '#e5e5e5',
            },
          ],
        },
        {
          featureType: 'poi.park',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#9e9e9e',
            },
          ],
        },
        {
          featureType: 'road',
          elementType: 'geometry',
          stylers: [
            {
              color: '#ffffff',
            },
          ],
        },
        {
          featureType: 'road.arterial',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#757575',
            },
          ],
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry',
          stylers: [
            {
              color: '#dadada',
            },
          ],
        },
        {
          featureType: 'road.highway',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#616161',
            },
          ],
        },
        {
          featureType: 'road.local',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#9e9e9e',
            },
          ],
        },
        {
          featureType: 'transit.line',
          elementType: 'geometry',
          stylers: [
            {
              color: '#e5e5e5',
            },
          ],
        },
        {
          featureType: 'transit.station',
          elementType: 'geometry',
          stylers: [
            {
              color: '#eeeeee',
            },
          ],
        },
        {
          featureType: 'water',
          elementType: 'geometry',
          stylers: [
            {
              color: '#c9c9c9',
            },
          ],
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#9e9e9e',
            },
          ],
        },
      ],
    });
    const icon = {
      url: '../../assets/img/stops.png',
      scaledSize: new google.maps.Size(100, 100),
    };
    const marketPosition = new google.maps.Marker({
      position: this.map.getCenter(),
      animation: google.maps.Animation.DROP,
      icon: icon,
    });
    let market = {
      position: {
        lat: this.lat,

        lng: this.lng,
      },
      title: 'Mi Ubicación',
    };
        this.addMarker(market)
    /*     marketPosition.setMap(this.map); */
  }
  //busqueda de datos por me dio de un input
  keyUpHandler() {
    if (this.stop.address.length > 0) {
      google.maps.event.addListener(
        this.GoogleAutocomplete,
        `place_changed`,
        () => {
          const place = this.GoogleAutocomplete.getPlace();
          let market = {
            position: {
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng(),
            },
            title: 'Mi Ubicación',
          };
          var centro = {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng(),
          };
          this.map.setCenter(centro);
          this.addMarker(market);
        }
      );
    }
  }

  //añadir marcador
  addMarker(marker: Marker) {
    this.deleteMarkers();
    let marke;
    const meta = '../../assets/img/stops.png';
    const icon2 = {
      url: meta,
      /*     size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
         anchor: new google.maps.Point(40, 60), */
      scaledSize: new google.maps.Size(100, 100),
    };
    marke = new google.maps.Marker({
      position: marker.position,
      draggable: true,
      animation: google.maps.Animation.DROP,
      map: this.map,
      icon: icon2,
      title: marker.title,
    });
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          //Info Windows de google
          var content = results[0].formatted_address;
          var infoWindow = new google.maps.InfoWindow({
            content: content,
          });
          //   infoWindow.close(this.map, marke);
          this.stop.lat = marke.getPosition().lat();
          this.stop.lng = marke.getPosition().lng();
          this.stop.address = results[0].formatted_address;
          this.stop.city = results[4].formatted_address;
          this.stop.provinces = results[7].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
    google.maps.event.addListener(marke, 'dragend', () => {
      var currentInfoWindow = null;
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            //Info Windows de google
            var content = results[0].formatted_address;
            var infoWindow = new google.maps.InfoWindow({
              content: content,
            });
            //   infoWindow.close(this.map, marke);
            this.stop.lat = marke.getPosition().lat();
            this.stop.lng = marke.getPosition().lng();
            this.stop.address = results[0].formatted_address;
            this.stop.city = results[4].formatted_address;
            this.stop.provinces = results[7].formatted_address;
          } else {
            window.alert('No results found');
          }
        } else {
          console.log('Geocoder failed due to: ' + status);
        }
      });
    });
    this.markers.push(marke);
  }
   update() {

    if (!this.thereAreEmptyFields(this.stop)) {
      this.stopsService.updateStop(this.stop);
      this.modalController.dismiss();
    } else {
      this.alertsService.presentAlert(
        'Por favor complete todos los campos requeridos.'
      );
    }
  }

  thereAreEmptyFields(stop: Stop): boolean {
    if (stop.address == '' || stop.name == '' || stop.description == '')
      return true;
    else return false;
  }

  closeModal(): void {
    this.modalController.dismiss();
  }
  //funciones para eliminar las marcas de mapa de google
  //desde
  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
  }
  clearMarkers() {
    this.setMapOnAll(null);
  }
  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }
}
