import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-principal-header',
  templateUrl: './principal-header.component.html',
  styleUrls: ['./principal-header.component.scss'],
})
export class PrincipalHeaderComponent implements OnInit {
  @Input() title: string;
  @Output() openMenu = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {}

}
