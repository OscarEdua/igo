import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-initial-modal-button',
  templateUrl: './initial-modal-button.component.html',
  styleUrls: ['./initial-modal-button.component.scss'],
})

export class InitialModalButtonComponent implements OnInit {
  @Output() openModal = new EventEmitter();

  constructor() { }

  ngOnInit() {}
}
