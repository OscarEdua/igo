import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Travel } from '../../interfaces/travel.interface';
import { Observable, combineLatest, of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { uniq } from 'lodash';
import app from 'firebase/app';
import { AuthenticationService } from '../../../../core/authentication/authentication.service';
import { Location } from '../../interfaces/location.interface';

@Injectable({
  providedIn: 'root'
})
export class TravelsService {
  travelsCollection: AngularFirestoreCollection<Travel>;
  joined$: Observable<any>;
  status: Observable<any>;
  private travelDoc: AngularFirestoreDocument<Travel>;

  constructor(
    private afs: AngularFirestore, 
    private authenticationService: AuthenticationService,) { 
    this.travelsCollection = afs.collection<Travel>('travels');
  }

  add(travel: Travel): Promise<any> {
    travel.createdAt = app.firestore.Timestamp.now().toDate();
    travel.currentPosition = { lat: 0, lng: 0 };
    return this.travelsCollection.add(travel);
  }

  getTravelsTransfer(): Observable<any> {
    //this.joined$ = this.afs.collection<Travel>("travels" , ref => ref.where('payMethod', '==' , 'transfer').orderBy('createdAt','desc'))
    this.joined$ = this.afs.collection<Travel>("travels" , ref => ref.where('payMethod', '==' , 'transfer'))
    .valueChanges({ idField: 'travelId'})
    .pipe(
      switchMap(travels => {
        const cliendIds = uniq(travels.map(travel => travel.clientId))
        return  combineLatest(
          of(travels),
          combineLatest(
            cliendIds.map(travelId => 
              this.afs.doc('users/' + travelId)
              .valueChanges({ idField: "clientId" })
              .pipe(map(user => user))
            )
          )
        );
      }),
      map(([travels, clients]) => {
        return travels.map(travel => {
          return {
            ...travel,
            client: clients.find((client: any) => client.clientId === travel.clientId)
          };
        });
      })
    )
    return this.joined$;
  }

  getTravelsHistorial(): Observable<any> {
    //this.joined$ = this.afs.collection<Travel>("travels" , ref => ref.where('payMethod', '==' , 'transfer').orderBy('createdAt','desc'))
    this.joined$ = this.afs.collection<Travel>("travels" , ref => ref.where('status', '==' , 'finished'))
    .valueChanges({ idField: 'travelId'})
    .pipe(
      switchMap(travels => {
        const cliendIds = uniq(travels.map(travel => travel.clientId))
        return  combineLatest(
          of(travels),
          combineLatest(
            cliendIds.map(travelId => 
              this.afs.doc('users/' + travelId)
              .valueChanges({ idField: "clientId" })
              .pipe(map(user => user))
            )
          )
        );
      }),
      map(([travels, clients]) => {
        return travels.map(travel => {
          return {
            ...travel,
            client: clients.find((client: any) => client.clientId === travel.clientId)
          };
        });
      })
    )
    return this.joined$;
  }

  getTravels(): Observable<any> {
    this.joined$ = this.afs.collection<Travel>("travels" , ref => ref.orderBy('createdAt','desc'))
    .valueChanges({ idField: 'travelId'})
    .pipe(
      switchMap(travels => {
        const cliendIds = uniq(travels.map(travel => travel.clientId))
        return  combineLatest(
          of(travels),
          combineLatest(
            cliendIds.map(travelId => 
              this.afs.doc('users/' + travelId)
              .valueChanges({ idField: "clientId" })
              .pipe(map(user => user))
            )
          )
        );
      }),
      map(([travels, clients]) => {
        return travels.map(travel => {
          return {
            ...travel,
            client: clients.find((client: any) => client.clientId === travel.clientId)
          };
        });
      })
    )
    return this.joined$;
  }

  acceptTravel(travelId: string): Promise<any> {
    this.travelDoc = this.afs.doc<Travel>('travels/' + travelId);
    return this.travelDoc.update({ status: 'waiting_payment', driverId: this.authenticationService.getCurrentUserId() });
  }

  getTravelId(travelId: string): Observable<Travel> {
    this.travelDoc = this.afs.doc<Travel>('travels/' + travelId);
    return this.travelDoc.valueChanges();
  }

  startTravel(travelId: string): Promise<any> {
    this.travelDoc = this.afs.doc<Travel>('travels/' + travelId);
    return this.travelDoc.update({ status: 'traveling' });
  }

  endTravel(travelId: string): Promise<any> {
    this.travelDoc = this.afs.doc<Travel>('travels/' + travelId);
    return this.travelDoc.update({ status: 'finished' });
  }

  getStatusTravelById(travelId: string): Observable<string> {
    this.status = this.afs.doc<Travel>('travels/' + travelId).valueChanges().pipe(
      map(travel => travel.status)
    );
    return this.status;
  }

  updatePayMethod(travelId: string, payMethod: string): Promise<any> {
    this.travelDoc = this.afs.doc<Travel>('travels/' + travelId);
    const state = (payMethod === 'transfer') ? 'waiting_payment' : 'paid';
    console.log('aoeu');
    return this.travelDoc.update({ payMethod: payMethod, status: state });
  }

  updatePayMethodTransfer(travelId: string, status: string): Promise<any> {
    this.travelDoc = this.afs.doc<Travel>('travels/' + travelId);
    return this.travelDoc.update({ status: status });
  }
  
  updateInvoiceImg(travelId: string, invoiceImg: string): Promise<any> {
    this.travelDoc = this.afs.doc<Travel>('travels/' + travelId);
    return this.travelDoc.update({ invoiceImg, status: 'waiting_payment' });
  }

  updateCurrentLocation(travelId: string, currentPosition: Location): Promise<any> {
    this.travelDoc = this.afs.doc<Travel>('travels/' + travelId);
    return this.travelDoc.update({ currentPosition: currentPosition });
  }

  getTravelAndUserById(travelId: string, roleCurrentUser: string): Observable<any> {
    this.joined$ = this.afs.doc('travels/' + travelId).valueChanges({ idField: 'travelId'})
    .pipe(
      switchMap((travel: Travel) => 
        this.afs.doc('users/' + (roleCurrentUser === 'client' ? travel.driverId : travel.clientId)).valueChanges().pipe(
          map(user => ({
            user,
            ...travel,
            price: { dollars: this.getWholePart(travel.cost), cents: this.getDecimalPart(travel.cost) }
          })),
        )
      ),
    );
    return this.joined$;
  }

  getWholePart(cost: number): number {
    return Math.trunc(cost);
  }
  
  getDecimalPart(cost: number): string {
    return (cost.toFixed(2)).split('.')[1];
  }

  updateTravel(travel : Travel, travelUid: string){
    travel.updateAt = app.firestore.Timestamp.now().toDate();
    return this.afs.collection('travels').doc(travelUid).update(travel);
  }
}
