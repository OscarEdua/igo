import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Stop } from '../../interfaces/stop.interface';
import app from 'firebase/app';
@Injectable({
  providedIn: 'root',
})

export class StopsService {
  stopsCollection: AngularFirestoreCollection<Stop>;
  stops: Observable<Stop[]>;
  stopDoc: AngularFirestoreDocument<Stop>;
  stop: Observable<Stop>;

  constructor(private firestore: AngularFirestore) {
    this.stopsCollection = firestore.collection<Stop>('stops');
    this.stops = this.stopsCollection.valueChanges({ idField: 'stopId' });
  }

  addStop(stop: Stop) {
    stop.updateDate = app.firestore.Timestamp.now().toDate();
    stop.createAt = app.firestore.Timestamp.now().toDate();
    this.firestore.collection('stops').add(stop);
  }

  getStop() {
    return this.firestore.collection('stops').valueChanges({ idField: 'uid' });
  }

  updateStop(stop: Stop) {
    const uid = stop.uid;
    delete stop.uid;
    this.firestore.collection('stops').doc(uid).update(stop);
  }

  deleteStop(uid: string) {
    this.firestore.collection('stops').doc(uid).delete();
  }
}
