"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TravelsService = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var lodash_1 = require("lodash");
var app_1 = require("firebase/app");
var TravelsService = /** @class */ (function () {
    function TravelsService(afs, authenticationService) {
        this.afs = afs;
        this.authenticationService = authenticationService;
        this.travelsCollection = afs.collection('travels');
    }
    TravelsService.prototype.add = function (travel) {
        travel.createdAt = app_1["default"].firestore.Timestamp.now().toDate();
        travel.currentPosition = { lat: 0, lng: 0 };
        return this.travelsCollection.add(travel);
    };
    TravelsService.prototype.getTravels = function () {
        var _this = this;
        this.joined$ = this.afs.collection("travels", function (ref) { return ref.orderBy('createdAt', 'desc'); })
            .valueChanges({ idField: 'travelId' })
            .pipe(operators_1.switchMap(function (travels) {
            var cliendIds = lodash_1.uniq(travels.map(function (travel) { return travel.clientId; }));
            return rxjs_1.combineLatest(rxjs_1.of(travels), rxjs_1.combineLatest(cliendIds.map(function (travelId) {
                return _this.afs.doc('users/' + travelId)
                    .valueChanges({ idField: "clientId" })
                    .pipe(operators_1.map(function (user) { return user; }));
            })));
        }), operators_1.map(function (_a) {
            var travels = _a[0], clients = _a[1];
            return travels.map(function (travel) {
                return __assign(__assign({}, travel), { client: clients.find(function (client) { return client.clientId === travel.clientId; }) });
            });
        }));
        return this.joined$;
    };
    TravelsService.prototype.acceptTravel = function (travelId) {
        this.travelDoc = this.afs.doc('travels/' + travelId);
        return this.travelDoc.update({ status: 'waiting_payment', driverId: this.authenticationService.getCurrentUserId() });
    };
    TravelsService.prototype.getTravelId = function (travelId) {
        this.travelDoc = this.afs.doc('travels/' + travelId);
        return this.travelDoc.valueChanges();
    };
    TravelsService.prototype.startTravel = function (travelId) {
        this.travelDoc = this.afs.doc('travels/' + travelId);
        return this.travelDoc.update({ status: 'traveling' });
    };
    TravelsService.prototype.endTravel = function (travelId) {
        this.travelDoc = this.afs.doc('travels/' + travelId);
        return this.travelDoc.update({ status: 'finished' });
    };
    TravelsService.prototype.getStatusTravelById = function (travelId) {
        this.status = this.afs.doc('travels/' + travelId).valueChanges().pipe(operators_1.map(function (travel) { return travel.status; }));
        return this.status;
    };
    TravelsService.prototype.updatePayMethod = function (travelId, payMethod) {
        this.travelDoc = this.afs.doc('travels/' + travelId);
        return this.travelDoc.update({ payMethod: payMethod, status: 'waiting_driver' });
    };
    TravelsService.prototype.updateCurrentLocation = function (travelId, currentPosition) {
        this.travelDoc = this.afs.doc('travels/' + travelId);
        return this.travelDoc.update({ currentPosition: currentPosition });
    };
    TravelsService.prototype.getTravelAndUserById = function (travelId, roleCurrentUser) {
        var _this = this;
        this.joined$ = this.afs.doc('travels/' + travelId).valueChanges({ idField: 'travelId' })
            .pipe(operators_1.switchMap(function (travel) {
            return _this.afs.doc('users/' + (roleCurrentUser === 'client' ? travel.driverId : travel.clientId)).valueChanges().pipe(operators_1.map(function (user) { return (__assign(__assign({ user: user }, travel), { price: { dollars: _this.getWholePart(travel.cost), cents: _this.getDecimalPart(travel.cost) } })); }));
        }));
        return this.joined$;
    };
    TravelsService.prototype.getWholePart = function (cost) {
        return Math.trunc(cost);
    };
    TravelsService.prototype.getDecimalPart = function (cost) {
        return (cost.toFixed(2)).split('.')[1];
    };
    TravelsService.prototype.updateTravel = function (travel, travelUid) {
        travel.updateAt = app_1["default"].firestore.Timestamp.now().toDate();
        return this.afs.collection('travels').doc(travelUid).update(travel);
    };
    TravelsService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], TravelsService);
    return TravelsService;
}());
exports.TravelsService = TravelsService;
