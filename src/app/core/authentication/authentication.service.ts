import { Injectable, NgZone} from '@angular/core';
import app from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';

import firebase from 'firebase/app';
import { User } from 'src/app/modules/home/interfaces/user.interface';
import { AlertsService } from 'src/app/shared/services/alerts.service';
import { LoadingController } from '@ionic/angular';
import { UsersService } from 'src/app/modules/home/services/users.service';

export interface FirebaseUser {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  userState: any;

  constructor(private afs: AngularFirestore, private angularFireAuth: AngularFireAuth, private router: Router, private ngZone: NgZone, private alertsService: AlertsService, private loadingController: LoadingController, private usersService: UsersService) {
    this.angularFireAuth.authState.subscribe(user => {
      if (user) {
        this.userState = user;
        localStorage.setItem('user', JSON.stringify(this.userState));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    });
  }

  async signIn(email, password) {
    return await app.auth().signInWithEmailAndPassword(email, password);
  }

  // signUp(user: User) {
  //   return this.angularFireAuth.createUserWithEmailAndPassword(user.email, user.password)
  //     .then(result => {
  //       this.sendVerificationEmail();
  //       this.setUserDataClient(user);
  //     }).catch(error => {
  //       console.log(error.message);
  //     })
  // }

  async signUp(user: User) {
    const newUser: app.auth.UserCredential = await app.auth().createUserWithEmailAndPassword(user.email, user.password);
    const uid = newUser.user.uid;
    user.isActive = true;
    user.createAt = app.firestore.Timestamp.now().toDate();
    user.updateAt = app.firestore.Timestamp.now().toDate();
    await this.afs.doc(`users/${uid}`).set(user);
    await newUser.user.sendEmailVerification();
    await this.logoutUser();
  }

  logoutUser(): Promise<void> {
    return firebase.auth().signOut();
  }

  sendVerificationEmail() {
    return this.angularFireAuth.currentUser.then(user => user.sendEmailVerification())
      .then(() => {
        // this.router.navigate(['email-verification']);
      })
  }

  forgotPassword(passwordResetEmail) {
    return this.angularFireAuth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        console.log('Password reset email sent, check your inbox.');
      }).catch(error => {
        console.log(error);
      })
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false) ? true : false;
  }

  googleAuth() {
    return this.authLogin(new firebase.auth.GoogleAuthProvider());
  }

  facebookAuth() {
    return this.authLogin(new firebase.auth.FacebookAuthProvider());
  }

  authLogin(provider) {
    return this.angularFireAuth.signInWithPopup(provider)
      .then(result => {
        this.ngZone.run(() => {
          this.router.navigate(['dashboard']);
        })
        this.setUserData(result.user);
      }).catch(error => {
        console.log(error);
      })
  }

  async signOut() {
    await this.alertsService.presentLoading('Saliendo...');
    return this.angularFireAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.alertsService.loading.dismiss();
      this.router.navigate(['../home/login']);
    })
  }

  setUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userState: User = {
      name: user.displayName,
      email: user.email,
      imagen: user.photoURL,
      role: 'client',
      gender: '',
      isActive: true,
      createAt: app.firestore.Timestamp.now().toDate(),
      updateAt: app.firestore.Timestamp.now().toDate(),
      telephoneNumber: user.phoneNumber
    }
    return userRef.set(userState, {
      merge: true
    })
  }

  setUserDataClient(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userState: User = user;
    return userRef.set(userState, {
      merge: true
    })
  }

  getCurrentUserId() {
    return firebase.auth().currentUser.uid;
  }
}
