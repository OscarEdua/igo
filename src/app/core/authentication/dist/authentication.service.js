"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.AuthenticationService = void 0;
var core_1 = require("@angular/core");
var app_1 = require("firebase/app");
var app_2 = require("firebase/app");
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(afs, angularFireAuth, router, ngZone, alertsService, loadingController, usersService) {
        var _this = this;
        this.afs = afs;
        this.angularFireAuth = angularFireAuth;
        this.router = router;
        this.ngZone = ngZone;
        this.alertsService = alertsService;
        this.loadingController = loadingController;
        this.usersService = usersService;
        this.angularFireAuth.authState.subscribe(function (user) {
            if (user) {
                _this.userState = user;
                localStorage.setItem('user', JSON.stringify(_this.userState));
                JSON.parse(localStorage.getItem('user'));
            }
            else {
                localStorage.setItem('user', null);
                JSON.parse(localStorage.getItem('user'));
            }
        });
    }
    AuthenticationService.prototype.signIn = function (email, password) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, app_1["default"].auth().signInWithEmailAndPassword(email, password)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    // signUp(user: User) {
    //   return this.angularFireAuth.createUserWithEmailAndPassword(user.email, user.password)
    //     .then(result => {
    //       this.sendVerificationEmail();
    //       this.setUserDataClient(user);
    //     }).catch(error => {
    //       console.log(error.message);
    //     })
    // }
    AuthenticationService.prototype.signUp = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var newUser, uid;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, app_1["default"].auth().createUserWithEmailAndPassword(user.email, user.password)];
                    case 1:
                        newUser = _a.sent();
                        uid = newUser.user.uid;
                        user.isActive = true;
                        user.createAt = app_1["default"].firestore.Timestamp.now().toDate();
                        user.updateAt = app_1["default"].firestore.Timestamp.now().toDate();
                        return [4 /*yield*/, this.afs.doc("users/" + uid).set(user)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, newUser.user.sendEmailVerification()];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.logoutUser()];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthenticationService.prototype.logoutUser = function () {
        return app_2["default"].auth().signOut();
    };
    AuthenticationService.prototype.sendVerificationEmail = function () {
        return this.angularFireAuth.currentUser.then(function (user) { return user.sendEmailVerification(); })
            .then(function () {
            // this.router.navigate(['email-verification']);
        });
    };
    AuthenticationService.prototype.forgotPassword = function (passwordResetEmail) {
        return this.angularFireAuth.sendPasswordResetEmail(passwordResetEmail)
            .then(function () {
            console.log('Password reset email sent, check your inbox.');
        })["catch"](function (error) {
            console.log(error);
        });
    };
    Object.defineProperty(AuthenticationService.prototype, "isLoggedIn", {
        get: function () {
            var user = JSON.parse(localStorage.getItem('user'));
            return (user !== null && user.emailVerified !== false) ? true : false;
        },
        enumerable: false,
        configurable: true
    });
    AuthenticationService.prototype.googleAuth = function () {
        return this.authLogin(new app_2["default"].auth.GoogleAuthProvider());
    };
    AuthenticationService.prototype.facebookAuth = function () {
        return this.authLogin(new app_2["default"].auth.FacebookAuthProvider());
    };
    AuthenticationService.prototype.authLogin = function (provider) {
        var _this = this;
        return this.angularFireAuth.signInWithPopup(provider)
            .then(function (result) {
            _this.ngZone.run(function () {
                _this.router.navigate(['dashboard']);
            });
            _this.setUserData(result.user);
        })["catch"](function (error) {
            console.log(error);
        });
    };
    AuthenticationService.prototype.signOut = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertsService.presentLoading('Saliendo...')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, this.angularFireAuth.signOut().then(function () {
                                localStorage.removeItem('user');
                                _this.alertsService.loading.dismiss();
                                _this.router.navigate(['../home/login']);
                            })];
                }
            });
        });
    };
    AuthenticationService.prototype.setUserData = function (user) {
        var userRef = this.afs.doc("users/" + user.uid);
        var userState = {
            name: user.displayName,
            email: user.email,
            imagen: user.photoURL,
            role: 'client',
            gender: '',
            isActive: true,
            createAt: app_1["default"].firestore.Timestamp.now().toDate(),
            updateAt: app_1["default"].firestore.Timestamp.now().toDate(),
            telephoneNumber: user.phoneNumber
        };
        return userRef.set(userState, {
            merge: true
        });
    };
    AuthenticationService.prototype.setUserDataClient = function (user) {
        var userRef = this.afs.doc("users/" + user.uid);
        var userState = user;
        return userRef.set(userState, {
            merge: true
        });
    };
    AuthenticationService.prototype.getCurrentUserId = function () {
        return app_2["default"].auth().currentUser.uid;
    };
    AuthenticationService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;
