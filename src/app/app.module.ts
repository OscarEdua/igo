import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import firebase from 'firebase';
import { StopUpdateComponent } from './modules/dashboard/components/stop-update/stop-update.component';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { StorageService } from 'src/app/shared/services/storage.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ServiceWorkerModule } from '@angular/service-worker';
import { ComponentsModule } from './modules/dashboard/components/components.module';
// Libreria para enviar datos por  el metodo POST
import { HttpClientModule } from '@angular/common/http';
//one signal para  recibir notificaciones
import { OneSignal } from '@ionic-native/onesignal/ngx';
firebase.initializeApp(environment.firebaseConfig);
@NgModule({
  declarations: [AppComponent, StopUpdateComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    ComponentsModule,
    HttpClientModule,
    IonicModule.forRoot(),
    AppRoutingModule, 
    AngularFireModule.initializeApp(environment.firebaseConfig), 
    AngularFireAuthModule, 
    AngularFirestoreModule, 
    FormsModule, 
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyDgC1m_Lj7v02-Q0ZPf9W_sy3EisEysXl0' }),
    ServiceWorkerModule.register('ngsw-worker.js', {
    enabled: environment.production,
  // Register the ServiceWorker as soon as the app is stable
  // or after 30 seconds (whichever comes first).
    registrationStrategy: 'registerWhenStable:30000'
})],
  providers: [CallNumber,{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, StorageService,OneSignal],
  bootstrap: [AppComponent],
})
export class AppModule {}
