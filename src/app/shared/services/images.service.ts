import { Injectable } from '@angular/core';
import app from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {

  constructor() { }

  async uploadImage(storageFolder: string, image: File): Promise<any> {
    try {
      const uploadTask = await app.storage().ref().child(`${ storageFolder }/${ image.name }`).put(image);
      return await uploadTask.ref.getDownloadURL();
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async deleteImage(storageFolder: string, image: File): Promise<any> {
    return await app.storage().ref().child(`${storageFolder} / ${image.name}`).delete();
  }

}
