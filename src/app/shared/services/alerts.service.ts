import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {
  loading: HTMLIonLoadingElement;

  constructor(
    private loadingController: LoadingController, 
    private alertController: AlertController, 
    private modalController: ModalController) { }

  async presentLoading(message: string) {
    this.loading = await this.loadingController.create({
      message
    });
    await this.loading.present();
  }

  async dismissLoading() {
    this.loading.dismiss();
  }

  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      message,
      buttons: [{
        text: 'Aceptar',
        role: 'cancel'
      }]
    });
    await alert.present();
  }

  async presentAlertConfirm(header: string, message: string) {
    return new Promise(async (resolve) => {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: header,
        message: message,
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              return resolve(false);
            },
          },
          {
            text: 'Aceptar',
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });
      await alert.present();
    });
  }

  async presentAlertConfirmAccept(header: string, message: string) {
    return new Promise(async (resolve) => {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: header,
        message: message,
        buttons: [
          {
            text: 'Aceptar',
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });
      await alert.present();
    });
  }

  async presentAlertOk(header: string, message: string) {
    return new Promise(async (resolve) => {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: header,
        message: message,
        buttons: [
          {
            text: 'Aceptar',
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });
      await alert.present();
    });
  }

  async presentModal(modalPageName: any, data: any) {
    const modal = await this.modalController.create({
      component: modalPageName,
      componentProps: data,
    });
    await modal.present();
  }

  async presentModalWithData(modalPageName: any, data: any, modalClass: string) {
    const modal = await this.modalController.create({
      component: modalPageName,
      componentProps: data,
      cssClass: modalClass
    });
    await modal.present();
  }

  async presentModalWithoutTravelData(modalPageName: any, modalClass: string) {
    const modal = await this.modalController.create({
      component: modalPageName,
      cssClass: modalClass,
      showBackdrop: true
    });
    await modal.present();
  }

  closeModal(): void {
    this.modalController.dismiss();
  }

  async presentAlertWithHeader(header: string, message: string) {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: [
        {
          text: 'Aceptar',
          role: 'cancel',
        },
      ],
    });
    await alert.present();
  }
}
