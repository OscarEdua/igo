import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Vehicle } from 'src/app/modules/home/interfaces/vehicle.interface';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
  vehicleCollection: AngularFirestoreCollection<Vehicle>;
  private vehicleDoc : AngularFirestoreDocument<Vehicle>

  constructor(private afs: AngularFirestore ) 
  { }
  add(vehicle: Vehicle): Promise<void>{
    return new Promise<void>((resolve, reject)=>{
      this.vehicleCollection.add(vehicle);
      resolve();
    });
  }
  getVehicleByUserId(userId: string){
    this.vehicleDoc = this.afs.doc<Vehicle>('vehicle/'+ userId)
  }
}
