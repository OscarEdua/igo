/// <reference path="../../../../node_modules/@types/googlemaps/index.d.ts" />
import { Injectable } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { Plugins } from '@capacitor/core';
import { Marker } from 'src/app/modules/dashboard/interfaces/marker';
const { Geolocation } = Plugins;

@Injectable({
  providedIn: 'root',
})

export class MapsService {
  public map: google.maps.Map;
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  public directionsService = new google.maps.DirectionsService();
  directionsDisplay: any;
  geocoder = new google.maps.Geocoder();
  markers: google.maps.Marker[];
  private geoCoder;

  constructor(private mapsAPILoader: MapsAPILoader) {
    this.geoCoder = new google.maps.Geocoder();
    this.markers = [];
  }

  async loadmap(map: string) {
    return new Promise((resolve) => {
      //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
      //  alert("Fuera");
      this.directionsDisplay = new google.maps.DirectionsRenderer({
        polylineOptions: { strokeColor: '#214E9D' },
      });
      Geolocation.getCurrentPosition()
        .then((resp) => {
          //  alert("dentro");
          const latLng = new google.maps.LatLng(
            resp.coords.latitude,
            resp.coords.longitude
          );
          const mapEle: HTMLElement = document.getElementById(map);
          this.map = new google.maps.Map(mapEle, {
            center: latLng,
            zoom: 17, //estilo del mapa (color gris)
            disableDefaultUI: true,
            styles: [
              {
                elementType: 'geometry',
                stylers: [
                  {
                    color: '#f5f5f5',
                  },
                ],
              },
              {
                elementType: 'labels.icon',
                stylers: [
                  {
                    visibility: 'off',
                  },
                ],
              },
              {
                elementType: 'labels.text.fill',
                stylers: [
                  {
                    color: '#616161',
                  },
                ],
              },
              {
                elementType: 'labels.text.stroke',
                stylers: [
                  {
                    color: '#f5f5f5',
                  },
                ],
              },
              {
                featureType: 'administrative.land_parcel',
                elementType: 'labels.text.fill',
                stylers: [
                  {
                    color: '#bdbdbd',
                  },
                ],
              },
              {
                featureType: 'poi',
                elementType: 'geometry',
                stylers: [
                  {
                    color: '#eeeeee',
                  },
                ],
              },
              {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [
                  {
                    color: '#757575',
                  },
                ],
              },
              {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [
                  {
                    color: '#e5e5e5',
                  },
                ],
              },
              {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [
                  {
                    color: '#9e9e9e',
                  },
                ],
              },
              {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [
                  {
                    color: '#ffffff',
                  },
                ],
              },
              {
                featureType: 'road.arterial',
                elementType: 'labels.text.fill',
                stylers: [
                  {
                    color: '#757575',
                  },
                ],
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [
                  {
                    color: '#dadada',
                  },
                ],
              },
              {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [
                  {
                    color: '#616161',
                  },
                ],
              },
              {
                featureType: 'road.local',
                elementType: 'labels.text.fill',
                stylers: [
                  {
                    color: '#9e9e9e',
                  },
                ],
              },
              {
                featureType: 'transit.line',
                elementType: 'geometry',
                stylers: [
                  {
                    color: '#e5e5e5',
                  },
                ],
              },
              {
                featureType: 'transit.station',
                elementType: 'geometry',
                stylers: [
                  {
                    color: '#eeeeee',
                  },
                ],
              },
              {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [
                  {
                    color: '#c9c9c9',
                  },
                ],
              },
              {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [
                  {
                    color: '#9e9e9e',
                  },
                ],
              },
            ],
          });
          /*        this.map.setMyLocationEnabled(false); */
          this.directionsDisplay.setMap(this.map);
          mapEle.classList.add('show-map');
          this.map.addListener('center_changed', () => {});
          resolve(this.map);
        })
        .catch((error) => {});
    });
  }

  getCurrentAddress(): string {
    const wait = Geolocation.watchPosition({}, (position, err) => {
      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
      this.zoom = 8;
      this.getAddress(this.latitude, this.longitude);
    });
    return this.address;
  }

  getCurrentAddressTest(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const wait = Geolocation.watchPosition({}, (position, err) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
        console.log(this.address);
        if (this.address != undefined) {
          resolve(this.address);
        } else {
          reject('No address');
        }
      });
    });
  }

  getAddress(latitude: any, longitude: any): string {
    this.geoCoder.geocode(
      { location: { lat: latitude, lng: longitude } },
      (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            this.zoom = 12;
            this.address = results[0].formatted_address;
            return this.address;
          } else {
            return 'No results found...';
          }
        } else {
          return 'Geocode failed due to: ' + status;
        }
      }
    );
    return '';
  }

  addMarker(marker: any, name: String, typeMarket: string) {
    let marke;
    let icon;
    if (typeMarket == 'myLocation') {
      icon = {
        url: '../../assets/img/ic_loc.png',
        //size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(60, 60),
      };
      /*     const image = '../../assets/img/Pineapplemenu.png'; */
    } else if (typeMarket == 'stops') {
      icon = {
        url: '../../assets/img/ic_dropoff.png',
        //size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        //    anchor: new google.maps.Point(34, 60),
        scaledSize: new google.maps.Size(50, 50),
      };
    }
    const geocoder = new google.maps.Geocoder();
    marke = new google.maps.Marker({
      position: marker.position,
      //  draggable: true,
      // animation: google.maps.Animation.DROP,
      map: this.map,
      icon,
      //animation: google.maps.Animation.DROP,
      title: marker.title,
    });
    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          marke.getPosition().lat();
          marke.getPosition().lng();
          results[0].formatted_address;
        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
      this.map.setCenter(marker.position);
    });
    const infoWindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marke, 'click', () => {
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            infoWindow.setContent(
              '<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address
            );
            if (infoWindow !== null) {
            }
            infoWindow.open(this.map, marke);
          } else {
          }
        } else {
        }
      });
    });
    this.markers.push(marke);
  }

  deleteMarkers(): void {
    for (const marker of this.markers) {
      marker.setMap(null);
    }
  }
  navegate(marck: Marker) {
    return (window.location.href =
      'https://www.google.com/maps/search/?api=1&query=' + marck.position.lat + ',' + marck.position.lng);
  }
}
