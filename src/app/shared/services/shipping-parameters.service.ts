import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { ShippingParameters } from '../../modules/dashboard/interfaces/shipping-parameters.interface';

@Injectable({
  providedIn: 'root',
})
export class ShippingParametersService {
  parametersCollection: AngularFirestoreCollection<any>;
  shippingParameters: Observable<ShippingParameters>;

  constructor(private afs: AngularFirestore) {
    this.parametersCollection = afs.collection<any>('parameters');
  }

  getShippingParameters(): Promise<Observable<ShippingParameters>> {
    return new Promise<Observable<ShippingParameters>>((resolve, reject) => {
      this.shippingParameters = this.parametersCollection.doc('shippingParameters').valueChanges();
      resolve(this.shippingParameters);
    });
  }

  getparametes() {
    return this.afs.collection('parameters').doc('shippingParameters').valueChanges();
  }
  
  getCollection<tipe>(){
    const collection = this.afs.collection<tipe>('parameters');
    return collection.valueChanges();
  }

  update(shippingParameters: ShippingParameters): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.parametersCollection.doc('shippingParameters').update(shippingParameters);
      resolve();
    });
  }
}
