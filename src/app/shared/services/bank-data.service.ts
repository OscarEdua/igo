import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { BankParameters } from '../../modules/dashboard/interfaces/bank-parameters.interface';

@Injectable({
  providedIn: 'root',
})
export class BankParametersService {
  parametersCollection: AngularFirestoreCollection<any>;
  bankParameters: Observable<BankParameters>;

  constructor(private afs: AngularFirestore) {
    this.parametersCollection = afs.collection<any>('bank');
  }

  getShippingParameters(): Promise<Observable<BankParameters>> {
    return new Promise<Observable<BankParameters>>((resolve, reject) => {
      this.bankParameters = this.parametersCollection.doc('bankParameters').valueChanges();
      resolve(this.bankParameters);
    });
  }

  getparametes() {
    return this.afs.collection('bank').doc('bankParameters').valueChanges();
  }
  
  getCollection<tipe>(){
    const collection = this.afs.collection<tipe>('parameters');
    return collection.valueChanges();
  }

  update(bankParameters: BankParameters): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.parametersCollection.doc('bankParameters').update(bankParameters);
      resolve();
    });
  }
}
