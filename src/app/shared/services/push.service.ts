import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root',
})
export class PushService {
  useId: string;
  ext: string;
  constructor(private oneSignal: OneSignal, public http: HttpClient, private router: Router) { }

  configuracionInicial(uid: string) {
    /*     this.oneSignal. */

    this.oneSignal.setExternalUserId(uid);
    this.oneSignal.startInit('4f3b79de-e102-47ff-a1ac-f7753796d478', '796178968721');
    this.oneSignal.enableSound(true);
    this.oneSignal.enableVibrate(true);
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.handleNotificationReceived().subscribe((noti) => {
      // Cuando recibe
      this.ext = noti.payload.additionalData['ruta'] + '';
    });
    this.oneSignal.handleNotificationOpened().subscribe((noti) => {
      // Cuando abre una notifiacion
      // this.router.navigate(['/notice/'+this.ext]);
      this.router.navigate([this.ext]);
    });
    //obtener uid de onsignal
    this.oneSignal.getIds().then((info) => {
      this.useId = info.userId;
    });

    this.oneSignal.endInit();
  }
  //Enviar  notificaciones individuales
  public sendByUid(title: String, subtitle: any, detalle: string, ruta: String, uiduser: string) {
    //dato a mandar
    const datos = {
      app_id: '4f3b79de-e102-47ff-a1ac-f7753796d478',
      included_segments: ['Active User', 'Inactive User'],
      headings: { en: title },
      subtitle: { en: subtitle },
      data: { ruta: ruta, fecha: '100-210-250' },
      contents: { en: detalle },
      channel_for_external_user_ids: 'push',
      include_external_user_ids: [uiduser],
    };
    this.post(datos);
  }

  deleteuser() {
    this.oneSignal.removeExternalUserId();
  }
  //Enviar  notificaciones Global
  public sendGlobal(title: String, subtitle: any, detalle: string, ruta: String) {
    //dato a mandar
    const datos = {
      app_id: '4f3b79de-e102-47ff-a1ac-f7753796d478',
      included_segments: ['Active Users', 'Inactive Users'],
      headings: { en: title },
      subtitle: { en: subtitle },
      data: { ruta: ruta, fecha: '100-210-250' },
      contents: { en: detalle },
    };
    this.post(datos);
  }

  post(datos: any) {
    const headers = {
      Authorization: 'Basic ODM0ODMzNzQtZjIzNi00NWU1LTk3ZDMtZmI1NmI3ZDhjZTFk',
    };
    var url = 'https://onesignal.com/api/v1/notifications';
    return new Promise((resolve) => {
      this.http.post(url, datos, { headers }).subscribe((data) => {
        resolve(data);
      });
    });
  }
  //Enviar  notificaciones globales
}
