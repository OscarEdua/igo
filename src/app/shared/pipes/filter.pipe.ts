import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(array: any[], text: string = '', field: string = 'name'): any[] {
    if (text === '') {
      return array;
    }
    if (!array) {
      return array;
    }
    text = text.toLocaleLowerCase();
    return array.filter(
      item => item[field].toLowerCase().includes(text)
    );
  }

}
