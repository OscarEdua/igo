// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBmAsDEwRcMFRcIGdJPYBP2mPsGjnHAh7I",
    authDomain: "igo-app-c3ed0.firebaseapp.com",
    projectId: "igo-app-c3ed0",
    storageBucket: "igo-app-c3ed0.appspot.com",
    messagingSenderId: "796178968721",
    appId: "1:796178968721:web:98db16ef1da21696394f30",
    measurementId: "G-S8FCVN9237"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
